import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { dimensions } from '../theme';
import {
  Splash,
  LandingPage,
  OnBoardingScreen,
  Login,
  Register,
  ForgotPassword,
  Invitation,
  TermsAndCondition,
  PersonalDetails,
  Profile,
  InviteFriends,
  UpdatePassword,
  Dashboard,
  Filter,
  Sidebar,
  SelectLeague,
  SelectMatch,
  PrizePayout,
  JoinContest,
  ContestDetails,
  ContestLeaderboard,
  CreateTeam,
  SubmitTeamLineUp,
  MyContest,
  MyContestDetails,
  PlayerCard,
  MyAccount,
  MyTeams,
  ListContestMatches,
  EditContestName,
} from '../screens';

// Drawer stack
const DrawerStack = DrawerNavigator({
  Dashboard: {
    screen: Dashboard,
    key: 'DashboardScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: true,
    },
  },
}, {
  initialRouteName: 'Dashboard',
  contentComponent: Sidebar,
  drawerWidth: dimensions.getViewportWidth() - 80,
});

// Drawer navigation stack
const RootNavigation = StackNavigator({
  DrawerStack: {
    screen: DrawerStack,
  },
  Splash: {
    screen: Splash,
    key: 'SplashScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  OnBoarding: {
    screen: OnBoardingScreen,
    key: 'OnBoardingScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  LandingPage: {
    screen: LandingPage,
    key: 'LandingPageScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  Login: {
    screen: Login,
    key: 'LoginScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: true,
    },
  },
  Register: {
    screen: Register,
    key: 'RegisterScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  ForgotPassword: {
    screen: ForgotPassword,
    key: 'ForgotPasswordScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  Invitation: {
    screen: Invitation,
    key: 'InvitationScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  TermsAndCondition: {
    screen: TermsAndCondition,
    key: 'TermsAndConditionScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  InviteFriends: {
    screen: InviteFriends,
    key: 'InviteFriendsScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  Filter: {
    screen: Filter,
    key: 'FilterScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  PersonalDetails: {
    screen: PersonalDetails,
    key: 'PersonalDetailsScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  Profile: {
    screen: Profile,
    key: 'ProfileScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  UpdatePassword: {
    screen: UpdatePassword,
    key: 'UpdatePasswordScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  SelectLeague: {
    screen: SelectLeague,
    key: 'SelectLeagueScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  SelectMatch: {
    screen: SelectMatch,
    key: 'SelectMatchScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  PrizePayout: {
    screen: PrizePayout,
    key: 'PrizePayoutScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  JoinContest: {
    screen: JoinContest,
    key: 'JoinContestScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: true,
    },
  },
  ContestDetails: {
    screen: ContestDetails,
    key: 'ContestDetailsScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: true,
    },
  },
  ListContestMatches: {
    screen: ListContestMatches,
    key: 'ListContestMatchesScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: true,
    },
  },
  ContestLeaderboard: {
    screen: ContestLeaderboard,
    key: 'ContestLeaderboardScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  CreateTeam: {
    screen: CreateTeam,
    key: 'CreateTeamScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  MyContest: {
    screen: MyContest,
    key: 'MyContestScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  MyContestDetails: {
    screen: MyContestDetails,
    key: 'MyContestDetailsScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  PlayerCard: {
    screen: PlayerCard,
    key: 'PlayerCardScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  MyAccount: {
    screen: MyAccount,
    key: 'MyAccountScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  MyTeams: {
    screen: MyTeams,
    key: 'MyTeamsScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  SubmitTeamLineUp: {
    screen: SubmitTeamLineUp,
    key: 'SubmitTeamLineUpScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
  EditContestName: {
    screen: EditContestName,
    key: 'EditContestNameScreen',
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
  },
}, {
  initialRouteName: 'Splash',
  headerMode: 'none',
});

export default RootNavigation;
