/* eslint prefer-destructuring: ["error", {VariableDeclarator: {object: false}}] */

// import { NativeModules } from 'react-native';
import Environments from './environments.json';

const defaultEnvironment = 'staging';

// const RNConfig = NativeModules.RNConfig;
// const environment = RNConfig.buildEnvironment ? RNConfig.buildEnvironment : defaultEnvironment;

export const getBaseUrl = function () {
  return defaultEnvironment === 'production' ? Environments['production'].BASE_URL : Environments['staging'].BASE_URL;
};
