import { combineReducers } from 'redux';
import sessionExpire from './sessionExpire';
import userProfile from './userProfile';
import updateUserProfile from './updateUserProfile';
import updatePassword from './updatePassword';
import userLogout from './logout';
import matchManagement from './matchManagement';
import contestManagement from './contestManagement';
import teamManagement from './teamManagement';
import playerProfile from './playerProfile';
import account from './accountManagement';
import user from './userManagement';

const reducers = {
  /* Auth/User management reducer(s) */
  sessionExpire,
  userProfile,
  updateUserProfile,
  updatePassword,
  userLogout,
  playerProfile,
  user,

  /* Account/Payment management */
  account,

  // TODO: It might me removed in future
  matchManagement,

  /* Contest management reducer(s) */
  contestManagement,

  /* Team management reducer(s) */
  teamManagement,
};

export default combineReducers(reducers);
