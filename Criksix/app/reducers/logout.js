import {
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILURE,
  RESET_LOGOUT,
} from '../actions/authentication';

const initialState = {
  isLoading: false,
  logoutUserResponse: {},
};

function userLogout(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case USER_LOGOUT_REQUEST:
      return {
        ...state,
        isLoading: true,
        logoutUserResponse: {},
      };

    case USER_LOGOUT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        logoutUserResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case USER_LOGOUT_FAILURE:
      return {
        ...state,
        isLoading: false,
        logoutUserResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case RESET_LOGOUT:
      return {
        isLoading: false,
        logoutUserResponse: {},
      };

    default:
      return state;
  }
}

export default userLogout;
