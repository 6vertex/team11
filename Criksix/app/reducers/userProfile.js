import {
  GET_USER_PROFILE_REQUEST,
  GET_USER_PROFILE_SUCCESS,
  GET_USER_PROFILE_FAILURE,
  RESET_LOGOUT,
} from '../actions/authentication';

const initialState = {
  isLoading: false,
  userProfileResponse: {},
};

function userProfile(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case GET_USER_PROFILE_REQUEST:
      return {
        ...state,
        isLoading: true,
        userProfileResponse: {},
      };

    case GET_USER_PROFILE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        userProfileResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case GET_USER_PROFILE_FAILURE:
      return {
        ...state,
        isLoading: false,
        userProfileResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case RESET_LOGOUT:
      return {
        isLoading: false,
        userProfileResponse: {},
      };

    default:
      return state;
  }
}

export default userProfile;
