import {
  RESET_LOGOUT,
} from '../actions/authentication';

import {
  // team Line-Up Rules
  GET_TEAM_LINEUP_RULES_REQUEST,
  GET_TEAM_LINEUP_RULES_SUCCESS,
  GET_TEAM_LINEUP_RULES_FAILURE,

  // players list for contest
  GET_CONTEST_PLAYERS_LINEUP_REQUEST,
  GET_CONTEST_PLAYERS_LINEUP_SUCCESS,
  GET_CONTEST_PLAYERS_LINEUP_FAILURE,
  RESET_GET_CONTEST_PLAYERS_LINEUP,

  SUBMIT_LINEUP_REQUEST,
  SUBMIT_LINEUP_SUCCESS,
  SUBMIT_LINEUP_FAILURE,

  CREATE_LINEUP_REQUEST,
  CREATE_LINEUP_SUCCESS,
  CREATE_LINEUP_FAILURE,

  GET_TEAMS_LIST_FOR_CONTEST_REQUEST,
  GET_TEAMS_LIST_FOR_CONTEST_SUCCESS,
  GET_TEAMS_LIST_FOR_CONTEST_FAILURE,
} from '../actions/teamManagement';

const initialState = {
  // team Line-Up Rules
  isGettingLineUpRules: false,
  lineUpRules: {},

  // players list for contest
  isGettingContestPlayers: false,
  contestPlayersLineUp: {},

  // players list for contest
  isLoading: false,
  lineupResponse: {},
  // Teams List Response in My Contest
  isLoadingTeamList: false,
  teamsListResponse: {},
};

function teamManagement(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    // Team Line-Up Submission
    case CREATE_LINEUP_REQUEST:
    case SUBMIT_LINEUP_REQUEST:
      return {
        ...state,
        isLoading: true,
        lineupResponse: {},
      };

    case CREATE_LINEUP_SUCCESS:
    case CREATE_LINEUP_FAILURE:
    case SUBMIT_LINEUP_SUCCESS:
    case SUBMIT_LINEUP_FAILURE:
      return {
        ...state,
        isLoading: false,
        lineupResponse: action.data,
      };

    // team Line-Up Rules
    case GET_TEAM_LINEUP_RULES_REQUEST:
      return {
        ...state,
        isGettingLineUpRules: true,
        lineUpRules: {},
        lineupResponse: {},
      };
    case GET_TEAM_LINEUP_RULES_SUCCESS:
      return {
        ...state,
        isGettingLineUpRules: false,
        lineUpRules: action.data,
      };
    case GET_TEAM_LINEUP_RULES_FAILURE:
      return {
        ...state,
        isGettingLineUpRules: false,
        lineUpRules: action.data,
      };

      // players list for contest
    case GET_CONTEST_PLAYERS_LINEUP_REQUEST:
      return {
        ...state,
        isGettingContestPlayers: true,
        contestPlayersLineUp: {},
      };
    case GET_CONTEST_PLAYERS_LINEUP_SUCCESS:
      return {
        ...state,
        isGettingContestPlayers: false,
        contestPlayersLineUp: action.data,
      };
    case GET_CONTEST_PLAYERS_LINEUP_FAILURE:
      return {
        ...state,
        isGettingContestPlayers: false,
        contestPlayersLineUp: action.data,
      };

    case RESET_GET_CONTEST_PLAYERS_LINEUP:
      return {
        ...state,
        isGettingContestPlayers: false,
        contestPlayersLineUp: {},
      };

    case GET_TEAMS_LIST_FOR_CONTEST_REQUEST:
      return {
        ...state,
        isLoadingTeamList: true,
        teamsListResponse: {},
      };
    case GET_TEAMS_LIST_FOR_CONTEST_SUCCESS:
      return {
        ...state,
        isLoadingTeamList: false,
        teamsListResponse: action.data,
      };
    case GET_TEAMS_LIST_FOR_CONTEST_FAILURE:
      return {
        ...state,
        isLoadingTeamList: false,
        teamsListResponse: action.data,
      };

    case RESET_LOGOUT:
      return {
        ...state,
        isGettingLineUpRules: false,
        lineUpRules: {},
        isGettingContestPlayers: false,
        contestPlayersLineUp: {},
      };

    default:
      return state;
  }
}

export default teamManagement;
