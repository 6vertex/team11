import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  SOCIAL_LOGIN_REQUEST,
  SOCIAL_LOGIN_SUCCESS,
  SOCIAL_LOGIN_FAILURE,
  FORGOT_PASSWORD_REQUEST,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  RESET_REGISTER,
  INVITE_CODE_REQUEST,
  INVITE_CODE_SUCCESS,
  INVITE_CODE_FAILURE,
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILURE,
  RESET_LOGOUT,
} from '../actions/authentication';

const initialState = {
  isLoading: false,
  loginUserResponse: {},
  socialLoginResponse: {},
  forgotPasswordResponse: {},
  registerUserResponse: {},
  inviteCodeResponse: {},
  logoutUserResponse: {},
};

function userManagement(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case USER_LOGOUT_REQUEST:
    case INVITE_CODE_REQUEST:
    case REGISTER_REQUEST:
    case FORGOT_PASSWORD_REQUEST:
    case LOGIN_REQUEST:
    case SOCIAL_LOGIN_REQUEST:
      return {
        ...state,
        isLoading: true,
        loginUserResponse: {},
        socialLoginResponse: {},
        forgotPasswordResponse: {},
        inviteCodeResponse: {},
        logoutUserResponse: {},
      };

    case LOGIN_SUCCESS:
    case LOGIN_FAILURE:
      return {
        ...state,
        isLoading: false,
        loginUserResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case SOCIAL_LOGIN_SUCCESS:
    case SOCIAL_LOGIN_FAILURE:
      return {
        ...state,
        isLoading: false,
        socialLoginResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case FORGOT_PASSWORD_SUCCESS:
    case FORGOT_PASSWORD_FAILURE:
      return {
        ...state,
        isLoading: false,
        forgotPasswordResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case REGISTER_SUCCESS:
    case REGISTER_FAILURE:
      return {
        ...state,
        isLoading: false,
        registerUserResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case RESET_REGISTER:
      return {
        ...state,
        isLoading: false,
        registerUserResponse: {},
      };

    case INVITE_CODE_SUCCESS:
    case INVITE_CODE_FAILURE:
      return {
        ...state,
        isLoading: false,
        inviteCodeResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case USER_LOGOUT_SUCCESS:
    case USER_LOGOUT_FAILURE:
      return {
        ...state,
        isLoading: false,
        logoutUserResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case RESET_LOGOUT:
      return {
        isLoading: false,
        socialLoginResponse: {},
        loginUserResponse: {},
        forgotPasswordResponse: {},
        registerUserResponse: {},
        inviteCodeResponse: {},
        logoutUserResponse: {},
      };

    default:
      return state;
  }
}

export default userManagement;
