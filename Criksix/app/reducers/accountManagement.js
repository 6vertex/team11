import {
  GET_ACCOUNT_REQUEST,
  GET_ACCOUNT_SUCCESS,
  GET_ACCOUNT_FAILURE,
  ADD_CASH_REQUEST,
  ADD_CASH_SUCCESS,
  ADD_CASH_FAILURE,
} from '../actions/accountManagement';

const initialState = {
  isLoading: false,
  accountResponse: {},
  currentBalance: 200,
  winningAmount: 0,
  cashBonus: 0,
  crikSixPoints: 200,
  btcAmount: 0.01,
  ethAmount: 0.01,
  virtualTickets: 0,
  recentTxns: {},
  paymentOptions: {},
};

function accountManagement(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case ADD_CASH_REQUEST:
    case GET_ACCOUNT_REQUEST:
      return {
        ...state,
        isLoading: true,
        accountResponse: {},
      };

    case GET_ACCOUNT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        currentBalance: action.data.response.amount,
        accountResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case ADD_CASH_SUCCESS:
      // TODO: Handle proper response.
      return {
        ...state,
        isLoading: false,
        accountResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case ADD_CASH_FAILURE:
    case GET_ACCOUNT_FAILURE:
      return {
        ...state,
        isLoading: false,
        accountResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    default:
      return state;
  }
}

export default accountManagement;
