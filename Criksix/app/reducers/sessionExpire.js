import {
  SESSION_EXPIRE_ACTION,
  RESET_LOGOUT,
} from '../actions/authentication';

const initialState = {
  isSessionExpired: false,
};

function sessionExpire(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case RESET_LOGOUT:
      return {
        ...state,
        isSessionExpired: false,
      };

    case SESSION_EXPIRE_ACTION:
      return {
        ...state,
        isSessionExpired: true,
      };
    default:
      return state;
  }
}

export default sessionExpire;
