import {
  UPDATE_PASSWORD_REQUEST,
  UPDATE_PASSWORD_SUCCESS,
  UPDATE_PASSWORD_FAILURE,
  RESET_LOGOUT,
} from '../actions/authentication';

const initialState = {
  isLoading: false,
  updatePasswordResponse: {},
};

function updatePassword(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case UPDATE_PASSWORD_REQUEST:
      return {
        ...state,
        isLoading: true,
        updatePasswordResponse: {},
      };

    case UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        updatePasswordResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case UPDATE_PASSWORD_FAILURE:
      return {
        ...state,
        isLoading: false,
        updatePasswordResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case RESET_LOGOUT:
      return {
        isLoading: false,
        updatePasswordResponse: {},
      };

    default:
      return state;
  }
}

export default updatePassword;
