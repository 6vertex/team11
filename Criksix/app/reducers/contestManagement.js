/* eslint no-case-declarations: 0 */
// import { contest_master_data, contest_duration } from '../utils/local';
import {
  GET_ALL_CONTESTS_REQUEST,
  GET_ALL_CONTESTS_SUCCESS,
  GET_ALL_CONTESTS_FAILURE,
  GET_CONTEST_DETAILS_REQUEST,
  GET_CONTEST_DETAILS_SUCCESS,
  GET_CONTEST_DETAILS_FAILURE,

  GET_MY_CONTESTS_DETAILS_REQUEST,
  GET_MY_CONTESTS_DETAILS_SUCCESS,
  GET_MY_CONTESTS_DETAILS_FAILURE,
  RESET_MY_CONTEST_DETAILS,

  // Create contest action types
  GET_CCO_REQUEST,
  GET_CCO_SUCCESS,
  GET_CCO_FAILURE,
  GET_MID_REQUEST,
  GET_MID_SUCCESS,
  GET_MID_FAILURE,
  GET_PAYOUT_REQUEST,
  GET_PAYOUT_SUCCESS,
  GET_PAYOUT_FAILURE,
  CREATE_CONTEST_REQUEST,
  CREATE_CONTEST_SUCCESS,
  CREATE_CONTEST_FAILURE,
  RESET_CONTEST_LOBBY,

  JOIN_CONTEST_REQUEST,
  JOIN_CONTEST_SUCCESS,
  JOIN_CONTEST_FAILURE,

  // Join Contest by submitting Team Line-Up
  JOIN_CONTEST_WITH_TEAM_LINEUP_REQUEST,
  JOIN_CONTEST_WITH_TEAM_LINEUP_SUCCESS,
  JOIN_CONTEST_WITH_TEAM_LINEUP_FAILURE,
  RESET_JOIN_CONTEST_WITH_TEAM_LINEUP,

  // My Contests
  GET_MY_CONTESTS_REQUEST,
  GET_MY_CONTESTS_SUCCESS,
  GET_MY_CONTESTS_FAILURE,
  RESET_CONTEST_DETAIL,

  // Join Contest by Invite Code
  JOIN_CONTEST_WITH_INVITE_CODE_REQUEST,
  JOIN_CONTEST_WITH_INVITE_CODE_SUCCESS,
  JOIN_CONTEST_WITH_INVITE_CODE_FAILURE,

  // Update Contest Name
  UPDATE_CONTEST_NAME_REQUEST,
  UPDATE_CONTEST_NAME_SUCCESS,
  UPDATE_CONTEST_NAME_FAILURE,
  REST_UPDATE_CONTEST_NAME_REDUCER,
} from '../actions/contestManagement';

const initialState = {
  // All Contests
  allContestsLoading: false,
  allContests: {},
  contests: [],
  contestDetailLoading: false,
  contestDetails: {},
  // Create contest
  isLoading: false,
  ccoResponse: {}, // cco = Create contest options
  midResponse: {}, // mid = Matches in duration
  payoutResponse: {},
  createContestResponse: {},
  leagues: [],
  contestTypes: {},
  durationTypes: {},
  prizeStructures: [],
  accessibility: {},
  entryFeesTypes: {},
  currencyTypes: {},
  matches: [],
  payoutStructure: {},

  joinContestLoading: false,
  joinContestResponse: {},

  myContestDetailsLoading: false,
  myContestDetailsResponse: {},

  // My Contests
  isFetchingMyContests: false,
  myContestsResponse: {},

  joinContestByInviteLoading: false,
  joinContestByInviteResponse: {},

  // Edit Contest Name
  updateContestNameLoading: false,
  updateContestNameResponse: {},
};

function contestManagement(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case RESET_CONTEST_LOBBY:
      return {
        ...state,
        allContestsLoading: true,
        joinContestResponse: {},
      };
    case GET_ALL_CONTESTS_REQUEST:
      return {
        ...state,
        allContestsLoading: true,
        allContests: {},
        contests: state.contests,
      };
    case GET_ALL_CONTESTS_SUCCESS:
      return {
        ...state,
        allContestsLoading: false,
        allContests: action.data,
        contests: action.data.response.data.filter(cont => cont.is_joined === false
          && cont.status.toLowerCase() !== 'closed'),
      };
    case GET_ALL_CONTESTS_FAILURE:
      return {
        ...state,
        allContestsLoading: false,
        allContests: action.data,
        contests: [],
      };


    case GET_CONTEST_DETAILS_REQUEST:
      return {
        ...state,
        contestDetailLoading: true,
        contestDetails: {},
      };
    case GET_CONTEST_DETAILS_SUCCESS:
      return {
        ...state,
        contestDetailLoading: false,
        contestDetails: action.data,
      };
    case GET_CONTEST_DETAILS_FAILURE:
      return {
        ...state,
        contestDetailLoading: false,
        contestDetails: action.data,
      };

    case GET_MY_CONTESTS_DETAILS_REQUEST:
      return {
        ...state,
        myContestDetailsLoading: true,
        myContestDetailsResponse: {},
      };
    case GET_MY_CONTESTS_DETAILS_SUCCESS:
      return {
        ...state,
        myContestDetailsLoading: false,
        myContestDetailsResponse: action.data,
      };
    case GET_MY_CONTESTS_DETAILS_FAILURE:
      return {
        ...state,
        myContestDetailsLoading: false,
        myContestDetailsResponse: action.data,
      };

    case RESET_MY_CONTEST_DETAILS:
      return {
        ...state,
        myContestDetailsLoading: false,
        myContestDetailsResponse: {},
      };

    // Create contest actions handling
    case GET_CCO_REQUEST:
    case GET_MID_REQUEST:
    case GET_PAYOUT_REQUEST:
    case CREATE_CONTEST_REQUEST:
      return {
        ...state,
        isLoading: true,
        ccoResponse: {},
        midResponse: {},
        payoutResponse: {},
        createContestResponse: {},
      };
    case GET_CCO_SUCCESS:
      return {
        ...state,
        isLoading: false,
        leagues: action.data.response.leagues,
        contestTypes: action.data.response.contest_types,
        durationTypes: action.data.response.duration_types,
        prizeStructures: action.data.response.prize_structures,
        accessibility: action.data.response.accessibility,
        entryFeesTypes: action.data.response.entry_fee_type,
        currencyTypes: action.data.response.currency_tpye,
        ccoResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };
    case GET_CCO_FAILURE:
      return {
        ...state,
        isLoading: false,
        ccoResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case GET_MID_SUCCESS:
      const acronym = action.data.response.daily ? 'daily' : 'weekly';
      return {
        ...state,
        isLoading: false,
        matches: action.data.response[acronym],
        midResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case GET_MID_FAILURE:
      return {
        ...state,
        isLoading: false,
        midResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case GET_PAYOUT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        payoutStructure: action.data.response.payouts,
        payoutResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case GET_PAYOUT_FAILURE:
      return {
        ...state,
        isLoading: false,
        payoutResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case CREATE_CONTEST_SUCCESS:
    case CREATE_CONTEST_FAILURE:
      return {
        ...state,
        isLoading: false,
        createContestResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    // case RESET_CREATE_CONTEST:
    //   console.log('RESET_CREATE_CONTEST');
    //   return {
    //     ...state,
    //     isLoading: false,
    //     createContestResponse: {},
    //     ccoResponse: {}, // cco = Create contest options
    //     midResponse: {}, // mid = Matches in duration
    //     payoutResponse: {},
    //     leagues: [],
    //     contestTypes: {},
    //     durationTypes: {},
    //     prizeStructures: [],
    //     accessibility: {},
    //     entryFeesTypes: {},
    //     currencyTypes: {},
    //     matches: [],
    //     payoutStructure: {},
    //   };

    case JOIN_CONTEST_REQUEST:
      return {
        ...state,
        joinContestLoading: true,
        joinContestResponse: {},
      };
    case JOIN_CONTEST_SUCCESS:
    case JOIN_CONTEST_FAILURE:
      return {
        ...state,
        joinContestLoading: false,
        joinContestResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    // Join Contest by submitting Team Line-Up
    case JOIN_CONTEST_WITH_TEAM_LINEUP_REQUEST:
      return {
        ...state,
        joinContestLoading: true,
        joinContestResponse: {},
      };
    case JOIN_CONTEST_WITH_TEAM_LINEUP_SUCCESS:
      return {
        ...state,
        joinContestLoading: false,
        joinContestResponse: action.data,
      };
    case JOIN_CONTEST_WITH_TEAM_LINEUP_FAILURE:
      return {
        ...state,
        joinContestLoading: false,
        joinContestResponse: action.data,
      };
    case RESET_JOIN_CONTEST_WITH_TEAM_LINEUP:
      return {
        ...state,
        joinContestLoading: false,
        joinContestResponse: {},
      };

    case RESET_CONTEST_DETAIL:
      return {
        ...state,
        contestDetails: {},
      };

      // Join Contest by submitting Team Line-Up
    case GET_MY_CONTESTS_REQUEST:
      return {
        ...state,
        isFetchingMyContests: true,
        myContestsResponse: {},
        contestDetails: {},
      };
    case GET_MY_CONTESTS_SUCCESS:
      return {
        ...state,
        isFetchingMyContests: false,
        myContestsResponse: action.data,
      };
    case GET_MY_CONTESTS_FAILURE:
      return {
        ...state,
        isFetchingMyContests: false,
        myContestsResponse: action.data,
      };

    case JOIN_CONTEST_WITH_INVITE_CODE_REQUEST:
      return {
        ...state,
        joinContestByInviteLoading: true,
        joinContestByInviteResponse: {},
      };
    case JOIN_CONTEST_WITH_INVITE_CODE_SUCCESS:
      return {
        ...state,
        joinContestByInviteLoading: false,
        joinContestByInviteResponse: action.data,
      };
    case JOIN_CONTEST_WITH_INVITE_CODE_FAILURE:
      return {
        ...state,
        joinContestByInviteLoading: false,
        joinContestByInviteResponse: action.data,
      };
    case UPDATE_CONTEST_NAME_REQUEST:
      return {
        ...state,
        updateContestNameLoading: true,
        updateContestNameResponse: {},
      };
    case UPDATE_CONTEST_NAME_SUCCESS:
      return {
        ...state,
        updateContestNameLoading: false,
        updateContestNameResponse: action.data,
      };
    case UPDATE_CONTEST_NAME_FAILURE:
      return {
        ...state,
        updateContestNameLoading: false,
        updateContestNameResponse: action.data,
      };
    case REST_UPDATE_CONTEST_NAME_REDUCER:
      return {
        ...state,
        updateContestNameLoading: false,
        updateContestNameResponse: {},
      };
    default:
      return state;
  }
}

export default contestManagement;
