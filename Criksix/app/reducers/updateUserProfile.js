import {
  UPDATE_USER_PROFILE_REQUEST,
  UPDATE_USER_PROFILE_SUCCESS,
  UPDATE_USER_PROFILE_FAILURE,
  RESET_LOGOUT,
} from '../actions/authentication';

const initialState = {
  isLoading: false,
  updateUserProfileResponse: {},
};

function updateProfile(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case UPDATE_USER_PROFILE_REQUEST:
      return {
        ...state,
        isLoading: true,
        updateUserProfileResponse: {},
      };

    case UPDATE_USER_PROFILE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        updateUserProfileResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case UPDATE_USER_PROFILE_FAILURE:
      return {
        ...state,
        isLoading: false,
        updateUserProfileResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case RESET_LOGOUT:
      return {
        isLoading: false,
        updateUserProfileResponse: {},
      };

    default:
      return state;
  }
}

export default updateProfile;
