import {
  GET_PLAYER_DETAILS_REQUEST,
  GET_PLAYER_DETAILS_SUCCESS,
  GET_PLAYER_DETAILS_FAILURE,
  RESET_PLAYER,
} from '../actions/player';

const initialState = {
  isLoading: false,
  playerProfileResponse: {},
};

function playerProfile(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case GET_PLAYER_DETAILS_REQUEST:
      return {
        ...state,
        isLoading: true,
        playerProfileResponse: {},
      };

    case GET_PLAYER_DETAILS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        playerProfileResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case GET_PLAYER_DETAILS_FAILURE:
      return {
        ...state,
        isLoading: false,
        playerProfileResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };

    case RESET_PLAYER:
      return {
        ...state,
        isLoading: false,
        playerProfileResponse: {},
      };

    default:
      return state;
  }
}

export default playerProfile;
