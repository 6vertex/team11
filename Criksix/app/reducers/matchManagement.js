import {
  FILTERED_MATHCES_REQUEST,
  FILTERED_MATHCES_SUCCESS,
  FILTERED_MATHCES_FAILURE,
  RESET_MATCH_MANAGEMENT,
  GET_MATCH_FILTERS_REQUEST,
  GET_MATCH_FILTERS_SUCCESS,
  GET_MATCH_FILTERS_FAILURE,
} from '../actions/matchManagement';
import { RESET_LOGOUT } from '../actions/authentication';

const initialState = {
  isLoading: false,
  matchManagementResponse: {},
  loadingMatchFilters: false,
  matchFiltersResponse: {},
  filters: [],
};

function matchManagement(state = initialState, action) {
  if (action.type === 'undefined') {
    return state;
  }

  switch (action.type) {
    case FILTERED_MATHCES_REQUEST:
      return {
        ...state,
        isLoading: true,
        matchManagementResponse: {},
      };
    case FILTERED_MATHCES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        matchManagementResponse: {
          response: action.data.response,
          status: action.data.status,
        },
        filters: action.filters,
      };
    case FILTERED_MATHCES_FAILURE:
      return {
        ...state,
        isLoading: false,
        matchManagementResponse: {
          response: action.data.response,
          status: action.data.status,
        },
        filters: state.filters,
      };
    case RESET_MATCH_MANAGEMENT:
    case RESET_LOGOUT:
      return {
        ...state,
        isLoading: false,
        matchManagementResponse: {},
      };

    case GET_MATCH_FILTERS_REQUEST:
      return {
        ...state,
        loadingMatchFilters: true,
        matchFiltersResponse: {},
      };
    case GET_MATCH_FILTERS_SUCCESS:
      return {
        ...state,
        loadingMatchFilters: false,
        matchFiltersResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };
    case GET_MATCH_FILTERS_FAILURE:
      return {
        ...state,
        loadingMatchFilters: false,
        matchFiltersResponse: {
          response: action.data.response,
          status: action.data.status,
        },
      };
    default:
      return state;
  }
}

export default matchManagement;
