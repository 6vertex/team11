import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { isIOS } from '../utils/PlatformSpecific';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  screenContainer: {
    width,
    height,
    backgroundColor: '#00000070',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  mainContainer: {
    width,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'white',
    marginBottom: isIOS ? 0 : 25,
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: 'lightgrey',
    marginHorizontal: 5,
  },
  topView: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    backgroundColor: 'white',
  },
  header: {
    width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 25,
    backgroundColor: '#e71702',
  },
  headerText: {
    fontSize: 16,
    fontWeight: '700',
    color: 'white',
  },
  teamListHeader: {
    width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 25,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  teamListText: {
    fontSize: 12,
    fontWeight: '700',
    textAlign: 'left',
    color: 'black',
  },
  BottomView: {
    width: width * 0.85,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'green',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  joinContestText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '700',
    paddingVertical: 12,
  },
  closeView: {
    position: 'absolute',
    right: 5,
    top: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  closeTouchable: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
  },
  closeText: {
    color: 'white',
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '400',
  },
});

const PrizeDistributionPopUp = props => (
  <View style={styles.screenContainer}>
    <View style={{ flex: 1, width, backgroundColor: 'transparent' }}>
      <TouchableOpacity
        style={{ flex: 1 }}
        onPress={() => props.close()}
      />
    </View>
    <View style={styles.mainContainer}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Prize Distribution</Text>
      </View>
      <ScrollView
        style={{ maxHeight: 300 }}
      >
        {
          props.contest.prize_distribution &&
          props.contest.prize_distribution.map((data, index) => {
            if (data.winning_type === 'percent') {
              return (
                <View key={index} style={styles.teamListHeader}>
                  <Text style={styles.teamListText}>Top #{data.position}</Text>
                  <Text style={styles.teamListText}>{data.winning_percentage}%</Text>
                  <Text style={styles.teamListText}>
                    {props.contest.currency_type}. {data.winning_amount}
                  </Text>
                </View>
              );
            }
            return (
              <View key={index} style={styles.teamListHeader}>
                <Text style={styles.teamListText}>Prize distributed in {data.title} teams</Text>
                <Text style={styles.teamListText}>{data.winning_amount}</Text>
              </View>
            );
          })
        }
      </ScrollView>
      <View style={styles.closeView}>
        <TouchableOpacity
          onPress={() => props.close()}
          style={styles.closeTouchable}
        >
          <Text style={styles.closeText}>Close</Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
);

PrizeDistributionPopUp.propTypes = {
  close: PropTypes.func,
  contest: PropTypes.objectOf(PropTypes.any),
};

PrizeDistributionPopUp.defaultProps = {
  close: () => {},
  contest: {},
};

export default PrizeDistributionPopUp;
