/* eslint arrow-body-style: 0 */
import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';
import { images } from '../assets/images';

const styles = StyleSheet.create({
  checkBoxHolder: {
    width: 30,
    height: 30,
    backgroundColor: 'transparent',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkedImage: {
    width: 20,
    height: 20,
    alignSelf: 'center',
  },
});

const CustomCheckbox = (props) => {
  return (
    <View
      onPress={() => props.onCheck()}
      style={[styles.checkBoxHolder, {
        backgroundColor: props.checked ? 'green' : 'transparent',
      }]}
    >
      {props.checked ?
        <Image
          style={styles.checkedImage}
          source={images.checkWhite}
        /> : null}
    </View>
  );
};

CustomCheckbox.propTypes = {
  checked: PropTypes.bool,
  onCheck: PropTypes.func,
};

CustomCheckbox.defaultProps = {
  checked: false,
  onCheck: () => {},
};

export default CustomCheckbox;
