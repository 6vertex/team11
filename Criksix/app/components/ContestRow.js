import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import { images } from '../assets/images';
import { constant, getCurrencyType } from '../utils';
import Utils from '../utils/utils';
import { Colors } from '../theme/index';
import { capitalizeFirstLetter } from '../utils/utils_functions';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  contestViewStyle: {
    width: width - 20,
    paddingVertical: 8,
    paddingHorizontal: 15,
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderBottomWidth: 2,
    marginVertical: 5,
    borderRadius: 5,
    marginHorizontal: 5,
    backgroundColor: Colors.primarybackgroundColor,
    // borderBottomColor: '#4f5386',
  },
  contestHeder: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  contestName: {
    color: 'white',
    textAlign: 'center',
    marginVertical: 3,
    paddingRight: 5,
    backgroundColor: 'transparent',
    fontSize: 16,
    fontWeight: '700',
  },
  headerView: {
    width: width - 35,
    flexDirection: 'row',
    padding: 2,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  detailView: {
    width: width - 35,
    flexDirection: 'row',
    paddingRight: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
  },
  totalEarnings: {
    fontSize: 15,
    fontWeight: '400',
    color: 'white',
  },
  earningsText: {
    fontSize: 15,
    fontWeight: '700',
    color: 'white',
  },
  detailHeader: {
    fontSize: 12,
    fontWeight: '400',
    color: '#4f5386',
  },
  detailText: {
    fontSize: 12,
    fontWeight: '700',
    color: 'white',
  },
  joinButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical: 8,
    backgroundColor: '#4f5386',
    borderRadius: 12,
  },
  nextIcon: {
    width: 10,
    height: 10,
    marginLeft: 3,
    transform: [{
      rotate: '180deg',
    }],
  },

  statisticsView: {
    marginVertical: 5,
    paddingVertical: 5,
    alignSelf: 'stretch',
  },
  statusContainer: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  statusView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    width: width * 0.62,
    height: 5,
    backgroundColor: 'white',
    marginTop: 10,
  },
  uncappedInfoView: {
    backgroundColor: 'transparent',
    width: width * 0.62,
  },
  textHeader: {
    color: 'white',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 12,
  },

  joinedStatus: {
    backgroundColor: Colors.navigationBackgroundColor,
  },
  roomStatus: {
    flex: 1,
  },
  teamsCountView: {
    width: width * 0.62,
    flexDirection: 'row',
    marginTop: -15,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});


const ContestRow = (props) => {
  const isContestUpcoming = props.contest.status.toLowerCase() === 'upcoming';
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={() => props.showContestDetails(props.contest)}
    >
      <View style={styles.contestViewStyle}>
        <View style={styles.contestHeder}>
          <Text
            style={styles.contestName}
            lineBreakMode="tail"
          >{props.contest.name.toUpperCase()}
          </Text>
          {
            <Text
              style={[styles.contestName, {
                color: isContestUpcoming ? Colors.navigationBackgroundColor : Colors.greenShadeColor,
                fontSize: 14,
                fontWeight: '400',
                }]}
              numberOfLines={2}
              lineBreakMode="tail"
            >({ !isContestUpcoming ? capitalizeFirstLetter(props.contest.status) :
              `${Utils.formatDate(props.contest.start_date)} ${Utils.formatTime(props.contest.start_date)}`
              })
            </Text>
          }
        </View>
        <View style={styles.detailView}>
          <View>
            <Text style={styles.detailHeader}>WINNINGS</Text>
            <Text style={styles.detailText}>
              {getCurrencyType(props.contest.entry_fee_type, props.contest.currency_type)}
              {props.contest.prize_pool}
            </Text>
          </View>
          <View>
            <Text style={styles.detailHeader}>WINNERS</Text>
            <Text style={styles.detailText}>{props.contest.winners}</Text>
          </View>
          <View>
            <Text style={styles.detailHeader}>ENTRY FEE</Text>
            <Text style={[styles.detailText, { color: Colors.greenShadeColor }]}>
              {getCurrencyType(props.contest.entry_fee_type, props.contest.currency_type)}
              {props.contest.entry_fee}
            </Text>
          </View>
        </View>
        <View style={styles.statisticsView}>
          <View style={styles.statusContainer}>
            {props.contest.contest_type !== constant.CONTEST_TYPES[2] &&
              <View style={styles.statusView}>
                <View
                  style={[
                    styles.joinedStatus,
                    { flex: (props.contest.joined_users_count / props.contest.max_teams) },
                  ]}
                />
                <View style={[
                  styles.roomStatus,
                  { flex: (1 - (props.contest.joined_users_count / props.contest.max_teams)) },
                  ]}
                />
              </View>
            }
            {props.contest.contest_type === constant.CONTEST_TYPES[2] &&
              <View style={styles.uncappedInfoView}>
                <Text style={styles.detailText}>This is Uncapped Contest</Text>
              </View>
            }
            <TouchableOpacity
              activeOpacity={isContestUpcoming ? 0.7 : 1}
              onPress={() => props.joinContest(props.contest)}
              disabled={!isContestUpcoming}
            >
              <View style={[styles.joinButtonView, props.contest.is_joined === true ? { backgroundColor: 'orange' } : { }]}>
                <Text style={styles.detailText}>{ isContestUpcoming ? 'JOIN' : 'DETAIL' }</Text>
                <Image source={images.backIcon} style={styles.nextIcon} />
              </View>
            </TouchableOpacity>
          </View>
          {props.contest.contest_type !== constant.CONTEST_TYPES[2] &&
            <View style={styles.teamsCountView}>
              <Text style={[styles.detailText, { color: 'orange' }]}>Only {props.contest.remaining_users_count && props.contest.remaining_users_count} Spots Left</Text>
              <Text style={styles.detailText}>
                {props.contest.max_teams && props.contest.max_teams} TEAMS
              </Text>
            </View>
          }
        </View>
      </View>
    </TouchableOpacity>
  );
};

ContestRow.propTypes = {
  contest: PropTypes.objectOf(PropTypes.any),
  joinContest: PropTypes.func,
  showContestDetails: PropTypes.func,
};

ContestRow.defaultProps = {
  contest: {},
  joinContest: () => {},
  showContestDetails: () => {},
};

export default ContestRow;
