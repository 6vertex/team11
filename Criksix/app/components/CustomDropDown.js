import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  ListView,
  Modal,
} from 'react-native';
import { images } from '../assets/images';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  dropDownContainerView: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0c0d18',
    marginTop: 5,
  },
  dropDownInfoView: {
    flexDirection: 'row',
    backgroundColor: '#0c0d18',
    width: width - 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 8,
    marginBottom: 3,
  },
  infoTextContainer: {
    flex: 1,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 8,
  },
  dropDownHeaderText: {
    fontSize: 18,
    fontWeight: '300',
    color: 'white',
    margin: 5,
  },
  dropDownSubHeaderText: {
    fontSize: 15,
    fontWeight: '700',
    color: 'white',
    margin: 5,
  },
  dropDownImage: {
    width: 25,
    height: 25,
  },
  dropDownTouchable: {
    flex: 2.5,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  dropDownSelectedView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 5,
    borderRadius: 5,
    padding: 5,
    borderWidth: 1,
  },
  dropDownInfoText: {
    fontSize: 14,
    fontWeight: '600',
    textAlign: 'center',
    padding: 3,
    color: 'rgb(105,105,105)',
  },
  dropDownDownImage: {
    width: 17,
    height: 20,
    marginRight: 5,
  },
  listRow: {
    flexDirection: 'row',
    width,
    height: 50,
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    backgroundColor: '#E2E2E2',
    paddingHorizontal: 8,
  },
  dropDownListItem: {
    flex: 1,
    height: 35,
  },
  interactiveView: {
    marginHorizontal: 15,
    width: 15,
    height: 50,
    backgroundColor: 'black',
  },
  dropDownListItemText: {
    fontSize: 14,
    fontWeight: '600',
    textAlign: 'center',
    color: 'black',
  },
  checkedImage: {
    marginLeft: 15,
    width: 22,
    height: 17,
  },
});

const CustomDropDown = (props) => {
  const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  return (
    <View style={styles.dropDownContainerView}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => props.toggleDropDown()}
      >
        <View style={styles.dropDownInfoView}>
          <View style={styles.infoTextContainer}>
            <Text
              style={styles.dropDownHeaderText}
            >
            {props.header}
            </Text>
            <Text
              style={styles.dropDownSubHeaderText}
            >
            {props.list[props.selectedIndex].name}
            </Text>
          </View>
          <View>
            <Image source={images.dropDownIcon} style={styles.dropDownImage} />
          </View>
        </View>
      </TouchableOpacity>
      {props.canShowList &&
      <ListView
        style={{ height: 150 }}
        dataSource={ds.cloneWithRows(props.list)}
        enableEmptySections
        renderRow={(rowData, sectionID, rowIndex) =>(
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => props.selectIndex(Number(rowIndex))}
          >
            <View style={[styles.listRow, props.selectedIndex === Number(rowIndex) ? { backgroundColor: 'orange' } : {}]}>
              <Text
                style={styles.dropDownListItemText}
              >
                {rowData.name}
              </Text>
              <Image style={styles.checkedImage} source={images.checkedImage} />
            </View>
          </TouchableOpacity>
          )
          }
      />
      }
    </View>
  );
};

CustomDropDown.propTypes = {
};

CustomDropDown.defaultProps = {
};

export default CustomDropDown;
