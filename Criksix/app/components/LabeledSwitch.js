import React from 'react';
import PropTypes from 'prop-types';
import { LayoutAnimation, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    width: 60,
    height: 22,
    backgroundColor: 'grey',
    flexDirection: 'row',
    overflow: 'visible',
    borderRadius: 15,
    shadowColor: 'black',
    shadowOpacity: 1.0,
    shadowOffset: {
      width: -2,
      height: 2,
    },
  },
  circle: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOpacity: 1.0,
    shadowOffset: {
      width: 2,
      height: 2,
    },
  },
  activeContainer: {
    backgroundColor: 'rgb(83, 181, 53)',
    flexDirection: 'row-reverse',
  },
  label: {
    alignSelf: 'center',
    backgroundColor: 'transparent',
    paddingHorizontal: 5,
    fontWeight: 'bold',
    fontSize: 12,
    color: 'white',
  },
});

class LabeledSwitch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
    };
    this.toggle = this.toggle.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    // update local state.value if props.value changes....
    if (nextProps.value !== this.state.value) {
      this.setState({ value: nextProps.value });
    }
  }
  toggle() {
    // define how we will use LayoutAnimation to give smooth transition between state change
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    const newValue = !this.state.value;
    this.setState({
      value: newValue,
    });

    // fire function if exists
    if (typeof this.props.onValueChange === 'function') {
      this.props.onValueChange(newValue);
    }
  }
  render() {
    const { value } = this.state;

    return (
      <TouchableOpacity
        style={{ ...this.props.style, margin: 5 }}
        onPress={this.toggle}
      >
        <View style={[
          styles.container,
          this.props.isSwitchOn && styles.activeContainer]}
        >
          <View style={styles.circle} />
          <Text style={styles.label}>
            { this.props.isSwitchOn ? 'ON' : 'OFF' }
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

LabeledSwitch.propTypes = {
  onValueChange: PropTypes.func,
  value: PropTypes.bool,
  style: PropTypes.any,
  isSwitchOn: PropTypes.bool,
};

LabeledSwitch.defaultProps = {
  onValueChange: undefined,
  value: false,
  style: {},
  isSwitchOn: false,
};

export default LabeledSwitch;
