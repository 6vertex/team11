import React, { Component } from 'react';
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center',
    paddingVertical: 5,
  },
  button: {
    height: 24,
    width: 24,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: '#575b91',
    alignItems: 'center',
    justifyContent: 'center',
  },
  activeButton: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor: '#575b91',
  },
  buttonTxt: {
    fontSize: 14,
    color: 'white',
    paddingLeft: 10,
  },
});

class RadioSet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedButtonIndex: props.defaultIndex,
    };
  }

  handleSelect = (r, i) => {
    this.props.onSelect(r);
    this.setState({
      selectedButtonIndex: i,
    });
  } 

  render() {
    const {
      radios,
      labelExtracter,
      onSelect,
      containerStyle,
      buttonStyle,
      buttonTextStyle,
    } = this.props;
    return (
      <View style={[styles.container, containerStyle]}>
        {
          radios.map((radio, index) => (
            <TouchableOpacity
              onPress={() => this.handleSelect(radio, index)}
              activeOpacity={1}
              key={`Radio-${index}`}
              style={styles.buttonContainer}
            >
              <View style={[styles.button, buttonStyle]}>
                {this.state.selectedButtonIndex === index &&
                  <View style={styles.activeButton} />}
              </View>
              <Text style={[styles.buttonTxt, buttonTextStyle]}>
                {labelExtracter === '' ? radio.toUpperCase() : radio[labelExtracter]}
              </Text>
            </TouchableOpacity>
          ))
        }
      </View>
    );
  }
}

RadioSet.propTypes = {
  radios: PropTypes.arrayOf(PropTypes.any).isRequired,
  labelExtracter: PropTypes.string.isRequired,
  onSelect: PropTypes.func,
  containerStyle: PropTypes.objectOf(PropTypes.any),
  buttonStyle: PropTypes.objectOf(PropTypes.any),
  buttonTextStyle: PropTypes.objectOf(PropTypes.any),
  defaultIndex: PropTypes.number,
};

RadioSet.defaultProps = {
  radios: [],
  labelExtracter: '',
  onSelect: () => {},
  containerStyle: {},
  buttonStyle: {},
  buttonTextStyle: {},
  defaultIndex: -1,
}

export default RadioSet;
