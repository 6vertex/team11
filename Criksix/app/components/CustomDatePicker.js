import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  DatePickerAndroid,
  DatePickerIOS,
  Dimensions,
  Platform,
} from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  overlay: {
    height,
    width,
    backgroundColor: "rgba(0,0,0,0.2)",
    position: "absolute",
    top: 0,
  },
});

const DatePicker = () => {
  const { submitButton } = styles;
  if (Platform.OS === 'ios') {
    return (
      <View style={styles.overlay}>
        <View style={{ padding: 10, backgroundColor: colors.darkWhite, opacity: 1 }}>
          <Text>Select Your Birthday</Text>
          <DatePickerIOS
            style={styles.birthdayPicker}
            mode="date"
            date={new Date()}
            onDateChange={(date) => {}}
          />
          
        </View>
      </View>
    );
  }
  try {
    DatePickerAndroid.open({
      date: new Date(),
    }).then((date) => {
      if (date.action === 'dateSetAction') {
      }
    }).catch((error) => {
      console.log('!!!!!!! error: ', error);
    });
  } catch (error) {
    console.warn('Cannot open date picker', error.message);
  }
  return null;
};

export default DatePicker;
