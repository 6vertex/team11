import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Dimensions,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  inputView: {
    width: (width / 3) * 2.6,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginTop: 15,
    backgroundColor: 'transparent',
  },
  inputTitle: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'left',
  },
  textInput: {
    width: (width / 3) * 2.6,
    fontSize: 16,
    fontWeight: '400',
    paddingBottom: 5,
    marginTop: Platform.OS === 'ios' ? 5 : -8,
    color: 'white',
    borderBottomWidth: 1,
    borderBottomColor: 'white',
  },
});

const CustomTextInput = ({
  title,
  titleStyle,
  value,
  onChangeText,
  placeholder,
  placeholderTextColor,
  selectionColor,
  keyboardType,
  editable,
  secureTextEntry,
  returnKeyType,
  autoCapitalize,
  autoCorrect,
  onFocus,
  onSubmitEditing,
  inputKey,
  getTextInputReference,
}) => (
  <View style={styles.inputView}>
    <Text style={[styles.inputTitle, titleStyle]}>{title}</Text>
    <TextInput
      style={styles.textInput}
      ref={(ref) => {
        getTextInputReference(inputKey, ref);
      }}
      value={value}
      onChangeText={onChangeText}
      placeholder={placeholder}
      placeholderTextColor={placeholderTextColor}
      selectionColor={selectionColor}
      keyboardType={keyboardType}
      underlineColorAndroid="transparent"
      secureTextEntry={secureTextEntry}
      editable={editable}
      returnKeyType={returnKeyType}
      autoCapitalize={autoCapitalize}
      autoCorrect={autoCorrect}
      onFocus={() => onFocus()}
      onSubmitEditing={() => onSubmitEditing(inputKey)}
    />
  </View>
);

CustomTextInput.propTypes = {
  title: PropTypes.string,
  titleStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  value: PropTypes.string,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  selectionColor: PropTypes.string,
  keyboardType: PropTypes.string,
  editable: PropTypes.bool,
  secureTextEntry: PropTypes.bool,
  returnKeyType: PropTypes.string,
  autoCapitalize: PropTypes.string,
  autoCorrect: PropTypes.bool,
  onFocus: PropTypes.func,
  onSubmitEditing: PropTypes.func,
  inputKey: PropTypes.string,
  getTextInputReference: PropTypes.func,
  onChangeText: PropTypes.func,
};

CustomTextInput.defaultProps = {
  title: '',
  titleStyle: {},
  value: '',
  placeholder: '',
  placeholderTextColor: 'white',
  selectionColor: 'white',
  keyboardType: 'default',
  editable: true,
  secureTextEntry: false,
  returnKeyType: 'done',
  autoCapitalize: 'none',
  autoCorrect: false,
  onFocus: () => {},
  onSubmitEditing: () => {},
  inputKey: '',
  getTextInputReference: () => {},
  onChangeText: () => {},
};
export default CustomTextInput;
