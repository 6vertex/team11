import React from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { statusBarHeight, isIOS } from '../utils/PlatformSpecific';
import { navigationBackgroundColor } from '../theme/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: navigationBackgroundColor,
    flexDirection: 'row',
    height: 52,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'space-between',
    // marginTop: statusBarHeight,
    paddingTop: isIOS ? 20 : 0,
  },
  headerView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Arial',
    fontWeight: '500',
  },
  leftView: {
    flex: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 10,
  },
  navImage: {
    width: 20,
    height: 20,
  },
  rightView: {
    flex: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingRight: 10,
  },


  touchableStyle: {
    width: 35,
    height: 35,
    margin: 8,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 17.5,
  },
  navBarIcon: {
    width: 21,
    height: 21,
  },
  titleIcon: {
    width: width - 156,
    height: 28,
    backgroundColor: 'transparent',
  },
  cartItemsCountView: {
    top: 0,
    right: isIOS ? -4 : 0,
    width: 20,
    height: 20,
    borderRadius: 10,
    position: 'absolute',
    backgroundColor: 'lightgrey',
    opacity: 0.85,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartItemsCountText: {
    color: 'white',
    fontSize: 11,
    fontWeight: '600',
  },
  appTitleText: {
    color: 'white',
    backgroundColor: 'transparent',
    fontSize: 20,
    fontWeight: '800',
    fontFamily: 'Helvetica',
  },
  navBarViewStyle: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
});

const NavBar = ({
  title,
  showBackButton,
  backButtonImage,
  backButtonAction,
  showRightButton,
  rightButtonImage,
  rightButtonAction,
}) => (
  <View style={styles.container}>
    <View style={styles.leftView}>
      {showBackButton ?
        <TouchableOpacity
          onPress={() => backButtonAction()}
        >
          <Image style={styles.navImage} source={backButtonImage} />
        </TouchableOpacity>
        : null }
    </View>
    <View style={styles.headerView}>
      <Text style={styles.titleText}>{title}</Text>
    </View>
    <View style={styles.rightView}>
      {showRightButton ?
        <TouchableOpacity
          onPress={() => rightButtonAction()}
        >
          <Image style={styles.navImage} source={rightButtonImage} />
        </TouchableOpacity>
        : null
      }
    </View>
  </View>
);

NavBar.propTypes = {
  title: PropTypes.string,
  leftView: PropTypes.element,
  rightView: PropTypes.element,
  showBackButton: PropTypes.bool,
  showRightButton: PropTypes.bool,
  backButtonImage: PropTypes.any,
  backButtonAction: PropTypes.func,
  rightButtonImage: PropTypes.any,
  rightButtonAction: PropTypes.func,
};

NavBar.defaultProps = {
  title: '',
  leftView: null,
  rightView: null,
  showBackButton: false,
  showRightButton: false,
  backButtonImage: null,
  backButtonAction: () => {},
  rightButtonImage: null,
  rightButtonAction: () => {},
};
export default NavBar;
