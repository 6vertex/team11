import React from 'react';
import { StatusBar } from 'react-native';
import { Colors } from '../theme';

const CommonStatusBar = props => (
  <StatusBar
    backgroundColor={Colors.statusBarColor}
    barStyle="light-content"
    {...props}
  />
);

export default CommonStatusBar;
