/* eslint import/no-unresolved: 0 */
const addButton = require('./add_button.png');
const alarm = require('./alarm.png');
const authBottom = require('./auth_bottom.png');
const backButton = require('./back_button.png');
const backIcon = require('./back-icon.png');
const bannerImage = require('./bannerimg.png');
const bannerImage1 = require('./bnnrad.png');
const bannerImage2 = require('./bnnr2.png');
const bannerImage3 = require('./bnnr3.png');
const bannerImage4 = require('./bnnr4.png');
const inviteFriendsBannerImage = require('./bnnrimg.png');
const cancel = require('./cancel.png');
const clock = require('./clock.png');
const crickSixLogo = require('./cricksix_logo.png');
const cup = require('./cup.png');
const ellipse1 = require('./ellipse1.png');
const ellipse2 = require('./ellipse2.png');
const facebookLogin = require('./facebook_login.png');
const filter = require('./filter.png');
const generalBackground = require('./general_bg.png');
const googlePlusLogin = require('./google_plus_login.png');
const givMoney = require('./givmoney.png');
const hierarchy = require('./hierarchica.png');
const home = require('./home.png');
const icon = require('./icon.png');
const avatar = require('./images.png');
const information = require('./information.png');
const inviteImage = require('./invite_image.png');
const landingBackground = require('./landing_bg_screen.png');
const loginBackground = require('./login_bg.png');
const logout = require('./logout.png');
const manUser = require('./man_user.png');
const menuIcon = require('./menu_icon.png');
const menuThree = require('./menu_three.png');
const crossIcon = require('./multiply.png');
const myAccount = require('./my-account.png');
const next = require('./next.png');
const personalAccount = require('./person_accoun.png');
const plusSign = require('./plus_sign.png');
const profilePic = require('./profile-pic.png');
const rectangle = require('./rectangle.png');
const rupeeCoin = require('./rupeecoin.png');
const contestIcon = require('./select-contest-icon.png');
const playerIcon = require('./select-player-icon.png');
const sideshape = require('./sideshape.png');
const software = require('./software.png');
const sortDown = require('./sort_down.png');
const splash = require('./splash.jpg');
const rightTick = require('./slct.png');
const videoPlay = require('./video-ply.png');
const wedding = require('./wedding.png');
const winnerIcon = require('./winner-icon.png');
const checkWhite = require('./white_check.png');
const IndiaTeamLogo = require('./India.png');
const slTeamLogo = require('./Sl.png');
const dropDownIcon = require('./drop-down-icon.png');
const editIcon = require('./edit.png');


const fbLoginIcon = require('./fb_login.png');
const gLoginIcon = require('./google_login.png');
const hidePassword = require('./hide_passowrd.png');
const eye = require('./eye.png');

export const images = {
  IndiaTeamLogo,
  slTeamLogo,
  addButton,
  alarm,
  authBottom,
  backButton,
  backIcon,
  bannerImage,
  bannerImage1,
  bannerImage2,
  bannerImage3,
  bannerImage4,
  inviteFriendsBannerImage,
  cancel,
  clock,
  crickSixLogo,
  cup,
  ellipse1,
  ellipse2,
  facebookLogin,
  filter,
  generalBackground,
  googlePlusLogin,
  givMoney,
  hierarchy,
  home,
  icon,
  avatar,
  information,
  inviteImage,
  landingBackground,
  loginBackground,
  logout,
  manUser,
  menuIcon,
  menuThree,
  crossIcon,
  myAccount,
  personalAccount,
  plusSign,
  profilePic,
  rectangle,
  rupeeCoin,
  contestIcon,
  playerIcon,
  sideshape,
  software,
  sortDown,
  splash,
  rightTick,
  videoPlay,
  wedding,
  winnerIcon,
  dropDownIcon,
  editIcon,
  next,
  checkWhite,
  fbLoginIcon,
  gLoginIcon,
  hidePassword,
  eye,
};
