import {
  Alert,
} from 'react-native';

export const showPopupAlert = (message, title = '') => {
  Alert.alert(
    title,
    message,
    [
      { text: 'OK' },
    ],
  );
};

export const showOptionAlert = (message, buttonOkText, buttonCancelText, action) => {
  Alert.alert(
    '',
    message,
    [
      { text: buttonOkText, onPress: action },
      { text: buttonCancelText },
    ],
  );
};
