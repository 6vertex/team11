import { Component } from 'react';
import Geocoder from 'react-native-geocoding';
import constant from './constant';

Geocoder.setApiKey(constant.GEOCODING_API_KEY);

class GeolocationModule extends Component {

  static findCurrentLocation(responseBlock) {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        Geocoder.getFromLatLng(position.coords.latitude, position.coords.longitude).then(
          (json) => {
            const responseObject = {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            };

            json.results[0].address_components.forEach((element) => {
              element.types.forEach((type) => {
                if (type === 'country') {
                  if (element.short_name === 'IN') {
                    responseObject.location_code = 0;
                  } else if (element.short_name === 'US') {
                    responseObject.location_code = 1;
                  } else {
                    responseObject.location_code = 2;
                  }
                  responseObject.country_name = element.long_name;
                  responseObject.country_abbr = element.short_name;
                }
                if (type === 'administrative_area_level_1') {
                  responseObject.state_name = element.long_name;
                  responseObject.state_abbr = element.short_name;
                }
              });
            });
            responseBlock(responseObject, null);
          },
          (error) => {
            responseBlock(null, error);
          },
        );
      },
      (error) => {
        responseBlock(null, error);
      },
      { timeout: 10000 },
    );
  }
}

export default GeolocationModule;
