export { default as linkState } from './linkState';
export { assignRef } from './assignRef';
export { focusOnNext } from './focusOnNext';
export {
  validateEmail,
  validatePassword,
} from './validations';
export { networkConnectivity } from './networkConnectivity';
export { default as constant } from './constant';
export {
  isIOS,
  NavBarHeight,
  statusBarHeight,
} from './PlatformSpecific';
export {
  showPopupAlert,
  showOptionAlert,
} from './showAlert';
export {
  getFilterMatchesApiBody,
  resetRoute,
  isResponseValid,
  isResponseValidated,
  isResponseSuccess,
  capitalizeFirstLetter,
  getCurrencyType,
  getContestStatus,
  getStatusColor,
} from './utils_functions';
