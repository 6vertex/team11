import { Component } from 'react';
import { AsyncStorage, NetInfo } from 'react-native';

class Utils {
  async getItemWithKey(key, action) {
    try {
      const data = await AsyncStorage.getItem(key);
      const parsedData = JSON.parse(data);
      action(parsedData);
    } catch (error) {
      action(null);
    }
  }

  async setItemWithKeyAndValue(key, value) {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(value));
      return true;
    } catch (error) {
      return false;
    }
  }

  async deleteItem(key) {
    try{
      await AsyncStorage.removeItem(key);
    }
    catch(error) {
    }
  }

  checkInternetConnectivity(action) {
    NetInfo.isConnected.fetch().then((isConnected) => {
      action(isConnected);
    });
  }

  getItemFromAsyc(key) {
    new Promise((resolve, reject) => {
      try {
        AsyncStorage.getItem(key).then((value) => {
         resolve(JSON.parse(value))
        }).catch((error) => {
          return reject(`${error}`);
        });
      } catch (error) {
        reject(`${error}`);
      }
    });
  }

  static formatDate(dateString) {
    const date = new Date(dateString);
    const monthNames = [
      'Jan', 'Feb', 'Mar',
      'Apr', 'May', 'Jun', 'Jul',
      'Aug', 'Sep', 'Oct',
      'Nov', 'Dec',
    ];
    const day = date.getDate() < 10 ? ['0', date.getDate()].join('') : date.getDate();
    const monthIndex = date.getMonth();
    const year = date.getFullYear();
    const dateArray = [day, monthNames[monthIndex], year];
    const finalDate = dateArray.join('-');
    return finalDate.toString();
  }

  static formatTime(dateString) {
    const date = new Date(dateString);
    return `${date.getHours()}:${date.getMinutes()}`;
  }
}

export default Utils;
