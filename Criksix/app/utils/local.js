export const contest_duration = {
  "daily": [
    {
      "label":"21-Jan-2018",
      "matches": [
        {
          "id": 1,
          "fid": "iplt20_2016_g30",
          "scheduled_for": "2018-01-21T00:00:00.000Z",
          "home_team_id": 2,
          "away_team_id": 1,
          "status": "notstarted",
          "name": "Team X vs Team Y",
          "match_format": "t20",
          "home_team_acronym": "rcb",
          "away_team_acronym": "kkr",
          "home_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg",
          "away_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg"
        },
        {
          "id": 2,
          "fid": "iplt20_2016_g30",
          "scheduled_for": "2018-01-21T00:00:00.000Z",
          "home_team_id": 2,
          "away_team_id": 1,
          "status": "notstarted",
          "name": "Team X vs Team Y",
          "match_format": "t20",
          "home_team_acronym": "rcb",
          "away_team_acronym": "kkr",
          "home_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg",
          "away_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg"
        }
      ]
    },
    {
      "label":"22-Jan-2018",
      "matches": [
        {
          "id": 1,
          "fid": "iplt20_2016_g30",
          "scheduled_for": "2018-01-22T00:00:00.000Z",
          "home_team_id": 2,
          "away_team_id": 1,
          "status": "notstarted",
          "name": "Team X vs Team Y",
          "match_format": "t20",
          "home_team_acronym": "rcb",
          "away_team_acronym": "kkr",
          "home_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg",
          "away_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg"
        },
        {
          "id": 2,
          "fid": "iplt20_2016_g30",
          "scheduled_for": "2018-01-22T00:00:00.000Z",
          "home_team_id": 2,
          "away_team_id": 1,
          "status": "notstarted",
          "name": "Team X vs Team Y",
          "match_format": "t20",
          "home_team_acronym": "rcb",
          "away_team_acronym": "kkr",
          "home_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg",
          "away_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg"
        }
      ]
    }
  ],
  "weekly": [
    {
      "label":"21_Jan_to_22_Jan",
      "matches": [
        {
          "id": 1,
          "fid": "iplt20_2016_g30",
          "scheduled_for": "2018-01-21T00:00:00.000Z",
          "home_team_id": 2,
          "away_team_id": 1,
          "status": "notstarted",
          "name": "Team X vs Team Y",
          "match_format": "t20",
          "home_team_acronym": "rcb",
          "away_team_acronym": "kkr",
          "home_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg",
          "away_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg"
        },
        {
          "id": 2,
          "fid": "iplt20_2016_g30",
          "scheduled_for": "2018-01-21T00:00:00.000Z",
          "home_team_id": 2,
          "away_team_id": 1,
          "status": "notstarted",
          "name": "Team X vs Team Y",
          "match_format": "t20",
          "home_team_acronym": "rcb",
          "away_team_acronym": "kkr",
          "home_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg",
          "away_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg"
        }
      ]
    },
    {
      "label":"23_jan_to_25_jan",
      "matches": [
        {
          "id": 1,
          "fid": "iplt20_2016_g30",
          "scheduled_for": "2018-01-22T00:00:00.000Z",
          "home_team_id": 2,
          "away_team_id": 1,
          "status": "notstarted",
          "name": "Team X vs Team Y",
          "match_format": "t20",
          "home_team_acronym": "rcb",
          "away_team_acronym": "kkr"
        },
        {
          "id": 2,
          "fid": "iplt20_2016_g30",
          "scheduled_for": "2018-01-22T00:00:00.000Z",
          "home_team_id": 2,
          "away_team_id": 1,
          "status": "notstarted",
          "name": "Team X vs Team Y",
          "match_format": "t20",
          "home_team_acronym": "rcb",
          "away_team_acronym": "kkr",
          "home_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg",
          "away_team_logo": "https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg"
        }
      ]
    }
  ]
};

export const contest_master_data = {
  "leagues": [
    {
      name: 'IPL',
      fid: '1234456',
    }
  ],
  "prize_structures": [
      {
          "id": 1,
          "title": "Top-1"
      },
      {
          "id": 2,
          "title": "Top-2"
      },
      {
          "id": 3,
          "title": "Top-3"
      },
      {
          "id": 4,
          "title": "Top-5"
      },
      {
          "id": 5,
          "title": "Top-10"
      },
      {
          "id": 6,
          "title": "Top-50%"
      },
      {
          "id": 7,
          "title": "Top-30%"
      },
      {
          "id": 8,
          "title": "Top-25%"
      }
  ],
  "contest_types": {
      "H2H": 0,
      "Multiuser_league": 1,
      "Uncapped": 2
  },
  "duration_types": {
      "DAILY": 0,
      "WEEKLY": 1
  },
  "accessibility": {
      "open": 0,
      "exclusive": 1
  }
};

export const contest_dummy_data = {
  "name": "Contest Lobby",
  "data": [
    {
      "id": 1,
      "invite_code": "At12q",
      "max_size": 150,
      "joined_users_count": 100,
      "winners": 10,
      "entry_fee": 10,
      "total_winnings": 1000
    },
    {
      "id": 2,
      "invite_code": "fB12q",
      "max_size": 50,
      "joined_users_count": 10,
      "winners": 5,
      "entry_fee": 5,
      "total_winnings": 100
    },
    {
      "id": 3,
      "invite_code": "Ao12q",
      "max_size": 200,
      "joined_users_count": 150,
      "winners": 50,
      "entry_fee": 5,
      "total_winnings": 10000
    },
    {
      "id": 4,
      "invite_code": "AB02q",
      "max_size": 10,
      "joined_users_count": 5,
      "winners": 5,
      "entry_fee": 5,
      "total_winnings": 100
    },
    {
      "id": 11,
      "invite_code": "Aty12q",
      "max_size": 10,
      "joined_users_count": 5,
      "winners": 2,
      "entry_fee": 0,
      "total_winnings": 100
    },
    {
      "id": 12,
      "invite_code": "dfg12q",
      "max_size": 100,
      "joined_users_count": 50,
      "winners": 20,
      "entry_fee": 0,
      "total_winnings": 3000
    },
    {
      "id": 13,
      "invite_code": "sfs54",
      "max_size": 1000,
      "joined_users_count": 50,
      "winners": 100,
      "entry_fee": 0,
      "total_winnings": 2000
    },
    {
      "id": 14,
      "invite_code": "sf354",
      "max_size": 500,
      "joined_users_count": 300,
      "winners": 50,
      "entry_fee": 0,
      "total_winnings": 5000
    }
  ]
};
export const contest_filter_dummy_data= [
    {
    "name": "League",
    "type": [
    {
    "id": 1,
    "acronym": "ipl",
    "name": "Indian Premier League",
    "created_at": "2018-01-08T10:32:07.801Z"
    },
    {
    "id": 2,
    "acronym": "icc",
    "name": "International",
    "created_at": "2018-01-08T10:32:07.801Z"
    }
    ]
    },
    {
    "name": "Game Type",
    "type": [
    {
    "id": 1,
    "acronym": "private",
    "name": "Private"
    },
    {
    "id": 2,
    "acronym": "public",
    "name": "Public"
    }
    ]
    },
    {
    "name": "Duration",
    "type": [
    {
    "id": 1,
    "acronym": "daily",
    "name": "Daily"
    },
    {
    "id": 2,
    "acronym": "weekly",
    "name": "Weekly"
    }
    ]
    },
    {
    "name": "Entry Fee",
    "type": [
    {
    "id": 1,
    "acronym": "paid",
    "name": "Paid"
    },
    {
    "id": 2,
    "acronym": "free",
    "name": "Free"
    }
    ]
    },
    {
    "name": "Draft",
    "type": [
    {}
    ]
    }
  ];

  export const my_contest_response = {
    "name": "Contest Lobby",
    "data": [
        // {
        //     "id": 30,
        //     "contest_user_lineups_id": 80,
        //     "name": "tl 1",
        //     "league_name": 'Indian Premier League 2018',
        //     "joined_users_count": 1,
        //     "contest_type": "uncapped",
        //     "start_date": "2018-03-21T00:00:00.000Z",
        //     "status": "notstarted",
        //     "invite_code": "f05af93306e1",
        //     "entry_fee_type": "CURRENCY",
        //     "entry_fee": 50,
        //     "multi_entry_count": 1,
        //     "prize_pool": 0,
        //     "duration": "DAILY",
        //     "currency_type": "INR",
        //     "max_teams": 2,
        //     "remaining_users_count": 1,
        //     "winners": "20",
        //     "is_joined": true,
        //     "is_created": true,
        //     "my_teams": [
        //       {
        //         "id": 1,
        //         "team_name": "VINAY1234",
        //       }
        //     ],
        // },
        {
            "id": 31,
            "contest_user_lineups_id": 81,
            "name": "tl 2",
            "league_name": 'Indian Premier League 2018',
            "joined_users_count": 1,
            "contest_type": "H2H",
            "start_date": "2018-03-21T00:00:00.000Z",
            "status": "live",
            "invite_code": "f05af93306e1",
            "entry_fee_type": "CURRENCY",
            "entry_fee": 50,
            "multi_entry_count": 1,
            "prize_pool": 0,
            "duration": "DAILY",
            "currency_type": "INR",
            "max_teams": 2,
            "remaining_users_count": 1,
            "winners": "20",
            "is_joined": true,
            "is_created": false,
            "my_teams": [],
        },
        {
          "id": 30,
          "contest_user_lineups_id": 80,
          "name": "tl 1",
          "league_name": 'Indian Premier League 2018',
          "joined_users_count": 1,
          "contest_type": "uncapped",
          "start_date": "2018-03-21T00:00:00.000Z",
          "status": "closed",
          "invite_code": "f05af93306e1",
          "entry_fee_type": "CURRENCY",
          "entry_fee": 50,
          "multi_entry_count": 1,
          "prize_pool": 0,
          "duration": "DAILY",
          "currency_type": "INR",
          "max_teams": 2,
          "remaining_users_count": 1,
          "winners": "20",
          "is_joined": true,
          "is_created": true,
          "my_teams": [
            {
              "id": 1,
              "team_name": "VINAY1234",
              "points": 97,
              "rank": 67,
            }
          ],
        },
        {
            "id": 31,
            "contest_user_lineups_id": 81,
            "name": "tl 2",
            "league_name": 'Indian Premier League 2018',
            "joined_users_count": 1,
            "contest_type": "H2H",
            "start_date": "2018-03-21T00:00:00.000Z",
            "status": "live",
            "invite_code": "f05af93306e1",
            "entry_fee_type": "CURRENCY",
            "entry_fee": 50,
            "multi_entry_count": 1,
            "prize_pool": 0,
            "duration": "DAILY",
            "currency_type": "INR",
            "max_teams": 2,
            "remaining_users_count": 1,
            "winners": "20",
            "is_joined": true,
            "is_created": false,
            "my_teams": [],
        },
        {
          "id": 30,
          "contest_user_lineups_id": 80,
          "name": "tl 1",
          "league_name": 'Indian Premier League 2018',
          "joined_users_count": 1,
          "contest_type": "uncapped",
          "start_date": "2018-03-21T00:00:00.000Z",
          "status": "closed",
          "invite_code": "f05af93306e1",
          "entry_fee_type": "CURRENCY",
          "entry_fee": 50,
          "multi_entry_count": 1,
          "prize_pool": 0,
          "duration": "DAILY",
          "currency_type": "INR",
          "max_teams": 2,
          "remaining_users_count": 1,
          "winners": "20",
          "is_joined": true,
          "is_created": true,
          "my_teams": [
            {
              "id": 1,
              "team_name": "VINAY1234",
              "points": 97,
              "rank": 67,
            }
          ],
        },
    ]
}

export const my_contests = [ { id: 31,
  contest_user_lineups_id: [ 101 ],
  name: 'tl 2',
  league_name: 'Indian Premier League 2018',
  joined_users_count: 1,
  contest_type: 'H2H',
  start_date: '2018-03-21T00:00:00.000Z',
  status: 'upcoming',
  invite_code: '58f0f42a7ae4',
  entry_fee_type: 'CURRENCY',
  entry_fee: 50,
  multi_entry_count: 1,
  prize_pool: 90,
  duration: 'DAILY',
  currency_type: 'INR',
  max_teams: 2,
  remaining_users_count: 1,
  winners: 'Top-1',
  is_joined: true,
  is_contest_created: false,
  my_teams: 
   [ { id: 101,
       team_name: 'Vinay-101',
       data: 
        [ { position: 'Wicket-Keeper',
            players: [ { id: 61, name: null } ] },
          { position: 'Batsman',
            players: 
             [ { id: 62, name: null },
               { id: 63, name: null },
               { id: 64, name: null } ] },
          { position: 'All-rounder', players: [ { id: 65, name: null } ] },
          { position: 'Bowler', players: [ { id: 66, name: null } ] } ] } ] },
// // { id: 40,
// //   contest_user_lineups_id: [ 97 ],
// //   name: 'myyssjsjsjs',
// //   league_name: 'Indian Premier League 2018',
// //   joined_users_count: 3,
// //   contest_type: 'MULTIUSER LEAGUE',
// //   start_date: '2018-03-21T00:00:00.000Z',
// //   status: 'live',
// //   invite_code: '905f0b8a405b',
// //   entry_fee_type: 'CURRENCY',
// //   entry_fee: 70,
// //   multi_entry_count: 1,
// //   prize_pool: 126,
// //   duration: 'DAILY',
// //   currency_type: 'INR',
// //   max_teams: 2,
// //   remaining_users_count: -1,
// //   winners: 'Top-1',
// //   is_joined: true,
// //   is_contest_created: false,
// //   my_teams: 
// //    [ { id: 97,
// //        team_name: 'Vinay\'s Team',
// //        data: 
// //         [ { position: 'Batsman', players: [ { id: 58, name: null } ] },
// //           { position: 'All-rounder',
// //             players: [ { id: 59, name: null }, { id: 60, name: null } ] } ] } ] },
// // { id: 30,
// //   contest_user_lineups_id: [ 100 ],
// //   name: 'tl 1',
// //   league_name: 'Indian Premier League 2018',
// //   joined_users_count: 3,
// //   contest_type: 'H2H',
// //   start_date: '2018-03-21T00:00:00.000Z',
// //   status: 'live',
// //   invite_code: 'f05af93306e1',
// //   entry_fee_type: 'CURRENCY',
// //   entry_fee: 50,
// //   multi_entry_count: 1,
// //   prize_pool: 90,
// //   duration: 'DAILY',
// //   currency_type: 'INR',
// //   max_teams: 2,
// //   remaining_users_count: -1,
// //   winners: 'Top-1',
// //   is_joined: true,
// //   is_contest_created: false,
// //   my_teams: [] },
// // { id: 21,
// //   contest_user_lineups_id: [ 98 ],
// //   name: 'Test Contest#1',
// //   league_name: 'Indian Premier League 2018',
// //   joined_users_count: 8,
// //   contest_type: 'MULTIUSER LEAGUE',
// //   start_date: '2018-02-09T00:00:00.000Z',
// //   status: 'closed',
// //   invite_code: '7d6f0eb327fa',
// //   entry_fee_type: 'CRIKSIX PTS',
// //   entry_fee: 100,
// //   multi_entry_count: 5,
// //   prize_pool: 900,
// //   duration: 'DAILY',
// //   currency_type: null,
// //   max_teams: 10,
// //   remaining_users_count: 2,
// //   winners: 'Top-3',
// //   is_joined: true,
// //   is_contest_created: false,
// //   my_teams: [] },
// // { id: 25,
//   contest_user_lineups_id: [ 99 ],
//   name: 'new',
//   league_name: 'Indian Premier League 2018',
//   joined_users_count: 4,
//   contest_type: 'H2H',
//   start_date: '2018-02-09T00:00:00.000Z',
//   status: 'closed',
//   invite_code: '5890730f7520',
//   entry_fee_type: 'CURRENCY',
//   entry_fee: 0.001,
//   multi_entry_count: 1,
//   prize_pool: 0,
//   duration: 'DAILY',
//   currency_type: 'ETH',
//   max_teams: 2,
//   remaining_users_count: -2,
//   winners: 'Top-1',
//   is_joined: true,
//   is_contest_created: false,
//   my_teams: [] },
]

// contest detail object
export const my_contest_detail_object = { id: 28,
  name: 'tesst contest',
  joined_users_count: 1,
  contest_type: 'H2H',
  start_date: '2018-02-09T00:00:00.000Z',
  status: 'closed',
  invite_code: 'f19a1792b341',
  accessibility: 'PUBLIC',
  entry_fee_type: 'CURRENCY',
  entry_fee: 50,
  multi_entry_count: 1,
  prize_pool: 0,
  duration: 'DAILY',
  max_teams: 2,
  currency_type: 'INR',
  winners: 'Top-1',
  remaining_users_count: 1,
  league_name: 'Indian Premier League 2018',
  is_joined: false,
  season_name: 'Ireland vs Sri Lanka 2016',
  matches: 
  [ { id: 2,
      fid: 'dev_season_2014_q2',
      scheduled_for: '2018-02-11T00:00:00.000Z',
      name: 'Team X vs Team Y',
      status: 'completed',
      home_team_acronym: 'Team X',
      away_team_acronym: 'Team Y' },
    { id: 1,
      fid: 'iplt20_2016_g30',
      scheduled_for: '2018-03-16T00:00:00.000Z',
      name: 'Team X vs Team Y',
      status: 'completed',
      home_team_acronym: 'Team X',
      away_team_acronym: 'Team Y' } ],
  prize_distribution: 
  [ { id: 1,
      title: 'Top-1',
      position: 1,
      winning_type: 'percent',
      winning_amount: 0,
      winning_percentage: 100 } ],
  my_teams: [{
    id: 1,
    team_name: 'Vinay_1234'
  }],
}

export const common_player_lineup = {
  "data": [
     {
      "bracket": "bracket_3",
      "players": [
        {
          "id": 25,
          "acronym": "player_x6",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        },
        {
          "id": 26,
          "acronym": "player_x7",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        },
        {
          "id": 32,
          "acronym": "player_x13",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 33,
          "acronym": "player_x14",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 34,
          "acronym": "player_x15",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 7,
          "acronym": "player_y7",
          "name": null,
          "team_name": "Team Y",
          "position": "Wicket-Keeper",
          "bracket": "bracket_3"
        },
        {
          "id": 20,
          "acronym": "player_x1",
          "name": null,
          "team_name": "Team X",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 31,
          "acronym": "player_x12",
          "name": null,
          "team_name": "Team X",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 13,
          "acronym": "player_y13",
          "name": null,
          "team_name": "Team Y",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 14,
          "acronym": "player_y14",
          "name": null,
          "team_name": "Team Y",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 35,
          "acronym": "player_x16",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 23,
          "acronym": "player_x4",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        },
        {
          "id": 24,
          "acronym": "player_x5",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        }
      ]
    },
    {
      "bracket": "bracket_2",
      "players": [
        {
          "id": 50,
          "acronym": "player_x50",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        },
        {
          "id": 51,
          "acronym": "player_x51",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        },
        {
          "id": 52,
          "acronym": "player_x52",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 53,
          "acronym": "player_x53",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 54,
          "acronym": "player_x54",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 55,
          "acronym": "player_y55",
          "name": null,
          "team_name": "Team Y",
          "position": "Wicket-Keeper",
          "bracket": "bracket_3"
        },
        {
          "id": 56,
          "acronym": "player_x56",
          "name": null,
          "team_name": "Team X",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 57,
          "acronym": "player_x57",
          "name": null,
          "team_name": "Team X",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 58,
          "acronym": "player_y58",
          "name": null,
          "team_name": "Team Y",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 59,
          "acronym": "player_y59",
          "name": null,
          "team_name": "Team Y",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 60,
          "acronym": "player_x60",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 61,
          "acronym": "player_x61",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        },
        {
          "id": 62,
          "acronym": "player_x62",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        }
      ]
    },
    {
      "bracket": "bracket_1",
      "players": [
        {
          "id": 70,
          "acronym": "player_x70",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        },
        {
          "id": 71,
          "acronym": "player_x71",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        },
        {
          "id": 72,
          "acronym": "player_x72",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 73,
          "acronym": "player_x73",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 74,
          "acronym": "player_x74",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 75,
          "acronym": "player_y75",
          "name": null,
          "team_name": "Team Y",
          "position": "Wicket-Keeper",
          "bracket": "bracket_3"
        },
        {
          "id": 76,
          "acronym": "player_x76",
          "name": null,
          "team_name": "Team X",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 77,
          "acronym": "player_x77",
          "name": null,
          "team_name": "Team X",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 78,
          "acronym": "player_y78",
          "name": null,
          "team_name": "Team Y",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 79,
          "acronym": "player_y79",
          "name": null,
          "team_name": "Team Y",
          "position": "All-rounder",
          "bracket": "bracket_3"
        },
        {
          "id": 80,
          "acronym": "player_x80",
          "name": null,
          "team_name": "Team X",
          "position": "Batsman",
          "bracket": "bracket_3"
        },
        {
          "id": 81,
          "acronym": "player_x81",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        },
        {
          "id": 82,
          "acronym": "player_x62",
          "name": null,
          "team_name": "Team X",
          "position": "Bowler",
          "bracket": "bracket_3"
        }
      ]
    },
  ]
 }

 export const lineup_rules = {
  "rules":[
    {
      "acronym":"wk",
      "minCount":1,"maxCount":1,"text":"PICK 1 WICKET KEEPER"
    },
    {
      "acronym":"bat",
      "minCount":1,
      "maxCount":3,
      "text":"PICK 1-3 BATSMEN"
    },
    {
      "acronym":"ar",
      "minCount":1,
      "maxCount":2,
      "text":"PICK 1-2 ALL-ROUNDERS"
      },
      {
        "acronym":"bowl",
        "minCount":1,
        "maxCount":3,
        "text":"PICK 1-3 BOWLERS"
        }
    ]
  }


  // contest lobby object
//   id: 27,
// name: 'tesst contest',
// joined_users_count: 1,
// contest_type: 'H2H',
// start_date: '2018-02-09T00:00:00.000Z',
// status: 'closed',
// invite_code: '08c74b88f2fa',
// entry_fee_type: 'CURRENCY',
// entry_fee: 50,
// multi_entry_count: 1,
// prize_pool: 0,
// duration: 'DAILY',
// currency_type: 'INR',
// max_teams: 2,
// remaining_users_count: 1,
// winners: 'Top-1',
// is_joined: false

  // joined response 
  // { response: { contest_user_lineup: 43, contest_name: 'new', contest_id: 24 },
  //                                                           status: 200 }
