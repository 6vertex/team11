export const focusOnNext = (self, key) => {
  self[key].focus();
};
