
const localData = {
	filterData:[
		{
			name: 'League',
			type: [
				{
					id: 1,
					name: 'Indian Premier League 2018'
				}
			]
		},
		{
			name: 'Prize Structure',
			type: [
				{
					"id": 1,
					"name": "Top-1"
				},
				{
					"id": 2,
					"name": "Top-2"
				},
				{
					"id": 3,
					"name": "Top-3"
				},
				{
					"id": 4,
					"name": "Top-5"
				},
				{
					"id": 5,
					"name": "Top-10"
				},
				{
					"id": 6,
					"name": "Top-50%"
				},
				{
					"id": 7,
					"name": "Top-30%"
				},
				{
					"id": 8,
					"name": "Top-25%"
				}
			]
		},
		{
			name: 'Contest Types',
			type: [
				{
					"id": 0,
					"name": "H2H"
				},
				{
					"id": 1,
					"name": "MULTIUSER LEAGUE"
				},
				{
					"id": 2,
					"name": "UNCAPPED"
				}
			]
		},
		{
			name: 'Duration',
			type: [
				{
					"id": 0,
					"name": "DAILY"
				},
				{
					"id": 1,
					"name": "WEEKLY"
				}
			]
		},
		{
			name: 'Game Fees',
			type: [
				{
					"id": 0,
					"name": "CRIKSIX PTS"
				},
				{
					"id": 1,
					"name": "CURRENCY"
				}
			]
		}
	],
  create_contest_data: {
    "leagues": [
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "ipl_t20_2018",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "iresl_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "triseries_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "engsl_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "icc_wc_t20_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "rsaaus_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "asiacup_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "indsl_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "pslt20_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "nzaus_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "nzpak_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "banzim_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "ausind_2016",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "rsaeng_2015",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "afgzim_2015",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "bblt20_2015",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "auswi_2015",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "nzsl_2015",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "banzim_2015",
          "max_players_per_team": 6
      },
      {
          "id": 1,
          "acronym": "ipl",
          "name": "IPL",
          "fid": "ausnz_2015",
          "max_players_per_team": 6
      }
  ],
  "contest_types": [
    {
        "id": 1,
        "acronym": "h2h",
        "name": "H2H"
    },
    {
        "id": 2,
        "acronym": "multiuser",
        "name": "Multiuser"
    },
    {
        "id": 3,
        "acronym": "uncapped",
        "name": "Uncapped"
    }
  ],
  "currency_types": [
    {
        "id": 1,
        "acronym": "inr",
        "name": "INR"
    },
    {
        "id": 2,
        "acronym": "usd",
        "name": "USD"
    },
    {
        "id": 3,
        "acronym": "btc",
        "name": "BTC"
    },
    {
        "id": 4,
        "acronym": "eth",
        "name": "ETH"
    }
  ]
  },

  PlayersList: {
    "players": {
      "ar": {
        "role": "All-rounder",
        "players":[
          {
            "id": 1,
            "acronym": "r_ashwin",
            "name": "Ravichandran Ashwin",
            "image_url": "https://s.ndtvimg.com/images/entities/300/ravichandran-ashwin-1109.png",
            "credit": 9,
            "points": 90
          },
          {
            "id": 2,
            "acronym": "r_jadeja",
            "name": "Ravindra Jadeja",
            "image_url": "https://s.ndtvimg.com/images/entities/300/ravindra-jadeja-855.png",
            "credit": 7,
            "points": 100
          }
        ]
      },
      "bowl": {
        "role": "Bowler",
        "players":[
          {
            "id": 3,
            "acronym": "a_russel",
            "name": "A Russel",
            "image_url": "https://s.ndtvimg.com/images/entities/300/ravichandran-ashwin-1109.png",
            "credit": 9,
            "points": 90
          },
          {
            "id": 4,
            "acronym": "b_kumar",
            "name": "B Kumar",
            "image_url": "https://s.ndtvimg.com/images/entities/300/ravindra-jadeja-855.png",
            "credit": 7,
            "points": 100
          }
        ]
      },
      "wk": {
        "role": "Wicket-Keeper",
        "players":[
          {
            "id": 5,
            "acronym": "p_patel",
            "name": "P Patel",
            "image_url": "https://s.ndtvimg.com/images/entities/300/ravichandran-ashwin-1109.png",
            "credit": 9,
            "points": 90
          },
          {
            "id": 6,
            "acronym": "ms_dhoni",
            "name": "MS Dhoni",
            "image_url": "https://s.ndtvimg.com/images/entities/300/ravindra-jadeja-855.png",
            "credit": 7,
            "points": 100
          }
        ]
      },
      "bat": {
        "role": "Batsman",
        "players":[
          {
            "id": 7,
            "acronym": "r_sharma",
            "name": "R Sharma",
            "image_url": "https://s.ndtvimg.com/images/entities/300/ravichandran-ashwin-1109.png",
            "credit": 9,
            "points": 90
          },
          {
            "id": 8,
            "acronym": "a_rahane",
            "name": "A Rahane",
            "image_url": "https://s.ndtvimg.com/images/entities/300/ravindra-jadeja-855.png",
            "credit": 7,
            "points": 100
          },
          {
            "id": 9,
            "acronym": "g_gambhir",
            "name": "Gautam Gambhir",
            "image_url": "https://s.ndtvimg.com/images/entities/300/gautam-gambhir-563.png",
            "credit": 6,
            "points": 80
          }
        ]
      }
    }
  }


  
};

export default localData;
