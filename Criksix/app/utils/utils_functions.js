import { NavigationActions } from 'react-navigation';
import { Colors, dimensions } from '../theme';

export const getFilterMatchesApiBody = (filterArea = 'live', pageNo = 1, filterOptions = []) => {
  let filterApiBody = {
    page: `${pageNo}`,
    filter: filterArea,
  };
  if (filterOptions.length === 0) {
    return filterApiBody;
  }
  const leagueFilters = filterOptions[0].type.reduce((result, item) => {
    if (item.active) {
      result.push(item.acronym);
    }
    return result;
  }, []);
  const teamFilters = filterOptions[1].type.reduce((result, item) => {
    if (item.active) {
      result.push(item.acronym);
    }
    return result;
  }, []);
  const durationFilters = filterOptions[2].type.reduce((result, item) => {
    if (item.active) {
      result.push(item.acronym);
    }
    return result;
  }, []);

  if (leagueFilters.length > 0) {
    filterApiBody.league = leagueFilters.join(',');
  }
  if (teamFilters.length > 0) {
    filterApiBody.team = teamFilters.join(',');
  }
  if (durationFilters.length > 0) {
    filterApiBody.duration = durationFilters.join(',');
  }
  return filterApiBody;
};

/**
* @param {any} navigation
* @param {string} screenName
*/
export const resetRoute = (navigation, screenName) => {
  const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: screenName }),
    ],
  });
  navigation.dispatch(resetAction);
};

/**
* @param {object} apiResponse
* @returns {boolean}
*/
export const isResponseValid = (apiResponse) => {
  if (apiResponse.status && apiResponse.status >= 200 && apiResponse.status <= 300) {
    return true;
  }
  return false;
};

/**
* @param {object} apiResponse
* @returns {boolean}
*/
export const isResponseValidated = (apiResponse) => {
  if (apiResponse.status !== undefined && apiResponse.response !== undefined) {
    return true;
  }
  return false;
};

/**
* @param {object} apiResponse
* @returns {boolean}
*/
export const isResponseSuccess = (apiResponse) => {
  if (apiResponse.status && apiResponse.status >= 200 && apiResponse.status <= 300) {
    return true;
  }
  return false;
};

export const capitalizeFirstLetter = string =>
  string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();

export const getCurrencyType = (entryType, currencyType) => {
  if (entryType === 'CURRENCY') {
    if (currencyType === 'INR') {
      return '₹ ';
    } else if (currencyType === 'USD') {
      return '$ ';
    }
    return `${currencyType} `;
  }
  return 'PTS ';
};

export const getContestStatus = (status, myTeams = []) => {
  if (status.toLowerCase() === 'upcoming' && myTeams.length === 0) {
    return {
      statusId: 0,
      label: 'Create Team',
      width: dimensions.getViewportWidth() * 0.55,
    };
  } else if (status.toLowerCase() === 'upcoming' && myTeams.length > 0) {
    return  {
      statusId: 1,
      label: 'Invite',
      width: dimensions.getViewportWidth() * 0.65,
    };
  }
  return {
    statusId: 2,
    label: 'Joined',
    width: dimensions.getViewportWidth() * 0.65,
  };
};

export const getStatusColor = (status) => {
  if (status.toLowerCase() === 'upcoming') {
    return Colors.orangeShadeColor;
  } else if (status.toLowerCase() === 'completed'
    || status.toLowerCase() === 'closed') {
    return Colors.greenShadeColor;
  }
  return Colors.navigationBackgroundColor;
};
