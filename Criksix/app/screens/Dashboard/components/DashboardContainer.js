import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  ListView,
} from 'react-native';
import { images } from '../../../assets/images';
import ContestRow from '../../../components/ContestRow';
import { dimensions, Colors } from '../../../theme';
import { constant } from '../../../utils';

const width = dimensions.getViewportWidth();

const styles = StyleSheet.create({
  containerImage: {
    flex: 1,
    width,
    resizeMode: 'cover',
  },
  lobbyHeader: {
    width: width - 10,
    alignSelf: 'stretch',
    flexDirection: 'column',
    backgroundColor: Colors.selectedFilterColor,
    borderRadius: 5,
    margin: 5,
  },
  selectMatchText: {
    color: 'orange',
    textAlign: 'center',
    padding: 5,
    fontSize: 14,
    backgroundColor: Colors.selectedFilterColor,
  },
  joinButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 45,
    paddingHorizontal: 10,
    margin: 5,
    backgroundColor: Colors.primarybackgroundColor,
    borderRadius: 5,
  },
  detailText: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
  },
  separator: {
    height: 1,
    alignSelf: 'stretch',
    // backgroundColor: 'rgb(141, 54, 66)',
    backgroundColor: 'transparent',
  },
});

const dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const DashboardContainer = props => (
  <Image style={styles.containerImage} source={images.generalBackground}>
    <View style={styles.lobbyHeader}>
      <Text style={[styles.selectMatchText,
        {
          textAlign: 'left',
          color: 'white',
          fontWeight: '500',
        }]}>
        PLAY WITH FRIENDS
      </Text>
      <TouchableOpacity
        style={styles.joinButtonView}
        activeOpacity={0.7}
        onPress={() => props.navigation.navigate(constant.screens.JOIN_CONTEST)}
      >
        <Text style={styles.detailText}>Join By Invite Code</Text>
        <Image
          style={{
            width: 20,
            height: 20,
          }}
          source={images.next}
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.joinButtonView}
        activeOpacity={0.7}
        onPress={() => props.navigation.navigate(constant.screens.CREATE_CONTEST_FIRST_PAGE)}
      >
        <Text style={styles.detailText}>Make Your Own Contest</Text>
        <Image
          style={{
            width: 20,
            height: 20,
          }}
          source={images.next}
        />
      </TouchableOpacity>
    </View>
    {props.contestList.length !== 0 &&
      <Text
        style={[
        styles.selectMatchText,
        {
          color: 'white',
          textAlign: 'left',
          marginHorizontal: 5,
          marginTop: 10,
        }]}
      >
        SELECT A CONTEST TO JOIN
      </Text>}
    {(props.contestList.length === 0 && props.isContestLoading === false) &&
      <Text
        style={{
          alignSelf: 'stretch',
          paddingVertical: 100,
          textAlign: 'center',
          marginHorizontal: 5,
          fontSize: 16,
          backgroundColor: Colors.selectedFilterColor,
          color: 'orange',
        }}
      >Oops! No contest available
      </Text>}
    <ListView
      style={{
        backgroundColor: Colors.selectedFilterColor,
        marginHorizontal: 5,
      }}
      dataSource={dataSource.cloneWithRows(props.contestList)}
      enableEmptySections
      renderRow={(rowData, sectionID, rowID) => (
        <ContestRow
          key={rowID}
          contest={rowData}
          joinContest={props.joinContest}
          showContestDetails={props.showContestDetails}
        />
      )}
    />
  </Image>
);

DashboardContainer.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  joinContest: PropTypes.func,
  showContestDetails: PropTypes.func,
  contestList: PropTypes.arrayOf(PropTypes.any),
  isContestLoading: PropTypes.bool,
  // loadMoreList: PropTypes.func,
};

DashboardContainer.defaultProps = {
  navigation: {},
  joinContest: () => {},
  showContestDetails: () => {},
  contestList: [],
  isContestLoading: false,
  // loadMoreList: () => {},
};

export default DashboardContainer;
