import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Checkbox } from '../../../components';
import Utils from '../../../utils/utils';
import { images } from '../../../assets/images/index';

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    marginHorizontal: 20,
    padding: 10,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
    alignItems: 'center',
  },
  matchViewStyle: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-around',
  },
  teamLogostyle: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  teamLogoImageStyle: {
    width: 40,
    height: 40,
    borderRadius: 25,
  },
  teamLogoTextStyle: {
    color: 'white',
    marginLeft: 10,
  },
  matchNameViewStyle: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

const MatchRow = props => (
  <TouchableOpacity
    activeOpacity={props.isShowSelection ? 0 : 1}
    style={[styles.container, {
      backgroundColor: 'transparent',
      borderColor: props.isSelected ? 'green' : 'white',
    }]}
    onPress={() => props.onCheck()}
  >
    {props.isShowSelection &&
      <Checkbox
        checked={props.isSelected}
      />}
    <View style={styles.matchViewStyle}>
      <View style={styles.teamLogostyle}>
        <Image style={styles.teamLogoImageStyle} source={{ uri: props.homeTeamLogo }} />
        <Text style={styles.teamLogoTextStyle}>{props.homeTeamAcronym.toUpperCase()}</Text>
      </View>

      <View style={styles.matchNameViewStyle}>
        <Text style={{ color: 'orange', fontSize: 12 }}>{Utils.formatTime(props.schedule)}</Text>
        <Text style={{ color: 'white' }}>{Utils.formatDate(props.schedule)}</Text>
        <Text style={{ color: 'white' }}>vs</Text>
      </View>

      <View style={styles.teamLogostyle}>
        <Image style={styles.teamLogoImageStyle} source={{ uri: props.awayTeamLogo }} />
        <Text style={styles.teamLogoTextStyle}>{props.awayTeamAcronym.toUpperCase()}</Text>
      </View>
    </View>
  </TouchableOpacity>
);

MatchRow.propTypes = {
  homeTeamAcronym: PropTypes.string,
  awayTeamAcronym: PropTypes.string,
  schedule: PropTypes.string,
  homeTeamLogo: PropTypes.string,
  awayTeamLogo: PropTypes.string,
  isSelected: PropTypes.bool,
  isShowSelection: PropTypes.bool,
  onCheck: PropTypes.func,
};

MatchRow.defaultProps = {
  homeTeamAcronym: '',
  awayTeamAcronym: '',
  schedule: '',
  homeTeamLogo: 'https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg',
  awayTeamLogo: 'https://i.pinimg.com/474x/21/cd/46/21cd4653e7973c2121713babf9f50a83.jpg',
  isSelected: false,
  isShowSelection: true,
  onCheck: () => {},
};

export default MatchRow;
