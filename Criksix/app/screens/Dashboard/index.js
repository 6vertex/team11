import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import UserActions from '../../actions';
import DashboardContainer from './components/DashboardContainer';
import { images } from '../../assets/images';
import Utils from '../../utils/utils';
import { NavigationBar, StatusBar, Loader } from '../../components';
import ConfirmationPopUp from '../ContestDetails/components/ConfirmationPopUp';
import {
  constant,
  resetRoute,
  isResponseValidated,
  isResponseSuccess,
} from '../../utils';
import { containerStyles } from '../../theme';
import GeolocationModule from '../../utils/GeolocationModule';
import localData from '../../utils/localData';

// TODO: Remove this in future implementation
const tabsResponseText = ['fixtures', 'live', 'results'];
let currentLocationID = 0; // 0 for india, 1 for usa and 2 for others.
class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      showConfirmationPopUp: false,
      contest: {},
      filterData: JSON.parse(JSON.stringify(localData.filterData)),
    };
  }

  componentDidMount() {
    this.getLocationPermission();
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.getAllContests(accessToken, 1, {});
            this.props.getUserProfile(accessToken);
            this.props.getMyAccount(accessToken, 'username/email');
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }

    // TODO: Handle contest detail response.
    if (isResponseValidated(nextProps.contestDetails)) {
      if (isResponseSuccess(nextProps.contestDetails)) {
        // Navigate contest detail page and reset in reducer

        this.props.navigation.navigate(
          constant.screens.CONTEST_DETAIL,
          { contest: nextProps.contestDetails.response },
        );
        this.props.resetContestDetailInfo();
        return;
      }
      if (nextProps.contestDetails.response.message &&
        typeof nextProps.contestDetails.response.message === 'string') {
        Alert.alert('Message', nextProps.contestDetails.response.message);
        this.props.resetContestDetailInfo();
        return;
      }
    }

    // TODO: Handle join contest response.
    if (isResponseValidated(nextProps.joinContestResponse)) {
      if (isResponseSuccess(nextProps.joinContestResponse)) {
        const resetAction = NavigationActions.reset({
          index: 1,
          actions: [
            NavigationActions.navigate({ routeName: constant.screens.DRAWER_STACK }),
            NavigationActions.navigate({
              routeName: constant.screens.CREATE_TEAM,
              params: {
                contest_user_lineup: nextProps.joinContestResponse.response.contest_user_lineup,
                contest_id: nextProps.joinContestResponse.response.contest_id,
                contest_name: nextProps.joinContestResponse.response.contest_name,
                invite_code: this.state.contest.invite_code,
              },
            }),
          ],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.resetContestLobby();
        return;
      }
      if (nextProps.joinContestResponse.response.message &&
        typeof nextProps.joinContestResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.joinContestResponse.response.message);
      }
    }

    // Handle contest list error response
    if (nextProps.allContests.response &&
        nextProps.allContests.status >= 400) {
      if (nextProps.allContests.response.message &&
          typeof nextProps.allContests.response.message === 'string') {
        Alert.alert('Message', nextProps.allContests.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }

    // Hnadle profile response
    if (nextProps.userProfileResponse.response && nextProps.userProfileResponse.status >= 400) {
      if (nextProps.userProfileResponse.response.message && typeof nextProps.userProfileResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.userProfileResponse.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  setFilterData(data) {
    this.setState({ filterData: data });
  }

  getLocationPermission() {
    GeolocationModule.findCurrentLocation((success, error) => {
      if (success !== null) {
        currentLocationID = success.location_code;
        console.log(currentLocationID);
      }
      if (error !== null) this.getLocationPermission();
    });
  }

  joinContestPressed(contestInfo) {
    this.setState({
      contest: contestInfo,
      showConfirmationPopUp: true,
    });
  }

  applyFilters(filterAPIData) {
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.getAllContests(accessToken, 1, filterAPIData);
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  confirmJoinContest() {
    this.setState({ showConfirmationPopUp: false });
    if (Object.keys(this.state.contest).length > 0
      && typeof this.state.contest.id === 'number'
      && this.state.contest.invite_code !== undefined) {
      this.utils.checkInternetConnectivity((reach) => {
        if (reach) {
          this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
            if (response && response.access_token) {
              this.props.joinContest(
                response.access_token,
                this.state.contest.invite_code,
              );
            } else {
              Alert.alert(constant.loadingError.header, constant.loadingError.message);
            }
          });
        } else {
          Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
        }
      });
    } else {
      Alert.alert(constant.JOINING_CONTEST_ERROR.header, constant.JOINING_CONTEST_ERROR.message);
    }
  }

  showContestDetails(contestInfo) {
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            this.props.getContestDetails(
              response.access_token,
              contestInfo.id,
            );
          } else {
            Alert.alert(constant.loadingError.header, constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  render() {
    const {
      navigation,
      allContestsLoading,
      joinContestLoading,
      contestDetailLoading,
      contests,
    } = this.props;
    const { showConfirmationPopUp, filterData } = this.state;
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar />
        <NavigationBar
          title={'CONTEST LOBBY'}
          showBackButton
          backButtonImage={images.menuThree}
          backButtonAction={() => navigation.navigate('DrawerOpen')}
          showRightButton
          rightButtonImage={images.filter}
          rightButtonAction={() =>
            navigation.navigate(
              constant.screens.FILTER,
              {
                filters: filterData,
                setFilterData: data => this.setFilterData(data),
                applyFilters: data => this.applyFilters(data),
              },
            )}
        />

        <DashboardContainer
          isContestLoading={allContestsLoading}
          navigation={navigation}
          contestList={contests}
          joinContest={contestInfo => this.joinContestPressed(contestInfo)}
          showContestDetails={contestInfo => this.showContestDetails(contestInfo)}
        />
        {showConfirmationPopUp &&
          <ConfirmationPopUp
            contest={this.state.contest}
            confirmJoinContest={() => this.confirmJoinContest()}
            close={() => this.setState({ showConfirmationPopUp: false })}
          />
        }
        {joinContestLoading && <Loader isAnimating={joinContestLoading} />}
        {allContestsLoading && <Loader isAnimating={allContestsLoading} />}
        {contestDetailLoading && <Loader isAnimating={contestDetailLoading} />}
      </View>
    );
  }
}

Dashboard.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  allContests: PropTypes.objectOf(PropTypes.any),
  contestDetails: PropTypes.objectOf(PropTypes.any),
  joinContestResponse: PropTypes.objectOf(PropTypes.any),
  userProfileResponse: PropTypes.objectOf(PropTypes.any),
  getAllContests: PropTypes.func,
  getUserProfile: PropTypes.func,
  resetlogout: PropTypes.func,
  getMyAccount: PropTypes.func,
  getContestDetails: PropTypes.func,
  joinContest: PropTypes.func,
  resetContestDetailInfo: PropTypes.func,
  resetContestLobby: PropTypes.func,
  contests: PropTypes.arrayOf(PropTypes.any),
  isSessionExpired: PropTypes.bool,
  allContestsLoading: PropTypes.bool,
  joinContestLoading: PropTypes.bool,
  contestDetailLoading: PropTypes.bool,
};

Dashboard.defaultProps = {
  navigation: {},
  contestDetails: {},
  joinContestResponse: {},
  userProfileResponse: {},
  getAllContests: () => {},
  getUserProfile: () => {},
  resetlogout: () => {},
  getMyAccount: () => {},
  getContestDetails: () => {},
  joinContest: () => {},
  resetContestDetailInfo: () => {},
  resetContestLobby: () => {},
  isSessionExpired: false,
  allContestsLoading: false,
  joinContestLoading: false,
  contestDetailLoading: false,
  allContests: {},
  contests: {},
};

const mapStateToProps = state => ({
  isSessionExpired: state.sessionExpire.isSessionExpired,
  allContestsLoading: state.contestManagement.allContestsLoading,
  allContests: state.contestManagement.allContests,
  contestDetailLoading: state.contestManagement.contestDetailLoading,
  contestDetails: state.contestManagement.contestDetails,
  userProfileResponse: state.userProfile.userProfileResponse,
  contests: state.contestManagement.contests,
  joinContestLoading: state.contestManagement.joinContestLoading,
  joinContestResponse: state.contestManagement.joinContestResponse,
  // TODO:  Apply contest filter than match filter
  matchFilters: state.matchManagement.filters,
});

const mapDispatchToProps = () => UserActions;

const DashboardScreen = connect(mapStateToProps, mapDispatchToProps)(Dashboard);

export default DashboardScreen;
