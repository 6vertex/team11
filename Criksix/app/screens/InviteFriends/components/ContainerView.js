import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  ScrollView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../../components/CustomTextInput';
import { images } from '../../../assets/images';
import { isIOS } from '../../../utils/PlatformSpecific'

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  containerImage: {
    width,
    height,
  },
  container: {
    width,
    height,
    backgroundColor: 'transparent',
  },
  backgroundImage: {
    width,
    height: 150,
    resizeMode: 'cover',
  },
  scrollStyle: {
    width,
    marginBottom: isIOS ? 0 : 40
  },
  bodyStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleStyle: {
    color: 'white',
    marginTop: 20, 
    fontSize: 18, 
    fontWeight: '500',
  },
  subTitleStyle: {
    color: 'white',
    fontSize: 18, 
    fontWeight: '500',
  },
  howItWorksStyle: {
    borderColor: '#197319',
    borderWidth: 2,
    borderRadius: 20,
    padding: 10,
  },
  inviteCodeContainer: {
    borderBottomColor: 'white',
    borderWidth: 2,
    padding: 8,
    marginTop: 12,
  },
  inviteCodeStyle: {
    color: 'white',
    fontWeight: '500', 
    fontSize: 20,
    paddingLeft: 15,
    paddingRight: 15,
  },
  shareStyle: {
    backgroundColor: '#197319',
    paddingLeft: 80,
    paddingRight: 80,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 20,
    borderRadius: 2,
  }
});

const InviteFriendsContainer = props => (
    <Image style={styles.containerImage} source={images.generalBackground}>
      <ScrollView style={styles.scrollStyle}>
        <View style={styles.container}>
          <Image style={styles.backgroundImage} source={images.inviteFriendsBannerImage}/>
          <View style={styles.bodyStyle}>
            <Text style={styles.titleStyle}>Kick off your friend's</Text>
            <Text style={styles.subTitleStyle}>Criksix journey!</Text>
            <Text style={{color: 'white', paddingTop:20,}}>For every friend that plays, </Text>
            <Text style={{color: 'white', paddingBottom: 20,}}>You both get 50 crikisix points for free!</Text>
            <TouchableOpacity style={styles.howItWorksStyle} onPress = {() => props.gameWorkingButton()}>
              <Text style={{color: 'white', fontWeight: '500',}}>HOW IT WORKS</Text>
            </TouchableOpacity>

            <Text style={{color: 'white', paddingTop: 20,}}>SHARE YOUR INVITE CODE</Text>
            <View style={styles.inviteCodeContainer} >
            <Text style={styles.inviteCodeStyle}>{props.inviteCode}</Text>
            </View>

            <TouchableOpacity style={styles.shareStyle} onPress={() => props.shareCode()}>
              <Text style={{color: 'white', fontWeight: '500', fontSize: 15,}}>SHARE</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </Image>
);

InviteFriendsContainer.propTypes = {
};

InviteFriendsContainer.defaultProps = {
};

export default InviteFriendsContainer;
