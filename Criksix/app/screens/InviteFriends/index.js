import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StatusBar,
  Share,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import Loader from '../../components/Loader';
import NavigationBar from '../../components/NavigationBar';
import InviteFriendsContainer from './components/ContainerView';
import { images } from '../../assets/images';

class InviteFriends extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inviteCode: props.userProfileResponse.response.invite_code,
    };
  }

  shareInviteCode() {
    Share.share({
      message: 'Your invite code for criksix is ' + this.state.inviteCode,
    }).catch(err => console.log(err));
  }

  gameWorkingButton() {
    Alert.alert('Message', 'Work in progress');
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          backgroundColor="#0C0D18"
          barStyle="light-content"
        />
        <NavigationBar
          title="Invite Friends"
          showBackButton={Boolean(true)}
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <InviteFriendsContainer
          inviteCode={this.state.inviteCode}
          shareCode={() => this.shareInviteCode()}
          gameWorkingButton={() => this.gameWorkingButton()}
        />
        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

InviteFriends.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

InviteFriends.defaultProps = {
  navigation: {},
};

const mapStateToProps = state => ({
  isLoading: state.userProfile.isLoading,
  userProfileResponse: state.userProfile.userProfileResponse,
});

const mapDispatchToProps = () => UserActions;

const InviteFriendsScreen = connect(mapStateToProps, mapDispatchToProps)(InviteFriends);

export default InviteFriendsScreen;
