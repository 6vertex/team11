import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput,
  Alert,
  Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import ContestActions from '../../actions';
import { NavigationBar, StatusBar, Loader, DropDownPicker } from '../../components';
import { images } from '../../assets/images';
import { dimensions, Colors } from '../../theme';
import { constant } from '../../utils';
import Utils from '../../utils/utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: dimensions.getViewportWidth(),
    backgroundColor: 'rgb(10, 10, 15)',
  },
  bodyContainer: {
    paddingVertical: 20,
    flex: 1,
    width: dimensions.getViewportWidth(),
  },
  body: {
    backgroundColor: Colors.selectedFilterColor,
    marginVertical: 10,
    marginHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 5,
  },
  inputOuterContainer: {
    height: 80,
    alignSelf: 'stretch',
    paddingHorizontal: 10,
  },
  titleLabel: {
    color: 'white',
    fontSize: 12,
    textAlign: 'left',
    paddingBottom: 5,
    paddingLeft: 20,
  },
  inputContainer: {
    height: 40,
    alignSelf: 'stretch',
    backgroundColor: '#575b91',
    borderRadius: 40 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputStyles: {
    marginHorizontal: 8,
    paddingHorizontal: 15,
    backgroundColor: 'transparent',
    fontFamily: 'Arial',
    fontSize: 16,
    color: 'white',
    alignSelf: 'stretch',
    textAlign: 'left',
  },
  bottomView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 8,
    backgroundColor: '#e71636',
  },
  pageIndicatorContainer: {
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  pageIndicator: {
    color: 'white',
    fontSize: 16,
    fontWeight: '400',
    textAlign: 'center',
  },
  nextButtonView: {
    paddingHorizontal: 25,
    paddingVertical: 10,
    backgroundColor: '#00000070',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  nextButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '700',
    textAlign: 'center',
  },
  selectionContainer: {
    height: 45,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: 0,
  },
  selectionButton: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'white',
    alignSelf: 'stretch',
    marginHorizontal: 10,
    justifyContent: 'center',
  },
  selectionButtonTxt: {
    color: 'white',
    fontSize: 14,
    alignSelf: 'center',
  },
  picker: {
    height: 40,
    alignSelf: 'stretch',
    backgroundColor: '#575b91',
    borderRadius: 40 / 2,
  },
  pickerText: {
    color: 'white',
    marginTop: -5,
    marginLeft: 5,
  },
});

const defaultLeague = {
  name: '--Select League --',
  fid: '--Select League --',
  id: -1,
};

class SelectLeague extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      contestName: '',
      selectedLeagueFID: '',
      selectedLeague: defaultLeague,
      selectedLeagueID: '-1',
      selectedAccessID: '-1',
      selectedDurationID: '-1',
    };
  }

  componentDidMount() {
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.getCCO(accessToken);
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header,
          constant.noInternetConnection.message);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    const { midResponse, ccoResponse, isLoading } = nextProps;
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }
  }

  handleChange(key, value) {
    if (key !== 'contestName') {
      Keyboard.dismiss();
    }
    if (key === 'selectedLeagueFID') {
      this.setState({
        selectedLeagueFID: value.fid,
        selectedLeagueID: value.id,
        selectedLeague: value,
      });
      return;
    }
    this.setState({
      [key]: value,
    });
  }

  handleNext() {
    const {
      contestName,
      selectedLeagueFID,
      selectedAccessID,
      selectedDurationID,
      selectedLeagueID,
    } = this.state;
    Keyboard.dismiss();
    if (contestName === '') {
      Alert.alert('Message', 'Give your contest a cool name');
      return;
    } else if (selectedLeagueFID === defaultLeague.fid) {
      Alert.alert('Message', 'Select a valid league');
      return;
    } else if (selectedAccessID === '-1') {
      Alert.alert('Message', 'Choose contest PUBLIC or PRIVATE');
      return;
    } else if (selectedDurationID === '-1') {
      Alert.alert('Message', 'Select a duration for your contest');
      return;
    }
    const contest = {
      name: contestName,
      league_id: selectedLeagueID,
      feed_season_id: selectedLeagueFID,
      duration: selectedDurationID,
      accessibility: selectedAccessID,
      contest_type: -1,
      max_teams: 0,
      matches: [],
      start_week_id: -1,
      start_date: '',
      end_date: '',
      entry_fee: 0,
      prize_id: -1,
      multi_entry_count: 1,
      entry_fee_type: -1,
      currency_type: -1,
    };
    this.props.navigation.navigate(
      constant.screens.CREATE_CONTEST_SECOND_PAGE,
      { contest },
    );
  }

  renderLeagues(availableLeagues) {
    const leagues = [];
    leagues.push(<DropDownPicker.Item
      key={-1}
      label={defaultLeague.name}
      value={defaultLeague}
    />);

    for (let i = 0; i < availableLeagues.length; i += 1) {
      leagues.push(<DropDownPicker.Item
        key={i}
        label={availableLeagues[i].name}
        value={availableLeagues[i]}
      />);
    }
    return leagues;
  }

  render() {
    const {
      contestName,
      selectedAccessID,
      selectedDurationID,
      selectedLeague,
    } = this.state;
    const {
      navigation,
      leagues,
      durationsTypes,
      accessibility,
    } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar />
        <NavigationBar
          title={'CREATE CONTEST'}
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => navigation.goBack()}
        />
        <View style={styles.bodyContainer}>
          <View style={styles.body}>
            { /* Contest name container */ }
            <View style={styles.inputOuterContainer}>
              <Text style={styles.titleLabel}>
                Give Your Contest a Cool Name
              </Text>
              <View style={styles.inputContainer}>
                <TextInput
                  value={contestName}
                  style={styles.inputStyles}
                  placeholder="Give your contest a name"
                  placeholderTextColor="#FFFFFF50"
                  returnKeyType="next"
                  onChangeText={txt => this.handleChange('contestName', txt)}
                  underlineColorAndroid="transparent"
                />
              </View>
            </View>
            { /* Drop down to show available leagues */ }
            <View style={styles.inputOuterContainer}>
              <Text style={styles.titleLabel}>
                {"What's Your Favourite League"}
              </Text>
              <DropDownPicker
                selectedValue={selectedLeague}
                onValueChange={league =>
                  this.handleChange('selectedLeagueFID', league)}
                prompt={'Choose your favorite league'}
                style={styles.picker}
                textStyle={styles.pickerText}
                cancel
              >
                {this.renderLeagues(leagues)}
              </DropDownPicker>
            </View>
            { /* Select contest private or public */ }
            <View style={styles.inputOuterContainer}>
              <Text style={styles.titleLabel}> You Want Your Contest </Text>
              <View style={styles.selectionContainer}>
                {
                  Array.from(Object.keys(accessibility)).map((access, index) => (
                    <TouchableOpacity
                      key={`access-${index}`}
                      onPress={() => this.handleChange('selectedAccessID', accessibility[access])}
                      style={[styles.selectionButton, {
                        backgroundColor: selectedAccessID === accessibility[access] ?
                          'white' : 'transparent',
                      }]}
                    >
                      <Text
                        style={[styles.selectionButtonTxt, {
                          color: selectedAccessID === accessibility[access] ?
                            'rgb(10, 10, 15)' : 'white',
                        }]}
                      >{access.toUpperCase()}
                      </Text>
                    </TouchableOpacity>))
                }
              </View>
            </View>
            { /* Select contest Duration */ }
            <View style={styles.inputOuterContainer}>
              <Text style={styles.titleLabel}> Duration Of The Contest </Text>
              <View style={styles.selectionContainer}>
                {
                  Array.from(Object.keys(durationsTypes)).map((duration, index) => (
                    <TouchableOpacity
                      key={`duration-${index}`}
                      onPress={() => this.handleChange('selectedDurationID', durationsTypes[duration])}
                      style={[styles.selectionButton, {
                        backgroundColor: selectedDurationID === durationsTypes[duration] ?
                          'white' : 'transparent',
                      }]}
                    >
                      <Text
                        style={[styles.selectionButtonTxt, {
                          color: selectedDurationID === durationsTypes[duration] ?
                            'rgb(10, 10, 15)' : 'white',
                        }]}
                      >{duration}
                      </Text>
                    </TouchableOpacity>))
                }
              </View>


            </View>
          </View>
        </View>
        <View style={styles.bottomView}>
          <View style={styles.pageIndicatorContainer}>
            <Text style={[styles.pageIndicator, { fontSize: 18 }]}>1</Text>
            <Text style={styles.pageIndicator}> / 3</Text>
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => this.handleNext()}
          >
            <View style={styles.nextButtonView}>
              <Text style={styles.nextButtonText}>NEXT</Text>
            </View>
          </TouchableOpacity>
        </View>
        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

SelectLeague.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  leagues: PropTypes.arrayOf(PropTypes.any),
  durationsTypes: PropTypes.objectOf(PropTypes.any),
  accessibility: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
};

SelectLeague.defaultProps = {
  navigation: {},
  leagues: [],
  durationsTypes: {},
  accessibility: {},
  isLoading: false,
};

const mapStateToProps = state => ({
  isSessionExpired: state.sessionExpire.isSessionExpired,
  isLoading: state.contestManagement.isLoading,
  leagues: state.contestManagement.leagues,
  durationsTypes: state.contestManagement.durationTypes,
  accessibility: state.contestManagement.accessibility,
});

const mapDispatchToProps = () => ContestActions;

const SelectLeagueScreen = connect(mapStateToProps, mapDispatchToProps)(SelectLeague);

export default SelectLeagueScreen;
