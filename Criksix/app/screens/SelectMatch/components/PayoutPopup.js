import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';

const { width, height } = Dimensions.get('window');


const styles = StyleSheet.create({
  screenContainer: {
    width,
    height,
    backgroundColor: '#00000070',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainContainer: {
    width: width * 0.85,
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderRadius: 8,
    backgroundColor: 'white',
  },
  topView: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    paddingVertical: 10,
    backgroundColor: 'white',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  headerView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: 'black',
    fontSize: 15,
    textAlign: 'center',
    fontWeight: '800',
  },
  separartor: {
    height: 1,
    backgroundColor: 'grey',
    alignSelf: 'stretch',
    marginVertical: 10,
  },
  usableAmountText: {
    color: 'green',
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '700',
  },
  winnerListItemContainer: {
    justifyContent: 'center',
    width: width * 0.80,
    maxHeight: 300,
  },
  winnerListItem: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
    paddingHorizontal: 5,
  },
  winnerListItemTxt: {
    color: '#575b91',
    fontWeight: 'bold',
  },
  termsText: {
    color: 'grey',
    fontSize: 14,
    textAlign: 'left',
    padding: 5,
    alignSelf: 'flex-start',
  },
  closeView: {
    position: 'absolute',
    right: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeTouchable: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  BottomView: {
    width: width * 0.85,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'green',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  editPayoutText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '700',
    paddingVertical: 12,
  },
});

const PayoutPopUp = props => (
  <View style={styles.screenContainer}>
    <View style={styles.mainContainer}>
      <View style={styles.topView}>
        <View style={styles.headerView}>
          <Text style={styles.headerText}>PAYOUT STRUCTURE</Text>
          <View style={styles.separartor} />
          {props.isLoading ? <Text>Loading ...</Text> :
          <View>
            <Text style={styles.usableAmountText}>Total Winnings: {props.entryType}. {props.payout.prize_pool}</Text>
            <View style={styles.winnerListItemContainer}>
              <ScrollView>
                {props.payout.prize_structure && props.payout.prize_structure.map((prize, index) => (
                  <View key={`${prize.id}-${index}`} style={styles.winnerListItem}>
                    <Text style={styles.winnerListItemTxt}>Top #{prize.position}</Text>
                    <Text style={styles.winnerListItemTxt}>{prize.winning_percentage}%</Text>
                    <Text style={styles.winnerListItemTxt}>{props.entryType}. {prize.winning_amount}</Text>
                  </View>))}
              </ScrollView>
            </View>
          </View>}
          <Text style={styles.termsText}>
            * Payout structure is calculated as per CrikSix Rules.
          </Text>
        </View>
        <View style={styles.closeView}>
          <TouchableOpacity
            onPress={() => props.onClose()}
            style={styles.closeTouchable}
          ><Text>Close</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  </View>
);

PayoutPopUp.propTypes = {
  onClose: PropTypes.func,
  onEdit: PropTypes.func,
  isLoading: PropTypes.bool,
  payout: PropTypes.objectOf(PropTypes.any),
  entryType: PropTypes.string,
};

PayoutPopUp.defaultProps = {
  onClose: () => {},
  onEdit: () => {},
  isLoading: false,
  payout: PropTypes.objectOf(PropTypes.any),
  entryType: '',
};

export default PayoutPopUp;
