import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ContestActions from '../../actions';
import { NavigationBar, Loader, StatusBar, DropDownPicker } from '../../components';
import { images } from '../../assets/images';
import { dimensions, Colors } from '../../theme';
import { constant, resetRoute } from '../../utils';
import Utils from '../../utils/utils';
import MatchRow from '../Dashboard/components/MatchRow';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'rgb(10, 10, 15)',
  },
  body: {
    flex: 1,
    width: dimensions.getViewportWidth(),
    paddingBottom: 20,
  },
  bodyContainer: {
    paddingBottom: 20,
    margin: 10,
    backgroundColor: Colors.selectedFilterColor,
  },
  inputOuterContainer: {
    height: 80,
    alignSelf: 'stretch',
    paddingHorizontal: 10,
  },
  titleLabel: {
    color: 'white',
    fontSize: 12,
    textAlign: 'left',
    paddingBottom: 5,
    paddingLeft: 20,
  },
  inputContainer: {
    height: 40,
    alignSelf: 'stretch',
    backgroundColor: '#575b91',
    borderRadius: 40 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputStyles: {
    marginHorizontal: 8,
    paddingHorizontal: 15,
    backgroundColor: 'transparent',
    fontFamily: 'Arial',
    fontSize: 16,
    color: 'white',
    alignSelf: 'stretch',
    textAlign: 'left',
  },
  bottomView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 8,
    backgroundColor: '#e71636',
  },
  pageIndicatorContainer: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  pageIndicator: {
    color: 'white',
    fontSize: 16,
    fontWeight: '400',
    textAlign: 'center',
  },
  nextButtonView: {
    paddingHorizontal: 25,
    paddingVertical: 10,
    backgroundColor: '#00000070',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  nextButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '700',
    textAlign: 'center',
  },
  picker: {
    height: 40,
    alignSelf: 'stretch',
    backgroundColor: '#575b91',
    borderRadius: 40 / 2,
  },
  pickerText: {
    color: 'white',
    marginTop: -5,
    marginLeft: 5,
  },
});

class SelectMatch extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.durationType = props.navigation.state.params.contest.duration === 0 ? ' Date' : ' Week';
    this.defaultDuration = { label: `--Select ${this.durationType}--`};
    this.state = {
      selectedContestTypeID: -1,
      selectedContestType: '--Select Contest Type--',
      contestSize: '2',
      selectedDurationLabel: '',
      matchesInDuration: [],
      selectedMatches: [],
      selectedMatchDuration: this.defaultDuration,
    };
  }

  componentDidMount() {
    const { contest } = this.props.navigation.state.params;
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.getMID(accessToken, contest.feed_season_id, contest.duration);
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header,
          constant.noInternetConnection.message);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
    }
  }

  setCheckedForSelectedMatches(match) {
    return this.state.selectedMatches.findIndex(m => m.id === match.id) > -1;
  }

  handleChange(key, value) {
    if (key === 'matchesInDuration') {
      const availbleMatches = value.label === this.defaultDuration.label ? [] : value.matches;

      this.setState({
        selectedDurationLabel: value.label,
        selectedMatchDuration: value,
        [key]: availbleMatches,
        selectedMatches: JSON.parse(JSON.stringify(availbleMatches)),
      });
      return;
    }
    if (key === 'selectedContestTypeID') {
      if (value !== '--Select Contest Type--' && this.props.contestTypes[value] === 0) {
        Alert.alert('Message', 'Your contest size should be considered 2, As you have selected H2H(Head to Head) Contest type ');
      }
      this.setState({
        selectedContestTypeID: value === '--Select Contest Type--' ? '-1' : this.props.contestTypes[value],
        selectedContestType: value,
      });
      return;
    }
    this.setState({
      [key]: value,
    });
  }

  handleMatchSelection(match) {
    const { selectedMatches } = this.state;
    const matchIndex = selectedMatches.findIndex(m => m.id === match.id);
    if (matchIndex > -1) {
      selectedMatches.splice(matchIndex, 1);
    } else {
      selectedMatches.push(match);
    }
    this.setState({
      selectedMatches,
    });
  }

  handleNext() {
    const {
      matchesInDuration,
      selectedMatches,
      selectedContestTypeID,
      contestSize,
      selectedDurationLabel,
    } = this.state;
    const durationType = this.props.navigation.state.params.contest.duration === 0
      ? ' date' : ' week';
    if (selectedContestTypeID === -1) {
      Alert.alert('Message', 'Select contest type');
      return;
    } else if (contestSize < 2) {
      Alert.alert('Message', 'Contest size less than 2 is not valid, Use 2 or more than 2');
      return;
    } else if (matchesInDuration.length === 0) {
      Alert.alert('Message', `Select a contest ${durationType}`);
      return;
    } else if (selectedMatches.length === 0) {
      Alert.alert('Message', 'You have not selected any match(s) yet');
      return;
    }
    if (contestSize > 100) {
      Alert.alert('Message', 'Contest size should be less than or equal to 100');
      return;
    }
    const contest = JSON.parse(JSON.stringify(this.props.navigation.state.params.contest));
    contest.contest_type = Number.parseInt(selectedContestTypeID, 10);
    contest.max_teams = Number.parseInt(contestSize, 10);
    contest.matches = matchesInDuration.map(match => match.id);
    if (this.durationType === ' Date') {
      contest.start_date = selectedDurationLabel;
      contest.end_date = selectedDurationLabel;
    } else {
      contest.start_week_id = selectedDurationLabel;
    }
    this.props.navigation.navigate(
      constant.screens.CREATE_CONTEST_THIRD_PAGE,
      { contest },
    );
  }

  renderContestSizeContainer() {
    const { contestSize, selectedContestTypeID } = this.state;
    return (
      <View>
        {selectedContestTypeID !== 0 ?
          <View style={styles.inputOuterContainer}>
            <Text style={styles.titleLabel}>
              {
                selectedContestTypeID === 1 ?
                  'Contest Size(min 2 & max 100)' : 'Min. Teams To Start (at least 2)'
              }
            </Text>
            <View style={styles.inputContainer}>
              <TextInput
                value={contestSize}
                style={styles.inputStyles}
                placeholder="Size of Contest"
                placeholderTextColor="#FFFFFF50"
                returnKeyType="next"
                keyboardType={'numeric'}
                onChangeText={txt => this.handleChange('contestSize', txt)}
                underlineColorAndroid="transparent"
              />
            </View>
          </View> : null}
      </View>
    );
  }

  renderContestTypes(availableContestTypes) {
    const contestTypes = [];
    contestTypes.push(<DropDownPicker.Item
      key={-1}
      label={'--Select Contest Type--'}
      value={'--Select Contest Type--'}
    />);

    for (let i = 0; i < availableContestTypes.length; i += 1) {
      contestTypes.push(<DropDownPicker.Item
        key={i}
        label={`${availableContestTypes[i]}`}
        value={availableContestTypes[i]}
      />);
    }
    return contestTypes;
  }

  renderContestDuration(availableContestDuration) {
    const contestDuration = [];
    contestDuration.push(<DropDownPicker.Item
      key={-1}
      label={this.defaultDuration.label}
      value={this.defaultDuration}
    />);

    for (let i = 0; i < availableContestDuration.length; i += 1) {
      const label = this.props.navigation.state.params.contest.duration === 0 ?
        availableContestDuration[i].label : `Week ${availableContestDuration[i].label}`;
      contestDuration.push(<DropDownPicker.Item
        key={i}
        label={`${label}`}
        value={availableContestDuration[i]}
      />);
    }
    return contestDuration;
  }

  render() {
    const {
      navigation,
      contestTypes,
      matches,
    } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar />
        <NavigationBar
          title="CREATE CONTEST"
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => navigation.goBack()}
        />
        <KeyboardAwareScrollView style={styles.body}>
          <View style={styles.bodyContainer}>
            <View style={[styles.inputOuterContainer, { marginTop: 10 }]}>
              <Text style={styles.titleLabel}>
                Type Of Contest You Like
              </Text>
              <DropDownPicker
                selectedValue={this.state.selectedContestType}
                onValueChange={selectCT =>
                  this.handleChange('selectedContestTypeID', selectCT)}
                prompt={'Choose a contest type'}
                style={styles.picker}
                textStyle={styles.pickerText}
                cancel
              >
                {this.renderContestTypes(Array.from(Object.keys(contestTypes)))}
              </DropDownPicker>
            </View>

            {this.state.selectedContestTypeID !== -1 && this.renderContestSizeContainer()}

            { /* Drop down to game dates / game weeks */ }
            <View style={styles.inputOuterContainer}>
              <Text style={styles.titleLabel}>
                Pick Contest {this.durationType}
              </Text>
              <DropDownPicker
                selectedValue={this.state.selectedMatchDuration}
                onValueChange={selectedMatch =>
                  this.handleChange('matchesInDuration', selectedMatch)}
                prompt={`Choose a ${this.durationType}`}
                style={styles.picker}
                textStyle={styles.pickerText}
                cancel
              >
                {this.renderContestDuration(matches)}
              </DropDownPicker>
            </View>
            {
              this.state.matchesInDuration.map((match, matchIndex) => {
                return (
                  <MatchRow
                    key={matchIndex}
                    isSelected={this.setCheckedForSelectedMatches(match)}
                    homeTeamAcronym={match.home_team_acronym.toUpperCase()}
                    awayTeamAcronym={match.away_team_acronym.toUpperCase()}
                    schedule={match.scheduled_for}
                    // TODO: Logo is null for now
                    // homeTeamLogo={match.home_team_logo ? match.home_team_logo}
                    // awayTeamLogo={match.away_team_logo ? match.away_team_logo}
                    onCheck={() => this.handleMatchSelection(match)}
                  />
                );
              })
            }
            {matches.length === 0 &&
              <Text
                style={[styles.pageIndicator, {
                  marginHorizontal: 50,
                  paddingVertical: 20,
                }]}
              >No Match(s) available for the league you selected.
              </Text>}
            {(matches.length !== 0 && this.state.matchesInDuration.length === 0 )&&
              <Text
                style={[styles.pageIndicator, {
                  marginHorizontal: 50,
                  paddingVertical: 20,
                }]}
              >Select a {this.durationType.toUpperCase()} from above to display match(s).
              </Text>}
          </View>
        </KeyboardAwareScrollView>
        <View style={styles.bottomView}>
          <View style={styles.pageIndicatorContainer}>
            <Text style={[styles.pageIndicator, { fontSize: 18 }]}>2</Text>
            <Text style={styles.pageIndicator}> / 3</Text>
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => this.handleNext()}
          >
            <View style={styles.nextButtonView}>
              <Text style={styles.nextButtonText}>NEXT</Text>
            </View>
          </TouchableOpacity>
        </View>
        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

SelectMatch.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  isSessionExpired: PropTypes.bool,
  isLoading: PropTypes.bool,
  contestTypes: PropTypes.objectOf(PropTypes.any),
  matches: PropTypes.arrayOf(PropTypes.any),
  getMID: PropTypes.func,
  resetlogout: PropTypes.func,
};

SelectMatch.defaultProps = {
  navigation: {},
  isSessionExpired: false,
  isLoading: false,
  contestTypes: {},
  matches: [],
  getMID: () => {},
  resetlogout: () => {},
};

const mapStateToProps = state => ({
  isSessionExpired: state.sessionExpire.isSessionExpired,
  isLoading: state.contestManagement.isLoading,
  contestTypes: state.contestManagement.contestTypes,
  matches: state.contestManagement.matches,
});

const mapDispatchToProps = () => ContestActions;

const SelectMatchScreen = connect(mapStateToProps, mapDispatchToProps)(SelectMatch);

export default SelectMatchScreen;
