import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../../components/CustomTextInput';
import { images } from '../../../assets/images';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  backgroundImage: {
    width,
    height,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  logo: {
    width: width * 0.60,
    height: 60,
    marginTop: height * 0.055,
    resizeMode: 'contain',
  },
  headerView: {
    width: (width / 3) * 2.6,
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 20,
    marginBottom: 0,
  },
  headerText: {
    color: 'white',
    fontSize: 28,
    fontWeight: '400',
    textAlign: 'left',
  },
  subHeaderView: {
    width: (width / 3) * 2.5,
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 5,
    marginBottom: 12,
  },
  subHeaderText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '400',
    textAlign: 'left',
  },
  buttonTouchable: {
    marginTop: 20,
  },
  buttonContainer: {
    width: (width / 3) * 1.4,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  signUpTextContainer: {
    flexDirection: 'row',
    // transform: [{ rotate: '12deg' }],
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'flex-end',
    alignSelf: 'stretch',
    bottom: 54,
    right: 20,
    position: 'absolute',
  },
  signUpTouchable: {
    marginLeft: 10,
  },
  signUpImage: {
    width: 50,
    height: 50,
    resizeMode: 'cover',
    // transform: [{ rotate: '-12deg' }],
  },
  signUpText: {
    fontSize: 14,
    color: 'white',
  },
});


const ForgotPasswordContainer = props => (
  <Image style={styles.backgroundImage} source={images.loginBackground}>
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <Image
          source={images.crickSixLogo}
          style={styles.logo}
        />
        <View style={styles.headerView}>
          <Text style={styles.headerText}>Forgot Password</Text>
        </View>
        <View style={styles.subHeaderView}>
          <Text style={styles.subHeaderText}>
          To reset your password please enter your email address
          </Text>
        </View>
        <CustomTextInput
          title="Enter Email Id*"
          keyboardType="email-address"
          value={props.emailText}
          returnKeyType="done"
          onChangeText={email => props.onChangeEmailText(email)}
        />

        <TouchableOpacity
          style={styles.buttonTouchable}
          activeOpacity={0.7}
          onPress={() => props.sendRequestPressed()}
        >
          <View style={styles.buttonContainer}>
            <Text style={styles.buttonText}>Send Request</Text>
          </View>
        </TouchableOpacity>
      </View>
    </KeyboardAwareScrollView>
    <View style={styles.signUpTextContainer}>
      {/* <Text style={styles.signUpText}>Back to sign in</Text> */}
      <TouchableOpacity
        style={styles.signUpTouchable}
        activeOpacity={0.7}
        onPress={() => props.popBack()}
      >
        <Image style={styles.signUpImage} source={images.backButton} />
      </TouchableOpacity>
    </View>
  </Image>
);

ForgotPasswordContainer.propTypes = {
  emailText: PropTypes.string,
  onChangeEmailText: PropTypes.func,
  sendRequestPressed: PropTypes.func,
  popBack: PropTypes.func,
};

ForgotPasswordContainer.defaultProps = {
  emailText: '',
  onChangeEmailText: () => {},
  sendRequestPressed: () => {},
  popBack: () => {},
};

export default ForgotPasswordContainer;
