import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import ForgotPasswordContainer from './components/ForgotPasswordContainer';
import Loader from '../../components/Loader';
import { validateEmail } from '../../utils/validations';
import {
  constant,
  isResponseValidated,
  isResponseSuccess,
} from '../../utils';
import { containerStyles } from '../../theme';
import Utils from '../../utils/utils';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    // Handle response for normal login
    if (isResponseValidated(nextProps.forgotPasswordResponse)) {
      if (isResponseSuccess(nextProps.forgotPasswordResponse)) {
        this.setState({ email: '' });
        Alert.alert('Message', nextProps.forgotPasswordResponse.response.message);
        this.props.navigation.goBack();
      } else {
        this.setState({ email: '' });
        Alert.alert('Message', nextProps.forgotPasswordResponse.response.message);
      }
    }
  }

  onChangeEmailText(email) {
    this.setState({ email });
  }

  sendRequestPressed() {
    if (validateEmail(this.state.email)) {
      Alert.alert(
        'Message',
        'Your e-mail is incorrect.',
      );
      return;
    }
    const utils = new Utils();
    utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.props.forgotPasswordRequest(this.state.email);
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  render() {
    return (
      <View style={containerStyles.defaultContainer}>
        <ForgotPasswordContainer
          emailText={this.state.email}
          onChangeEmailText={email => this.onChangeEmailText(email)}
          sendRequestPressed={() => this.sendRequestPressed()}
          popBack={() => this.props.navigation.goBack()}
        />
        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

ForgotPassword.propTypes = {
  isLoading: PropTypes.bool,
  forgotPasswordResponse: PropTypes.objectOf(PropTypes.any),
  forgotPasswordRequest: PropTypes.func,
  navigation: PropTypes.objectOf(PropTypes.any),
};

ForgotPassword.defaultProps = {
  isLoading: false,
  forgotPasswordResponse: {},
  forgotPasswordRequest: () => {},
  navigation: {},
};

const mapStateToProps = state => ({
  isLoading: state.user.isLoading,
  forgotPasswordResponse: state.user.forgotPasswordResponse,
});

const mapDispatchToProps = () => UserActions;

const ForgotPasswordScreen = connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);

export default ForgotPasswordScreen;
