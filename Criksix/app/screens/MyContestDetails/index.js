import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import ContestDetailContainer from './components/Container';
import PrizeDistributionPopUp from '../../components/PrizeDistributionPopUp';
import ConfirmationPopUp from './components/ConfirmationPopUp';
import { NavigationBar, StatusBar, Loader } from '../../components';
import { images } from '../../assets/images';
import { constant, resetRoute } from '../../utils';
import Utils from '../../utils/utils';

class ContestDetails extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      showPrizeDistributionPopUp: false,
      showConfirmationPopUp: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }
    if (nextProps.contestDetails.response &&
        nextProps.contestDetails.status >= 400) {
      if (nextProps.contestDetails.response.message &&
          typeof nextProps.contestDetails.response.message === 'string') {
        Alert.alert('Message', nextProps.contestDetails.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  goToMyTeams(contest) {
    console.log('WIP', contest);
    if (this.props.contestDetails.response && this.props.contestDetails.response.id === undefined) {
      Alert.alert('Can\'t Join Contest', 'Please talk to admin for Join Contest');
      return;
    }
    this.props.navigation.navigate(constant.screens.MY_TEAMS, { contest });
  }

  goToCreateTeam(contest) {
    this.props.navigation.navigate(
      constant.screens.CREATE_TEAM,
      {
        contest_user_lineup: contest.contest_user_lineups_id[0],
        contest_id: contest.id,
        contest_name: contest.name,
        invite_code: contest.invite_code,
        create: false,
      },
    );
  }

  showPrizeDistributionPopUp() {
    this.setState({ showPrizeDistributionPopUp: true });
  }

  closePrizeDistributionPopUp() {
    this.setState({ showPrizeDistributionPopUp: false });
  }

  joinContestPressed(contest) {
    if (this.props.contestDetails.response && this.props.contestDetails.response.id === undefined) {
      Alert.alert('Can\'t Join Contest', 'Please talk to admin for Join Contest');
      return;
    }
    this.showConfirmationPopUp(contest);
  }

  showConfirmationPopUp(contest) {
    // TODO: Use contest object.
    console.log(contest);
    this.setState({ showConfirmationPopUp: true });
  }

  closeConfirmationPopUp() {
    this.setState({ showConfirmationPopUp: false });
  }

  confirmJoinContest() {
    this.setState({ showConfirmationPopUp: false });
    if (Object.keys(this.props.contestDetails.response).length > 0
      && typeof this.props.contestDetails.response.id === 'number'
      && this.props.contestDetails.response.invite_code !== undefined) {
      this.props.navigation.navigate(
        constant.screens.CREATE_TEAM,
        { contestDetails: this.props.contestDetails.response },
      );
    } else {
      Alert.alert(constant.JOINING_CONTEST_ERROR.header, constant.JOINING_CONTEST_ERROR.message);
    }
  }


  render() {
    const { showPrizeDistributionPopUp, showConfirmationPopUp } = this.state;
    const { contestDetailLoading, contestDetails } = this.props;
    const { contest } = this.props.navigation.state.params;
    const screenTitle = contest.entry_fee_type === 'CURRENCY' ? 'CASH' : 'PRACTICE';
    return (
      <View style={{ flex: 1 }}>
        <StatusBar />
        <NavigationBar
          title={`${screenTitle} CONTEST`}
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <ContestDetailContainer
          contest={contest}
          onPressCreateTeam={() => this.goToCreateTeam(contest)}
          onPressMyTeam={() => this.goToMyTeams(contest)}
          showPrizeMatrix={() => this.showPrizeDistributionPopUp()}
          handleShowMatches={() =>
            this.props.navigation.navigate(
              constant.screens.LIST_CONTEST_MATCH,
              { contest: this.props.navigation.state.params.contest },
            )}
          onPressViewTeams={() =>
            this.props.navigation.navigate(constant.screens.CONTEST_LEADERBOARD)}
        />
        {showPrizeDistributionPopUp &&
          <PrizeDistributionPopUp
            contest={contest}
            close={() => this.closePrizeDistributionPopUp()}
          />
        }
        {showConfirmationPopUp &&
          <ConfirmationPopUp
            contest={contest}
            confirmJoinContest={() => this.confirmJoinContest()}
            close={() => this.closeConfirmationPopUp()}
          />
        }
        {contestDetailLoading &&
          <Loader isAnimating={contestDetailLoading} />
        }
      </View>
    );
  }
}

ContestDetails.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  contestDetailLoading: PropTypes.bool,
  contestDetails: PropTypes.objectOf(PropTypes.any),
  getContestDetails: PropTypes.func,
  isSessionExpired: PropTypes.bool,
};

ContestDetails.defaultProps = {
  navigation: {},
  contestDetailLoading: false,
  contestDetails: {},
  getContestDetails: () => {},
  isSessionExpired: false,
};

const mapStateToProps = state => ({
  // Session Expire
  isSessionExpired: state.sessionExpire.isSessionExpired,

  // Contest Details
  contestDetailLoading: state.contestManagement.contestDetailLoading,
  contestDetails: state.contestManagement.contestDetails,

});

const mapDispatchToProps = () => UserActions;

const ContestDetailsScreen = connect(mapStateToProps, mapDispatchToProps)(ContestDetails);


export default ContestDetailsScreen;
