import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import { Colors } from '../../../theme/index';
import Utils from '../../../utils/utils';
import { getCurrencyType } from '../../../utils';
import { images } from '../../../assets/images/index';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  contestViewStyle: {
    alignSelf: 'stretch',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  contestName: {
    color: 'white',
    marginVertical: 3,
    backgroundColor: 'transparent',
    textAlign: 'left',
    fontSize: 14,
    fontWeight: '700',
  },
  headerView: {
    width: width - 35,
    flexDirection: 'row',
    padding: 2,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  detailView: {
    width: width - 35,
    flexDirection: 'row',
    padding: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
  },
  totalEarnings: {
    fontSize: 15,
    fontWeight: '400',
    color: 'white',
  },
  earningsText: {
    fontSize: 15,
    fontWeight: '700',
    color: 'white',
  },
  detailHeader: {
    fontSize: 12,
    fontWeight: '400',
    color: 'darkgrey',
  },
  detailText: {
    fontSize: 12,
    fontWeight: '700',
    color: 'white',
  },
  joinButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical: 8,
    backgroundColor: '#4f5386',
    borderRadius: 12,
  },
  nextIcon: {
    width: 12,
    height: 12,
    transform: [{
      rotate: '-90deg',
    }],
  },
  contestStatusText: {
    fontSize: 12,
    color: Colors.navigationBackgroundColor,
    paddingBottom: 15,
  },
});

const ContestInfo = props => (
  <View style={styles.contestViewStyle}>
    <View >
      <Text
        style={styles.contestName}
        numberOfLines={2}
        lineBreakMode="tail"
      >{props.contest.name ? props.contest.name.toUpperCase() : 'CONTEST DETAIL:'}
      </Text>
      <Text style={styles.contestStatusText}>
        ({props.contest.start_date && `${Utils.formatDate(props.contest.start_date)} ${Utils.formatTime(props.contest.start_date)}`})
      </Text>
    </View>
    <View style={styles.detailView}>
      <View>
        <Text style={styles.detailHeader}>PRIZE POOL</Text>
        <Text style={styles.detailText}>
          {getCurrencyType(props.contest.entry_fee_type, props.contest.currency_type)}
          {props.contest.prize_pool}
        </Text>
      </View>
      <View>
        <TouchableOpacity
          activeOpacity={props.isShowPrizeMatrix ? 0 : 1}
          disabled={!props.isShowPrizeMatrix}
          onPress={() => props.showPrizeMatrix()}
          style={{ flexDirection: 'row', alignItems: 'center' }}
        >
          <Text style={styles.detailHeader}>WINNERS </Text>
          {props.isShowPrizeMatrix &&
            <Image style={styles.nextIcon} source={images.backIcon} />}
        </TouchableOpacity>
        <Text style={styles.detailText}>
          {props.contest.winners && props.contest.winners}
        </Text>
      </View>
      <View>
        <Text style={styles.detailHeader}>ENTRY FEE</Text>
        <Text style={[styles.detailText, { color: Colors.greenShadeColor }]}>
          {getCurrencyType(props.contest.entry_fee_type, props.contest.currency_type)}
          {props.contest.entry_fee && props.contest.entry_fee}
        </Text>
      </View>
    </View>
  </View>
);

ContestInfo.propTypes = {
  contest: PropTypes.objectOf(PropTypes.any),
  joinContest: PropTypes.func,
  showContestDetails: PropTypes.func,
  showPrizeMatrix: PropTypes.func,
  isShowPrizeMatrix: PropTypes.bool,
};

ContestInfo.defaultProps = {
  contest: {},
  joinContest: () => {},
  showContestDetails: () => {},
  showPrizeMatrix: () => {},
  isShowPrizeMatrix: true,
};

export default ContestInfo;
