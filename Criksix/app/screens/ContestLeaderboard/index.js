import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  Image,
  StyleSheet,
} from 'react-native';
import { NavigationBar, StatusBar } from '../../components';
import { images } from '../../assets/images';
import { dimensions } from '../../theme';
import { constant } from '../../utils';

const getLargeColoredText = (size = 16, color = 'white', weight = 'normal') => {
  return {
    fontSize: size,
    color,
    fontWeight: weight,
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: dimensions.getViewportWidth(),
    resizeMode: 'cover',
  },
  topTextContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 15,
  },
  listHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 15,
    backgroundColor: 'rgb(141, 54, 66)',
  },
  listItemContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    height: 70,
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: 'rgb(190,60,67)',
    marginHorizontal: 10,
    alignItems: 'center',
    paddingHorizontal: 10,
    marginVertical: 3,
  },
  teamNameText: {
    fontSize: 16,
    color: 'rgb(184, 67, 65)',
  },
  teamPointText: {
    fontSize: 16,
    color: 'rgb(184, 67, 65)',
  },
  logo: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
  },
  crownLogo: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
});

class ContestLeaderboard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { navigation } = this.props;
    return (
      <Image
        style={styles.container}
        source={images.generalBackground}
      >
        <StatusBar />
        <NavigationBar
          title={constant.screen_title.CONTEST_LEADERBOARD}
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => navigation.goBack()}
        />
        <View style={styles.topTextContainer}>
          <Text style={getLargeColoredText(14)}>Series</Text>
          <Text style={getLargeColoredText(14, 'rgb(141, 54, 66)')}>ASIA DOMESTIC TROPHY</Text>
        </View>
        <View style={styles.listHeaderContainer}>
          <Text style={getLargeColoredText()}>TEAM</Text>
          <Text style={getLargeColoredText()}>RANK</Text>
        </View>
        <View style={styles.listItemContainer}>
          <Image
            source={images.profilePic}
            style={styles.logo}
          />
          <View style={{ paddingHorizontal: 10 }}>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>KINGPG84NO</Text>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>0 POINTS</Text>
          </View>
          <View style={{
            flex: 1,
            alignItems: 'flex-end',
            paddingRight: 15,
          }}>
            <Image
              source={images.alarm}
              style={styles.crownLogo}
            />
            <Text style={
              getLargeColoredText(14, 'black', 'bold')
            }>#1 ></Text>
          </View>
        </View>
        <View style={styles.listItemContainer}>
          <Image
            source={images.profilePic}
            style={styles.logo}
          />
          <View style={{ paddingHorizontal: 10 }}>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>KINGPG84NO</Text>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>0 POINTS</Text>
          </View>
          <View style={{
            flex: 1,
            alignItems: 'flex-end',
            paddingRight: 15,
          }}>
            <Image
              source={images.alarm}
              style={styles.crownLogo}
            />
            <Text style={
              getLargeColoredText(14, 'black', 'bold')
            }>#1 ></Text>
          </View>
        </View>
        <View style={styles.listItemContainer}>
          <Image
            source={images.profilePic}
            style={styles.logo}
          />
          <View style={{ paddingHorizontal: 10 }}>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>KINGPG84NO</Text>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>0 POINTS</Text>
          </View>
          <View style={{
            flex: 1,
            alignItems: 'flex-end',
            paddingRight: 15,
          }}>
            <Image
              source={images.alarm}
              style={styles.crownLogo}
            />
            <Text style={
              getLargeColoredText(14, 'black', 'bold')
            }>#1 ></Text>
          </View>
        </View>
        <View style={styles.listItemContainer}>
          <Image
            source={images.profilePic}
            style={styles.logo}
          />
          <View style={{ paddingHorizontal: 10 }}>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>KINGPG84NO</Text>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>0 POINTS</Text>
          </View>
          <View style={{
            flex: 1,
            alignItems: 'flex-end',
            paddingRight: 15,
          }}>
            <Image
              source={images.alarm}
              style={styles.crownLogo}
            />
            <Text style={
              getLargeColoredText(14, 'black', 'bold')
            }>#1 ></Text>
          </View>
        </View>
        <View style={styles.listItemContainer}>
          <Image
            source={images.profilePic}
            style={styles.logo}
          />
          <View style={{ paddingHorizontal: 10 }}>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>KINGPG84NO</Text>
            <Text style={getLargeColoredText(14, 'white', 'bold')}>0 POINTS</Text>
          </View>
          <View style={{
            flex: 1,
            alignItems: 'flex-end',
            paddingRight: 15,
          }}>
            <Image
              source={images.alarm}
              style={styles.crownLogo}
            />
            <Text style={
              getLargeColoredText(14, 'black', 'bold')
            }>#1 ></Text>
          </View>
        </View>
      </Image>
    );
  }
}

ContestLeaderboard.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

ContestLeaderboard.defaultProps = {
  navigation: {},
};

export default ContestLeaderboard;
