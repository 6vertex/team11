import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import UserActions from '../../actions';
import ContestDetailContainer from './components/Container';
import ConfirmationPopUp from './components/ConfirmationPopUp';
import { NavigationBar, StatusBar, Loader, PrizeDistributionPopUp } from '../../components';
import { images } from '../../assets/images';
import {
  constant,
  resetRoute,
  isResponseValidated,
  isResponseSuccess,
} from '../../utils';
import { containerStyles } from '../../theme';
import Utils from '../../utils/utils';

class ContestDetails extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      showPrizeDistributionPopUp: false,
      showConfirmationPopUp: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }
    // TODO: Handle join contest response.
    if (isResponseValidated(nextProps.joinContestResponse)) {
      if (isResponseSuccess(nextProps.joinContestResponse)) {
        const { contest } = this.props.navigation.state.params;
        const resetAction = NavigationActions.reset({
          index: 1,
          actions: [
            NavigationActions.navigate({ routeName: constant.screens.DRAWER_STACK }),
            NavigationActions.navigate({
              routeName: constant.screens.CREATE_TEAM,
              params: {
                contest_user_lineup: nextProps.joinContestResponse.response.contest_user_lineup,
                contest_id: nextProps.joinContestResponse.response.contest_id,
                contest_name: nextProps.joinContestResponse.response.contest_name,
                invite_code: contest.invite_code,
              },
            }),
          ],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.resetContestLobby();
        return;
      }
      if (nextProps.joinContestResponse.response.message &&
        typeof nextProps.joinContestResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.joinContestResponse.response.message);
      }
    }
  }

  joinContestPressed(contest) {
    this.showConfirmationPopUp(contest);
  }

  showConfirmationPopUp(contest) {
    // TODO: Use contest object.
    console.log(contest);
    this.setState({ showConfirmationPopUp: true });
  }

  closeConfirmationPopUp() {
    this.setState({ showConfirmationPopUp: false });
  }

  confirmJoinContest() {
    this.setState({ showConfirmationPopUp: false });
    if (Object.keys(this.props.navigation.state.params.contest).length > 0
      && typeof this.props.navigation.state.params.contest.id === 'number'
      && this.props.navigation.state.params.contest.invite_code !== undefined) {
      this.utils.checkInternetConnectivity((reach) => {
        if (reach) {
          this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
            if (response && response.access_token) {
              this.props.joinContest(
                response.access_token,
                this.props.navigation.state.params.contest.invite_code,
              );
            } else {
              Alert.alert(constant.loadingError.header, constant.loadingError.message);
            }
          });
        } else {
          Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
        }
      });
    } else {
      Alert.alert(constant.JOINING_CONTEST_ERROR.header, constant.JOINING_CONTEST_ERROR.message);
    }
  }

  showPrizeDistributionPopUp() {
    this.setState({ showPrizeDistributionPopUp: true });
  }

  closePrizeDistributionPopUp() {
    this.setState({ showPrizeDistributionPopUp: false });
  }

  render() {
    const screenTitle = this.props.navigation.state.params.contest.entry_fee_type === 'CURRENCY' ?
      'CASH' : 'PRACTICE';
    const { showPrizeDistributionPopUp, showConfirmationPopUp } = this.state;
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar />
        <NavigationBar
          title={`${screenTitle} CONTEST`}
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <ContestDetailContainer
          contest={this.props.navigation.state.params.contest}
          joinContestPressed={contest => this.joinContestPressed(contest)}
          showPrizeMatrix={() => this.showPrizeDistributionPopUp()}
          handleShowMatches={() =>
            this.props.navigation.navigate(
              constant.screens.LIST_CONTEST_MATCH,
              { contest: this.props.navigation.state.params.contest },
            )}
          onPressViewTeams={contestToViewTeams =>
            this.props.navigation.navigate(constant.screens.CONTEST_LEADERBOARD, {
              contest: contestToViewTeams,
            })}
        />
        {showPrizeDistributionPopUp &&
          <PrizeDistributionPopUp
            contest={this.props.navigation.state.params.contest}
            close={() => this.closePrizeDistributionPopUp()}
          />
        }
        {showConfirmationPopUp &&
          <ConfirmationPopUp
            contest={this.props.navigation.state.params.contest}
            confirmJoinContest={() => this.confirmJoinContest()}
            close={() => this.closeConfirmationPopUp()}
          />
        }
        {this.props.joinContestLoading &&
          <Loader isAnimating={this.props.joinContestLoading} />
        }
      </View>
    );
  }
}

ContestDetails.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  joinContestLoading: PropTypes.bool,
  contest: PropTypes.objectOf(PropTypes.any),
  joinContestResponse: PropTypes.objectOf(PropTypes.any),
  joinContest: PropTypes.func,
  resetlogout: PropTypes.func,
  resetContestLobby: PropTypes.func,
  isSessionExpired: PropTypes.bool,
};

ContestDetails.defaultProps = {
  navigation: {},
  joinContestLoading: false,
  contest: {},
  joinContestResponse: {},
  joinContest: () => {},
  resetlogout: () => {},
  resetContestLobby: () => {},
  isSessionExpired: false,
};

const mapStateToProps = state => ({
  isSessionExpired: state.sessionExpire.isSessionExpired,
  joinContestLoading: state.contestManagement.joinContestLoading,
  joinContestResponse: state.contestManagement.joinContestResponse,

});

const mapDispatchToProps = () => UserActions;

const ContestDetailsScreen = connect(mapStateToProps, mapDispatchToProps)(ContestDetails);

export default ContestDetailsScreen;
