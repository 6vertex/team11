import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  ListView,
  ScrollView,
} from 'react-native';
import Utils from '../../../utils/utils';
import ContestInfo from './ContestInfo';
import { images } from '../../../assets/images';
import { Colors } from '../../../theme';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    width,
    backgroundColor: Colors.primarybackgroundColor,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignSelf: 'stretch',
  },
  containerView: {
    width: width - 10,
    margin: 5,
    borderRadius: 5,
    padding: 5,
    backgroundColor: Colors.selectedFilterColor,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  statisticsView: {
    paddingVertical: 5,
    backgroundColor: 'transparent',
    alignSelf: 'stretch',
  },
  statusView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    height: 5,
    backgroundColor: 'white',
  },
  joinedStatus: {
    backgroundColor: Colors.navigationBackgroundColor,
  },
  roomStatus: {
    flex: 1,
  },
  teamsCountView: {
    flexDirection: 'row',
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textHeader: {
    color: 'white',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 12,
  },
  detailText: {
    fontSize: 12,
    color: 'white',
  },
  joinContestView: {
    height: 40,
    backgroundColor: Colors.greenShadeColor,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 8,
    paddingHorizontal: 80,
    marginVertical: 12,
  },
  joinContestText: {
    color: 'white',
    fontSize: 13,
    fontWeight: '500',
  },
  leaderboardContainer: {
    width,
    maxHeight: height - 85,
    justifyContent: 'flex-start',
    backgroundColor: Colors.selectedFilterColor,
    alignItems: 'center',
    paddingVertical: 5,
    marginVertical: 5,
  },
  leaderBoardHeaderView: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 5,
    alignSelf: 'stretch',
    backgroundColor: 'transparent',
  },
  leaderBoardText: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white',
  },
  viewTeamsView: {
    backgroundColor: Colors.buttonColor,
    borderRadius: 17.5,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  teamListHeader: {
    width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 5,
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: Colors.orangeShadeColor,
    opacity: 0.5,
  },
  teamListRow: {
    width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: Colors.primarybackgroundColor,
    paddingVertical: 8,
    paddingHorizontal: 8,
    borderBottomColor: Colors.buttonColor,
    borderBottomWidth: 1,
  },
  teamLogo: {
    width: 40,
    height: 40,
  },
  teamListText: {
    minWidth: width / 8,
    fontSize: 13,
    fontWeight: '500',
    textAlign: 'left',
    color: 'white',
    paddingLeft: 5,
    backgroundColor: 'transparent',
  },
  flagContainer: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    paddingHorizontal: 5,
    paddingVertical: 10,
  },
  flagLetter: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 3,
    marginRight: 10,
    color: 'white',
    backgroundColor: Colors.greenShadeColor,
  },
  flagText: {
    flex: 1,
    color: 'grey',
  },
});

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const TeamListRow = (rowData, rowID) => (
  <View
    key={rowID}
    style={[
      styles.teamListRow,
      { backgroundColor: rowID / 2 ? Colors.selectedFilterColor : Colors.primarybackgroundColor },
    ]}
  >
    <Image source={images.profilePic} style={styles.teamLogo} />
    <Text style={[styles.teamListText, { flex: 1 }]}>{rowData.name}</Text>
    <Text style={styles.teamListText}>
      {rowData.rank ? rowData.rank : '-'}
    </Text>
  </View>
);

const FlagView = (flagLetter, flagText, color = Colors.greenShadeColor) => (
  <View style={styles.flagContainer}>
    <Text style={[styles.flagLetter, { backgroundColor: color }]}>{flagLetter}</Text>
    <Text style={styles.flagText}>{flagText}</Text>
  </View>
);

const ContestDetailContainer = (props) => {
  const isContestUpcoming = props.contest.status.toLowerCase() === 'upcoming';
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.mainContainer}>
          <View style={styles.containerView}>
            <ContestInfo
              contest={props.contest}
              showPrizeMatrix={() => props.showPrizeMatrix()}
            />
            {props.contest.multi_entry_count > 1 &&
              FlagView('M', 'You can join this contest with multiple team(s)')}
            {props.contest.joined_users_count >= props.contest.max_teams ?
              FlagView('C', "This contest wo'nt be cancel now.") :
              FlagView(
                'J',
                'Please join the contest ASAP before the deadline.',
                Colors.greenShadeColor,
              )}
            <View style={styles.statisticsView}>
              <View style={styles.statusView}>
                <View
                  style={[
                    styles.joinedStatus,
                    { flex: (props.contest.joined_users_count / props.contest.max_teams) }
                  ]}
                />
                <View style={[
                  styles.roomStatus,
                  { flex: (1 - (props.contest.joined_users_count / props.contest.max_teams)) },
                  ]}
                />
              </View>
              <View style={styles.teamsCountView}>
                <Text style={[styles.detailText, { color: 'orange' }]}>
                  Only {props.contest.remaining_users_count
                    && props.contest.remaining_users_count} Spot(s) Left
                </Text>
                <Text style={styles.detailText}>
                  {props.contest.max_teams && props.contest.max_teams} TEAMS
                </Text>
              </View>
            </View>
            <TouchableOpacity
              style={[
                styles.leaderBoardHeaderView,
                {
                  backgroundColor: Colors.primarybackgroundColor,
                  marginVertical: 5,
                  paddingVertical: 10,
                  borderRadius: 5,
                }]}
              onPress={() => props.handleShowMatches()}
            >
              <View style={{ flex: 1 }}>
                <Text style={styles.leaderBoardText}>Match(s)</Text>
                <Text
                  style={[styles.leaderBoardText, {
                    color: Colors.greenShadeColor,
                    fontSize: 12,
                  }]}
                >
                  ({props.contest.league_name})
                </Text>
              </View>
              <Text
                style={[styles.leaderBoardText,
                {
                  backgroundColor: Colors.navigationBackgroundColor,
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  borderRadius: 20,
                  marginRight: 20,
                  alignSelf: 'center',
                }]}
              >{props.contest.matches.length}
              </Text>
              <Image
                style={{
                  width: 20,
                  height: 20,
                  alignSelf: 'center',
                }}
                source={images.next}
              />
            </TouchableOpacity>
            {isContestUpcoming && props.contest.is_joined === false && props.contest.remaining_users_count > 0 &&
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ marginHorizontal: 5 }}
                onPress={() => props.joinContestPressed(props.contest)}
              >
                <View style={styles.joinContestView}>
                  <Text style={styles.joinContestText}>Join This Contest</Text>
                </View>
              </TouchableOpacity>
              }
            {!isContestUpcoming &&
              <Text
                style={[styles.leaderBoardText, {
                  paddingVertical: 10,
                  fontSize: 16,
                  color: Colors.navigationBackgroundColor,
                  fontWeight: '400',
                }]}
              >
                  Contest has crossed the deadline.
              </Text>}
          </View>
          <View style={styles.leaderboardContainer}>
            <View style={styles.leaderBoardHeaderView}>
              <Text style={styles.leaderBoardText}>LEADERBOARD</Text>
              <Text style={styles.leaderBoardText}>{props.contest.joined_users_count} TEAMS</Text>
            </View>
            <View style={styles.leaderBoardHeaderView}>
              <Text style={[
                styles.leaderBoardText,
                {
                  fontWeight: '400',
                  color: 'darkgrey',
                  flex: 1,
                  paddingRight: 10,
                }]}
              >
                  You can view teams after the deadline of contest
              </Text>
              {/* <TouchableOpacity
                activeOpacity={isContestUpcoming ? 0.7 : 1}
                onPress={() => {
                  isContestUpcoming ?
                  props.onPressViewTeams(props.contest) : null;
                }}
              >
                <View style={styles.viewTeamsView}>
                  <Text style={styles.leaderBoardText}>View Teams</Text>
                </View>
              </TouchableOpacity> */}
            </View>
            <View style={styles.teamListHeader}>
              <Text style={styles.leaderBoardText}>TEAM</Text>
              <Text style={styles.leaderBoardText}>RANK</Text>
            </View>
            {props.contest.teams.length > 0 ?
              <ListView
                dataSource={ds.cloneWithRows(props.contest.teams)}
                enableEmptySections
                renderRow={(rowData, sectionID, rowID) => TeamListRow(rowData, rowID)}
              /> :
              <Text
                style={[styles.leaderBoardText, {
                  backgroundColor: Colors.primarybackgroundColor,
                  alignSelf: 'stretch',
                  paddingVertical: 20,
                  textAlign: 'center',
                  color: 'darkgrey',
                  marginBottom: -10,
                }]}
              >No Team Lineup submitted yet!
              </Text>}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

ContestDetailContainer.propTypes = {
  contest: PropTypes.objectOf(PropTypes.any),
  onChangeInviteCode: PropTypes.func,
  joinContestPressed: PropTypes.func,
  showPrizeDistributionPopUp: PropTypes.func,
  onPressViewTeams: PropTypes.func,
  showPrizeMatrix: PropTypes.func,
  handleShowMatches: PropTypes.func,
};

ContestDetailContainer.defaultProps = {
  contest: {},
  onChangeInviteCode: () => {},
  joinContestPressed: () => {},
  showPrizeDistributionPopUp: () => {},
  onPressViewTeams: () => {},
  showPrizeMatrix: () => {},
  handleShowMatches: () => {},
};

export default ContestDetailContainer;
