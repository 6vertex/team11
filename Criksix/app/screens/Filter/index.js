import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import { images } from '../../assets/images';
import FilterContainer from './components/FilterContainer';
import { NavigationBar, StatusBar, Loader } from '../../components';
import { containerStyles } from '../../theme';
import Utils from '../../utils/utils';
import localData from '../../utils/localData';
import { constant, getFilterMatchesApiBody } from '../../utils';

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedByFilter: 0,
      filterData: props.navigation.state.params.filters,
    };
  }
  componentDidMount() {
    // new Utils().checkInternetConnectivity((reach) => {
    //   if (reach) {
    //     new Utils().getItemWithKey(constant.USER_DETAILS, (response) => {
    //       if (response && response.access_token) {
    //         const accessToken = response.access_token;
    //         this.props.getMatchesFilters(accessToken);
    //       } else {
    //         Alert.alert('Message', constant.loadingError.message);
    //       }
    //     });
    //   } else {
    //     Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
    //   }
    // });
  }

  componentWillReceiveProps(nextProps) {
  }

  selectedFilterType(activeFilterIndex, seletedFilterOptionIndex, status) {
    const { filterData } = this.state;
    filterData[activeFilterIndex].type[seletedFilterOptionIndex].active = status;
    this.setState({ filterData });
  }

  handleApplyFilter() {
    this.props.navigation.state.params.setFilterData(this.state.filterData);

    const leagueFilter = this.state.filterData[0].type;
    const prizeFilter = this.state.filterData[1].type;
    const contestTypeFilter = this.state.filterData[2].type;
    const durationFilter = this.state.filterData[3].type;
    const gameFeeFilter = this.state.filterData[4].type;

    const filterAPIData = {
      league: (leagueFilter.filter(data => data.active)).map(data => data.id),
      contest_type: (contestTypeFilter.filter(data => data.active)).map(data => data.id),
      duration: (durationFilter.filter(data => data.active)).map(data => data.id),
      prize: (prizeFilter.filter(data => data.active)).map(data => data.id),
      game_fee: (gameFeeFilter.filter(data => data.active)).map(data => data.id),
    };
    this.props.navigation.state.params.applyFilters(filterAPIData);
    this.props.navigation.goBack();
  }

  handleClearAll() {
    this.setState({ filterData: JSON.parse(JSON.stringify(localData.filterData)) }, () => {
      this.props.navigation.state.params.setFilterData(this.state.filterData);
    });
  }

  render() {
    const { navigation, loadingMatchFilters } = this.props;
    const { selectedByFilter, filterData } = this.state;
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar />
        <NavigationBar
          title="FILTER BY"
          showBackButton
          backButtonImage={images.crossIcon}
          backButtonAction={() => navigation.goBack()}
        />
        <FilterContainer
          filters={filterData}
          activeFilter={selectedByFilter}
          onPressByFilter={filterIndex => this.setState({ selectedByFilter: filterIndex })}
          onPressFilterOption={(activeFilterIndex, seletedFilterOptionIndex, status) => this.selectedFilterType(activeFilterIndex, seletedFilterOptionIndex, status)}
          selectedFilterOption={filterData[selectedByFilter]}
          onApplyFilter={() => this.handleApplyFilter()}
          onPressClearAll={() => this.handleClearAll()}
        />
        {loadingMatchFilters && <Loader isAnimating={loadingMatchFilters} />}
      </View>
    );
  }
}

Filter.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  ccoResponse: PropTypes.objectOf(PropTypes.any),
  filters: PropTypes.arrayOf(PropTypes.any),
};

Filter.defaultProps = {
  navigation: {},
  isLoading: false,
  ccoResponse: {},
  filters: [],
};

const mapStateToProps = state => ({
  isLoading: state.contestManagement.isLoading,
  ccoResponse: state.contestManagement.ccoResponse,

});

const mapDispatchToProps = () => UserActions;

const FilterScreen = connect(mapStateToProps, mapDispatchToProps)(Filter);

export default FilterScreen;
