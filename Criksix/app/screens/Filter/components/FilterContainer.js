import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import { images } from '../../../assets/images';
import { Colors, labelStyles, iconStyles, containerStyles } from '../../../theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'stretch',
    backgroundColor: 'green',
  },
  leftContainer: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: Colors.filterByColor,
  },
  rightContainer: {
    flex: 2,
    alignSelf: 'stretch',
    backgroundColor: Colors.selectedFilterColor,
    borderLeftWidth: 1,
    borderLeftColor: 'white',
  },
  filterByContainer: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  filterByOptionContainer: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
  },
  buttonText: {
    ...labelStyles.largeWhiteLabel,
    alignSelf: 'center',
    paddingVertical: 12,
  },
});

const FilterContainer = props => (
  <View style={styles.container}>
    <View style={styles.leftContainer}>
      <View style={containerStyles.defaultContainer}>
        {props.filters.map((filter, index) => {
          return (
            <TouchableOpacity
              key={index}
              onPress={() => props.onPressByFilter(index)}
              style={[
                styles.filterByContainer,
                {
                  backgroundColor: props.activeFilter === index ?
                  'orange' : null,
                },
              ]}
            ><Text style={labelStyles.defaultWhiteLabel}>{filter.name}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
      <TouchableOpacity
        onPress={() => props.onPressClearAll()}
        style={{ backgroundColor: Colors.selectedFilterColor }}
      >
        <Text style={styles.buttonText}>Clear All</Text>
      </TouchableOpacity>
    </View>
    <View style={styles.rightContainer}>
      <View style={containerStyles.defaultContainer}>
        {Object.keys(props.selectedFilterOption).length > 0 &&
          props.selectedFilterOption.type.map((filterOption, index) => {
          return (
            <TouchableOpacity
              key={index}
              onPress={() =>
                props.onPressFilterOption(props.activeFilter, index, !filterOption.active)}
              style={styles.filterByOptionContainer}
            >
              <Text style={[labelStyles.defaultWhiteLabel, { flex: 1 }]}>{filterOption.name}</Text>
              {filterOption.active &&
                <Image style={iconStyles.defaultIcon} source={images.rightTick} />}
            </TouchableOpacity>
          );
        })}
      </View>
      <TouchableOpacity
        onPress={() => props.onApplyFilter(props.filters)}
        style={{ backgroundColor: Colors.navigationBackgroundColor }}
      >
        <Text style={styles.buttonText}>Apply</Text>
      </TouchableOpacity>
    </View>
  </View>
);

FilterContainer.propTypes = {
  filters: PropTypes.arrayOf(PropTypes.any),
  activeFilter: PropTypes.number,
  onPressByFilter: PropTypes.func,
  onPressFilterOption: PropTypes.func,
  selectedFilterOption: PropTypes.objectOf(PropTypes.any),
  onApplyFilter: PropTypes.func,
  onPressClearAll: PropTypes.func,
};

FilterContainer.defaultProps = {
  filters: [],
  activeFilter: -1,
  onPressByFilter: undefined,
  onPressFilterOption: undefined,
  selectedFilterOption: {},
  onApplyFilter: undefined,
  onPressClearAll: undefined,
};

export default FilterContainer;
