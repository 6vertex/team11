import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
  StatusBar,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import UpdatePasswordContainer from './components/ContainerView';
import Loader from '../../components/Loader';
import { constant, resetRoute } from '../../utils';
import Utils from '../../utils/utils';
import NavigationBar from '../../components/NavigationBar';
import { images } from '../../assets/images';

const utils = new Utils();

class UpdatePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
    };
  }

  componentDidMount() {
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      this.props.resetlogout();
      return;
    }
    if (nextProps.updatePasswordResponse.response
      && nextProps.updatePasswordResponse.status >= 200
      && nextProps.updatePasswordResponse.status <= 300) {
      Alert.alert(
        constant.PROFILE_UPDATE_SUCCESS_MSG.header,
        constant.PROFILE_UPDATE_SUCCESS_MSG.message,
      );
      this.props.navigation.goBack();
      return;
    }
    if (nextProps.updatePasswordResponse.response
      && nextProps.updatePasswordResponse.status >= 400) {
      if (nextProps.updatePasswordResponse.response.message
        && typeof nextProps.updatePasswordResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.updatePasswordResponse.response.message);
        this.clearOldPasswordText();
        return;
      }
      this.clearOldPasswordText();
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  onChangeOldPasswordText(oldPassword) {
    this.setState({ oldPassword });
  }

  onChangeNewPasswordText(newPassword) {
    this.setState({ newPassword });
  }

  onChangeConfirmPasswordText(confirmPassword) {
    this.setState({ confirmPassword });
  }

  onSubmitEditing(key) {
    try {
      switch (key) {
        case 'oldPassword': this.newPasswordInput.focus();
          break;
        case 'newPassword': this.confirmPasswordInput.focus();
          break;
        default:
          break;
      }
    } catch (error) {
      console.log(error);
    }
  }

  getTextInputReference(key, reference) {
    switch (key) {
      case 'oldPassword': this.oldPasswordInput = reference;
        break;
      case 'newPassword': this.newPasswordInput = reference;
        break;
      case 'confirmPassword': this.confirmPasswordInput = reference;
        break;
      default:
        break;
    }
  }

  clearOldPasswordText() {
    this.oldPasswordInput.focus();
    this.setState({ oldPassword: '' });
  }

  submitPress() {
    if (this.state.oldPassword === '' || this.state.oldPassword.length < 6) {
      Alert.alert(
        'Message',
        'Please enter your old password of length more than 6 characters.',
      );
      return;
    }

    if (this.state.newPassword === '' || this.state.newPassword.length < 6) {
      Alert.alert(
        'Message',
        'Please enter your new password of length more than 6 characters.',
      );
      return;
    }

    if (this.state.oldPassword === this.state.newPassword) {
      Alert.alert(
        'Message',
        'New password can\'t be same as old one.',
      );
      return;
    }

    if (this.state.confirmPassword !== this.state.newPassword) {
      this.setState({ confirmPassword: '' });
      Alert.alert(
        'Message',
        'New and Confirm Password is not matching.',
      );
      return;
    }

    utils.checkInternetConnectivity((reach) => {
      if (reach) {
        utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.updatePassword(
              accessToken,
              this.state.oldPassword,
              this.state.newPassword,
            );
          } else {
            Alert.alert(
              constant.loadingError.header,
              constant.loadingError.message,
            );
          }
        });
      } else {
        Alert.alert(
          constant.noInternetConnection.header,
          constant.noInternetConnection.message,
        );
      }
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          backgroundColor="#0C0D18"
          barStyle="light-content"
        />
        <NavigationBar
          title="Update Password"
          showBackButton={Boolean(true)}
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />

        <UpdatePasswordContainer
          oldPasswordText={this.state.oldPassword}
          newPasswordText={this.state.newPassword}
          confirmPasswordText={this.state.confirmPassword}
          onChangeOldPasswordText={oldPassword => this.onChangeOldPasswordText(oldPassword)}
          onChangeNewPasswordText={newPassword => this.onChangeNewPasswordText(newPassword)}
          onChangeConfirmPasswordText={confirmPassword =>
            this.onChangeConfirmPasswordText(confirmPassword)}
          getTextInputReference={(key, reference) => this.getTextInputReference(key, reference)}
          onSubmitEditing={key => this.onSubmitEditing(key)}
          submitPress={() => this.submitPress()}
        />

        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

UpdatePassword.propTypes = {
  isLoading: PropTypes.bool,
  updatePasswordResponse: PropTypes.objectOf(PropTypes.any),
  navigation: PropTypes.objectOf(PropTypes.any),
  updatePassword: PropTypes.func,
};

UpdatePassword.defaultProps = {
  isLoading: false,
  updatePasswordResponse: {},
  navigation: {},
  updatePassword: () => {},
};

const mapStateToProps = state => ({
  isSessionExpired: state.sessionExpire.isSessionExpired,
  isLoading: state.updatePassword.isLoading,
  updatePasswordResponse: state.updatePassword.updatePasswordResponse,

});

const mapDispatchToProps = () => UserActions;

const UpdatePasswordScreen = connect(mapStateToProps, mapDispatchToProps)(UpdatePassword);

export default UpdatePasswordScreen;
