import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  ScrollView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../../components/CustomTextInput';
import { images } from '../../../assets/images';
import { isIOS } from '../../../utils/PlatformSpecific'

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  containerImage: {
    width,
    height,
  },
  container: {
    width,
    height,
    padding: 20,
    paddingTop: 40,
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  titleStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 40,
    backgroundColor: 'transparent',
  },
  buttonTouchable: {
    marginTop: Platform.OS === 'ios' ? 18 : 10,
    marginBottom: Platform.OS === 'ios' ? 0 : 100,
  },
  buttonContainer: {
    backgroundColor: 'green',
    width: (width / 3),
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e71636',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
    // marginBottom: Platform.OS === 'ios' ? 0 : 40,
  },
  buttonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '600',
  },
});

const UpdatePasswordContainer = props => (
  <Image style={styles.containerImage} source={images.generalBackground}>
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <View style= {styles.titleStyle}>
          <Text style={{color: 'white', fontWeight: '500', fontSize: 14,}}>Your password should be minimum of 6 characters</Text>
        </View>

        <CustomTextInput
          title="Old password"
          inputKey={'oldPassword'}
          secureTextEntry={Boolean(true)}
          returnKeyType="next"
          value={props.oldPasswordText}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          onChangeText={text => props.onChangeOldPasswordText(text)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />
        
        <CustomTextInput
          title="New password"
          inputKey={'newPassword'}
          returnKeyType="next"
          secureTextEntry={Boolean(true)}
          value={props.newPasswordText}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          onChangeText={text => props.onChangeNewPasswordText(text)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />

        <CustomTextInput
          title="Confirm password"
          inputKey={'confirmPassword'}
          returnKeyType="done"
          secureTextEntry={Boolean(true)}
          value={props.confirmPasswordText}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          onChangeText={text => props.onChangeConfirmPasswordText(text)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />

        <View style={{marginTop: 40, justifyContent: 'center', alignItems: 'center',}}>
          <TouchableOpacity
            style={styles.buttonTouchable}
            activeOpacity={0.7}
            onPress={() => props.submitPress()}
          >
            <View style={styles.buttonContainer}>
              <Text style={styles.buttonText}>Update</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAwareScrollView>
  </Image>
);

UpdatePasswordContainer.propTypes = {
};

UpdatePasswordContainer.defaultProps = {
};

export default UpdatePasswordContainer;
