import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View, Alert,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import Container from './components/Container';
import { NavigationBar, StatusBar, Loader } from '../../components';
import { images } from '../../assets/images';
import {
  constant,
  resetRoute,
} from '../../utils';
import Utils from '../../utils/utils';

class EditContestName extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      contestName: props.navigation.state.params.contest.name,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }

    if (nextProps.updateContestNameResponse.response &&
      nextProps.updateContestNameResponse.status >= 200 &&
      nextProps.updateContestNameResponse.status <= 300) {
      Alert.alert('Message', nextProps.updateContestNameResponse.response.message);
      this.props.navigation.state.params.actionAfterUpdated();
      this.props.navigation.goBack();
    }
    if (nextProps.updateContestNameResponse.response &&
      nextProps.updateContestNameResponse.status >= 400) {
      if (nextProps.updateContestNameResponse.response.message &&
          typeof nextProps.updateContestNameResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.updateContestNameResponse.response.message);
        this.props.resetUpdateContestName();
        return;
      }
      this.props.resetUpdateContestName();
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  onChangeName(text) {
    this.setState({ contestName: text });
  }

  updateContestName() {
    if (this.state.contestName === '') {
      Alert.alert('Please enter a name for your contest.');
    }
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            this.props.updateContestName(
              response.access_token,
              this.state.contestName,
              this.props.navigation.state.params.contest.id,
            );
          } else {
            Alert.alert(constant.loadingError.header, constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  render() {
    const { contestName } = this.state;
    const { updateContestNameLoading } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <StatusBar />
        <NavigationBar
          title="Edit Contest Name"
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <Container
          contestName={contestName}
          onChangeName={text => this.onChangeName(text)}
          updateContestName={() => this.updateContestName()}
        />
        {updateContestNameLoading &&
          <Loader isAnimating={updateContestNameLoading} />
        }
      </View>
    );
  }
}

EditContestName.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  isSessionExpired: PropTypes.bool,
  updateContestNameResponse: PropTypes.objectOf(PropTypes.any),
  updateContestNameLoading: PropTypes.bool,
  updateContestName: PropTypes.func,
};

EditContestName.defaultProps = {
  navigation: {},
  isSessionExpired: false,
  updateContestNameResponse: {},
  updateContestNameLoading: false,
  updateContestName: () => {},
};

const mapStateToProps = state => ({
  // Session Expire
  isSessionExpired: state.sessionExpire.isSessionExpired,

  // Edit Contest Name
  updateContestNameLoading: state.contestManagement.updateContestNameLoading,
  updateContestNameResponse: state.contestManagement.updateContestNameResponse,
});

const mapDispatchToProps = () => UserActions;

const EditContestNameScreen = connect(mapStateToProps, mapDispatchToProps)(EditContestName);


export default EditContestNameScreen;
