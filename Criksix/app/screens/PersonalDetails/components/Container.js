import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../../components/CustomTextInput';
import { images } from '../../../assets/images';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  backgroundImage: {
    width,
    height,
    resizeMode: 'cover',
  },
  flexOne: {
    flex: 1,
    width,
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  logo: {
    width: width * 0.44,
    height: 44,
    marginTop: height * 0.041,
    resizeMode: 'contain',
  },
  headerView: {
    width: (width / 3) * 2.5,
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 20,
    marginBottom: 8,
  },
  headerText: {
    color: 'white',
    fontSize: 22,
    fontWeight: '500',
    textAlign: 'left',
  },
  forgotPasswordView: {
    width: (width / 3) * 2.5,
    height: 30,
    marginTop: 0,
    marginBottom: 5,
    alignItems: 'flex-end',
  },
  forgotPasswordButton: {
    padding: 8,
    paddingRight: 0,
  },
  forgotPasswordText: {
    color: 'white',
    fontSize: 12,
    fontWeight: '500',
    textAlign: 'right',
  },

  buttonTouchable: {
    marginTop: Platform.OS === 'ios' ? 18 : 10,
    marginBottom: Platform.OS === 'ios' ? 0 : 100,
  },
  buttonContainer: {
    width: (width / 3),
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e71636',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
    // marginBottom: Platform.OS === 'ios' ? 0 : 40,
  },
  buttonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '600',
    textAlign: 'center',
  },
  orText: {
    marginVertical: 3,
    color: 'white',
    fontSize: 20,
    fontWeight: '400',
    textAlign: 'center',
  },
  socialButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: (width / 3) * 2.5,
  },
  socialImage: {
    width: width * 0.40,
    height: 50,
    resizeMode: 'contain',
  },
  authBottom: {
    position: 'absolute',
    width,
    height: 135,
    bottom: 0,
    left: 0,
    resizeMode: 'cover',
  },
  signUpTouchable: {
    padding: 8,
    position: 'absolute',
    bottom: 25,
    right: 10,
  },
  signUpImage: {
    width: 50,
    height: 50,
    resizeMode: 'cover',
  },
  changePasswordText: {
    color: 'white',
    fontSize: 13,
    fontWeight: '600',
  },
  genderView: {
    width: (width / 3) * 2.5,
    backgroundColor: 'transparent',
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginVertical: 8,
    paddingBottom: 8,
  },
  genderSeperatorView: {
    width: 25,
  },
  genderHeader: {
    color: 'white',
    fontSize: 15,
    fontWeight: '400',
    textAlign: 'left',
  },
  genderButtonsContainer: {
    flexDirection: 'row',
  },
  genderButtonTouchable: {
    marginTop: 10,
    marginHorizontal: 8,
  },
  genderButtonContainer: {
    width: (width / 3),
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
  },
  genderButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '600',
    textAlign: 'center',
  },
  selectedGenderButtonContainer: {
    width: (width / 3),
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
  },
  selectedGenderButtonText: {
    color: 'black',
    fontSize: 15,
    fontWeight: '600',
    textAlign: 'center',
  },
});

const genderView = (gender, setGender) => {
  return (
    <View style={styles.genderView}>
      <Text style={styles.genderHeader}>{'Gender:'}</Text>
      <View style={styles.genderButtonsContainer}>
        <TouchableOpacity
          style={styles.genderButtonTouchable}
          activeOpacity={0.7}
          onPress={() => setGender('male')}
        >
          <View style={gender === 'male' ? styles.selectedGenderButtonContainer : styles.genderButtonContainer}>
            <Text style={gender === 'male' ? styles.selectedGenderButtonText : styles.genderButtonText}>Male</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.genderButtonTouchable}
          activeOpacity={0.7}
          onPress={() => setGender('female')}
        >
          <View style={gender === 'female' ? styles.selectedGenderButtonContainer : styles.genderButtonContainer}>
            <Text style={gender === 'female' ? styles.selectedGenderButtonText : styles.genderButtonText}>Female</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const PersonalDetailsContainer = props => (
  <Image style={styles.backgroundImage} source={images.generalBackground}>
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <CustomTextInput
          title="First Name"
          inputKey={'firtsName'}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          autoCapitalize="sentences"
          value={props.firstNameText}
          returnKeyType="next"
          onChangeText={text => props.onChangeFirstName(text)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />

        <CustomTextInput
          title="Last Name"
          inputKey={'lastName'}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          autoCapitalize="sentences"
          value={props.lastNameText}
          returnKeyType="next"
          onChangeText={text => props.onChangeLastName(text)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />
        <CustomTextInput
          title="Email"
          inputKey={'email'}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          keyboardType="email-address"
          value={props.emailText}
          editable={Boolean(false)}
          returnKeyType="next"
          onChangeText={text => props.onChangeEmailText(text)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => props.onchangeButtonPress()}
        >
          <View style={{flexDirection: 'row', }}>
            <View style={{marginLeft: 5, width: width * 0.72}}>
              <CustomTextInput
                title="Password"
                inputKey={'password'}
                getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
                secureTextEntry={Boolean(true)}
                editable={Boolean(false)}
                value={props.passwordText}
                returnKeyType="next"
                onFocus={() => props.onchangeButtonPress()}
                onChangeText={text => props.onChangePasswordText(text)}
                onSubmitEditing={key => props.onSubmitEditing(key)}
              />
            </View>
            <View style={{marginTop: 30, height: 30 }}>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => props.onchangeButtonPress()}
              >
                <View>
                  <Text style={styles.changePasswordText}>Change</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
        {genderView(props.gender, props.setGender)}
        <TouchableOpacity
          style={styles.buttonTouchable}
          activeOpacity={0.7}
          onPress={() => props.submitPress()}
        >
          <View style={styles.buttonContainer}>
            <Text style={styles.buttonText}>Update</Text>
          </View>
        </TouchableOpacity>
      </View>
    </KeyboardAwareScrollView>
  </Image>
);

PersonalDetailsContainer.propTypes = {
};

PersonalDetailsContainer.defaultProps = {
};

export default PersonalDetailsContainer;
