import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
  StatusBar,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import PersonalDetailsContainer from './components/Container';
import { Loader, NavigationBar } from '../../components';
import { constant, resetRoute } from '../../utils';
import Utils from '../../utils/utils';
import { images } from '../../assets/images';
import { containerStyles } from '../../theme';

const utils = new Utils();

class PersonalDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: props.userProfileResponse.response.first_name,
      lastName: props.userProfileResponse.response.last_name,
      email: props.userProfileResponse.response.email,
      password: '********',
      gender: props.userProfileResponse.response.gender,
      address: props.userProfileResponse.response.city,
      state: props.userProfileResponse.response.state,
      country: props.userProfileResponse.response.country_alpha2,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }
    if (nextProps.updateUserProfileResponse.response
      && nextProps.updateUserProfileResponse.status >= 200
      && nextProps.updateUserProfileResponse.status <= 300) {
      Alert.alert(
        constant.PROFILE_UPDATE_SUCCESS_MSG.header,
        constant.PROFILE_UPDATE_SUCCESS_MSG.message,
      );
      return;
    }
    if (nextProps.updateUserProfileResponse.response
      && nextProps.updateUserProfileResponse.status >= 400) {
      if (nextProps.updateUserProfileResponse.response.message
        && typeof nextProps.updateUserProfileResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.updateUserProfileResponse.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  componentWillUnmount() {
    this.updateProfileData();
  }

  onChangeFirstName(firstName) {
    this.setState({ firstName });
  }

  onChangeLastName(lastName) {
    this.setState({ lastName });
  }

  onChangeEmailText(email) {
    this.setState({ email });
  }

  onChangePasswordText(password) {
    this.setState({ password });
  }

  onChangeAddressText(address) {
    this.setState({ address });
  }

  onChangeStateText(state) {
    this.setState({ state });
  }

  onChangeCountryText(country) {
    this.setState({ country });
  }

  onChangeConfirmPasswordText(confirmPassword) {
    this.setState({ confirmPassword });
  }

  onSubmitEditing(key) {
    try {
      switch (key) {
        case 'firtsName':
          this.lastNameInput.focus();
          break;
        case 'lastName':
          this.emailInput.focus();
          break;
        case 'password':
          this.addressInput.focus();
          break;
        case 'address':
          this.stateInput.focus();
          break;
        case 'state':
          this.countryInput.focus();
          break;
        case 'country':
          break;
        default:
          break;
      }
    } catch (error) {
      console.log('=====>>> error: ', error);
    }
  }

  setGender(gender) {
    this.setState({ gender });
  }

  getTextInputReference(key, reference) {
    switch (key) {
      case 'firtsName':
        this.firtsNameInput = reference;
        break;
      case 'lastName':
        this.lastNameInput = reference;
        break;
      case 'password':
        this.passwordInput = reference;
        break;
      case 'address':
        this.addressInput = reference;
        break;
      case 'state':
        this.stateInput = reference;
        break;
      case 'country':
        this.countryInput = reference;
        break;
      default:
        break;
    }
  }

  changeButtonPress() {
    this.props.navigation.navigate(constant.screens.UPDATE_PASSWORD);
  }

  submitPress() {
    if (this.state.firstName === '') {
      Alert.alert(
        'Message',
        'Please enter First Name.',
      );
      return;
    }
    if (this.state.lastName === '') {
      Alert.alert(
        'Message',
        'Please enter Last Name.',
      );
      return;
    }
    if (this.state.address === '') {
      Alert.alert(
        'Message',
        'Please enter address.',
      );
      return;
    }
    if (this.state.state === '') {
      Alert.alert(
        'Message',
        'Please enter state.',
      );
      return;
    }
    if (this.state.country === '') {
      Alert.alert(
        'Message',
        'Please enter country.',
      );
      return;
    }

    utils.checkInternetConnectivity((reach) => {
      if (reach) {
        utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.updateUserProfile(
              accessToken,
              this.state.firstName,
              this.state.lastName,
              this.state.gender,
              this.state.address,
              this.state.state,
              this.state.country,
            );
          } else {
            Alert.alert(
              constant.loadingError.header,
              constant.loadingError.message,
            );
          }
        });
      } else {
        Alert.alert(
          constant.noInternetConnection.header,
          constant.noInternetConnection.message,
        );
      }
    });
  }

  invitedByFriend() {
    this.props.navigation.navigate(constant.screens.INVITATION);
  }

  goToTermsAndCondition() {
    this.props.navigation.navigate(constant.screens.TERMS_AND_CONDITION);
  }

  updateProfileData() {
    utils.checkInternetConnectivity((reach) => {
      if (reach) {
        utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.getUserProfile(accessToken);
          } else {
            Alert.alert(
              constant.loadingError.header,
              constant.loadingError.message,
            );
          }
        });
      } else {
        Alert.alert(
          constant.noInternetConnection.header,
          constant.noInternetConnection.message,
        );
      }
    });
  }

  render() {
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar
          backgroundColor="#0C0D18"
          barStyle="light-content"
        />
        <NavigationBar
          title="Personal Details"
          showBackButton={Boolean(true)}
          backButtonImage = {images.backIcon}
          backButtonAction = {() => this.props.navigation.goBack()}
        />
        <PersonalDetailsContainer
          getTextInputReference={(key, reference) => this.getTextInputReference(key, reference)}
          firstNameText={this.state.firstName}
          onChangeFirstName={firstName => this.onChangeFirstName(firstName)}
          lastNameText={this.state.lastName}
          onChangeLastName={lastName => this.onChangeLastName(lastName)}
          emailText={this.state.email}
          passwordText={this.state.password}
          onChangePasswordText={password => this.onChangePasswordText(password)}
          gender={this.state.gender}
          setGender={gender => this.setGender(gender)}
          addressText={this.state.address}
          onChangeAddressText={text => this.onChangeAddressText(text)}
          stateText={this.state.state}
          onChangeStateText={text => this.onChangeStateText(text)}
          countryText={this.state.country}
          onChangeCountryText={text => this.onChangeCountryText(text)}
          submitPress={() => this.submitPress()}
          popBack={() => this.props.navigation.goBack()}
          invitedByFriend={() => this.invitedByFriend()}
          goToTermsAndCondition={() => this.goToTermsAndCondition()}
          onchangeButtonPress={() => this.changeButtonPress()}
          onSubmitEditing={key => this.onSubmitEditing(key)}
        />
        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

PersonalDetails.propTypes = {
  isLoading: PropTypes.bool,
  registerUserResponse: PropTypes.objectOf(PropTypes.any),
  navigation: PropTypes.objectOf(PropTypes.any),
  updateUserProfile: PropTypes.func,
};

PersonalDetails.defaultProps = {
  isLoading: false,
  registerUserResponse: {},
  navigation: {},
  updateUserProfile: () => {},
};

const mapStateToProps = state => ({
  isSessionExpired: state.sessionExpire.isSessionExpired,
  isLoading: state.updateUserProfile.isLoading,
  updateUserProfileResponse: state.updateUserProfile.updateUserProfileResponse,
  userProfileResponse: state.userProfile.userProfileResponse,
});

const mapDispatchToProps = () => UserActions;

const PersonalDetailsScreen = connect(mapStateToProps, mapDispatchToProps)(PersonalDetails);

export default PersonalDetailsScreen;
