import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import SubmitTeamLineUpContainer from './components/Container';
import { NavigationBar, StatusBar, Loader } from '../../components';
import { images } from '../../assets/images';
import {
  constant,
  resetRoute,
} from '../../utils';
import Utils from '../../utils/utils';
import { containerStyles } from '../../theme/index';

class SubmitTeamLineUp extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      teamName: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
    }

    // handle submit Team Line-Up response
    if (nextProps.lineupResponse.response &&
      nextProps.lineupResponse.status >= 200 &&
      nextProps.lineupResponse.status <= 300) {
      this.goToLobby();
      return;
    }
    if (nextProps.lineupResponse.response &&
        nextProps.lineupResponse.status >= 400) {
      if (nextProps.lineupResponse.response.message &&
          typeof nextProps.lineupResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.lineupResponse.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  submitTeamLineUp() {
    if (this.state.teamName === '') {
      Alert.alert('Please enter a Team Name');
      return;
    }

    const playerLineup = this.props.navigation.state.params.teamLineUp;
    const lineup_id = this.props.navigation.state.params.contest_user_lineup;
    const players = [];
    for (let i = 0; i < playerLineup.length; i++) {
      players.push(playerLineup[i].id);
    }

    const { contest_id, create } = this.props.navigation.state.params;

    if (create === undefined || create === false) {
      this.utils.checkInternetConnectivity((reach) => {
        if (reach) {
          this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
            if (response && response.access_token) {
              const accessToken = response.access_token;
              this.props.submitTeamLineup(
                accessToken,
                this.state.teamName,
                lineup_id,
                players,
              );
            } else {
              Alert.alert('Message', constant.loadingError.message);
            }
          });
        } else {
          Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
        }
      });
      return;
    }

    if (create === true) {
      this.utils.checkInternetConnectivity((reach) => {
        if (reach) {
          this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
            if (response && response.access_token) {
              const accessToken = response.access_token;
              this.props.createTeamLineup(
                accessToken,
                this.state.teamName,
                contest_id,
                players,
              );
            } else {
              Alert.alert('Message', constant.loadingError.message);
            }
          });
        } else {
          Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
        }
      });
    }
  }

  goToLobby() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'DrawerStack' }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar />
        <NavigationBar
          title="SUBMIT LINEUP"
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <SubmitTeamLineUpContainer
          handleNameChange={txt => this.setState({ teamName: txt })}
          teamName={this.state.teamName}
          lineup={this.props.navigation.state.params.teamLineUp}
          submitTeamLineUp={() => this.submitTeamLineUp()}
        />
        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

SubmitTeamLineUp.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  lineupResponse: PropTypes.objectOf(PropTypes.any),
  isSessionExpired: PropTypes.bool,
  isLoading: PropTypes.bool,
  submitTeamLineup: PropTypes.func,
  resetlogout: PropTypes.func,
};

SubmitTeamLineUp.defaultProps = {
  navigation: {},
  lineupResponse: {},
  isSessionExpired: false,
  isLoading: false,
  submitTeamLineup: () => {},
  resetlogout: () => {},
};

const mapStateToProps = state => ({
  // Session Expire
  isSessionExpired: state.sessionExpire.isSessionExpired,
  isLoading: state.teamManagement.isLoading,
  lineupResponse: state.teamManagement.lineupResponse,
});

const mapDispatchToProps = () => UserActions;

const SubmitTeamLineUpScreen = connect(mapStateToProps, mapDispatchToProps)(SubmitTeamLineUp);

export default SubmitTeamLineUpScreen;
