import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Dimensions,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { images } from '../../../assets/images';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width,
    alignSelf: 'stretch',
    backgroundColor: '#080913',
  },
  listHeader: {
    alignSelf: 'stretch',
    paddingVertical: 10,
    paddingHorizontal: 15,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#6970a2',
  },
  listHeaderText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '600',
    textAlign: 'center',
  },
  listRow: {
    backgroundColor: '#0c0d18',
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginBottom: 5,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: 'white',
    borderBottomWidth: 1,
  },
  rowLeftView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowRightView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  playerAvatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  playerNameText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
  },
  playerPointText: {
    color: '#6970a2',
    fontSize: 15,
    fontWeight: '500',
    textAlign: 'center',
  },
  playerCreditText: {
    color: 'white',
    fontSize: 13,
    fontWeight: '700',
    textAlign: 'center',
  },
  bottomView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
    backgroundColor: '#e71636',
  },
  nextButtonView: {
    paddingHorizontal: 25,
    paddingVertical: 10,
    backgroundColor: '#00000070',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  nextButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '700',
    textAlign: 'center',
  },
  inputOuterContainer: {
    height: 80,
    alignSelf: 'stretch',
    paddingHorizontal: 10,
  },
  titleLabel: {
    color: 'white',
    fontSize: 12,
    textAlign: 'left',
    paddingBottom: 5,
    paddingLeft: 20,
  },
  inputContainer: {
    height: 40,
    alignSelf: 'stretch',
    backgroundColor: '#575b91',
    borderRadius: 40 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputStyles: {
    marginHorizontal: 8,
    paddingHorizontal: 15,
    backgroundColor: 'transparent',
    fontFamily: 'Arial',
    fontSize: 16,
    color: 'white',
    alignSelf: 'stretch',
    textAlign: 'left',
  },
});

const Container = props => (
  <View style={{ flex: 1 }}>
    <ScrollView>
      <View style={[styles.inputOuterContainer, { marginVertical: 10 }]}>
        <Text style={styles.titleLabel}>
          Your Team Name
        </Text>
        <View style={[styles.inputContainer]}>
          <TextInput
            value={props.teamName}
            style={styles.inputStyles}
            placeholder="Give your team a name"
            placeholderTextColor="#FFFFFF50"
            returnKeyType="next"
            onChangeText={txt => props.handleNameChange(txt)}
            underlineColorAndroid="transparent"
          />
        </View>
      </View>
      <View style={styles.listHeader}>
        <Text style={styles.listHeaderText}>Your Selected Team Lineup</Text>
      </View>
      <View
        style={{
          flex: 1,
          paddingVertical: 10,
        }}
      >
        {
          props.lineup.map((line, index) => (
            <View
              key={index}
              style={styles.listRow}
            >
              <Image style={styles.playerAvatar} source={images.profilePic} />
              <View style={styles.rowLeftView}>
                <Text style={styles.playerNameText}>{line.name ? line.name : 'Name'}</Text>
                <Text style={styles.playerPointText}>{line.position}</Text>
              </View>
              <View style={styles.rowRightView}>
                <Text style={styles.playerCreditText}>{line.team_name}</Text>
              </View>
            </View>
          ))
        }
      </View>
    </ScrollView>
    <View style={styles.bottomView}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => props.submitTeamLineUp()}
      >
        <View style={styles.nextButtonView}>
          <Text style={styles.nextButtonText}>SUBMIT</Text>
        </View>
      </TouchableOpacity>
    </View>
  </View>
);

Container.propTypes = {
  lineup: PropTypes.arrayOf(PropTypes.any),
  submitTeamLineUp: PropTypes.func,
  teamName: PropTypes.string,
};

Container.defaultProps = {
  lineup: [],
  submitTeamLineUp: () => {},
  teamName: '',
};

export default Container;
