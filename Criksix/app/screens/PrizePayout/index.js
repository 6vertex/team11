/* eslint radix: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ContestActions from '../../actions';
import { images } from '../../assets/images';
import {
  NavigationBar,
  StatusBar,
  LabeledSwitch,
  Loader,
  DropDownPicker,
  ConfirmationPopUp,
} from '../../components';
import { dimensions, Colors } from '../../theme';
import { constant, isResponseValidated, isResponseSuccess, resetRoute } from '../../utils';
import Utils from '../../utils/utils';
import GeolocationModule from '../../utils/GeolocationModule';
import PayoutPopup from '../SelectMatch//components/PayoutPopup';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: dimensions.getViewportWidth(),
    backgroundColor: 'rgb(10, 10, 15)',
  },
  bodyContainer: {
    paddingBottom: 20,
    flex: 1,
    width: dimensions.getViewportWidth(),
  },
  blcNotifierContainer: {
    height: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    alignItems: 'center',
    backgroundColor: Colors.greenShadeColor,
  },
  body: {
    backgroundColor: 'rgb(14, 15, 30)',
    marginVertical: 10,
    marginHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 5,
  },
  inputOuterContainer: {
    height: 80,
    alignSelf: 'stretch',
    paddingHorizontal: 10,
  },
  titleLabel: {
    color: 'white',
    fontSize: 12,
    textAlign: 'left',
    paddingBottom: 5,
    paddingLeft: 20,
  },
  inputContainer: {
    height: 40,
    alignSelf: 'stretch',
    backgroundColor: '#575b91',
    borderRadius: 40 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputStyles: {
    marginHorizontal: 8,
    paddingHorizontal: 15,
    backgroundColor: 'transparent',
    fontFamily: 'Arial',
    fontSize: 16,
    color: 'white',
    alignSelf: 'stretch',
    textAlign: 'left',
  },
  dropdownPicker: {
    backgroundColor: 'rgb(107, 113, 159)',
    marginTop: 50,
    alignItems: 'center',
  },
  bottomView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 8,
    backgroundColor: '#e71636',
  },
  pageIndicatorContainer: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  pageIndicator: {
    color: 'white',
    fontSize: 16,
    fontWeight: '400',
    textAlign: 'center',
  },
  nextButtonView: {
    paddingHorizontal: 25,
    paddingVertical: 10,
    backgroundColor: '#00000070',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  nextButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '700',
    textAlign: 'center',
  },
  viewPayout: {
    fontSize: 14,
    color: 'orange',
    textDecorationLine: 'underline',
    alignSelf: 'flex-end',
    paddingTop: 10,
    padding: 20,
  },
  switchContainer: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    height: 50,
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    paddingHorizontal: 15,
  },
  switchContainerTxt: {
    color: 'white',
    fontSize: 12,
    paddingRight: 20,
  },
  picker: {
    height: 40,
    alignSelf: 'stretch',
    backgroundColor: '#575b91',
    borderRadius: 40 / 2,
  },
  pickerText: {
    color: 'white',
    marginTop: -5,
    marginLeft: 5,
  },
  joiningAmountTxt: {
    color: Colors.buttonColor,
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '600',
  },
  entryFeesSymbol: {
    width: 40,
    height: 40,
    paddingTop: 6,
    backgroundColor: 'white',
    borderRadius: 20,
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '600',
    alignItems: 'center',
    justifyContent: 'center',
  },
  blcText: {
    color: 'white',
    fontSize: 14,
  },
  selectionContainer: {
    height: 45,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: 0,
  },
  selectionButton: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'white',
    alignSelf: 'stretch',
    marginHorizontal: 10,
    justifyContent: 'center',
  },
  selectionButtonTxt: {
    color: 'white',
    fontSize: 14,
    alignSelf: 'center',
  },
});

const defaultPS = {
  title: '--Select Winnings--',
  id: -1,
};

let currentLocationID = 0; // 0 for india, 1 for usa and 2 for others.

class PrizePayout extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      selectedFeesTypeID: -1,
      selectedCurrencyTypeID: -1,
      entryFee: '0',
      showPayoutContainer: false,
      showMultiEntryCountInput: false,
      multiEntryCount: '1',
      selectedPrizeStructureID: defaultPS.id,
      showConfirmationPopUp: false,
    };
  }

  componentDidMount() {
    GeolocationModule.findCurrentLocation((success, error) => {
      if (success !== null) {
        currentLocationID = success.location_code;
      }
      if (error !== null) Alert.alert(error.message);
    });
  }

  componentWillReceiveProps(nextProps) {
    const { createContestResponse } = nextProps;
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }
    if (isResponseValidated(createContestResponse)) {
      if (isResponseSuccess(nextProps.createContestResponse)) {
        const resetAction = NavigationActions.reset({
          index: 1,
          actions: [
            NavigationActions.navigate({ routeName: constant.screens.DRAWER_STACK }),
            NavigationActions.navigate({
              routeName: constant.screens.CREATE_TEAM,
              params: {
                contest_user_lineup: createContestResponse.response.contest_user_lineups_id,
                contest_id: createContestResponse.response.id,
                contest_name: createContestResponse.response.name,
                invite_code: createContestResponse.response.invite_code,
              },
            }),
          ],
        });
        this.props.navigation.dispatch(resetAction);
        // this.props.resetCreateContestResponse();
      } else {
        if (nextProps.createContestResponse.response.message
          && typeof nextProps.createContestResponse.response.message === 'string') {
          Alert.alert('Message', nextProps.createContestResponse.response.message);
          return;
        }
        Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
      }
    }
  }

  onSubmit() {
    const {
      entryFee,
      selectedPrizeStructureID,
      multiEntryCount,
      selectedFeesTypeID,
      selectedCurrencyTypeID,
    } = this.state;
    if (this.state.entryFee > this.props.currentBalance) {
      // this.props.navigation.navigate(constant.screens.MY_ACCOUNT, { screenName: 'PrizePayout' });
    } else {
      const contest = JSON.parse(JSON.stringify(this.props.navigation.state.params.contest));
      contest.entry_fee = Number.parseInt(entryFee);
      contest.prize_id = Number.parseInt(selectedPrizeStructureID);
      contest.multi_entry_count = Number.parseInt(multiEntryCount);
      contest.entry_fee_type = Number.parseInt(selectedFeesTypeID);
      contest.currency_type = Number.parseInt(selectedCurrencyTypeID);
      this.setState({
        showConfirmationPopUp: false,
      });
      this.utils.checkInternetConnectivity((reach) => {
        if (reach) {
          this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
            if (response && response.access_token) {
              const accessToken = response.access_token;
              this.props.createContest(accessToken, contest);
            } else {
              Alert.alert('Message', constant.loadingError.message);
            }
          });
        } else {
          Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
        }
      });
    }
  }

  getEntryFeeAvailability() {
    const { selectedFeesTypeID, entryFee, selectedCurrencyTypeID } = this.state;
    if (selectedFeesTypeID === 0) {
      return (
        <Text
          style={[styles.entryFeesSymbol, {
            color: entryFee < this.props.crikSixPoints ? 'green' : 'red',
          }]}
        >★
        </Text>
      );
    } else if (selectedFeesTypeID === 1) {
      if (selectedCurrencyTypeID === 2) {
        return (
          <Text
            style={[styles.entryFeesSymbol, {
              color: entryFee < this.props.btcAmount ? 'green' : 'red',
            }]}
          >Ƀ
          </Text>
        );
      } else if (selectedCurrencyTypeID === 3) {
        return (
          <Text
            style={[styles.entryFeesSymbol, {
              color: entryFee < this.props.ethAmount ? 'green' : 'red',
            }]}
          >Ξ
          </Text>
        );
      } else if (selectedCurrencyTypeID === 1) {
        return (
          <Text
            style={[styles.entryFeesSymbol, {
              color: entryFee < this.props.currentBalance ? 'green' : 'red',
            }]}
          >$
          </Text>
        );
      }
      return (
        <Text
          style={[styles.entryFeesSymbol, {
            color: entryFee < this.props.currentBalance ? 'green' : 'red',
          }]}
        >₹
        </Text>
      );
    }
    return null;
  }

  showPrizePayout() {
    if (this.state.selectedPrizeStructureID === -1) {
      Alert.alert('Message', 'Please select a winning structure');
      return;
    }
    if (this.state.entryFee === undefined || this.state.entryFee === '') {
      // TODO: Handle entry fees min entry
      Alert.alert('Message', 'Please select valid entry fees');
      return;
    }
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            const { contest } = this.props.navigation.state.params;
            const body = {
              contest_type: contest.contest_type,
              max_teams: contest.max_teams,
              entry_fee: this.state.entryFee,
              prize_structure: this.state.selectedPrizeStructureID,
            };
            this.props.getPayoutStructure(accessToken, body);
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(
          constant.noInternetConnection.header,
          constant.noInternetConnection.message,
        );
      }
    });
    this.setState({ showPayoutContainer: true });
  }

  calculatePrizeStructure() {
    let calculatedPrizeStructures = [];
    const activeSize = Number.parseInt(this.props.navigation.state.params.contest.max_teams);
    if (activeSize > 10) {
      calculatedPrizeStructures = JSON.parse(JSON.stringify(this.props.prizeStructures));
    } else if (activeSize <= 10 && activeSize >= 6) {
      calculatedPrizeStructures.push(this.props.prizeStructures[0]);
      calculatedPrizeStructures.push(this.props.prizeStructures[1]);
      calculatedPrizeStructures.push(this.props.prizeStructures[2]);
      calculatedPrizeStructures.push(this.props.prizeStructures[3]);
    } else if (activeSize <= 5 && activeSize >= 4) {
      calculatedPrizeStructures.push(this.props.prizeStructures[0]);
      calculatedPrizeStructures.push(this.props.prizeStructures[1]);
      calculatedPrizeStructures.push(this.props.prizeStructures[2]);
    } else if (activeSize === 3) {
      calculatedPrizeStructures.push(this.props.prizeStructures[0]);
      calculatedPrizeStructures.push(this.props.prizeStructures[1]);
    } else if (activeSize === 2) {
      calculatedPrizeStructures.push(this.props.prizeStructures[0]);
    }
    return calculatedPrizeStructures;
  }

  handleChange(key, value) {
    if (key === 'selectedFeesTypeID') {
      if (value === -1) {
        this.setState({
          selectedCurrencyTypeID: value,
          [key]: value,
        });
        return;
      }
      this.setState({
        selectedCurrencyTypeID: currentLocationID,
        [key]: value,
      });
    }
    this.setState({
      [key]: value,
    });
  }

  handleCreate() {
    const {
      selectedPrizeStructureID,
      multiEntryCount,
    } = this.state;
    if (selectedPrizeStructureID === -1) {
      Alert.alert('Message', 'Please select a winning breakup');
      return;
    }
    if (multiEntryCount >= this.props.navigation.state.params.contest.max_teams
      || multiEntryCount < 1 || multiEntryCount > 10) {
      Alert.alert('Message', 'Multiple team entry with this size is not valid');
      return;
    }
    this.setState({
      showConfirmationPopUp: true,
    });
  }

  renderPrizeStructure(availablePS) {
    const prizeStructure = [];
    prizeStructure.push(
      <DropDownPicker.Item
        key={-1}
        label={defaultPS.title}
        value={defaultPS.id}
      />
    );

    for (let i = 0; i < availablePS.length; i++) {
      prizeStructure.push(
        <DropDownPicker.Item
          key={i}
          label={availablePS[i].title}
          value={availablePS[i].id}
        />
      );
    }
    return prizeStructure;
  }

  renderCryptoSelection() {
    const { selectedCurrencyTypeID } = this.state;
    return (
      <View style={styles.inputOuterContainer}>
        <Text style={styles.titleLabel}> Select a crypto </Text>
        <View style={styles.selectionContainer}>
          <TouchableOpacity
            onPress={() => this.setState({ selectedCurrencyTypeID: 2 })}
            style={[styles.selectionButton, {
              backgroundColor: selectedCurrencyTypeID === 2 ?
                'white' : 'transparent',
            }]}
          >
            <Text
              style={[styles.selectionButtonTxt, {
                color: selectedCurrencyTypeID === 2 ?
                  'rgb(10, 10, 15)' : 'white',
              }]}
            >BTC
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.setState({ selectedCurrencyTypeID: 3 })}
            style={[styles.selectionButton, {
              backgroundColor: selectedCurrencyTypeID === 3 ?
                'white' : 'transparent',
            }]}
          >
            <Text
              style={[styles.selectionButtonTxt, {
                color: selectedCurrencyTypeID === 3 ?
                  'rgb(10, 10, 15)' : 'white',
              }]}
            >ETH
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderEntryFeesTypes(entryFeesTypes) {
    const entryTypes = [];
    entryTypes.push(<DropDownPicker.Item
      key={-1}
      label={'--Select entry type--'}
      value={-1}
    />);

    for (let i = 0; i < entryFeesTypes.length; i += 1) {
      entryTypes.push(<DropDownPicker.Item
        key={i}
        label={entryFeesTypes[i]}
        value={i}
      />);
    }
    return entryTypes;
  }

  render() {
    const isBalSufficient = this.state.entryFee < this.props.currentBalance;
    const availableEntryTypes = Array.from(Object.keys(this.props.entryFeesTypes));
    const currency = (this.state.selectedFeesTypeID !== -1 && this.state.selectedFeesTypeID === 0) ?
      'PTS' : Array.from(Object.keys(this.props.currencyTypes))[this.state.selectedCurrencyTypeID];
    return (
      <View style={styles.container}>
        <StatusBar />
        <NavigationBar
          title={'CREATE CONTEST'}
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <View style={styles.bodyContainer}>
          <View style={styles.blcNotifierContainer}>
            <Text style={styles.blcText}>Availbale balance:</Text>
            <Text style={styles.blcText}>{`INR. ${this.props.currentBalance}`}</Text>
          </View>
          <KeyboardAwareScrollView
            style={[styles.body, { backgroundColor: Colors.selectedFilterColor }]}
          >

            { /* Drop down to show available entry fees type */ }
            <View style={styles.inputOuterContainer}>
              <Text style={styles.titleLabel}>
                {"What's your entry fees type"}
              </Text>
              <DropDownPicker
                selectedValue={this.state.selectedFeesTypeID}
                onValueChange={selectedEFTID =>
                  this.handleChange('selectedFeesTypeID', selectedEFTID)}
                prompt={'Choose entry fees type'}
                style={styles.picker}
                textStyle={styles.pickerText}
                cancel
              >{this.renderEntryFeesTypes(availableEntryTypes)}
              </DropDownPicker>
            </View>

            {(this.state.selectedFeesTypeID === 1 && currentLocationID > 1) &&
              this.renderCryptoSelection()}

            {this.state.selectedFeesTypeID !== -1 &&
              <View style={styles.inputOuterContainer}>
                <Text style={styles.titleLabel}>
                  Entry fees
                </Text>
                <View
                  style={[
                    styles.inputContainer,
                    {
                      flexDirection: 'row',
                      paddingRight: 0,
                    }]}
                >
                  {this.getEntryFeeAvailability()}
                  <TextInput
                    value={this.state.entryFee}
                    style={[styles.inputStyles, { flex: 1, paddingLeft: 0 }]}
                    placeholder="Entry fees"
                    placeholderTextColor="#FFFFFF50"
                    returnKeyType="next"
                    keyboardType={'numeric'}
                    onChangeText={txt => this.handleChange('entryFee', txt)}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>}

            { /* Drop down to show available prize structure */ }
            <View style={[styles.inputOuterContainer, { marginTop: 10 }]}>
              <Text style={styles.titleLabel}>
                Define winning breakup
              </Text>
              <DropDownPicker
                selectedValue={this.state.selectedPrizeStructureID}
                onValueChange={selectedPSID =>
                  this.setState({ selectedPrizeStructureID: selectedPSID })}
                prompt={'Choose winning breakup'}
                style={styles.picker}
                textStyle={styles.pickerText}
                cancel
              >
                {this.renderPrizeStructure(this.calculatePrizeStructure())}
              </DropDownPicker>
            </View>

            <View style={styles.switchContainer}>
              <Text
                style={styles.switchContainerTxt}
              >Allow friends to join with multiple teams
              </Text>
              <LabeledSwitch
                isSwitchOn={this.state.showMultiEntryCountInput}
                onValueChange={switchStatus =>
                  this.setState({ showMultiEntryCountInput: switchStatus })}
              />
            </View>
            {this.state.showMultiEntryCountInput &&
              <View style={styles.inputOuterContainer}>
                <Text style={styles.titleLabel}>
                  Multiple Team Entry (min 1 & max 10)
                </Text>
                <View style={styles.inputContainer}>
                  <TextInput
                    value={this.state.multiEntryCount}
                    style={styles.inputStyles}
                    placeholder="No. of teams"
                    placeholderTextColor="#FFFFFF50"
                    returnKeyType="next"
                    keyboardType={'numeric'}
                    onChangeText={txt => this.setState({ multiEntryCount: txt })}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>
            }
            <Text
              style={styles.viewPayout}
              onPress={() => this.showPrizePayout()}
            >Calculate Winnings Structure
            </Text>
          </KeyboardAwareScrollView>
        </View>
        {this.state.showPayoutContainer &&
          <PayoutPopup
            payout={this.props.payoutStructure}
            isLoading={this.props.isLoading}
            entryType={currency}
            onEdit={() => this.setState({ showPayoutContainer: false })}
            onClose={() => this.setState({ showPayoutContainer: false })}
          />}
        <View style={styles.bottomView}>
          <View style={styles.pageIndicatorContainer}>
            <Text style={[styles.pageIndicator, { fontSize: 18 }]}>3</Text>
            <Text style={styles.pageIndicator}> / 3</Text>
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => this.handleCreate()}
          >
            <View style={styles.nextButtonView}>
              <Text style={styles.nextButtonText}>CREATE</Text>
            </View>
          </TouchableOpacity>
        </View>
        {this.state.showConfirmationPopUp &&
          <ConfirmationPopUp
            title={'CONFIRMATION'}
            buttonLabel={isBalSufficient ? 'Create Contest' : 'Add Cash'}
            TandC={!isBalSufficient ?
              "You don't have enough BALANCE. Add enough amount to create this contest" :
              '*By Creating this contest you accept Crik-Six Tearms & Conditions'}
            buttonContainerStyle={{
              backgroundColor: isBalSufficient ? 'green' : 'red',
            }}
            onPress={() => this.onSubmit()}
            close={() => this.setState({ showConfirmationPopUp: false })}
          >
            <View style={{ alignSelf: 'center', padding: 10 }}>
              <Text style={styles.joiningAmountTxt}>Joining Amount: {currency}. {this.state.entryFee}</Text>
              <Text style={[styles.joiningAmountTxt, {
                color: isBalSufficient && currency !== 'PTS' ? 'green' : 'red',
                }]}
              >
                Current Balance: {currency !== 'PTS' ? this.props.currentBalance : 'NA'}
              </Text>
              {this.state.entryFee > 0 &&
              <Text>Joining amount will be deducted from your current balance.
                If are agreed please proceed further
              </Text>}
            </View>
          </ConfirmationPopUp>
        }
        {(!this.state.showPayoutContainer && this.props.isLoading)
          && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

PrizePayout.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  createContestResponse: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  isSessionExpired: PropTypes.bool,
  prizeStructures: PropTypes.arrayOf(PropTypes.any),
  payoutStructure: PropTypes.objectOf(PropTypes.any),
  entryFeesTypes: PropTypes.objectOf(PropTypes.any),
  currencyTypes: PropTypes.objectOf(PropTypes.any),
  payoutResponse: PropTypes.objectOf(PropTypes.any),
  contestSize: PropTypes.number,
  currentBalance: PropTypes.number,
  crikSixPoints: PropTypes.number,
  btcAmount: PropTypes.number,
  ethAmount: PropTypes.number,
  getPayoutStructure: PropTypes.func,
  createContest: PropTypes.func,
  resetCreateContestResponse: PropTypes.func,
  resetlogout: PropTypes.func,
};

PrizePayout.defaultProps = {
  navigation: {},
  createContestResponse: {},
  isLoading: true,
  isSessionExpired: true,
  prizeStructures: [],
  payoutStructure: {},
  entryFeesTypes: {},
  currencyTypes: {},
  payoutResponse: {},
  contestSize: -1,
  currentBalance: 0,
  crikSixPoints: 0,
  btcAmount: 0,
  ethAmount: 0,
  getPayoutStructure: () => {},
  createContest: () => {},
  resetCreateContestResponse: () => {},
  resetlogout: () => {},
};

const mapStateToProps = state => ({
  isSessionExpired: state.sessionExpire.isSessionExpired,
  isLoading: state.contestManagement.isLoading,
  prizeStructures: state.contestManagement.prizeStructures,
  payoutStructure: state.contestManagement.payoutStructure,
  entryFeesTypes: state.contestManagement.entryFeesTypes,
  currencyTypes: state.contestManagement.currencyTypes,
  createContestResponse: state.contestManagement.createContestResponse,
  payoutResponse: state.contestManagement.payoutResponse,
  currentBalance: state.account.currentBalance,
  crikSixPoints: state.account.crikSixPoints,
  btcAmount: state.account.btcAmount,
  ethAmount: state.account.ethAmount,
});

const mapDispatchToProps = () => ContestActions;

const PrizePayoutScreen = connect(mapStateToProps, mapDispatchToProps)(PrizePayout);

export default PrizePayoutScreen;
