import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  Alert,
  AsyncStorage,
  Dimensions,
} from 'react-native';
import {GoogleSignin} from 'react-native-google-signin';
import { Colors, labelStyles } from '../../theme';
import { images } from '../../assets/images';
import sidebarActions from '../../actions';
import MenuIconButton from './components/MenuIconButton';
import { constant, resetRoute } from '../../utils';
import Utils from '../../utils/utils';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.sidebarColor,
    paddingTop: 20,
  },
  headerContainer: {
    minHeight: 160,
    alignSelf: 'stretch',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingTop: 10,
  },
  avatarStyle: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  headerTextContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  closeButtonText: {
    ...labelStyles.defaultWhiteLabel,
  },
  blcDisplayContainer: {
    height: 40,
    backgroundColor: 'black',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  bodyStyle: {
    flex: 1,
    alignSelf: 'stretch',
  },
  blcVerifierBtn: {
    padding: 5,
    borderWidth: 1,
    borderColor: 'white',
  },
  blcVerifierBtnText: {
    ...labelStyles.defaultWhiteLabel,
    fontSize: 12,
    padding: 0,
  },
});

let utils = new Utils();
class Sidebar extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      sidebarMenues: constant.sidebarMenus,
      username: '',
      email: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      this.props.resetlogout();
      return;
    }
    const { sidebarMenusResponse, logoutUserResponse } = nextProps;
    // Handle logout response and clear all data from reducers
    if (nextProps.logoutUserResponse.response &&
      nextProps.logoutUserResponse.status >= 200 &&
      nextProps.logoutUserResponse.status <= 300) {
      this.props.resetlogout();
      AsyncStorage.removeItem(constant.USER_DETAILS)
      .then(fullfilled => {
        if(GoogleSignin.currentUserAsync().access_token) {
          GoogleSignin.signOut()
          .then((success) => console.log('Successfully logout'));
          }
          this.pushRoute('Login');
          return;
        },
        onRejected => {
          console.log('Something Went wrong');
        });
    }
    if (nextProps.logoutUserResponse.response &&
      nextProps.logoutUserResponse.status >= 400) {
      if (nextProps.logoutUserResponse.response.message &&
        typeof nextProps.logoutUserResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.logoutUserResponse.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }

    if (nextProps.userProfileResponse.response &&
        nextProps.userProfileResponse.status >= 200 &&
        nextProps.userProfileResponse.status <= 300) {
        this.setState({
          username: nextProps.userProfileResponse.response.first_name + ' ' + nextProps.userProfileResponse.response.last_name,
          email: nextProps.userProfileResponse.response.email,
        });
    }
    if (nextProps.userProfileResponse.response && nextProps.userProfileResponse.status >= 400) {
      if (nextProps.userProfileResponse.response.message && typeof nextProps.userProfileResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.userProfileResponse.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  pushRoute = (routeName) =>{
    resetRoute(this.props.navigation, routeName);
  }

  logoutUser = () => {
    utils.checkInternetConnectivity((reach) => {
      if (reach) {
        utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.userLogoutRequest(accessToken);
          } else {
            // TODO: Handle this error properly.
            Alert.alert(constant.loadingError.header, constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header,
          constant.noInternetConnection.message);
      }
    });
  }

  goTo(screenName) {
    this.props.navigation.navigate('DrawerClose');
    this.props.navigation.navigate(screenName);
  }

  handleSubmitMenu = (menuKey) => {
    const { sidebarMenues } = this.state;
    switch (menuKey) {
      case 'home':
        this.props.navigation.navigate('DrawerClose');
        break;
      case 'profile':
        this.goTo('Profile');
        break;
      case 'my_account':
        this.goTo('MyAccount');
        break;
      case 'leade_board':
        Alert.alert('Work-in-Progress', 'This will be covered in next milestone');
        break;
      case 'invite_friends':
        this.goTo(constant.screens.INVITE_FRIENDS);
        break;
      case 'my_contest':
        this.goTo('MyContest');
        break;
      case 'fantasy_points_system':
        Alert.alert('Work-in-Progress', 'This will be covered in next milestone');
        break;
      case 'help':
        Alert.alert('Work-in-Progress', 'This will be covered in next milestone')
        break;
      case 'contest_invite_code':
        break;
      case 'logout':
        this.logoutUser();
        break;
      default:
        // Do nothing
    }
  } 

  renderHeader() {
    return (
      <View style={styles.headerContainer}>
      <View style={{flexDirection: 'row', justifyContent: 'center'}}>
        <Image
          style={styles.avatarStyle}
          source={images.profilePic}
        />
      </View>
        <View style={styles.headerTextContainer}>
          <Text style={labelStyles.largeWhiteLabel}>{this.state.username}</Text>
          <Text
            style={[labelStyles.defaultWhiteLabel, { padding: 0 }]}
          >{this.state.email}
          </Text>
        </View>
      </View>
    );
  }

  render() {
    const { sidebarMenues } = this.state;
    return (
      <View style={styles.container}>
        {this.props.hasHeader && this.renderHeader()}
        <View style={styles.blcDisplayContainer}>
          <Text
            style={[labelStyles.defaultWhiteLabel, { flex: 1, paddingLeft: 0 }]}
          >Account balance ₹ {65}
          </Text>
          <TouchableOpacity
            style={styles.blcVerifierBtn}
            onPress={() => Alert.alert('WIP')}
          >
            <Text style={styles.blcVerifierBtnText}>Verify Now</Text>
          </TouchableOpacity>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.bodyStyle}>
            {
              sidebarMenues.map(menu => {
                return (
                  <MenuIconButton
                    key={`Menu-${menu.sequence_id}`}
                    label={menu.value}
                    icon={menu.image_url}
                    onMenuIconPress={() => this.handleSubmitMenu(menu.key)}
                  />
                );
              })
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

Sidebar.propTypes = {
  hasHeader: PropTypes.bool,
  navigation: PropTypes.objectOf(PropTypes.any),
};

Sidebar.defaultProps = {
  hasHeader: true,
  navigation: {},
};

const mapStateToProps = state => ({
  isSessionExpired: state.sessionExpire.isSessionExpired,
  isLoading: state.userLogout.isLoading,
  logoutUserResponse: state.userLogout.logoutUserResponse,
  userProfileResponse: state.userProfile.userProfileResponse,
});

const mapDispatchToProps = () => sidebarActions;

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
