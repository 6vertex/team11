import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import { iconStyles, labelStyles } from '../../../theme';

const styles = StyleSheet.create({
  menuIconContainer: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    alignItems: 'center',
    flexDirection: 'row',
  },
});

const MenuIconButton = props => (
  <TouchableOpacity
    style={styles.menuIconContainer}
    onPress={props.onMenuIconPress}
  >
    <Image style={iconStyles.defaultIcon} source={props.icon} />
    <Text style={labelStyles.defaultWhiteLabel}>{props.label}</Text>
  </TouchableOpacity>
);

MenuIconButton.propTypes = {
  label: PropTypes.string,
  icon: PropTypes.number,
  onMenuIconPress: PropTypes.func,
  active: PropTypes.bool,
};

MenuIconButton.defaultProps = {
  label: 'Label missing',
  icon: '',
  onMenuIconPress: undefined,
  active: false,
};

export default MenuIconButton;
