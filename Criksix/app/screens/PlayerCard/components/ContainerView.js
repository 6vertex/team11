import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  ScrollView,
  ListView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { images } from '../../../assets/images';
import { isIOS } from '../../../utils/PlatformSpecific'

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  containerImage: {
    width,
    height,
  },
  container: {
    width,
    backgroundColor: 'transparent',
  },
  backgroundImage: {
    width,
    height: 150,
    resizeMode: 'cover',
  },
  scrollStyle: {
    width,
    marginBottom: isIOS ? 0 : 40
  },
  bodyStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleStyle: {
    color: 'white',
    marginTop: 20, 
    fontSize: 18, 
    fontWeight: '500',
  },
  subTitleStyle: {
    color: 'white',
    fontSize: 18,
    fontWeight: '500',
    marginTop: 60,
    textAlign: 'center',
  },
  howItWorksStyle: {
    borderColor: '#197319',
    borderWidth: 2,
    borderRadius: 20,
    padding: 10,
  },
  inviteCodeContainer: {
    borderBottomColor: 'white',
    borderWidth: 2,
    padding: 8,
    marginTop: 12,
  },
  inviteCodeStyle: {
    color: 'white',
    fontWeight: '500', 
    fontSize: 20,
    paddingLeft: 15,
    paddingRight: 15,
  },
  shareStyle: {
    backgroundColor: '#197319',
    paddingLeft: 80,
    paddingRight: 80,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 20,
    borderRadius: 2,
  },
  playerIconStyle: {
    width: 100,
    height: 100,
    borderRadius: 25,
    position: 'absolute',
  },
  playerDetailStyle: {
    borderWidth: 1,
    borderColor: 'white',
    marginTop: 5,
    padding: 10,
    paddingLeft: 30,
    paddingRight: 30,
    borderRadius: 10,
  },
  creditTextStyle: {
    color: 'white',
    position: 'absolute',
    fontSize: 14,
    fontWeight: '300',
    padding: 20,
    paddingTop: 60,
  },
  headerTextStyle: {
    width, 
    position: 'absolute', 
    alignItems: 'center', 
    paddingTop: 10,
  },
  listHeader: {
    alignSelf: 'stretch',
    padding: 3,
    marginTop: 50,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#6970a2',
  },
  listHeaderText: {
    color: 'white',
    fontSize: 12,
    fontWeight: '600',
    textAlign: 'center',
  },
  listRow: {
    backgroundColor: '#0c0d18',
    padding: 5,
    marginBottom: 5,
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rowLeftView: {
    flex:1,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  rowRightView: {
    flex:1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  playerNameText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
  },
  playerPointText: {
    color: '#6970a2',
    fontSize: 15,
    fontWeight: '500',
    textAlign: 'center',
  },
  playerCreditText: {
    color: 'white',
    fontSize: 13,
    fontWeight: '700',
    textAlign: 'center',
  },
  playerCardButton: {
    position: 'absolute',
    width,
    backgroundColor: '#e71636',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: height-70,
    padding: 15,
  },
  playerCardButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
  },
  mainViewStyle: {
    flex: 1,
    // width,
    backgroundColor: '#080913',
  },
  backIconStyle: {
    width: 20,
    height: 20,
  },

  statsSelectionView: {
    width: width * 0.75,
    backgroundColor: '#6970a2',
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 3,
    marginTop: 10,
  },
  statsTouchable: {
    flex: 1,
    marginHorizontal: 3,
    paddingVertical: 8,
  },

  statsView: {
    width,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    paddingVertical: 5,
  },
  eventText: {
    color: 'white',
    fontSize: 18,
    fontWeight: '600',
  },
  dataRow: {
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal: 5,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  dataRowText: {
    flex: 1,
    color: 'white',
    fontSize: 12,
    fontWeight: '600',
    textAlign: 'center',
  }
});

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const playerRecord = (rowData, rowID) => (
  <TouchableOpacity
    activeOpacity={0.7}
  >
  <View style={styles.listRow}>
    <View style={styles.rowLeftView}>
      <Text style={styles.playerNameText}>KKR vs MI</Text>
      <Text style={styles.playerPointText}>Mar 04, 2018</Text>
    </View>

    <View style={styles.rowRightView}>
      <Text style={styles.playerCreditText}>40.5</Text>
    </View>

    <View style={{justifyContent: 'center', flex:1, alignItems: 'center',}}>
      <Text style={styles.playerCreditText}>0</Text>
    </View>
  </View>
  </TouchableOpacity>
);


const getPlayerAchievement = (playerData) => {
  if (playerData.scoring_type === 'fielding') {
    return (
      <View style={{ flex: 1 }}>
        <Text style={styles.dataRowText}>Catches: {playerData.catches && playerData.catches}</Text>
      </View>
    );
  }
  if (playerData.scoring_type === 'batting') {
    return (
      <View style={{ flex: 1 }}>
        <Text style={styles.dataRowText}>Runs: {playerData.runs && playerData.runs}</Text>
        <Text style={styles.dataRowText}>Strike Rate: {playerData.strike_rate && Number(playerData.strike_rate).toFixed(2)}</Text>
      </View>
    );
  }
  if (playerData.scoring_type === 'bowling') {
    return (
      <View style={{ flex: 1 }}>
        <Text style={styles.dataRowText}>Wickets: {playerData.wickets && playerData.wickets}</Text>
        <Text style={styles.dataRowText}>Economy: {playerData.economy && Number(playerData.economy).toFixed(2)}</Text>
      </View>
    );
  }
  return null;
};

const PlayerCardContainer = props => (
    <View style={styles.mainViewStyle}>
      <ScrollView style={styles.scrollStyle}>
        <View style={styles.container}>
          <Image style={styles.backgroundImage} source={images.inviteFriendsBannerImage}/>
          <View style={{justifyContent: 'center', alignItems: 'center',}}>
            <Image style={styles.playerIconStyle} source={images.profilePic}/>
          </View>
          <View style={styles.headerTextStyle}>
            <Text style={{color: 'white',fontSize: 18, fontWeight: '500'}}>PLAYER CARD</Text>
          </View>
          <Text style={styles.creditTextStyle}>Credits</Text> 
          <Text style={[styles.creditTextStyle, {paddingTop: 80,}]}>NA</Text>

          <Text style={[styles.creditTextStyle, {paddingLeft: width-100,}]}>Total Points</Text>
          <Text style={[styles.creditTextStyle, {paddingTop: 80, paddingLeft: width-80,}]}>NA</Text>

          <Text style={styles.subTitleStyle}>{props.playerProfile.full_name === null ? 'PLAYER NAME' : props.playerProfile.full_name }</Text>
          {/* <View style={styles.bodyStyle}>
            <Text style={styles.subTitleStyle}>{props.playerProfile.full_name === null ? 'PLAYER NAME' : props.playerProfile.full_name }</Text>
            <View style={styles.playerDetailStyle}>
              <View style={{flexDirection: 'row', paddingTop: 5, justifyContent: 'flex-start',}}>
              <Text style={{color: 'white'}}>BATS</Text>
              <Text style={{color: 'white', paddingLeft: 80, paddingRight: 40, textAlign: 'center'}}> - </Text>
              <Text style={{color: 'white',}}>NA</Text>
            </View>
            <View style={{flexDirection: 'row', paddingTop: 5, justifyContent: 'flex-start',}}>
              <Text style={{color: 'white'}}>BOWLS</Text>
              <Text style={{color: 'white', paddingLeft: 67, paddingRight: 40,}}> - </Text>
              <Text style={{color: 'white'}}>No </Text>
            </View>
            <View style={{flexDirection: 'row', paddingTop: 5, justifyContent: 'flex-start',}}>
              <Text style={{color: 'white'}}>NATIONALITY</Text>
              <Text style={{color: 'white', paddingLeft: 27, paddingRight: 40,}}> - </Text>
              <Text style={{color: 'white'}}>NA</Text>
            </View>
            <View style={{flexDirection: 'row', paddingTop: 5, justifyContent: 'flex-start',}}>
              <Text style={{color: 'white'}}>DOB</Text>
              <Text style={{color: 'white', paddingLeft: 87, paddingRight: 40,}}> - </Text>
              <Text style={{color: 'white'}}>NA</Text>
            </View>
            </View>

            <Text style={{color: 'white', fontSize: 15, fontWeight: '300', paddingTop: 20}}>
              MATCHWISE FANTASY STATS
            </Text>
          </View> */}

          {/* <View style={styles.listHeader}>
            <Text style={styles.listHeaderText}>MATCH</Text>
            <Text style={[styles.listHeaderText, {marginLeft: 15,}]}>POINTS</Text>
            <Text style={styles.listHeaderText}>SELECTED</Text>
          </View>
          <View>
            <ListView
              style={{ marginTop: 5 }}
              enableEmptySections 
              dataSource={ds.cloneWithRows([])}
              renderRow={(rowData, sectionID, rowID) => playerRecord(rowData, rowID)}
            />
          </View> */}

          <View style={styles.statsSelectionView}>
            <TouchableOpacity
              style={[styles.statsTouchable, { backgroundColor: props.showStatsType === 0 ? '#e71636' : null }]}
            >
              <View style={{ flex: 1 }}>
                <Text style={styles.dataRowText}>Actual Stats</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.statsTouchable, { backgroundColor: props.showStatsType === 1 ? '#e71636' : null }]}
            >
              <View style={{ flex: 1 }}>
                <Text style={styles.dataRowText}>Fantasy Stats</Text>
              </View>
            </TouchableOpacity>
          </View>

          {
            props.playerProfile.actual_stats.map(actualStats => (
              <View style={styles.statsView}>
                <View>
                  <Text style={styles.eventText}>{actualStats.global_event}</Text>
                </View>
                <View style={[styles.listHeader, { marginTop: 5 }]}>
                  <Text style={styles.dataRowText}>MATCH FORMAT</Text>
                  <Text style={styles.dataRowText}>ROLE</Text>
                  <Text style={styles.dataRowText}>ACHIVEMENTS</Text>
                </View>
                {actualStats.global_event && actualStats.global_data.map(data => (
                    <View style={styles.dataRow}>
                      <Text style={styles.dataRowText}>{data.match_format && data.match_format.toUpperCase()}</Text>
                      <Text style={styles.dataRowText}>{data.scoring_type && data.scoring_type.toUpperCase()}</Text>
                      {
                        getPlayerAchievement(data)
                      }
                    </View>
                  ))
                }
              </View>
            ))
          }
        </View>
      </ScrollView>
      <TouchableOpacity style={{position: 'absolute', left: 10, top: 20, }} onPress = {() => props.onBackIconClick()}>
        <Image style={styles.backIconStyle} source={images.backIcon} />
      </TouchableOpacity>
    </View>
);

PlayerCardContainer.propTypes = {
  playerProfile: PropTypes.objectOf(PropTypes.any),
};

PlayerCardContainer.defaultProps = {
  playerProfile: {},
};

export default PlayerCardContainer;
