import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
  StatusBar,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import PlayerCardContainer from './components/ContainerView';
import { constant } from '../../utils';
import { Loader } from '../../components';
import { containerStyles } from '../../theme';


class PlayerCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showStatsType: 0,
    };
  }

  render() {
    const { playerData } = this.props.navigation.state.params;
    const { showStatsType } = this.state;
    console.log('====>>> ', playerData);
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar
          backgroundColor="#0C0D18"
          barStyle="light-content"
        />
        <PlayerCardContainer
          playerProfile={playerData}
          showStatsType={showStatsType}
          onBackIconClick={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

PlayerCard.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

PlayerCard.defaultProps = {
  navigation: {},
};

const mapStateToProps = state => ({
  isLoading: state.playerProfile.isLoading,
  playerProfileResponse: state.playerProfile.playerProfileResponse,
});

const mapDispatchToProps = () => UserActions;

const PlayerCardScreen = connect(mapStateToProps, mapDispatchToProps)(PlayerCard);

export default PlayerCardScreen;
