/* eslint-disable react/sort-comp, react/prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View, Alert,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import { NavigationBar, StatusBar, Loader } from '../../components';
import Utils from '../../utils/utils';
import { images } from '../../assets/images';
import { constant, resetRoute } from '../../utils';
import Container from './components/Container';
import { containerStyles } from '../../theme/index';

class CreateTeam extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    const isCreateNew = !props.navigation.state.params.create ?
      props.navigation.state.params.create : true;
    console.log('########## isCreateNew: ', isCreateNew);
    const players = props.navigation.state.params.players === undefined ? [] : props.navigation.state.params.players;
    const allBracketsCount = players.length === 6 ? [2, 2, 2] : [0, 0, 0];

    const wk = players.filter(player => player.position === 'Wicket-Keeper');
    const bat = players.filter(player => player.position === 'Batsman');
    const ar = players.filter(player => player.position === 'All-rounder');
    const bowl = players.filter(player => player.position === 'Bowler');

    const rolewisePlayersCount = [wk.length, bat.length, ar.length, bowl.length];

    this.state = {
      create: isCreateNew,
      teamLineUpRules: [],
      sortedBracketLineup: [],
      selectedBracket: 0,
      selectedPlayersListTypeIndex: 0, // Identified as wk, bat, bowl, ar (according to rules array)
      selectPlayersLineup: players,
      allBracketsCount: allBracketsCount,
      rolewisePlayersCount: rolewisePlayersCount,
    };
  }

  componentDidMount() {
    console.log(' Create team>>componentDidMount>>', this.props);
    // const { contest_id, contest_name, contest_user_lineup, invite_code } = this.props.navigation.state.params;  
    // const { routeName } = this.props.navigation.state;  
    const { contest_id } = this.props.navigation.state.params;
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.getTeamLineUpRules(accessToken);
            this.props.getContestPlayersLineUp(
              accessToken,
              contest_id,
            );
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    // Session Expire
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }

    // Team Line-up Rules
    if (nextProps.lineUpRules.response &&
      nextProps.lineUpRules.status >= 200 &&
      nextProps.lineUpRules.status <= 300) {
      this.setState({ teamLineUpRules: nextProps.lineUpRules.response.rules });
    }
    if (nextProps.lineUpRules.response &&
        nextProps.lineUpRules.status >= 400) {
      if (nextProps.lineUpRules.response.message &&
          typeof nextProps.lineUpRules.response.message === 'string') {
        Alert.alert('Message', nextProps.lineUpRules.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }

    // Contest Team Line-Up
    if (nextProps.contestPlayersLineUp.response &&
      nextProps.contestPlayersLineUp.status >= 200 &&
      nextProps.contestPlayersLineUp.status <= 300) {
      this.configureList(nextProps.contestPlayersLineUp.response);
      this.props.resetContestPlayersLineUp();
    }
    if (nextProps.contestPlayersLineUp.response &&
        nextProps.contestPlayersLineUp.status >= 400) {
      if (nextProps.contestPlayersLineUp.response.message &&
          typeof nextProps.contestPlayersLineUp.response.message === 'string') {
        Alert.alert('Message', nextProps.contestPlayersLineUp.response.message);
        this.props.resetContestPlayersLineUp();
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
      this.props.resetContestPlayersLineUp();
      // return;
    }

    // Player Profile response
    if (nextProps.playerProfileResponse.response &&
      nextProps.playerProfileResponse.status >= 200 &&
      nextProps.playerProfileResponse.status <= 300) {
      const { navigate } = this.props.navigation;
      navigate(
        constant.screens.PLAYER_CARD,
        { playerData: nextProps.playerProfileResponse.response },
      );
      this.props.resetPlayer();
    }
    if (nextProps.playerProfileResponse.response
      && nextProps.playerProfileResponse.status >= 400) {
      if (nextProps.playerProfileResponse.response.message
        && typeof nextProps.playerProfileResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.playerProfileResponse.response.message);
        this.props.resetPlayer();
        return;
      }
      this.props.resetPlayer();
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  configureList(response) {
    const brackets = response.data;
    const bracketNames = brackets.map(bracketData => (bracketData.bracket));
    const sortedBracketLineup = [];

    bracketNames.map((bracketName, index) => {
      const bracketNameString = bracketName;
      const players = brackets[index].players;
      const WicketKeeper = players.filter((player) => {
        if (player.position === constant.playerPositions.WicketKeeper) {
          return player;
        }
      });
      const Batsman = players.filter((player) => {
        if (player.position === constant.playerPositions.Batsman) {
          return player;
        }
      });
      const AllRounder = players.filter((player) => {
        if (player.position === constant.playerPositions.AllRounder) {
          return player;
        }
      });
      const Bowler = players.filter((player) => {
        if (player.position === constant.playerPositions.Bowler) {
          return player;
        }
      });

      sortedBracketLineup.push({
        bracket: bracketNameString,
        players: {
          WicketKeeper,
          Batsman,
          AllRounder,
          Bowler,
        },
      });
    });
    this.setState({ sortedBracketLineup });
  }

  handlePlayerSelection(player) {
    const {
      selectPlayersLineup,
      selectedBracket,
      allBracketsCount,
      teamLineUpRules,
      selectedPlayersListTypeIndex,
      rolewisePlayersCount,
    } = this.state;

    // Check player have to remove or add.
    const playerIndex = selectPlayersLineup.findIndex(p => p.id === player.id);
    if (playerIndex > -1) {
      // Remove player
      const activeBracketPlayersCount = allBracketsCount[selectedBracket];
      const activeRolePlayersCount = rolewisePlayersCount[selectedPlayersListTypeIndex];
      allBracketsCount[selectedBracket] = activeBracketPlayersCount - 1;
      rolewisePlayersCount[selectedPlayersListTypeIndex] = activeRolePlayersCount - 1;
      selectPlayersLineup.splice(playerIndex, 1);
      this.setState({
        selectPlayersLineup,
      });
    } else {
      // Validate bracket count
      const activeBracketPlayersCount = allBracketsCount[selectedBracket];
      const activeRolePlayersCount = rolewisePlayersCount[selectedPlayersListTypeIndex];
      if (activeBracketPlayersCount < 2) {
        // Validate player role count
        if (activeRolePlayersCount === teamLineUpRules[selectedPlayersListTypeIndex].maxCount) {
          Alert.alert(
            'Maximum player',
            `You can ${teamLineUpRules[selectedPlayersListTypeIndex].text.toUpperCase()}.`,
          );
        } else {
          rolewisePlayersCount[selectedPlayersListTypeIndex] = activeRolePlayersCount + 1;
          allBracketsCount[selectedBracket] = activeBracketPlayersCount + 1;
          selectPlayersLineup.push(player);
          this.setState({
            selectPlayersLineup,
            allBracketsCount,
            rolewisePlayersCount,
          });
        }
      } else {
        Alert.alert('Change your bracket', 'You can choose 2 players per bracket');
      }
    }
  }

  finalizeList() {
    const { selectPlayersLineup, rolewisePlayersCount } = this.state;
    if (selectPlayersLineup.length < constant.maxTeamSize) {
      Alert.alert('Please select 6 players to finalize Team');
      return;
    }
    // Validate wk
    if (rolewisePlayersCount[0] === 0) {
      Alert.alert('Minimum Wicket keeper', 'Please choose at least one wicket keeper');
      return;
    } else if (rolewisePlayersCount[1] === 0) {
      Alert.alert('Minimum Batsman', 'Please choose at least one Batsman');
      return;
    } else if (rolewisePlayersCount[2] === 0) {
      Alert.alert('Minimum All Rounder', 'Please choose at least one All Rounder');
      return;
    } else if (rolewisePlayersCount[3] === 0) {
      Alert.alert('Minimum Bowler', 'Please choose at least one Bowler');
      return;
    }
    Alert.alert(
      'Submit Team Line-Up',
      'Click submit to finalize Team Line-Up',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed!'), style: 'destructive' },
        {
          text: 'Submit',
          onPress: () => {
            this.submitTeamLineUp();
          },
        },
      ],
    );
  }

  submitTeamLineUp() {
    const {
      contest_id,
      contest_name,
      contest_user_lineup,
      invite_code,
    } = this.props.navigation.state.params;
    const { routeName } = this.props.navigation.state;
    this.props.navigation.navigate(constant.screens.SUBMIT_TEAM, {
      teamLineUp: this.state.selectPlayersLineup,
      contest_id,
      contest_name,
      contest_user_lineup,
      invite_code,
      routeName,
      create: this.state.create,
    });
  }

  navigateToPlayerDetails(player) {
    const utils = new Utils();
    utils.checkInternetConnectivity((reach) => {
      if (reach) {
        utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.getPlayerDetails(accessToken, player.acronym);
            // this.props.getPlayerDetails(accessToken, 'player_y1');
          } else {
            Alert.alert(constant.loadingError.header, constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  render() {
    const { isGettingLineUpRules, isGettingContestPlayers, joinContestLoading } = this.props;
    const {
      sortedBracketLineup,
      selectedBracket,
      selectedPlayersListTypeIndex,
      teamLineUpRules,
      selectPlayersLineup,
      rolewisePlayersCount,
      allBracketsCount,
    } = this.state;
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar />
        <NavigationBar
          title={constant.screen_title.CREATE_TEAM}
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <Container
          sortedBracketLineup={sortedBracketLineup}
          selectedBracket={selectedBracket}
          teamLineUpRules={teamLineUpRules}
          selectedBracketWithIndex={index => this.setState({ selectedBracket: index })}
          selectedPlayersListTypeIndex={selectedPlayersListTypeIndex}
          changePlayerListType={index => this.setState({ selectedPlayersListTypeIndex: index })}
          onAddPlayer={player => this.handlePlayerSelection(player)}
          selectPlayersLineup={selectPlayersLineup}
          rolewisePlayersCount={rolewisePlayersCount}
          allBracketsCount={allBracketsCount}
          onSubmit={() => this.finalizeList()}
          navigateToPlayerDetails={pid => this.navigateToPlayerDetails(pid)}
        />
        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
        {isGettingLineUpRules && <Loader isAnimating={isGettingLineUpRules} />}
        {isGettingContestPlayers && <Loader isAnimating={isGettingContestPlayers} />}
        {joinContestLoading && <Loader isAnimating={joinContestLoading} />}
      </View>
    );
  }
}

CreateTeam.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

CreateTeam.defaultProps = {
  navigation: {},
};

const mapStateToProps = state => ({
  // Session Expire
  isSessionExpired: state.sessionExpire.isSessionExpired,

  // Team Line-up Rules
  isGettingLineUpRules: state.teamManagement.isGettingLineUpRules,
  lineUpRules: state.teamManagement.lineUpRules,

  // Contest Team Line-Up
  isGettingContestPlayers: state.teamManagement.isGettingContestPlayers,
  contestPlayersLineUp: state.teamManagement.contestPlayersLineUp,

  isLoading: state.playerProfile.isLoading,
  playerProfileResponse: state.playerProfile.playerProfileResponse,
});

const mapDispatchToProps = () => UserActions;

const CreateTeamScreen = connect(mapStateToProps, mapDispatchToProps)(CreateTeam);

export default CreateTeamScreen;
