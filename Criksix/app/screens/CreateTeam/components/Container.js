import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  Text,
  ListView,
  TouchableOpacity,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CustomDropDown } from '../../../components';
import { images } from '../../../assets/images';
import constant from '../../../utils/constant';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width,
    backgroundColor: '#080913',
  },
  bracketContainerView: {
    width: width - 20,
    marginTop: 8,
    flexDirection: 'row',
    marginHorizontal: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4f5386',
    borderRadius: 8,
  },
  bracketView: {
    width: width * 0.28,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e71636',
    marginHorizontal: 5,
    paddingHorizontal: 8,
    borderRadius: 8,
  },
  bracketViewTouchable: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
    flexDirection: 'row',
  },
  brackText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '700',
    textAlign: 'center',
  },
  bracketCountView: {
    backgroundColor: 'white',
    padding: 5,
    marginLeft: 5,
    borderRadius: 10,
  },
  bracketCountText: {
    fontSize: 14,
    fontWeight: '500',
    borderRadius: 20,
  },
  matchDetailHeader: {
    width,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
  },
  matchStartTimeText: {
    color: 'yellow',
    fontSize: 12,
    fontWeight: '400',
    textAlign: 'center',
  },
  matchNameText: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  lineUpView: {
    width,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    marginVertical: 15,
  },
  lineUpInfoView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 3,
  },
  pickWKText: {
    flex: 0.7,
    color: 'white',
    fontSize: 13,
    fontWeight: '500',
    textAlign: 'center',
  },
  positionText: {
    color: 'white',
    fontSize: 12,
    fontWeight: '500',
    textAlign: 'center',
    marginBottom: 3,
  },
  positionImage: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 4,
  },
  // Player Info View

  playerCountView: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    left: 0,
    bottom: 2,
    width: 22,
    height: 22,
    backgroundColor: '#ebb90b',
    transform: [{ rotate: '-20deg' }],
    borderWidth: 1,
    borderColor: 'black',
  },
  playerCountText: {
    color: 'black',
    fontSize: 12,
    fontWeight: '400',
    textAlign: 'center',
  },
  playersListContainer: {
    marginHorizontal: 15,
    backgroundColor: '#0e1020',
    padding: 8,
  },
  listHeader: {
    alignSelf: 'stretch',
    padding: 3,
    paddingHorizontal: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#6970a2',
  },
  listHeaderText: {
    color: 'white',
    fontSize: 12,
    fontWeight: '600',
    textAlign: 'center',
    paddingRight: 40,
  },
  playerAvatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  listRow: {
    backgroundColor: '#0c0d18',
    padding: 5,
    marginBottom: 5,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rowLeftView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowRightView: {
    width: 50,
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: 'blue',
  },

  playerNameText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
  },
  playerPointText: {
    color: '#6970a2',
    fontSize: 15,
    fontWeight: '500',
    textAlign: 'center',
  },
  playerCreditText: {
    color: 'white',
    fontSize: 13,
    fontWeight: '700',
    textAlign: 'center',
    paddingRight: 10,
  },
  addPlayerImage: {
    width: 30,
    height: 30,
    marginLeft: 8,
  },
// Bottom View
  bottomView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 8,
    backgroundColor: '#e71636',
  },

  selectedPlayersView: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  selectedPlayersCount: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  selectedPlayersText: {
    color: 'white',
    fontSize: 13,
    fontWeight: '600',
    textAlign: 'center',
  },
  nextButtonView: {
    paddingHorizontal: 25,
    paddingVertical: 10,
    backgroundColor: '#00000070',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  nextButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '700',
    textAlign: 'center',
  },
  viewLineStyle: {
    width: 1,
    height: 40,
    backgroundColor: 'white',
    marginRight: 0,
  },
});

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const LineUpInfo = (
  lineUp,
  image,
  rolewisePlayersCount,
  index,
  selectedPlayersListTypeIndex,
  changePlayerListType,
) => (
  <TouchableOpacity
    key={index}
    activeOpacity={0.7}
    onPress={() => changePlayerListType(index)}
  >
    <View style={styles.lineUpInfoView}>
      <Text style={styles.positionText}>{lineUp.acronym.toUpperCase()}</Text>
      <Image style={[styles.positionImage, { borderColor: index === selectedPlayersListTypeIndex ? 'green' : 'lightgrey' }]} source={image} />
      <View
        style={[styles.playerCountView, {
          backgroundColor: rolewisePlayersCount[index] > 0 ? 'green' : '#ebb90b',
        }]}
      >
        <Text
          style={[styles.playerCountText, {
            color: rolewisePlayersCount[index] > 0 ? 'white' : 'black',
            fontSize: 14,
          }]}
        >{rolewisePlayersCount[index]}
        </Text>
      </View>
    </View>
  </TouchableOpacity>
);

const PlayerListHeader = () => (
  <View style={styles.listHeader}>
    <Text style={styles.listHeaderText}>PLAYER INFO</Text>
    <Text style={styles.listHeaderText}>CREDITS</Text>
  </View>
);

const PlayerListRow = (
  rowData,
  sectionID,
  rowID,
  selectedPlayersListTypeIndex,
  onAddPlayer,
  selectPlayersLineup,
  navigateToPlayerDetails,
) => {
  const isPlayerSelected = selectPlayersLineup.findIndex(p => p.id === rowData.id) > -1;
  return (
    <TouchableOpacity
      key={rowID}
      activeOpacity={0.7}
      onPress={() => navigateToPlayerDetails(rowData)}
    >
      <View style={[styles.listRow, { backgroundColor: isPlayerSelected ? 'orange' : 'transparent' }]}>
        <Image style={styles.playerAvatar} source={{ uri: rowData.image_url }} />
        <View style={styles.rowLeftView}>
          <Text style={styles.playerNameText}>Name{rowData.name}</Text>
          <Text style={styles.playerPointText}>{rowData.team_name} </Text>
          <Text style={styles.playerCreditText}>{rowData.position}</Text>
        </View>
        <View style={styles.viewLineStyle} />
        <View style={styles.rowRightView}>
          <TouchableOpacity style={styles.rowRightView} onPress={() => onAddPlayer(rowData)}>
            <Image source={images.addButton} style={[styles.addPlayerImage, isPlayerSelected ? { transform: [{ rotate: '45deg' }] } : {}]} />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const Container = props => (
  <View style={styles.background}>
    <KeyboardAwareScrollView>
      {/* <View style={styles.matchDetailHeader}>
        <Text style={styles.matchStartTimeText}>02:40:00</Text>
        <Text style={styles.matchNameText}>INDIA vs SRI LANKA</Text>
      </View> */}
      <View style={styles.bracketContainerView}>
        {props.sortedBracketLineup.map((data, index) => (
          <View
            key={index}
            style={[
              styles.bracketView,
              { backgroundColor: props.selectedBracket === index ? '#e71636' : '#6970a2' }]}
          >
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.bracketViewTouchable}
              onPress={() => props.selectedBracketWithIndex(index)}
            >
              <Text style={styles.brackText}>{data.bracket}</Text>
              <View style={styles.bracketCountView}>
                <Text style={styles.bracketCountText}>{props.allBracketsCount[index]}</Text>
              </View>
            </TouchableOpacity>
          </View>))

        }
      </View>
      <View style={styles.lineUpView}>
        <Text style={styles.pickWKText}>
          {props.teamLineUpRules.length > 0
            && props.teamLineUpRules[props.selectedPlayersListTypeIndex].text}
        </Text>
        {
          props.teamLineUpRules.map((lineUpConstant, index) =>
            LineUpInfo(
              lineUpConstant,
              images.inviteImage,
              props.rolewisePlayersCount,
              index,
              props.selectedPlayersListTypeIndex,
              props.changePlayerListType,
            ))
        }
      </View>
      <View style={styles.playersListContainer}>
        {props.teamLineUpRules.length > 0 &&
          PlayerListHeader()
        }
        {props.sortedBracketLineup.length > 0 &&
          <ListView
            style={{ marginTop: 5 }}
            enableEmptySections
            dataSource={ds.cloneWithRows(
              props.sortedBracketLineup[props.selectedBracket]
              .players[constant.playerRoles[props.selectedPlayersListTypeIndex].role])}
            renderRow={(rowData, sectionID, rowID) =>
              PlayerListRow(
                rowData,
                sectionID,
                rowID,
                props.selectedPlayersListTypeIndex,
                props.onAddPlayer,
                props.selectPlayersLineup,
                props.navigateToPlayerDetails,
              )}
          />}
      </View>
    </KeyboardAwareScrollView>
    <View style={styles.bottomView}>
      <View style={styles.selectedPlayersView}>
        <Text style={styles.selectedPlayersCount}>{props.selectPlayersLineup.length} / 6</Text>
        <Text style={styles.selectedPlayersText}>PLAYERS</Text>
      </View>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => props.onSubmit()}
      >
        <View style={styles.nextButtonView}>
          <Text style={styles.nextButtonText}>NEXT</Text>
        </View>
      </TouchableOpacity>
    </View>
  </View>
);

Container.propTypes = {
  teamLineUpRules: PropTypes.arrayOf(PropTypes.any),
  sortedBracketLineup: PropTypes.arrayOf(PropTypes.any),
  selectedBracket: PropTypes.number,
  selectedPlayersListTypeIndex: PropTypes.number,
  onAddPlayer: PropTypes.func,
  selectPlayersLineup: PropTypes.arrayOf(PropTypes.any),
  rolewisePlayersCount: PropTypes.arrayOf(PropTypes.any),
  allBracketsCount: PropTypes.arrayOf(PropTypes.any),
  changePlayerListType: PropTypes.func,
  navigateToPlayerDetails: PropTypes.func,
  onSubmit: PropTypes.func,
  create: PropTypes.bool,
};

Container.defaultProps = {
  teamLineUpRules: [],
  sortedBracketLineup: [],
  selectedBracket: 0,
  selectedPlayersListTypeIndex: 0,
  onAddPlayer: () => {},
  selectPlayersLineup: [],
  allBracketsCount: [0, 0, 0],
  rolewisePlayersCount: [0, 0, 0, 0],
  changePlayerListType: () => {},
  navigateToPlayerDetails: () => {},
  onSubmit: () => {},
  create: true,
};

export default Container;
