import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
  Share,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import Container from './components/Container';
import { NavigationBar, StatusBar, Loader } from '../../components';
import { images } from '../../assets/images';
import {
  constant,
  resetRoute,
  isResponseValidated,
  isResponseSuccess,
} from '../../utils';
import Utils from '../../utils/utils';
import { containerStyles } from '../../theme/index';

class TransactionHistory extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      transactions: [],
    };
  }

  componentDidMount() {
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }
  }

  render() {
    const { transactions } = this.state;
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar />
        <NavigationBar
          title="Transaction History"
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <Container
          transactions={transactions}
        />
      </View>
    );
  }
}

TransactionHistory.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

TransactionHistory.defaultProps = {
  navigation: {},
};

const mapStateToProps = state => ({
  // Session Expire
  isSessionExpired: state.sessionExpire.isSessionExpired,
});

const mapDispatchToProps = () => UserActions;

const TransactionHistoryScreen = connect(mapStateToProps, mapDispatchToProps)(TransactionHistory);

export default TransactionHistoryScreen;
