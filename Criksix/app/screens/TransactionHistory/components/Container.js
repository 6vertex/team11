import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Dimensions,
  ListView,
  Text,
} from 'react-native';
import { images } from '../../../assets/images';
import TransactionRow from './TransactionRow';
import { Colors } from '../../../theme/index';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignSelf: 'stretch',
    width,
    backgroundColor: Colors.primarybackgroundColor,
  },
});

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const Container = props => (
  <View style={styles.background}>
    
    <ListView
      enableEmptySections
      dataSource={ds.cloneWithRows(props.transactions)}
      renderRow={(rowData, sectionID, rowID) => (
        <TransactionRow
          key={rowID}
        />
      )}
    />
  </View>
);

Container.propTypes = {
  transactions: PropTypes.arrayOf(PropTypes.any),
};

Container.defaultProps = {
  transactions: [],
};

export default Container;
