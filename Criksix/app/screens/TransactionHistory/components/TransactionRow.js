import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Colors } from '../../../theme/index';
import { constant, } from '../../../utils';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  contestViewStyle: {
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 5,
    paddingVertical: 12,
    margin: 5,
    backgroundColor: Colors.selectedFilterColor,
    borderRadius: 5,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  headerText: {
    fontSize: 16,
    fontWeight: '600',
    color: 'white',
  },
  contestName: {
    color: 'white',
    textAlign: 'center',
    marginVertical: 3,
    backgroundColor: 'transparent',
    fontSize: 16,
    fontWeight: '700',
  },
  headerView: {
    width: width - 35,
    flexDirection: 'row',
    padding: 2,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  detailView: {
    width: width - 35,
    flexDirection: 'row',
    padding: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
  },
  totalEarnings: {
    fontSize: 15,
    fontWeight: '400',
    color: 'white',
  },
  earningsText: {
    fontSize: 15,
    fontWeight: '700',
    color: 'white',
  },
  detailHeader: {
    fontSize: 12,
    fontWeight: '400',
    color: '#4f5386',
  },
  detailText: {
    fontSize: 12,
    fontWeight: '700',
    color: 'white',
  },
  joinButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical: 8,
    backgroundColor: '#4f5386',
    borderRadius: 12,
    alignSelf: 'stretch',
  },
  teamsCountView: {
    width: width * 0.63,
    flexDirection: 'row',
    marginTop: -20,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  inviteButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical: 8,
    backgroundColor: 'orange',
    borderRadius: 12,
  },
  inviteButtonText: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
  },
  nextIcon: {
    width: 10,
    height: 10,
    marginLeft: 3,
    transform: [{
      rotate: '180deg',
    }],
  },
  statisticsView: {
    marginVertical: 5,
    paddingVertical: 5,
    alignSelf: 'stretch',
  },
  statusContainer: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  statusView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    width: width * 0.63,
    height: 5,
    backgroundColor: 'white',
    marginTop: 10,
  },
  joinedStatus: {
    backgroundColor: Colors.navigationBackgroundColor,
  },
  roomStatus: {
    flex: 1,
  },
  uncappedInfoView: {
    backgroundColor: 'transparent',
    width: width * 0.58,
  },
  topHeaderView: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    paddingVertical: 5,
    alignSelf: 'stretch',
    backgroundColor: 'transparent',
  },
  headerText: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white',
  },
  separator: {
    height: 1,
    alignSelf: 'stretch',
    backgroundColor: 'white',
    opacity: 0.2,
    marginVertical: 5,
  },
});

const TransactionRow = (props) => {
  return (
    <View style={styles.contestViewStyle}>
      <Text style={styles.headerText}>500</Text>
      <Text style={styles.headerText}>Credit/debit</Text>
      <Text style={styles.headerText}>status</Text>
    </View>
  );
};

TransactionRow.propTypes = {
};

TransactionRow.defaultProps = {
};

export default TransactionRow;
