import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Image,
  StyleSheet,
} from 'react-native';
import NotificationsIOS, { NotificationsAndroid } from 'react-native-notifications';
import Utils from '../../utils/utils';
import { constant, isIOS, resetRoute } from '../../utils';
import { dimensions, containerStyles } from '../../theme';
import { images } from '../../assets/images';
import { StatusBar } from '../../components';

const styles = StyleSheet.create({
  splashImageContainer: {
    width: dimensions.getViewportWidth(),
    height: dimensions.getViewportHeight(),
    resizeMode: 'cover',
  },
  container: {
    width: dimensions.getViewportWidth(),
    height: dimensions.getViewportHeight(),
  },
});

let mainScreen;

function onPushRegistered(deviceToken) {
  if (mainScreen) {
    mainScreen.onPushRegistered(deviceToken);
  }
}

function onNotificationOpened(notification) {
  if (mainScreen) {
    mainScreen.onNotificationOpened(notification);
  }
}

if (!isIOS) {
  NotificationsAndroid.setRegistrationTokenUpdateListener(onPushRegistered);
  NotificationsAndroid.setNotificationOpenedListener(onNotificationOpened);
}

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.utils = new Utils();
    if (!isIOS) {
      mainScreen = this;
    } else {
      NotificationsIOS.addEventListener('remoteNotificationsRegistered', this.onPushRegistered.bind(this));
      // NotificationsIOS.addEventListener('remoteNotificationsRegistrationFailed',
      // this.onPushRegistrationFailed.bind(this));
      NotificationsIOS.requestPermissions();
      NotificationsIOS.consumeBackgroundQueue();
      NotificationsIOS.addEventListener('notificationReceivedForeground', this.onNotificationReceivedForeground.bind(this));
      NotificationsIOS.addEventListener('notificationReceivedBackground', this.onNotificationReceivedBackground.bind(this));
      NotificationsIOS.addEventListener('notificationOpened', this.onNotificationOpened.bind(this));
    }
  }

  componentDidMount() {
    this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
      if (response && response.access_token) {
        this.goToScreen(constant.screens.DRAWER_STACK);
      } else {
        this.checkAppOpenedFirstTime();
      }
    });
  }

  onPushRegistered(deviceToken) {
    console.log('Device Token Received', deviceToken);
    this.utils.setItemWithKeyAndValue(constant.DEVICE_TOKEN_PN, deviceToken);
  }

  onNotificationOpened(notification) {
    console.log('onNotificationOpened', notification);
  }

  onNotificationReceivedForeground(notification) {
    console.log('Notification Received - Foreground', notification);
  }

  onNotificationReceivedBackground(notification) {
    console.log('Notification Received - Background', notification);
  }

  checkAppOpenedFirstTime() {
    this.utils.getItemWithKey(constant.OPEN_APP_FIRST_TIME, (response) => {
      if (response !== undefined && response === false) {
        this.goToScreen(constant.screens.LANDING_PAGE);
      } else {
        this.goToScreen(constant.screens.ON_BOARDING);
      }
    });
  }

  goToScreen(screenName) {
    setTimeout(() => {
      resetRoute(this.props.navigation, screenName);
    }, 1500);
  }

  render() {
    return (
      <View style={containerStyles.container}>
        <StatusBar hidden />
        <Image style={styles.splashImageContainer} source={images.splash} />
      </View>
    );
  }
}

Splash.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

Splash.defaultProps = {
  navigation: {},
};

export default Splash;
