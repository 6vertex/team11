import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import RegisterContainer from './components/RegisterContainer';
import { Loader } from '../../components';
import {
  constant,
  validateEmail,
  isResponseValidated,
  isResponseSuccess,
  resetRoute,
} from '../../utils';
import Utils from '../../utils/utils';
import { containerStyles } from '../../theme';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: '',
      invitedByCode: '',
      showRePassword: false,
      showPassword: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    // Handle response for signup
    if (isResponseValidated(nextProps.registerUserResponse)) {
      if (isResponseSuccess(nextProps.registerUserResponse)) {
        Alert.alert(constant.REGISTER_SUCCESS_MSG.header, constant.REGISTER_SUCCESS_MSG.message);
        this.props.resetRegister();
        this.pushNext();
      } else {
        if (nextProps.registerUserResponse.response.message
          && typeof nextProps.registerUserResponse.response.message === 'string') {
          this.setState({
            email: '',
          });
          Alert.alert('Message', nextProps.registerUserResponse.response.message);
          this.props.resetRegister();
          return;
        }
        Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
        this.props.resetRegister();
      }
    }
  }

  onChangeFirstName(firstName) {
    this.setState({ firstName });
  }

  onChangeLastName(lastName) {
    this.setState({ lastName });
  }

  onChangeEmailText(email) {
    this.setState({ email });
  }

  onChangePasswordText(password) {
    this.setState({ password });
  }

  onChangeConfirmPasswordText(confirmPassword) {
    this.setState({ confirmPassword });
  }

  onSubmitEditing(key) {
    try {
      switch (key) {
        case 'firtsName':
          this.lastNameInput.focus();
          break;
        case 'lastName':
          this.emailInput.focus();
          break;
        case 'email':
          this.passwordInput.focus();
          break;
        case 'password':
          this.confirmPasswordInput.focus();
          break;
        case 'confirmPassword':
          break;
        default:
          break;
      }
    } catch (error) {
      console.log(error);
    }
  }

  getTextInputReference(key, reference) {
    switch (key) {
      case 'firtsName':
        this.firtsNameInput = reference;
        break;
      case 'lastName':
        this.lastNameInput = reference;
        break;
      case 'email':
        this.emailInput = reference;
        break;
      case 'password':
        this.passwordInput = reference;
        break;
      case 'confirmPassword':
        this.confirmPasswordInput = reference;
        break;
      default:
        break;
    }
  }

  submitPress() {
    if (this.state.firstName === '') {
      Alert.alert(
        'Message',
        'Please enter First Name.',
      );
      return;
    }
    if (this.state.lastName === '') {
      Alert.alert(
        'Message',
        'Please enter Last Name.',
      );
      return;
    }
    if (validateEmail(this.state.email)) {
      Alert.alert(
        'Message',
        'Your e-mail is incorrect.',
      );
      return;
    }
    if (this.state.password === '' || this.state.password.length < 6) {
      Alert.alert(
        'Message',
        'Please enter atleast 6-digit password.',
      );
      return;
    }
    if (this.state.confirmPassword !== this.state.password) {
      Alert.alert(
        'Message',
        'Password is not matching.',
      );
      return;
    }
    const utils = new Utils();
    utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.props.userRegisterRequest(
          this.state.firstName,
          this.state.lastName,
          this.state.email,
          this.state.password,
          this.state.invitedByCode,
        );
      } else {
        Alert.alert(
          constant.noInternetConnection.header,
          constant.noInternetConnection.message,
        );
      }
    });
  }

  pushNext() {
    resetRoute(this.props.navigation, constant.screens.LOGIN);
  }

  handleInviteCodeValidation(code) {
    if (code.length > 1) {
      this.setState({
        invitedByCode: code,
      });
    }
    Alert.alert(`${code} ${constant.INVITE_CODE_SUCCESS_MSG.header}`, constant.INVITE_CODE_SUCCESS_MSG.message);
  }

  invitedByFriend() {
    this.props.navigation.navigate(
      constant.screens.INVITATION,
      { applyValidationCode: code => this.handleInviteCodeValidation(code) },
    );
  }

  goToTermsAndCondition() {
    this.props.navigation.navigate(constant.screens.TERMS_AND_CONDITION);
  }

  render() {
    return (
      <View style={containerStyles.defaultContainer}>
        <RegisterContainer
          getTextInputReference={(key, reference) => this.getTextInputReference(key, reference)}
          firstNameText={this.state.firstName}
          onChangeFirstName={firstName => this.onChangeFirstName(firstName)}
          lastNameText={this.state.lastName}
          onChangeLastName={lastName => this.onChangeLastName(lastName)}
          emailText={this.state.email}
          onChangeEmailText={email => this.onChangeEmailText(email)}
          passwordText={this.state.password}
          onChangePasswordText={password => this.onChangePasswordText(password)}
          confirmPasswordText={this.state.confirmPassword}
          onChangeConfirmPasswordText={confirmPassword =>
            this.onChangeConfirmPasswordText(confirmPassword)}
          submitPress={() => this.submitPress()}
          popBack={() => this.props.navigation.goBack()}
          invitedByFriend={() => this.invitedByFriend()}
          goToTermsAndCondition={() => this.goToTermsAndCondition()}
          onSubmitEditing={key => this.onSubmitEditing(key)}
          showPassowrdText={showPassword => this.setState({ showPassword })}
          showRePassowrdText={showRePassword => this.setState({ showRePassword })}
          isShowPassword={this.state.showPassword}
          isShowRePassword={this.state.showRePassword}
        />
        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

Register.propTypes = {
  isLoading: PropTypes.bool,
  registerUserResponse: PropTypes.objectOf(PropTypes.any),
  navigation: PropTypes.objectOf(PropTypes.any),
  userRegisterRequest: PropTypes.func,
  resetRegister: PropTypes.func,
};

Register.defaultProps = {
  isLoading: false,
  registerUserResponse: {},
  navigation: {},
  userRegisterRequest: () => {},
  resetRegister: () => {},
};

const mapStateToProps = state => ({
  isLoading: state.user.isLoading,
  registerUserResponse: state.user.registerUserResponse,
});

const mapDispatchToProps = () => UserActions;

const RegisterScreen = connect(mapStateToProps, mapDispatchToProps)(Register);

export default RegisterScreen;
