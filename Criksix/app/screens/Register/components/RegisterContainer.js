import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../../components/CustomTextInput';
import { images } from '../../../assets/images';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  backgroundImage: {
    width,
    height,
    resizeMode: 'cover',
  },
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  logo: {
    width: width * 0.60,
    height: 60,
    resizeMode: 'contain',
    marginTop: height * 0.036,
  },
  headerView: {
    width: (width / 3) * 2.6,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 20,
  },
  headerText: {
    color: 'white',
    fontSize: 28,
    fontWeight: '400',
    textAlign: 'left',
  },
  labelStyle: {
    paddingBottom: -5,
    fontSize: 17,
  },
  buttonTouchable: {
    marginTop: Platform.OS === 'ios' ? 18 : 10,
  },
  buttonContainer: {
    width: (width / 3),
    paddingVertical: 5,
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  signUpTouchable: {
    padding: 8,
    position: 'absolute',
    bottom: 25,
    right: 10,
  },
  signUpImage: {
    width: 50,
    height: 50,
    resizeMode: 'cover',
  },
  InputMainView: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  showPasswordButton: {
    width: 35,
    height: 36,
    position: 'absolute',
    justifyContent: 'center',
    right: 5,
    bottom: 0,
  },
  eyeLogo: {
    width: 30,
    height: 31,
    resizeMode: 'contain',
  },
});

const RegisterContainer = props => (
  <Image
    style={styles.backgroundImage}
    source={images.loginBackground}
  >
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <Image
          source={images.crickSixLogo}
          style={styles.logo}
        />
        <View style={styles.headerView}>
          <Text style={styles.headerText}>Create Account</Text>
        </View>
        <CustomTextInput
          title="First Name*"
          titleStyle={styles.labelStyle}
          inputKey={'firtsName'}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          autoCapitalize="sentences"
          value={props.firstNameText}
          returnKeyType="next"
          onChangeText={text => props.onChangeFirstName(text)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />

        <CustomTextInput
          title="Last Name*"
          titleStyle={styles.labelStyle}
          inputKey={'lastName'}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          autoCapitalize="sentences"
          value={props.lastNameText}
          returnKeyType="next"
          onChangeText={text => props.onChangeLastName(text)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />
        <CustomTextInput
          title="Email*"
          titleStyle={styles.labelStyle}
          inputKey={'email'}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          keyboardType="email-address"
          value={props.emailText}
          returnKeyType="next"
          onChangeText={text => props.onChangeEmailText(text)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />
        <View style={styles.InputMainView}>
          <CustomTextInput
            title="Password*"
            titleStyle={styles.labelStyle}
            inputKey={'password'}
            getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
            secureTextEntry={!props.isShowPassword}
            value={props.passwordText}
            returnKeyType="next"
            onChangeText={text => props.onChangePasswordText(text)}
            onSubmitEditing={key => props.onSubmitEditing(key)}
          />
          <TouchableOpacity
            style={styles.showPasswordButton}
            onPress={() => props.showPassowrdText(!props.isShowPassword)}
          >
            <Image
              style={styles.eyeLogo}
              source={props.isShowPassword ? images.eye : images.hidePassword}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.InputMainView}>
          <CustomTextInput
            title="Confirm Password*"
            titleStyle={styles.labelStyle}
            inputKey={'confirmPassword'}
            getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
            secureTextEntry={!props.isShowRePassword}
            value={props.confirmPasswordText}
            onChangeText={text => props.onChangeConfirmPasswordText(text)}
            onSubmitEditing={key => props.onSubmitEditing(key)}
          />
          <TouchableOpacity
            style={styles.showPasswordButton}
            onPress={() => props.showRePassowrdText(!props.isShowRePassword)}
          >
            <Image
              style={styles.eyeLogo}
              source={props.isShowRePassword ? images.eye : images.hidePassword}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.buttonTouchable}
          activeOpacity={0.7}
          onPress={() => props.submitPress()}
        >
          <View style={styles.buttonContainer}>
            <Text style={[styles.buttonText, { fontSize: 20 }]}>Submit</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonTouchable}
          activeOpacity={0.7}
          onPress={() => props.invitedByFriend()}
        >
          <View style={[styles.buttonContainer, { width: (width / 3) * 1.5, backgroundColor: '#AE1828' }]}>
            <Text style={[styles.buttonText, { fontSize: 15 }]}>Invited by friend ?</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.buttonTouchable, { marginBottom: 40 }]}
          activeOpacity={0.7}
          onPress={() => props.goToTermsAndCondition()}
        >
          <View style={[styles.buttonContainer, { width: (width / 3) * 1.5, backgroundColor: '#AE1828' }]}>
            <Text style={[styles.buttonText, { fontSize: 15 }]}>Terms And Conditions</Text>
          </View>
        </TouchableOpacity>
      </View>
    </KeyboardAwareScrollView>
    <TouchableOpacity
      style={styles.signUpTouchable}
      activeOpacity={0.7}
      onPress={() => props.popBack()}
    >
      <Image style={styles.signUpImage} source={images.backButton} />
    </TouchableOpacity>
  </Image>
);

RegisterContainer.propTypes = {
  getTextInputReference: PropTypes.func,
  firstNameText: PropTypes.string,
  onChangeFirstName: PropTypes.func,
  lastNameText: PropTypes.string,
  onChangeLastName: PropTypes.func,
  emailText: PropTypes.string,
  onChangeEmailText: PropTypes.func,
  passwordText: PropTypes.string,
  onChangePasswordText: PropTypes.func,
  confirmPasswordText: PropTypes.string,
  onChangeConfirmPasswordText: PropTypes.func,
  submitPress: PropTypes.func,
  popBack: PropTypes.func,
  invitedByFriend: PropTypes.func,
  goToTermsAndCondition: PropTypes.func,
  onSubmitEditing: PropTypes.func,
  showPassowrdText: PropTypes.func,
  showRePassowrdText: PropTypes.func,
  isShowPassword: PropTypes.bool,
  isShowRePassword: PropTypes.bool,
};

RegisterContainer.defaultProps = {
  getTextInputReference: () => {},
  firstNameText: '',
  onChangeFirstName: () => {},
  lastNameText: '',
  onChangeLastName: () => {},
  emailText: '',
  onChangeEmailText: () => {},
  passwordText: '',
  onChangePasswordText: () => {},
  confirmPasswordText: '',
  onChangeConfirmPasswordText: () => {},
  submitPress: () => {},
  popBack: () => {},
  invitedByFriend: () => {},
  goToTermsAndCondition: () => {},
  onSubmitEditing: () => {},
  showPassowrdText: () => {},
  showRePassowrdText: () => {},
  isShowPassword: false,
  isShowRePassword: false,
};

export default RegisterContainer;
