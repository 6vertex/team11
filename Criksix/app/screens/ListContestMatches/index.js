import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { NavigationBar, StatusBar } from '../../components';
import { images } from '../../assets/images';
import { containerStyles, Colors } from '../../theme';
import MatchRow from '../Dashboard/components/MatchRow';

const styles = StyleSheet.create({
  innerContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignSelf: 'stretch',
  },
  matchRow: {
    backgroundColor: Colors.selectedFilterColor,
    borderRadius: 5,
    margin: 5,
  },
  matchNameText: {
    color: 'white',
    fontSize: 14,
    paddingHorizontal: 10,
  },
  matchStatusText: {
    color: Colors.greenShadeColor,
    fontSize: 12,
    paddingHorizontal: 10,
  },
});

class ListContestMatches extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { contest } = this.props.navigation.state.params;
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar />
        <NavigationBar
          title={contest.league_name}
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <View style={styles.innerContainer}>
          {
            contest.matches.map((match, matchIndex) => {
              return (
                <View style={styles.matchRow} key={matchIndex}>
                  <Text style={styles.matchNameText}>{match.name}</Text>
                  <Text style={styles.matchStatusText}>({match.status})</Text>
                  <MatchRow
                    isSelected
                    homeTeamAcronym={match.home_team_acronym.toUpperCase()}
                    awayTeamAcronym={match.away_team_acronym.toUpperCase()}
                    schedule={match.scheduled_for}
                    isShowSelection={Boolean(false)}
                  />
                </View>
              );
            })
          }
        </View>
      </View>
    );
  }
}

ListContestMatches.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  contest: PropTypes.objectOf(PropTypes.any),
};

ListContestMatches.defaultProps = {
  navigation: {},
  contest: {},
};

export default ListContestMatches;
