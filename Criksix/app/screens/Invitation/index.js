import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import Loader from '../../components/Loader';
import {
  constant,
  isResponseValidated,
  isResponseSuccess,
} from '../../utils';
import Utils from '../../utils/utils';
import InvitationContainer from './components/InvitationContainer';
import { containerStyles } from '../../theme';

class Invitation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inviteCode: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    // Handle response for code validation
    if (isResponseValidated(nextProps.inviteCodeResponse)) {
      if (isResponseSuccess(nextProps.inviteCodeResponse)) {
        this.goToRegister(this.state.inviteCode);
      } else {
        if (nextProps.inviteCodeResponse.response.message
          && typeof nextProps.inviteCodeResponse.response.message === 'string') {
          this.setState({
            inviteCode: '',
          });
          Alert.alert('Message', nextProps.inviteCodeResponse.response.message);
          return;
        }
        Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
      }
    }
  }

  onChangeInviteCodeText(inviteCode) {
    this.setState({ inviteCode });
  }

  applyPress() {
    if (this.state.inviteCode === '') {
      Alert.alert(
        'Message',
        'Invite code should not be empty.',
      );
      return;
    }
    new Utils().checkInternetConnectivity((reach) => {
      if (reach) {
        this.props.inviteCodeValidateRequest(this.state.inviteCode);
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  goToRegister(code = '') {
    this.props.navigation.state.params.applyValidationCode(code);
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={containerStyles.defaultContainer}>
        <InvitationContainer
          inviteCodeText={this.state.inviteCode}
          onChangeInviteCodeText={text => this.onChangeInviteCodeText(text)}
          applyPress={() => this.applyPress()}
          popBack={() => this.props.navigation.goBack()}
        />
        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

Invitation.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  inviteCodeResponse: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  inviteCodeValidateRequest: PropTypes.func,
  applyValidationCode: PropTypes.func,
};

Invitation.defaultProps = {
  navigation: {},
  inviteCodeResponse: {},
  isLoading: false,
  inviteCodeValidateRequest: () => {},
  applyValidationCode: () => {},
};

const mapStateToProps = state => ({
  isLoading: state.user.isLoading,
  inviteCodeResponse: state.user.inviteCodeResponse,
});

const mapDispatchToProps = () => UserActions;

const InvitationScreen = connect(mapStateToProps, mapDispatchToProps)(Invitation);

export default InvitationScreen;
