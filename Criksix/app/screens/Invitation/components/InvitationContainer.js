import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../../components/CustomTextInput';
import { images } from '../../../assets/images';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  backgroundImage: {
    width,
    height,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  logo: {
    width: width * 0.60,
    height: 60,
    marginTop: height * 0.08,
    resizeMode: 'contain',
  },
  inviteImage: {
    width: width * 0.42,
    height: width * 0.42,
    resizeMode: 'cover',
    marginTop: 50,
    marginBottom: 15,
  },

  buttonTouchable: {
    marginTop: 20,
  },
  buttonContainer: {
    width: (width / 3),
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  signUpTouchable: {
    padding: 8,
    position: 'absolute',
    bottom: 25,
    right: 10,
  },
  signUpImage: {
    width: 50,
    height: 50,
    resizeMode: 'cover',
  },
});


const InvitationContainer = props => (
  <Image style={styles.backgroundImage} source={images.generalBackground}>
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <Image
          source={images.crickSixLogo}
          style={styles.logo}
        />
        <Image
          style={styles.inviteImage}
          source={images.inviteImage}
        />
        <CustomTextInput
          title="Enter Invite Code*"
          value={props.inviteCodeText}
          returnKeyType="done"
          onChangeText={email => props.onChangeInviteCodeText(email)}
        />

        <TouchableOpacity
          style={styles.buttonTouchable}
          activeOpacity={0.7}
          onPress={() => props.applyPress()}
        >
          <View style={styles.buttonContainer}>
            <Text style={styles.buttonText}>Apply</Text>
          </View>
        </TouchableOpacity>
      </View>
    </KeyboardAwareScrollView>
    <TouchableOpacity
      style={styles.signUpTouchable}
      activeOpacity={0.7}
      onPress={() => props.popBack()}
    >
      <Image style={styles.signUpImage} source={images.backButton} />
    </TouchableOpacity>
  </Image>
);

InvitationContainer.propTypes = {
  inviteCodeText: PropTypes.string,
  onChangeInviteCodeText: PropTypes.func,
  applyPress: PropTypes.func,
  popBack: PropTypes.func,
};

InvitationContainer.defaultProps = {
  inviteCodeText: '',
  onChangeInviteCodeText: () => {},
  applyPress: () => {},
  popBack: () => {},
};

export default InvitationContainer;
