import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import TermsAndConditionContainer from './components/Container';
import { containerStyles } from '../../theme';


class TermsAndCondition extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={containerStyles.defaultContainer}>
        <TermsAndConditionContainer
          popBack={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

TermsAndCondition.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

TermsAndCondition.defaultProps = {
  navigation: {},
};

export default TermsAndCondition;
