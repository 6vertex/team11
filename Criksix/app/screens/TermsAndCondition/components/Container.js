import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { images } from '../../../assets/images';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  backgroundImage: {
    width,
    height,
    resizeMode: 'cover',
  },
  flexOne: {
    flex: 1,
    width,
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  logo: {
    width: width * 0.44,
    height: 44,
    marginTop: height * 0.081,
    resizeMode: 'contain',
  },
  headerView: {
    width: (width / 3) * 2.5,
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 20,
    marginBottom: 8,
  },
  headerText: {
    color: 'white',
    fontSize: 22,
    fontWeight: '500',
    textAlign: 'left',
  },
  subHeaderView: {
    width: (width / 3) * 2.5,
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 5,
    marginBottom: 12,
  },
  subHeaderText: {
    color: 'white',
    fontSize: 14,
    fontWeight: '500',
    textAlign: 'left',
  },
  forgotPasswordView: {
    width: (width / 3) * 2.5,
    height: 30,
    marginTop: 0,
    marginBottom: 5,
    alignItems: 'flex-end',
  },
  forgotPasswordButton: {
    padding: 8,
    paddingRight: 0,
  },
  forgotPasswordText: {
    color: 'white',
    fontSize: 12,
    fontWeight: '500',
    textAlign: 'right',
  },

  buttonTouchable: {
    marginTop: 20,
  },
  buttonContainer: {
    width: (width / 3) * 1.5,
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  orText: {
    marginVertical: 6,
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  socialButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: (width / 3) * 2.5,
  },
  socialImage: {
    width: width * 0.41,
    resizeMode: 'contain',
  },
  authBottom: {
    position: 'absolute',
    width,
    height: 135,
    bottom: 0,
    left: 0,
    resizeMode: 'cover',
  },
  signUpTouchable: {
    padding: 8,
    position: 'absolute',
    bottom: 25,
    right: 10,
  },
  signUpImage: {
    width: 50,
    height: 50,
    resizeMode: 'cover',
  },
});


const ForgotPasswordContainer = props => (
  <Image style={styles.backgroundImage} source={images.loginBackground}>
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <Image
          source={images.crickSixLogo}
          style={styles.logo}
        />
        <View style={styles.headerView}>
          <Text style={styles.headerText}>Terms and Conditions</Text>
        </View>
        <View style={styles.subHeaderView}>
          <Text style={styles.subHeaderText}>
          Terms of service (also known as terms of use and terms and conditions, commonly abbreviated as TOS or ToS and ToU) are rules by which one must agree to abide in order to use a service.[1] Terms of service can also be merely a disclaimer, especially regarding the use of websites.
          The Terms of Service Agreement is mainly used for legal purposes by companies which provide software or services, such as browsers, e-commerce, search engines, social media, and transport services.
A legitimate terms-of-service agreement is legally binding and may be subject to change.[2] Companies can enforce the terms by refusing service. Customers can enforce by filing a suit or arbitration case if they can show they were actually harmed by a breach of the terms. There is a heightened risk of data going astray during corporate changes, including mergers, divestitures, buyouts, downsizing, etc., when data can be transferred improperly.[3]

          </Text>
        </View>
      </View>
    </KeyboardAwareScrollView>
    <TouchableOpacity
      style={styles.signUpTouchable}
      activeOpacity={0.7}
      onPress={() => props.popBack()}
    >
      <Image style={styles.signUpImage} source={images.backButton} />
    </TouchableOpacity>
  </Image>
);

ForgotPasswordContainer.propTypes = {
};

ForgotPasswordContainer.defaultProps = {
};

export default ForgotPasswordContainer;
