import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import FBSDK from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import LoginActions from '../../actions';
import LoginContainer from './components/LoginContainer';
import { Loader, StatusBar } from '../../components';
import {
  constant,
  validateEmail,
  isIOS,
  validatePassword,
  networkConnectivity,
  isResponseValidated,
  isResponseSuccess,
  resetRoute,
} from '../../utils';
import Utils from '../../utils/utils';
import { containerStyles } from '../../theme';

const { LoginManager, AccessToken } = FBSDK;

class Login extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.deviceToken = '';
    this.deviceType = '';
    this.state = {
      email: '',
      password: '',
    };
  }

  componentDidMount() {
    this.utils.getItemWithKey(constant.DEVICE_TOKEN_PN, (value) => { this.deviceToken = value; });
    if (!isIOS) {
      LoginManager.setLoginBehavior('web_only');
      this.deviceType = 'android';
    } else {
      LoginManager.setLoginBehavior('web');
      this.deviceType = 'ios';
    }
    this.resetLoginState();
    this.setupGoogleSignin();
  }

  componentWillReceiveProps(nextProps) {
    // Handle response for normal login
    if (isResponseValidated(nextProps.loginUserResponse)) {
      if (isResponseSuccess(nextProps.loginUserResponse)) {
        this.saveUserDetailsInStorage(nextProps.loginUserResponse.response);
      } else {
        Alert.alert('Message', nextProps.loginUserResponse.response.message);
      }
    }

    // Handle response for social login
    if (isResponseValidated(nextProps.socialLoginResponse)) {
      if (isResponseSuccess(nextProps.socialLoginResponse)) {
        this.saveUserDetailsInStorage(nextProps.socialLoginResponse.response);
      } else {
        Alert.alert('Message', nextProps.socialLoginResponse.response.message);
      }
    }
  }

  onChangeEmailText(email) {
    this.setState({ email });
  }

  onChangePasswordText(password) {
    this.setState({ password });
  }

  onSubmitEditing(key) {
    try {
      switch (key) {
        case 'email':
          this.passwordInput.focus();
          break;
        case 'password':
          break;
        default:
          break;
      }
    } catch (error) {
      console.log(error);
    }
  }

  async setupGoogleSignin() {
    try {
      await GoogleSignin.hasPlayServices({ autoResolve: true });
      await GoogleSignin.configure({
        androidClientId: constant.androidClientId,
        iosClientId: constant.iosClientId,
        webClientId: constant.webClientId,
        offlineAccess: false,
      });
    } catch (err) {
      console.log('Google signin error', err.code, err.message);
    }
  }

  getTextInputReference(key, reference) {
    switch (key) {
      case 'email':
        this.emailInput = reference;
        break;
      case 'password':
        this.passwordInput = reference;
        break;
      default:
        break;
    }
  }

  async saveUserDetailsInStorage(userDetails) {
    await this.utils.setItemWithKeyAndValue(constant.USER_DETAILS, userDetails);
    this.pushNext();
  }

  pushNext() {
    this.props.resetlogout();
    resetRoute(this.props.navigation, constant.screens.DRAWER_STACK);
  }

  resetLoginState() {
    LoginManager.logOut();
  }

  loginPress() {
    const { email, password } = this.state;
    if (validateEmail(email)) {
      Alert.alert(
        'Message',
        'Your e-mail is incorrect.',
      );
      return;
    }
    if (validatePassword(password)) {
      Alert.alert(
        'Message',
        'Please enter atleast 6-digit password.',
      );
      return;
    }

    networkConnectivity().then(() => {
      this.props.userLoginRequest(email, password, this.deviceToken, this.deviceType);
    }).catch((error) => {
      console.log(error);
      Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
    });
  }

  facebookLoginPressed() {
    LoginManager.logInWithPublishPermissions(['publish_actions']).then(
      (result) => {
        if (result.isCancelled) {
          Alert.alert('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              this.callSocialLogin('facebook', data.accessToken);
            },
          );
        }
      },
      (error) => {
        console.log(error);
      },
    );
  }

  googleLoginPressed() {
    GoogleSignin.signIn().then((user) => {
      this.callSocialLogin('google_oauth2', user.accessToken);
    }).catch((googleError) => {
      console.log(googleError);
    }).done();
  }

  callSocialLogin(provider, token) {
    networkConnectivity().then(() => {
      this.props.socialLoginRequest(provider, token, this.deviceToken, this.deviceType);
    }).catch((error) => {
      console.log(error);
      Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
    });
  }

  goToForgotPassword() {
    this.props.navigation.navigate(constant.screens.FORGOT_PASSWORD);
  }

  goToRegister() {
    this.props.navigation.navigate(constant.screens.REGISTER);
  }

  render() {
    const { isLoading } = this.props;
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar
          hidden
          translucent
        />
        <LoginContainer
          getTextInputReference={(key, reference) => this.getTextInputReference(key, reference)}
          emailText={this.state.email}
          onChangeEmailText={email => this.onChangeEmailText(email)}
          passwordText={this.state.password}
          onChangePasswordText={password => this.onChangePasswordText(password)}
          loginPress={() => this.loginPress()}
          goToRegister={() => this.goToRegister()}
          goToForgotPassword={() => this.goToForgotPassword()}
          onSubmitEditing={key => this.onSubmitEditing(key)}
          facebookLoginPressed={() => this.facebookLoginPressed()}
          googleLoginPressed={() => this.googleLoginPressed()}
        />
        {isLoading && <Loader isAnimating={isLoading} />}
      </View>
    );
  }
}

Login.propTypes = {
  isLoading: PropTypes.bool,
  loginUserResponse: PropTypes.objectOf(PropTypes.any),
  userLoginRequest: PropTypes.func,
  navigation: PropTypes.objectOf(PropTypes.any),
  isSocialLoading: PropTypes.bool,
  socialLoginRequest: PropTypes.func,
  resetlogout: PropTypes.func,
  socialLoginResponse: PropTypes.objectOf(PropTypes.any),
};

Login.defaultProps = {
  isLoading: false,
  loginUserResponse: {},
  userLoginRequest: () => {},
  navigation: {},
  isSocialLoading: false,
  socialLoginRequest: () => {},
  resetlogout: () => {},
  socialLoginResponse: {},
};

const mapStateToProps = state => ({
  isLoading: state.user.isLoading,
  loginUserResponse: state.user.loginUserResponse,
  isSocialLoading: state.user.isLoading,
  socialLoginResponse: state.user.socialLoginResponse,
});

const mapDispatchToProps = () => LoginActions;

const LoginScreen = connect(mapStateToProps, mapDispatchToProps)(Login);

export default LoginScreen;
