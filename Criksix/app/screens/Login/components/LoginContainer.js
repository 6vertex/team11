import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../../components/CustomTextInput';
import { images } from '../../../assets/images';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  backgroundImage: {
    width,
    height,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  logo: {
    width: width * 0.60,
    height: 60,
    marginTop: height * 0.055,
    resizeMode: 'contain',
  },
  headerView: {
    width: (width / 3) * 2.6,
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 20,
    marginBottom: 0,
  },
  headerText: {
    color: 'white',
    fontSize: 28,
    fontWeight: '400',
    textAlign: 'left',
  },
  forgotPasswordView: {
    width: (width / 3) * 2.5,
    height: 30,
    marginTop: 0,
    marginBottom: 5,
    alignItems: 'flex-end',
  },
  forgotPasswordButton: {
    padding: 8,
    paddingRight: 0,
  },
  forgotPasswordText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '400',
    textAlign: 'right',
  },
  buttonTouchable: {
    marginBottom: 8,
    marginTop: 14,
  },
  buttonContainer: {
    width: (width / 2.2),
    paddingVertical: 6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 22,
    fontWeight: '400',
    textAlign: 'center',
  },
  orContainer: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
  },
  orText: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  separator: {
    height: 1,
    backgroundColor: '#768F70',
    width: 50,
    marginHorizontal: 12,
  },
  socialButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  socialImageButton: {
    marginHorizontal: 12,
  },
  socialImage: {
    width: (width / 3) + 20,
    height: 42,
    backgroundColor: 'transparent',
    borderRadius: 5,
  },
  signUpTextContainer: {
    flexDirection: 'row',
    // transform: [{ rotate: '12deg' }],
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'flex-end',
    alignSelf: 'stretch',
    bottom: 50,
    right: 20,
    position: 'absolute',
  },
  signUpTouchable: {
    marginLeft: 10,
  },
  signUpImage: {
    width: 50,
    height: 50,
    resizeMode: 'cover',
    // transform: [{ rotate: '-12deg' }],
  },
  signUpText: {
    fontSize: 14,
    color: 'white',
  },
});


const LoginContainer = props => (
  <Image
    style={styles.backgroundImage}
    source={images.loginBackground}
  >
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <Image
          source={images.crickSixLogo}
          style={styles.logo}
        />
        <View style={styles.headerView}>
          <Text style={styles.headerText}>Login</Text>
        </View>
        <CustomTextInput
          title="Email*"
          inputKey={'email'}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          keyboardType="email-address"
          value={props.emailText}
          returnKeyType="next"
          onChangeText={email => props.onChangeEmailText(email)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />

        <CustomTextInput
          title="Password*"
          inputKey={'password'}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          secureTextEntry={Boolean(true)}
          returnKeyType="done"
          value={props.passwordText}
          onChangeText={password => props.onChangePasswordText(password)}
          onSubmitEditing={key => props.onSubmitEditing(key)}
        />

        <View style={styles.forgotPasswordView}>
          <TouchableOpacity
            style={styles.forgotPasswordButton}
            onPress={() => props.goToForgotPassword()}
          >
            <Text style={styles.forgotPasswordText}>
              Forgot Password?
            </Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          style={styles.buttonTouchable}
          activeOpacity={0.7}
          onPress={() => props.loginPress()}
        >
          <View style={styles.buttonContainer}>
            <Text style={styles.buttonText}>Login</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.orContainer}>
          <View style={styles.separator} />
          <Text style={styles.orText}>OR</Text>
          <View style={styles.separator} />
        </View>

        <View style={styles.socialButtonContainer}>
          <TouchableOpacity
            style={styles.socialImageButton}
            activeOpacity={0.7}
            onPress={() => props.facebookLoginPressed()}
          >
            <Image
              style={styles.socialImage}
              source={images.fbLoginIcon}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.socialImageButton}
            activeOpacity={0.7}
            onPress={() => props.googleLoginPressed()}
          >
            <Image
              style={styles.socialImage}
              source={images.gLoginIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAwareScrollView>
    <View style={styles.signUpTextContainer}>
      {/* <Text style={styles.signUpText}>Not a member ? </Text>
      <Text style={[styles.signUpText, { color: 'red' }]}>Register</Text> */}
      <TouchableOpacity
        style={styles.signUpTouchable}
        activeOpacity={0.7}
        onPress={() => props.goToRegister()}
      >
        <Image style={styles.signUpImage} source={images.addButton} />
      </TouchableOpacity>
    </View>
  </Image>
);

LoginContainer.propTypes = {
  getTextInputReference: PropTypes.func,
  emailText: PropTypes.string,
  onChangeEmailText: PropTypes.func,
  passwordText: PropTypes.string,
  onChangePasswordText: PropTypes.func,
  loginPress: PropTypes.func,
  goToRegister: PropTypes.func,
  goToForgotPassword: PropTypes.func,
  onSubmitEditing: PropTypes.func,
  facebookLoginPressed: PropTypes.func,
  googleLoginPressed: PropTypes.func,
};

LoginContainer.defaultProps = {
  getTextInputReference: () => {},
  emailText: '',
  onChangeEmailText: () => {},
  passwordText: '',
  onChangePasswordText: () => {},
  loginPress: () => {},
  goToRegister: () => {},
  goToForgotPassword: () => {},
  onSubmitEditing: () => {},
  facebookLoginPressed: () => {},
  googleLoginPressed: () => {},
};

export default LoginContainer;
