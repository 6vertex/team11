import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, } from 'react-native';
import { images } from '../../../assets/images';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  text: {
    marginLeft: 12,
    fontSize: 16,
  },
  photo: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  rowIcon: {

  },
  rowIconStyle: {
    width: 32,
    height: 32,
  },
  rowText: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  rowTitle: {

  },
  rowTitleStyle: {
    color: 'white', 
    fontWeight: '500', 
    paddingLeft: 15, 
    fontSize: 18,
  },
  rowSubtitle: {

  },
  rowSubtitleStyle: {
    color: 'white', 
    fontWeight: '200', 
    paddingLeft: 15, 
    fontSize: 12,
  },
  rowImage: {
    justifyContent: 'center',
  },
});

const Row = (props) => (
  <TouchableOpacity 
    activeOpacity={0.7}
    onPress={() => props.rowAction()}
  >
  <View style={styles.container}>
    <View style={styles.rowIcon}>
      <Image style={styles.rowIconStyle} source={props.rowImage}/>
    </View>

    <View style={styles.rowText}>
        <Text style={styles.rowTitleStyle}>{props.rowTitle}  {props.payload} </Text> 
        <Text style={styles.rowSubtitleStyle}>{props.subTitle}</Text> 
    </View>
    <View style={styles.rowImage}>
       <Image style={{ width: 20, height: 20 }} source={images.icon}/>
    </View>
  </View>
  </TouchableOpacity>
);

Row.propTypes = {
  rowImage: PropTypes.number,
  rowTitle: PropTypes.string, 
  payload: PropTypes.string,
  subTitle: PropTypes.string,
};

Row.defaultProps = {
  rowAction: () => {},
};

export default Row;