import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  ScrollView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../../components/CustomTextInput';
import { images } from '../../../assets/images';
import { isIOS } from '../../../utils/PlatformSpecific'
import Row from './Row';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  containerImage: {
    width,
    height,
  },
  container: {
    width,
    height,
  },
  backgroundImage: {
    width,
    height: 200,
    resizeMode: 'cover',
  },
  headerStyle: {
    width,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: 'transparent',
  },
  headerUserNameStyle: {
    fontSize: 16,
    fontWeight: '500',
    color: 'white',
    marginTop: 10,
  },
  headerLocationStyle: {
    fontSize: 13,
    fontWeight: '300',
    color: 'white',
  },
  contestStyle: {
    width,
    height: 70,
    flexDirection: 'row',
  },
  contestPlayedStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    backgroundColor: 'transparent',
  },
  contestWonStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    backgroundColor: 'transparent',
  },
  profilePicStyle: {
    width: 70,
    height: 70,
  },
  scrollStyle: {
    width,
    marginBottom: isIOS ? 0 : 100
  },
});

const ProfileContainer = props => (
  <Image style={styles.containerImage} source={images.generalBackground}>
  <View style={styles.container}>
    <Image style={styles.backgroundImage} source={images.bannerImage}/>
    <View style={styles.headerStyle}>
      <Image style={styles.profilePicStyle} source={images.profilePic}/>
      <Text style={styles.headerUserNameStyle}>{props.username}</Text>
      <Text style={styles.headerLocationStyle}>{props.locationUser}</Text>
    </View>

    <View style={styles.contestStyle}>
      <View style={styles.contestPlayedStyle}>
        <Text style={styles.headerUserNameStyle}>Contest Played</Text>
        <Text style={styles.headerLocationStyle}>{props.numberOfContestPlayed}</Text>
      </View>
      <View style={{backgroundColor: 'white', width: 2, height: 70,}}/>
      <View style={styles.contestWonStyle}>
        <Text style={styles.headerUserNameStyle}>Contest Won</Text>
        <Text style={styles.headerLocationStyle}>{props.numberOfContestWon}</Text>
      </View>
    </View>
    <View style={{backgroundColor: 'white', width, height: 1,}}/>
    <ScrollView style={styles.scrollStyle}>
      <Row
        rowImage={images.myAccount}
        rowTitle={'My Account'}
        payload={props.balance}
        subTitle={'For deposits, withdrawals, bonus etc'}
      />
      <View style={{backgroundColor: 'white', width, height: 1,}}/>
      <Row
       rowImage={images.personalAccount}
       rowTitle={'Personal Details'}
       rowAction={() => props.goToPersonalDetails()}
       subTitle={'Your email, password, date of birth etc'}
     />
     <View style={{backgroundColor: 'white', width, height: 1,}}/>
     <Row
       rowImage={images.rupeeCoin}
       rowTitle={'REFER A FRIEND'}
       rowAction={() => props.goToInviteFriends()}
       subTitle={'get 200 criksix points for every friend that joins'}
     />
    </ScrollView>
  </View>
  </Image>
);

ProfileContainer.propTypes = {
  username: PropTypes.string,
  locationUser: PropTypes.string,
  numberOfContestPlayed: PropTypes.number,
  numberOfContestWon: PropTypes.number,
  balance: PropTypes.string,
  dataSource: PropTypes.objectOf(PropTypes.any),
};

ProfileContainer.defaultProps = {
};

export default ProfileContainer;
