import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
  StatusBar,
  ListView,
  AsyncStorage,
} from 'react-native';
import { connect } from 'react-redux';
import { GoogleSignin } from 'react-native-google-signin';
import UserActions from '../../actions';
import ProfileContainer from './components/ContainerView';
import Loader from '../../components/Loader';
import { constant, resetRoute } from '../../utils';
import Utils from '../../utils/utils';
import NavigationBar from '../../components/NavigationBar';
import { images } from '../../assets/images';
import GeolocationModule from '../../utils/GeolocationModule';

let utils = new Utils();
class Profile extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      userName: '',
      locationUser: '',
      numberOfContestPlayed: 0,
      numberOfContestWon: 0,
      balance: '',
      dataSource: ds.cloneWithRows([]),
    };
  }

  componentDidMount() {
    GeolocationModule.findCurrentLocation((location, error) => {
      if (location !== null) {
        this.setState({ locationUser: `${location.state_name}, ${location.country_name}` });
      }
    });
    utils.checkInternetConnectivity((reach) => {
      if (reach) {
        utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.getUserProfile(accessToken);
          } else {
            Alert.alert('Message', 'There is some issue while uploading.\nPlease login again.');
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }

    if (nextProps.logoutUserResponse.response &&
      nextProps.logoutUserResponse.status >= 200 &&
      nextProps.logoutUserResponse.status <= 300) {
      this.props.resetlogout();
      AsyncStorage.removeItem(constant.USER_DETAILS)
        .then(fullfilled => {
          if(GoogleSignin.currentUserAsync().access_token) {
            GoogleSignin.signOut()
            .then((success) => console.log('Successfully logout'));
            }
            resetRoute(this.props.navigation, constant.screens.LOGIN);
            return;
        }, onRejected => {
          console.log('Something Went wrong');
      });
    }

    if (nextProps.userProfileResponse.response &&
        nextProps.userProfileResponse.status >= 200 &&
        nextProps.userProfileResponse.status <= 300) {
      this.setState({
        userName: nextProps.userProfileResponse.response.first_name,
        // locationUser: nextProps.userProfileResponse.response.state,
        numberOfContestPlayed: nextProps.userProfileResponse.response.contest_played,
        numberOfContestWon: nextProps.userProfileResponse.response.contest_won,
        balance: `( Pts ${nextProps.userProfileResponse.response.balance} )`,
      });

      // if (this.state.location == null) {
      //   this.setState({
      //     locationUser: 'not updated',
      //   });
      // }
    }
    if (nextProps.userProfileResponse.response && nextProps.userProfileResponse.status >= 400) {
      if (nextProps.userProfileResponse.response.message && typeof nextProps.userProfileResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.userProfileResponse.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  goToPersonalDetails() {
    this.props.navigation.navigate(constant.screens.PERSONAL_DETAILS);
  }

  goToInviteFriends() {
    this.props.navigation.navigate(constant.screens.INVITE_FRIENDS);
  }

  logoutUser() {
    utils.checkInternetConnectivity((reach) => {
      if (reach) {
        utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.userLogoutRequest(accessToken);
          } else {
            // TODO: Handle this error properly.
            Alert.alert(constant.loadingError.header, constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(
          constant.noInternetConnection.header,
          constant.noInternetConnection.message,
        );
      }
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          backgroundColor="#0C0D18"
          barStyle="light-content"
        />
        <NavigationBar
          title="My Profile"
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <ProfileContainer
          username={this.state.userName}
          locationUser={this.state.locationUser}
          numberOfContestPlayed={this.state.numberOfContestPlayed}
          numberOfContestWon={this.state.numberOfContestWon}
          balance={this.state.balance}
          dataSource={this.state.dataSource}
          goToPersonalDetails={() => this.goToPersonalDetails()}
          goToInviteFriends={() => this.goToInviteFriends()}
        />

        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
        {this.props.isLogoutLoading && <Loader isAnimating={this.props.isLogoutLoading} />}
      </View>
    );
  }
}

Profile.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  isSessionExpired: PropTypes.bool,
  isLoading: PropTypes.bool,
  isLogoutLoading: PropTypes.bool,
  userProfileResponse: PropTypes.objectOf(PropTypes.any),
  logoutUserResponse: PropTypes.objectOf(PropTypes.any),
  resetlogout: PropTypes.func,
  getUserProfile: PropTypes.func,
};

Profile.defaultProps = {
  navigation: {},
  isSessionExpired: false,
  isLoading: false,
  isLogoutLoading: false,
  userProfileResponse: {},
  logoutUserResponse: {},
  resetlogout: undefined,
  getUserProfile: undefined,
};

const mapStateToProps = state => ({
  isSessionExpired: state.sessionExpire.isSessionExpired,
  isLoading: state.userProfile.isLoading,
  isLogoutLoading: state.userLogout.isLoading,
  userProfileResponse: state.userProfile.userProfileResponse,
  logoutUserResponse: state.userLogout.logoutUserResponse,
});

const mapDispatchToProps = () => UserActions;

const ProfileScreen = connect(mapStateToProps, mapDispatchToProps)(Profile);

export default ProfileScreen;
