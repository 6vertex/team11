import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  ScrollView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../../../components/CustomTextInput';
import { images } from '../../../assets/images';
import { isIOS } from '../../../utils/PlatformSpecific'

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  containerImage: {
    width,
    height,
  },
  container: {
    width,
    height,
    alignItems: 'center',
    padding: 20,
  },
  mainViewStyle: {
    flex: 1,
    width,
    backgroundColor: '#080913',
  },
});

const MyAccountContainer = props => (
  <View style={styles.mainViewStyle}>
  <View style={styles.container}>
    <View style={{ width, height: 50, marginBottom: 40, backgroundColor: '#e71636', alignItems: 'center', justifyContent: 'center',}}>
      <Text style={{color: 'white', fontSize: 18, fontWeight: '500',}}>{props.amount}</Text>
    </View>

    <CustomTextInput
          title="Enter Amount"
          inputKey={'amount'}
          getTextInputReference={(key, reference) => props.getTextInputReference(key, reference)}
          value={props.amountToAdd}
          onChangeText={text => props.onChangeLastName(text)}
    />

    <TouchableOpacity style={{ width: 100, height: 50, marginTop: 20, backgroundColor: '#e71636', alignItems: 'center', justifyContent: 'center',}}
      onPress={() => props.onAddAmountButtonClick()}
    >
      <Text style={{color: 'white', fontSize: 18, fontWeight: '500',}}>ADD CASH</Text>
    </TouchableOpacity>
  </View>
  </View>
);

MyAccountContainer.propTypes = {
};

MyAccountContainer.defaultProps = {
};

export default MyAccountContainer;
