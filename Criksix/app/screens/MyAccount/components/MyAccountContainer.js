import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { containerStyles, labelStyles } from '../../../theme';

const styles = StyleSheet.create({
  container: {
    ...containerStyles.defaultContainer,
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const DashboardContainer = props => (
  <View style={styles.container}>
    <Text style={labelStyles.largeWhiteLabel}>Coming Soon ....</Text>
  </View>
);

DashboardContainer.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

DashboardContainer.defaultProps = {
  navigation: {},
};

export default DashboardContainer;
