import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import MyAccountContainer from './components/ContainerView';
import Loader from '../../components/Loader';
import constant from '../../utils/constant';
import Utils from '../../utils/utils';
import NavigationBar from '../../components/NavigationBar';
import { isResponseValid } from '../../utils';
import { StatusBar } from '../../components';
import { images } from '../../assets/images';

class MyAccount extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      amount: '',
      amountToAdd: '',
    };
  }

  componentDidMount() {
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
            this.props.getMyAccount(accessToken, 'username/email');
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  componentWillReceiveProps(nextProps) {

    if (isResponseValid(nextProps.accountResponse)) {
      this.setState({
        amount: nextProps.accountResponse.response.amount,
      });
    }
    if (nextProps.accountResponse.response && nextProps.accountResponse.status >= 400) {
      if (nextProps.accountResponse.response.message &&
          typeof nextProps.accountResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.accountResponse.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  onAddAmountButtonClick() {
    this.setState({
      amount: parseInt(this.state.amount) + parseInt(this.state.amountToAdd) + '',
      amountToAdd: '',
    });
  }

  onChangeLastName(amount) {
    this.setState({ amountToAdd: amount });
  }


  getTextInputReference(key, reference) {
    switch (key) {
      case 'amount':
        this.amountInput = reference;
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar />
        <NavigationBar
          title={'MY ACCOUNT'}
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <MyAccountContainer
          amount={this.state.amount}
          amountToAdd={this.state.amountToAdd}
          onAddAmountButtonClick={() => this.onAddAmountButtonClick()}
          getTextInputReference={(key, reference) => this.getTextInputReference(key, reference)}
          onChangeLastName={amount => this.onChangeLastName(amount)}
        />

        {this.props.isLoading && <Loader isAnimating={this.props.isLoading} />}
      </View>
    );
  }
}

MyAccount.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  getMyAccount: PropTypes.func,
  accountResponse: PropTypes.objectOf(PropTypes.any),
};

MyAccount.defaultProps = {
  isLoading: false,
  navigation: {},
  getMyAccount: () => {},
  accountResponse: {},
};

const mapStateToProps = state => ({
  accountResponse: state.account.accountResponse,
  isLoading: state.account.isLoading,
});

const mapDispatchToProps = () => UserActions;

const MyAccountScreen = connect(mapStateToProps, mapDispatchToProps)(MyAccount);

export default MyAccountScreen;
