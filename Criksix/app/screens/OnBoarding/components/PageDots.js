import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

const styles = {
  container: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  element: {
    marginHorizontal: 3,
  },
  elementCheck: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '900',
  },
  elementDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
  },
};

const PageCheckmark = ({ style }) => (
  <Text style={{ ...styles.element, ...styles.elementCheck, ...style }}>✓</Text>
);

PageCheckmark.propTypes = {
  style: PropTypes.shape({}),
};

PageCheckmark.defaultProps = {
  style: {},
};

const getBackgroundColor = (isLight, selected) => {
  if (isLight) {
    if (selected) {
      return 'rgba(0, 0, 0, 0.8)';
    }
    return 'rgba(0, 0, 0, 0.3)';
  }
  if (selected) {
    return '#fff';
  }
  return 'rgba(255, 255, 255, 0.5)';
};

const PageDot = ({ isLight, selected }) => (
  <View
    style={{
      ...styles.element,
      ...styles.elementDot,
      backgroundColor: getBackgroundColor(isLight, selected),
    }}
  />
);

PageDot.propTypes = {
  isLight: PropTypes.bool,
  selected: PropTypes.bool,
};

PageDot.defaultProps = {
  isLight: true,
  selected: true,
};

const PageDots = ({ isLight, pages, currentPage }) => (
  <View style={styles.container}>
    <PageCheckmark style={{ color: 'rgba(255, 255, 255, 0)' }} />
    {Array.from(new Array(pages), (x, i) => i).map(page => (
      <PageDot key={page} selected={page === currentPage} isLight={isLight} />
    ))}
    <PageCheckmark style={{ color: isLight ? 'rgba(0, 0, 0, 0.3)' : 'rgba(255, 255, 255, 0.5)' }} />
  </View>
);

PageDots.propTypes = {
  isLight: PropTypes.bool,
  pages: PropTypes.number,
  currentPage: PropTypes.number,
};

PageDots.defaultProps = {
  isLight: true,
  pages: 0,
  currentPage: 0,
};

export default PageDots;
