import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Text } from 'react-native';

const SymbolButton = ({
  size, onPress, style, textStyle, children,
}) => (
  <View style={{
    height: size, width: size, justifyContent: 'center', ...style,
    }}
  >
    <TouchableOpacity style={{ flex: 0 }} onPress={onPress}>
      <Text style={{ textAlign: 'center', fontSize: size / 1.7, ...textStyle }}>{children}</Text>
    </TouchableOpacity>
  </View>
);

SymbolButton.propTypes = {
  isLight: PropTypes.bool,
  size: PropTypes.number,
  onPress: PropTypes.func,
  style: PropTypes.shape({}),
  textStyle: PropTypes.shape({}),
  children: PropTypes.string,
};

SymbolButton.defaultProps = {
  isLight: true,
  size: 40,
  onPress: () => {},
  style: {},
  textStyle: {},
  children: '',
};

const TextButton = ({
  size, onPress, textStyle, children,
}) => (
  <View style={{ flex: 0 }}>
    <TouchableOpacity style={{ flex: 0 }} onPress={onPress}>
      <Text style={{ fontSize: size / 2.5, ...textStyle }}>{children}</Text>
    </TouchableOpacity>
  </View>
);

TextButton.propTypes = {
  size: PropTypes.number,
  onPress: PropTypes.func,
  textStyle: PropTypes.shape({}),
  children: PropTypes.string,
};

TextButton.defaultProps = {
  size: 20,
  onPress: () => {},
  textStyle: {},
  children: '',
};

export { SymbolButton, TextButton };
