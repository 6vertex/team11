import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';


const styles = {
  container: {
    backgroundColor: '#000000',
    opacity: 0.85,
    height: 130,
    paddingHorizontal: 0,
    marginBottom: 0,
    justifyContent: 'center',
  },
  joinStyle: {
    width: 180,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
  },
  joinButton: {
    alignItems: 'center',
  },
};

const JoinButton = ({ title, onJoin, onLogin }) => (
  <View style={styles.container}>
    <TouchableOpacity style={styles.joinButton} onPress={() => onJoin()}> 
      <View style={styles.joinStyle}>
        <Text style = {{color: 'white', fontSize: 22,}}>{title}</Text>
      </View>
    </TouchableOpacity>
    <View style= {{flex:1, justifyContent: 'center', marginTop: 20, alignItems: 'flex-start', flexDirection:'row'}}>
       <Text style= {{color:'white', fontSize: 15}}>Already Have An account?</Text>
       <Text style= {{color:'red', fontSize: 15}} onPress={() => onLogin()}> Log in</Text>
    </View>
  </View>
);

JoinButton.propTypes = {
  title: PropTypes.string,
  onJoin: PropTypes.func,
  onLogin: PropTypes.func,
};

JoinButton.defaultProps = {
  title: '',
  onJoin: () => {},
  onLogin: () => {},
};

export default JoinButton;