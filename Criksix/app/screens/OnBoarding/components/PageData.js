import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, } from 'react-native';
import { images } from '../../../assets/images';

const styles = {
  content: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 100,
  },
  image: {
    flex: 0,
    marginTop: 40,
    paddingBottom: 0,
    alignItems: 'center',
  },
  centerImage: {
    marginTop: 0,
    alignItems: 'center',
    paddingTop: 175,
  },
  titleLight: {
    color: '#000',
  },
  subtitle: {
    marginTop: -20,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: 'rgba(255, 255, 255, 0.7)',
  },
  subtitle2: {
    marginTop: 10,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: 'rgba(255, 255, 255, 0.7)',
  },
  subtitleLight: {
    color: 'rgba(0, 0, 0, 0.7)',
  },
};

const Page = ({ width, height, children }) => (
  <View style={{ width, height }}>
    {children}
  </View>
);

Page.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  children: PropTypes.node.isRequired,
};

Page.defaultProps = {
  width: 0,
  height: 0,
};

const PageContent = ({ children }) => (
  <View style={styles.content}>
    <View style={{ flex: 0 }}>
      {children}
    </View>
  </View>
);

PageContent.propTypes = {
  children: PropTypes.node.isRequired,
};

const PageData = ({
  isLight, image, centerImage, subtitle, subtitle2, ...rest
}) => (
  <Page {...rest}>
    <PageContent>
      <View style={styles.image}>
        {image}
      </View>
      <View style={styles.centerImage}>
        {centerImage}
      </View>
      <Text style={styles.subtitle}>
        {subtitle}
      </Text>
      <Text style={styles.subtitle2}>
        {subtitle2}
      </Text>
    </PageContent>
  </Page>
);

PageData.propTypes = {
  isLight: PropTypes.bool,
  image: PropTypes.element,
  centerImage: PropTypes.element,
  subtitle: PropTypes.string,
  subtitle2: PropTypes.string,
};

PageData.defaultProps = {
  isLight: true,
  image: null,
  centerImage: null,
  subtitle: '',
  subtitle2: '',
};

export default PageData;
