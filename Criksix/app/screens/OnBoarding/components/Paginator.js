import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import PageDots from './PageDots';
import { SymbolButton, TextButton } from './Buttons';


const styles = {
  container: {
    backgroundColor: '#00000090',
    height: 60,
    paddingHorizontal: 0,
    marginBottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerOverlay: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
  buttonLeft: {
    width: 70,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  buttonRight: {
    width: 70,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
};


const getDefaultStyle = isLight => ({
  color: isLight ? 'rgba(0, 0, 0, 0.8)' : '#fff',
});

const SkipButton = ({ isLight, ...props }) => (
  <TextButton {...props} textStyle={getDefaultStyle(isLight)}>
    Skip
  </TextButton>
);

SkipButton.propTypes = {
  isLight: PropTypes.bool,
};

SkipButton.defaultProps = {
  isLight: true,
};

const NextButton = ({ isLight, ...props }) => (
  <SymbolButton {...props} textStyle={getDefaultStyle(isLight)}>
    →
  </SymbolButton>
);

NextButton.propTypes = {
  isLight: PropTypes.bool,
};

NextButton.defaultProps = {
  isLight: true,
};

const DoneButton = ({ isLight, size, ...props }) => (
  <SymbolButton {...props} size={size} textStyle={getDefaultStyle(isLight)} style={{ borderRadius: size / 2, backgroundColor: 'rgba(255, 255, 255, 0.10)' }}>
    ✓
  </SymbolButton>
);

DoneButton.propTypes = {
  isLight: PropTypes.bool,
  size: PropTypes.number,
};

DoneButton.defaultProps = {
  isLight: true,
  size: 40,
};

const showDoneButton = (showDone, isLight, BUTTON_SIZE, onEnd) => (
  showDone ? <DoneButton isLight={isLight} size={BUTTON_SIZE} onPress={onEnd} /> : null
);

const showNextButton = (showNext, isLight, BUTTON_SIZE, onNext) => (
  showNext ? <NextButton isLight={isLight} size={BUTTON_SIZE} onPress={onNext} /> : null
);

const BUTTON_SIZE = 40;

const Paginator = ({
  isLight, overlay, showSkip, showNext, showDone, pages, currentPage, onEnd, onNext,
}) => (
  <View style={{ ...styles.container, ...(overlay ? styles.containerOverlay : {}) }}>
    <View style={styles.buttonLeft}>
      {showSkip && currentPage + 1 !== pages ?
        <SkipButton isLight={isLight} size={BUTTON_SIZE} onPress={onEnd} /> :
        null
      }
    </View>
    <PageDots isLight={isLight} pages={pages} currentPage={currentPage} />
    <View style={styles.buttonRight}>
      {currentPage + 1 === pages ?
        showDoneButton(showDone, isLight, BUTTON_SIZE, onEnd) :
        showNextButton(showNext, isLight, BUTTON_SIZE, onNext)
      }
    </View>
  </View>
);

Paginator.propTypes = {
  isLight: PropTypes.bool,
  overlay: PropTypes.bool,
  showSkip: PropTypes.bool,
  showNext: PropTypes.bool,
  showDone: PropTypes.bool,
  pages: PropTypes.number,
  currentPage: PropTypes.number,
  onEnd: PropTypes.func,
  onNext: PropTypes.func,
};

Paginator.defaultProps = {
  isLight: true,
  overlay: true,
  showSkip: true,
  showNext: true,
  showDone: true,
  pages: 0,
  currentPage: 0,
  onEnd: () => {},
  onNext: () => {},
};

export default Paginator;
