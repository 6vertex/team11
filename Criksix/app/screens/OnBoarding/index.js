import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView, StyleSheet, Image } from 'react-native';
import tinycolor from 'tinycolor2';
import PageData from './components/PageData';
import Paginator from './components/Paginator';
import JoinButton from './components/JoinButton';
import { images } from '../../assets/images';
import Utils from '../../utils/utils';
import { constant } from '../../utils';
import { StatusBar } from '../../components';
import { dimensions } from '../../theme';

const width = dimensions.getViewportWidth();
const height = dimensions.getViewportHeight();

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#00000095',
  },
  imageContainer: {
    width: 50,
    height: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: 60,
    marginBottom: height * 0.19,
    resizeMode: 'contain',
  },
  imageLogo: {
    height: 80,
    width: 240,
    marginBottom: height * 0.19,
    resizeMode: 'contain',
  },
  backgroundImage: {
    width,
    height,
    resizeMode: 'cover',
    opacity: 0.7,
  },
});

const logoImageView = (imagesSource) => {
  return (
    <View style={styles.imageContainer}>
      <Image style={styles.imageLogo} source={imagesSource} />
    </View>
  );
};

const infoImageView = (imagesSource) => {
  return (
    <View style={styles.imageContainer}>
      <Image style={styles.image} source={imagesSource} />
    </View>
  );
};

const pages = [
  {
    backgroundColor: 'transparent',
    image: logoImageView(images.crickSixLogo),
    centerImage: infoImageView(images.videoPlay),
    subtitle: 'HOW TO PLAY >>',
    subtitle2: '',
  },
  {
    backgroundColor: 'transparent',
    image: logoImageView(images.crickSixLogo),
    centerImage: infoImageView(images.contestIcon),
    subtitle: 'SELECT A CONTEST',
    subtitle2: 'ENTER FOR CASH OR FOR FUN',
  },
  {
    backgroundColor: 'transparent',
    image: logoImageView(images.crickSixLogo),
    centerImage: infoImageView(images.playerIcon),
    subtitle: 'SELECT ANY 6 PLAYERS',
    subtitle2: 'TO CREATE YOUR TEAM',
  },
  {
    backgroundColor: 'transparent',
    image: logoImageView(images.crickSixLogo),
    centerImage: infoImageView(images.winnerIcon),
    subtitle: 'WATCH AND WIN',
    subtitle2: 'TOP SCORE WINS',
  },
];

class OnBoarding extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      currentPage: 0,
    };
  }

  updatePosition(event) {
    const { contentOffset, layoutMeasurement } = event.nativeEvent;
    const pageFraction = contentOffset.x / layoutMeasurement.width;
    const page = Math.round(pageFraction);
    const isLastPage = pages.length === page + 1;
    if (isLastPage && pageFraction - page > 0.3) {
      this.onEndPress();
    } else {
      this.setState({ currentPage: page });
    }
  }

  goNextPressed() {
    const { currentPage } = this.state;
    const nextPage = currentPage + 1;
    const offsetX = nextPage * width;
    this.scrollViewRef.scrollTo({ x: offsetX, animated: true });
    this.setState({ currentPage: nextPage });
  }

  displayLandingPage() {
    this.utils.setItemWithKeyAndValue(constant.OPEN_APP_FIRST_TIME, false);
    this.props.navigation.navigate(constant.screens.LANDING_PAGE);
  }

  displayLoginPage() {
    this.utils.setItemWithKeyAndValue(constant.OPEN_APP_FIRST_TIME, false);
    this.props.navigation.navigate(constant.screens.LOGIN);
  }

  render() {
    const currentPage = pages[this.state.currentPage] || pages[0];
    const { backgroundColor } = currentPage;
    const isLight = tinycolor(backgroundColor).getBrightness() > 180;

    return (
      <Image
        style={styles.backgroundImage}
        source={images.landingBackground}
      >
        <StatusBar
          hidden
        />
        <View style={styles.container}>
          <ScrollView
            ref={(ref) => {
              this.scrollViewRef = ref;
            }}
            pagingEnabled={Boolean(true)}
            horizontal={Boolean(true)}
            showsHorizontalScrollIndicator={false}
            onScroll={(event) => {
              this.updatePosition(event);
            }}
            scrollEventThrottle={100}
          >
            {pages.map(({
              image,
              centerImage,
              subtitle,
              subtitle2,
            }, idx) => {
              const index = idx;
              return (
                <PageData
                  key={index}
                  isLight={isLight}
                  image={image}
                  centerImage={centerImage}
                  subtitle={subtitle}
                  subtitle2={subtitle2}
                  width={width}
                  height={height}
                />
              );
            })}
          </ScrollView>

          <View style={{
            backgroundColor: '#000000',
            opacity: 0.85,
            }}
          >
            <Paginator
              isLight={isLight}
              overlay={Boolean(true)}
              showSkip={Boolean(false)}
              showNext={Boolean(false)}
              showDone={Boolean(false)}
              pages={pages.length}
              currentPage={this.state.currentPage}
              onEnd={() => this.displayLandingPage()}
              onNext={() => this.goNextPressed()}
            />
          </View>
          <JoinButton
            title={'Join'}
            onJoin={() => this.displayLandingPage()}
            onLogin={() => this.displayLoginPage()}
          />
        </View>
      </Image>
    );
  }
}

OnBoarding.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
};

OnBoarding.defaultProps = {
  navigation: {},
};
export default OnBoarding;
