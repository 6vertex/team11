import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import MyTeamsContainer from './components/Container';
import { NavigationBar, StatusBar } from '../../components';
import { images } from '../../assets/images';
import { constant, resetRoute } from '../../utils';
import Utils from '../../utils/utils';

class MyTeams extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      myTeams: props.navigation.state.params.contest.my_teams,
    };
  }

  componentDidMount() {
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            const accessToken = response.access_token;
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
    }
  }

  goToEditTeam(teamData) {
    if (teamData.data.length === 0) {
      Alert.alert('There is some issues', 'You can\'t edit team.');
      return;
    }
    const players = [];
    teamData.data.forEach((element) => {
      element.players.forEach((player) => {
        players.push(player);
      });
    });
    const { contest } = this.props.navigation.state.params;

    this.props.navigation.navigate(
      constant.screens.CREATE_TEAM,
      {
        contest_user_lineup: teamData.id,
        contest_id: contest.id,
        contest_name: contest.name,
        invite_code: contest.invite_code,
        create: false,
        players,
      },
    );
  }

  addNewTeam() {
    const { contest } = this.props.navigation.state.params;
    if (contest.multi_entry_count === contest.my_teams.length) {
      Alert.alert(`You can only submit ${contest.multi_entry_count} Team(s) in this Contest`);
      return;
    }
    this.props.navigation.navigate(
      constant.screens.CREATE_TEAM,
      {
        contest_id: contest.id,
        contest_name: contest.name,
        invite_code: contest.invite_code,
        create: true,
      },
    );
  }

  render() {
    const { myTeams } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <StatusBar />
        <NavigationBar
          title="MY TEAMS"
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <MyTeamsContainer
          myTeams={myTeams}
          goToEditTeam={teamData => this.goToEditTeam(teamData)}
          contest={this.props.navigation.state.params.contest}
          addNewTeam={() => this.addNewTeam()}
        />
      </View>
    );
  }
}

MyTeams.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  isSessionExpired: PropTypes.bool,
};

MyTeams.defaultProps = {
  navigation: {},
  isSessionExpired: false,
};

const mapStateToProps = state => ({
  // Session Expire
  isSessionExpired: state.sessionExpire.isSessionExpired,

  // Teams List
  isLoadingTeamList: state.teamManagement.isLoadingTeamList,
  teamsListResponse: state.teamManagement.teamsListResponse,
});

const mapDispatchToProps = () => UserActions;

const MyTeamsScreen = connect(mapStateToProps, mapDispatchToProps)(MyTeams);

export default MyTeamsScreen;
