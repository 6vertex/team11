import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Dimensions,
  ListView,
  Text,
  TouchableOpacity,
} from 'react-native';
import { images } from '../../../assets/images';
import TeamRow from './TeamRow';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width,
    backgroundColor: '#080913',
  },
  rowSeparatorView: {
    width,
    height: 2,
    backgroundColor: 'white',
  },
  bottomView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
    backgroundColor: '#e71636',
  },
  nextButtonView: {
    paddingHorizontal: 25,
    paddingVertical: 10,
    backgroundColor: '#00000070',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  nextButtonText: {
    color: 'white',
    fontSize: 15,
    fontWeight: '700',
    textAlign: 'center',
  },
});

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const Container = props => (
  <View style={styles.background}>
    <ListView
      enableEmptySections
      dataSource={ds.cloneWithRows(props.myTeams)}
      renderRow={(rowData, sectionID, rowID) => (
        <TeamRow
          teamData={rowData}
          goToEditTeam={props.goToEditTeam}
        />
      )}
      renderSeparator={() => <View style={styles.rowSeparatorView} />}
    />
    {<View style={styles.bottomView}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => props.addNewTeam()}
      >
        <View style={styles.nextButtonView}>
          <Text style={styles.nextButtonText}>Add New Team</Text>
        </View>
      </TouchableOpacity>
    </View>}
  </View>
);

Container.propTypes = {
  myTeams: PropTypes.arrayOf(PropTypes.any),
  goToEditTeam: PropTypes.func,
  addNewTeam: PropTypes.func,
};

Container.defaultProps = {
  myTeams: [],
  goToEditTeam: () => {},
  addNewTeam: () => {},
};

export default Container;
