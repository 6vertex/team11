import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import { images } from '../../../assets/images';
import { navigationBackgroundColor } from '../../../theme/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  background: {
    width: width - 30,
    marginHorizontal: 15,
    marginVertical: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: '#0c0d18',
    alignItems: 'center',
    justifyContent: 'center',
  },
  teamNameContainer: {
    marginVertical: 3,
    alignSelf: 'stretch',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  teamNameText: {
    color: 'orange',
    fontSize: 18,
    fontWeight: '600',
  },
  detailView: {
    marginTop: 12,
    alignSelf: 'stretch',
    flexDirection: 'row',
    padding: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
  },
  detailHeaderView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  detailHeader: {
    fontSize: 14,
    fontWeight: '400',
    color: '#4f5386',
  },
  detailText: {
    fontSize: 14,
    fontWeight: '700',
    textAlign: 'center',
    color: 'white',
  },
  verticalSeparatorView: {
    height: 40,
    width: 2,
    backgroundColor: 'white',
  },
  actionbuttonTouchable: {
    marginHorizontal: 6,
  },
  actionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: width * 0.25,
    height: 35,
    backgroundColor: navigationBackgroundColor,
  },
  actionButtonImage: {
    width: 15,
    height: 15,
    marginHorizontal: 2,
  },
  actionButtonText: {
    fontSize: 13,
    fontWeight: '500',
    textAlign: 'center',
    color: 'white',
    marginHorizontal: 2,
  },
});

const VerticalSeparatorView = () => (
  <View style={styles.verticalSeparatorView} />
);

const TeamRow = props => (
  <View style={styles.background}>
    <View style={styles.teamNameContainer}>
      <Text style={styles.teamNameText}>{props.teamData.team_name}</Text>
    </View>
    {props.teamData.data.length > 0 && props.teamData.data.length === 4 &&
    <View style={styles.detailView}>
      <View style={styles.detailHeaderView}>
        <Text style={styles.detailHeader}>{props.teamData.data[0].position}</Text>
        <Text style={styles.detailText}>{props.teamData.data[0].players.length}</Text>
      </View>
      {VerticalSeparatorView()}
      <View style={styles.detailHeaderView}>
        <Text style={styles.detailHeader}>{props.teamData.data[1].position}</Text>
        <Text style={styles.detailText}>{props.teamData.data[1].players.length}</Text>
      </View>
      {VerticalSeparatorView()}
      <View style={styles.detailHeaderView}>
        <Text style={styles.detailHeader}>{props.teamData.data[2].position}</Text>
        <Text style={styles.detailText}>{props.teamData.data[2].players.length}</Text>
      </View>
      {VerticalSeparatorView()}
      <View style={styles.detailHeaderView}>
        <Text style={styles.detailHeader}>{props.teamData.data[3].position}</Text>
        <Text style={styles.detailText}>{props.teamData.data[3].players.length}</Text>
      </View>
    </View>}
    <View style={[styles.detailView, { justifyContent: 'center' }]}>
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.actionbuttonTouchable}
        onPress={() => props.goToEditTeam(props.teamData)}
      >
        <View style={styles.actionButton}>
          <Image source={images.editIcon} style={styles.actionButtonImage}/>
          <Text style={styles.actionButtonText}>Edit</Text>
        </View>
      </TouchableOpacity>
      {/* <TouchableOpacity
        activeOpacity={0.7}
        style={styles.actionbuttonTouchable}
      >
        <View style={styles.actionButton}>
          <Image source={images.information} style={styles.actionButtonImage}/>
          <Text style={styles.actionButtonText}>Preview</Text>
        </View>
      </TouchableOpacity> */}
    </View>
  </View>
);

TeamRow.propTypes = {
  teamData: PropTypes.objectOf(PropTypes.any),
  goToEditTeam: PropTypes.func,
};

TeamRow.defaultProps = {
  teamData: {},
  goToEditTeam: () => {},
};

export default TeamRow;
