import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

const { width, height } = Dimensions.get('window');


const styles = StyleSheet.create({
  screenContainer: {
    width,
    height,
    backgroundColor: '#00000070',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainContainer: {
    width: width * 0.85,
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderRadius: 8,
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: 'lightgrey',
    marginHorizontal: 5,
    borderRadius: 8,
  },
  topView: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    paddingVertical: 15,
    backgroundColor: 'white',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  BottomView: {
    width: width * 0.85,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'green',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  headerView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: 'black',
    fontSize: 15,
    textAlign: 'center',
    fontWeight: '800',
    marginVertical: 3,
  },
  usableAmountText: {
    color: 'green',
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '700',
    marginTop: 25,
  },
  joiningAmountText: {
    color: 'black',
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '700',
    marginTop: 15,
  },
  termsText: {
    color: 'black',
    fontSize: 13,
    textAlign: 'left',
    fontWeight: '500',
    marginTop: 15,
  },
  joinContestText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: '700',
    paddingVertical: 12,
  },
  closeView: {
    position: 'absolute',
    right: 5,
    top: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeTouchable: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
});

const ConfirmationPopUp = props => (
  <View style={styles.screenContainer}>
    <View style={styles.mainContainer}>
      <View style={styles.topView}>
        <View style={styles.headerView}>
          <Text
            style={styles.headerText}
          >
            CONFIRMATION
          </Text>
          <Text
            style={styles.usableAmountText}
          >
            Usable Balance: ₹0
          </Text>
          <Text
            style={styles.joiningAmountText}
          >
            Joining Amount: ₹0
          </Text>
          <Text
            style={styles.termsText}
          >
            By joining this contest you accept Crick Six Terms and Condition.
          </Text>
        </View>
        <View style={styles.closeView}>
          <TouchableOpacity
            onPress={() => props.close()}
            style={styles.closeTouchable}
          >
            <Text>Close</Text>
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => props.confirmJoinContest()}
      >
        <View style={styles.BottomView}>
          <Text style={styles.joinContestText}>Join Contest</Text>
        </View>
      </TouchableOpacity>
    </View>
  </View>
);

ConfirmationPopUp.propTypes = {
  close: PropTypes.func,
  confirmJoinContest: PropTypes.func,
};

ConfirmationPopUp.defaultProps = {
  close: () => {},
  confirmJoinContest: () => {},
};

export default ConfirmationPopUp;
