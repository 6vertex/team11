import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';
import { images } from '../../../assets/images';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width,
    resizeMode: 'cover',
  },
  containerView: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0c0d18',
    marginHorizontal: 10,
    marginVertical: 10,
    paddingHorizontal: 8,
    paddingVertical: 15,
  },
  textHeader: {
    color: 'white',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 12,
  },
  inviteCodeView: {
    height: 45,
    backgroundColor: '#575b91',
    borderRadius: 45 / 2,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    marginHorizontal: 15,
    marginTop: 10,
    marginBottom: 20,
  },
  inviteCodeInput: {
    marginHorizontal: 8,
    paddingHorizontal: 20,
    backgroundColor: 'transparent',
    fontFamily: 'Arial',
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
    alignSelf: 'stretch',
  },
  joinContestView: {
    height: 40,
    backgroundColor: 'green',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 8,
    paddingHorizontal: 12,
    marginBottom: 12,
  },
  joinContestText: {
    color: 'white',
    fontSize: 13,
    fontWeight: '500',
  },
});

const JoinContestContainer = props => (
  <Image style={styles.backgroundImage} source={images.generalBackground}>
    <View style={styles.containerView}>
      <Text style={styles.textHeader}>If you have a contest Invite Code, Enter it and join</Text>
      <View style={styles.inviteCodeView}>
        <TextInput
          value={props.inviteCode}
          style={styles.inviteCodeInput}
          placeholder="--- Enter Invite Code ---"
          placeholderTextColor="#FFFFFF50"
          returnKeyType="done"
          onChangeText={text => props.onChangeInviteCode(text)}
          underlineColorAndroid="transparent"
        />
      </View>
      <View>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => props.joinThisContest()}
        >
          <View style={styles.joinContestView}>
            <Text style={styles.joinContestText}>Join This Contest</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  </Image>
);

JoinContestContainer.propTypes = {
  inviteCode: PropTypes.string,
  onChangeInviteCode: PropTypes.func,
};

JoinContestContainer.defaultProps = {
  inviteCode: '',
  onChangeInviteCode: () => {},
};

export default JoinContestContainer;
