import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View, Alert,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import JoinContestContainer from './components/Container';
import ConfirmationPopUp from '../ContestDetails/components/ConfirmationPopUp';
import { NavigationBar, StatusBar, Loader } from '../../components';
import { images } from '../../assets/images';
import {
  constant,
  resetRoute,
} from '../../utils';
import Utils from '../../utils/utils';

class JoinContest extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      inviteCode: '',
      showJoinContestPopUp: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }

    if (nextProps.joinContestByInviteResponse.response &&
      nextProps.joinContestByInviteResponse.status >= 200 &&
      nextProps.joinContestByInviteResponse.status <= 300) {
      Alert.alert('You have joined Contest');
      this.props.navigation.goBack();
      this.props.resetJoinContestWithTeamLineUp();
    }
    if (nextProps.joinContestByInviteResponse.response &&
      nextProps.joinContestByInviteResponse.status >= 400) {
      this.setState({ inviteCode: '' });
      if (nextProps.joinContestByInviteResponse.response.message &&
          typeof nextProps.joinContestByInviteResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.joinContestByInviteResponse.response.message);
        this.props.resetJoinContestWithTeamLineUp();
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
      this.props.resetJoinContestWithTeamLineUp();
    }
  }

  onChangeInviteCode(inviteCode) {
    this.setState({ inviteCode });
  }

  closeJoinContestPopUp() {
    this.setState({ showJoinContestPopUp: false });
  }

  joinThisContestClicked() {
    if (this.state.inviteCode === '') {
      Alert.alert('Message', 'Please Enter Invite Code to Join Contest');
      return;
    }
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            this.props.joinContestByInviteCode(
              response.access_token,
              this.state.inviteCode,
            );
          } else {
            Alert.alert(constant.loadingError.header, constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
    // this.setState({ showJoinContestPopUp: true });
  }

  confirmJoinContestClicked() {
    this.setState({ showJoinContestPopUp: false });
  }

  render() {
    const { showJoinContestPopUp } = this.state;
    const { joinContestByInviteLoading } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <StatusBar />
        <NavigationBar
          title="JOIN CONTEST"
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <JoinContestContainer
          inviteCode={this.state.inviteCode}
          onChangeInviteCode={text => this.onChangeInviteCode(text)}
          joinThisContest={() => this.joinThisContestClicked()}
        />
        {showJoinContestPopUp &&
          <ConfirmationPopUp
            close={() => this.closeJoinContestPopUp()}
            confirmJoinContest={() => this.confirmJoinContestClicked()}
          />
        }
        {joinContestByInviteLoading &&
          <Loader isAnimating={joinContestByInviteLoading} />
        }
      </View>
    );
  }
}

JoinContest.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  isSessionExpired: PropTypes.bool,
  joinContestByInviteLoading: PropTypes.bool,
  joinContestByInviteResponse: PropTypes.objectOf(PropTypes.any),
  joinContestByInviteCode: PropTypes.func,
};

JoinContest.defaultProps = {
  navigation: {},
  isSessionExpired: false,
  joinContestByInviteLoading: false,
  joinContestByInviteResponse: {},
  joinContestByInviteCode: () => {},
};

const mapStateToProps = state => ({
  // Session Expire
  isSessionExpired: state.sessionExpire.isSessionExpired,

  // Join Contest
  joinContestByInviteLoading: state.contestManagement.joinContestByInviteLoading,
  joinContestByInviteResponse: state.contestManagement.joinContestByInviteResponse,
});

const mapDispatchToProps = () => UserActions;

const JoinContestScreen = connect(mapStateToProps, mapDispatchToProps)(JoinContest);


export default JoinContestScreen;
