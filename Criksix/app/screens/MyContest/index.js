import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
  Share,
} from 'react-native';
import { connect } from 'react-redux';
import UserActions from '../../actions';
import MyContestContainer from './components/Container';
import { NavigationBar, StatusBar, Loader } from '../../components';
import { images } from '../../assets/images';
import {
  constant,
  resetRoute,
  isResponseValidated,
  isResponseSuccess,
} from '../../utils';
import Utils from '../../utils/utils';
import { containerStyles } from '../../theme/index';

class MyContest extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.state = {
      myContests: [],
    };
  }

  componentDidMount() {
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            this.props.getMyContests(response.access_token);
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSessionExpired === true) {
      this.props.resetlogout();
      resetRoute(this.props.navigation, constant.screens.LOGIN);
      return;
    }

    // TODO: Handle contest detail response.
    if (isResponseValidated(nextProps.myContestDetailsResponse)) {
      if (isResponseSuccess(nextProps.myContestDetailsResponse)) {
        // Navigate contest detail page and reset in reducer
        this.props.navigation.navigate(
          'MyContestDetails',
          { contest: nextProps.myContestDetailsResponse.response },
        );
        this.props.resetMyContestDetailInfo();
        return;
      }
      if (nextProps.myContestDetailsResponse.response.message &&
        typeof nextProps.myContestDetailsResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.myContestDetailsResponse.response.message);
        this.props.resetMyContestDetailInfo();
        return;
      }
    }

    if (nextProps.myContestsResponse.response &&
      nextProps.myContestsResponse.status >= 200 &&
      nextProps.myContestsResponse.status <= 300) {
      this.setState({ myContests: nextProps.myContestsResponse.response.data });
    }
    if (nextProps.myContestsResponse.response &&
      nextProps.myContestsResponse.status >= 400) {
      if (nextProps.myContestsResponse.response.message &&
          typeof nextProps.myContestsResponse.response.message === 'string') {
        Alert.alert('Message', nextProps.myContestsResponse.response.message);
        return;
      }
      Alert.alert('Message', constant.SERVER_ERROR_MESSAGE);
    }
  }

  inviteFriends(contest) {
    Share.share({
      title: 'Invite Friends to Join Contest',
      message: `Hey checkout this contest on CrikSix App and play with me.\ncriksix:${contest.invite_code}`,
      url: `criksix:${contest.invite_code}`,
    });
  }

  goToEditContestName(contest) {
    this.props.navigation.navigate(constant.screens.EDIT_CONTEST_NAME, {
      contest,
      actionAfterUpdated: () => this.performActionAfterContestNameUpdated(),
    });
  }

  performActionAfterContestNameUpdated() {
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            this.props.getMyContests(response.access_token);
          } else {
            Alert.alert('Message', constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  goToMyContestDetails(myContest) {
    this.utils.checkInternetConnectivity((reach) => {
      if (reach) {
        this.utils.getItemWithKey(constant.USER_DETAILS, (response) => {
          if (response && response.access_token) {
            this.props.getMyContestDetails(
              response.access_token,
              myContest.id,
            );
          } else {
            Alert.alert(constant.loadingError.header, constant.loadingError.message);
          }
        });
      } else {
        Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
      }
    });
  }

  goToCreateTeam(contest) {
    this.props.navigation.navigate(
      constant.screens.CREATE_TEAM,
      {
        contest_user_lineup: contest.contest_user_lineups_id[0],
        contest_id: contest.id,
        contest_name: contest.name,
        invite_code: contest.invite_code,
        create: false,
      },
    );
  }

  render() {
    const { isFetchingMyContests, myContestDetailsLoading } = this.props;
    const { myContests } = this.state;
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar />
        <NavigationBar
          title="MY CONTESTS"
          showBackButton
          backButtonImage={images.backIcon}
          backButtonAction={() => this.props.navigation.goBack()}
        />
        <MyContestContainer
          myContests={myContests}
          myContestDetailsLoading={myContestDetailsLoading}
          goToMyContestDetails={myContest => this.goToMyContestDetails(myContest)}
          inviteFriends={contest => this.inviteFriends(contest)}
          createTeam={contest => this.goToCreateTeam(contest)}
          goToEditContestName={contest => this.goToEditContestName(contest)}
        />
        {myContestDetailsLoading && <Loader isAnimating={myContestDetailsLoading} />}
        {isFetchingMyContests && <Loader isAnimating={isFetchingMyContests} />}
      </View>
    );
  }
}

MyContest.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  isSessionExpired: PropTypes.bool,
  isFetchingMyContests: PropTypes.bool,
  myContestDetailsLoading: PropTypes.bool,
  myContestDetailsResponse: PropTypes.objectOf(PropTypes.any),
  myContestsResponse: PropTypes.objectOf(PropTypes.any),
  getMyContests: PropTypes.func,
  resetMyContestDetailInfo: PropTypes.func,
  resetlogout: PropTypes.func,
  getMyContestDetails: PropTypes.func,
};

MyContest.defaultProps = {
  navigation: {},
  isSessionExpired: false,
  isFetchingMyContests: false,
  myContestDetailsLoading: false,
  myContestDetailsResponse: {},
  myContestsResponse: {},
  getMyContests: () => {},
  resetMyContestDetailInfo: () => {},
  getMyContestDetails: () => {},
  resetlogout: () => {},
};

const mapStateToProps = state => ({
  // Session Expire
  isSessionExpired: state.sessionExpire.isSessionExpired,

  // My Contests
  isFetchingMyContests: state.contestManagement.isFetchingMyContests,
  myContestsResponse: state.contestManagement.myContestsResponse,

  contestDetailLoading: state.contestManagement.contestDetailLoading,

  // My Contest Details
  myContestDetailsLoading: state.contestManagement.myContestDetailsLoading,
  myContestDetailsResponse: state.contestManagement.myContestDetailsResponse,
});

const mapDispatchToProps = () => UserActions;

const MyContestScreen = connect(mapStateToProps, mapDispatchToProps)(MyContest);

export default MyContestScreen;
