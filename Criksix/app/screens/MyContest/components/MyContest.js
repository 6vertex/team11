import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image } from 'react-native';
import { images } from '../../../assets/images';
import { Colors } from '../../../theme/index';
import ContestInfo from '../../ContestDetails/components/ContestInfo';
import { constant, getContestStatus, getStatusColor } from '../../../utils';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  contestViewStyle: {
    alignSelf: 'stretch',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingHorizontal: 5,
    margin: 5,
    backgroundColor: Colors.selectedFilterColor,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  contestName: {
    color: 'white',
    textAlign: 'center',
    marginVertical: 3,
    backgroundColor: 'transparent',
    fontSize: 16,
    fontWeight: '700',
  },
  headerView: {
    width: width - 35,
    flexDirection: 'row',
    padding: 2,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  detailView: {
    width: width - 35,
    flexDirection: 'row',
    padding: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
  },
  totalEarnings: {
    fontSize: 15,
    fontWeight: '400',
    color: 'white',
  },
  earningsText: {
    fontSize: 15,
    fontWeight: '700',
    color: 'white',
  },
  detailHeader: {
    fontSize: 12,
    fontWeight: '400',
    color: '#4f5386',
  },
  detailText: {
    fontSize: 12,
    fontWeight: '700',
    color: 'white',
  },
  joinButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical: 8,
    backgroundColor: '#4f5386',
    borderRadius: 12,
    alignSelf: 'stretch',
  },
  teamsCountView: {
    width: width * 0.63,
    flexDirection: 'row',
    marginTop: -20,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  inviteButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical: 8,
    backgroundColor: 'orange',
    borderRadius: 12,
  },
  inviteButtonText: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
  },
  nextIcon: {
    width: 10,
    height: 10,
    marginLeft: 3,
    transform: [{
      rotate: '180deg',
    }],
  },
  statisticsView: {
    marginVertical: 5,
    paddingVertical: 5,
    alignSelf: 'stretch',
  },
  statusContainer: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  statusView: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    width: width * 0.63,
    height: 5,
    backgroundColor: 'white',
    marginTop: 10,
  },
  joinedStatus: {
    backgroundColor: Colors.navigationBackgroundColor,
  },
  roomStatus: {
    flex: 1,
  },
  uncappedInfoView: {
    backgroundColor: 'transparent',
    width: width * 0.58,
  },
  topHeaderView: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    paddingVertical: 5,
    alignSelf: 'stretch',
    backgroundColor: 'transparent',
  },
  headerText: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white',
  },
  separator: {
    height: 1,
    alignSelf: 'stretch',
    backgroundColor: 'white',
    opacity: 0.2,
    marginVertical: 5,
  },
});

const MyContest = (props) => {
  const contestStatus = getContestStatus(props.contest.status, props.contest.my_teams);
  return (
    <TouchableOpacity
      activeOpacity={props.contest.status.toLowerCase() === 'closed' ? 1 : 0.7}
      disabled={props.contest.status.toLowerCase() === 'closed'}
      onPress={() => props.goToMyContestDetails(props.contest)}
    >
      <View style={styles.contestViewStyle}>
        <View style={styles.topHeaderView}>
          <Text style={styles.headerText}>{props.contest.league_name}</Text>
          <Text
            style={[styles.headerText, {
              color: getStatusColor(props.contest.status),
              fontWeight: '400',
            }]}
          >{props.contest.status.toUpperCase()}
          </Text>
        </View>

        <View
          style={{
            backgroundColor: Colors.primarybackgroundColor,
            marginVertical: 5,
            padding: 5,
            width: width - 20,
            alignSelf: 'stretch',
            borderRadius: 5,
          }}
        >
          <ContestInfo
            showStartDate
            contest={props.contest}
            isShowPrizeMatrix={Boolean(false)}
            goToEditContestName={props.goToEditContestName}
          />
          {props.contest.status.toLowerCase() !== 'closed' ?
            <View style={styles.statisticsView}>
              <View style={styles.statusContainer}>
                {props.contest.contest_type.toUpperCase() !== constant.CONTEST_TYPES[2] &&
                  <View style={[styles.statusView, { width: contestStatus.width }]}>
                    <View
                      style={[
                        styles.joinedStatus,
                        { flex: (props.contest.joined_users_count / props.contest.max_teams) },
                      ]}
                    />
                    <View style={[
                      styles.roomStatus,
                      { flex: (1 - (props.contest.joined_users_count / props.contest.max_teams)) },
                      ]}
                    />
                  </View>
                }
                {props.contest.contest_type.toUpperCase() === constant.CONTEST_TYPES[2] &&
                  <View style={styles.uncappedInfoView}>
                    <Text style={[styles.detailText, { color: 'darkgrey' }]}>
                      This is UNCAPPED contest so  prize pool amount and winners might be changing.
                    </Text>
                  </View>
                }
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    contestStatus.statusId === 0 ?
                      props.createTeam(props.contest) : props.inviteFriends(props.contest);
                    }
                  }
                  disabled={contestStatus.statusId === 2}
                >
                  <View style={styles.joinButtonView}>
                    <Text style={[styles.detailText, { textAlign: 'center' }]}>{contestStatus.label.toUpperCase()}</Text>
                    {contestStatus.statusId !== 2 &&
                      <Image source={images.backIcon} style={styles.nextIcon} />}
                  </View>
                </TouchableOpacity>
              </View>
              {props.contest.contest_type.toUpperCase() !== constant.CONTEST_TYPES[2] &&
                <View style={[styles.teamsCountView, { width: contestStatus.width }]}>
                  <Text style={[styles.detailText, { color: 'orange' }]}>Only {props.contest.remaining_users_count
                    && props.contest.remaining_users_count} Spots Left
                  </Text>
                  <Text style={styles.detailText}>
                    {props.contest.max_teams && props.contest.max_teams} TEAMS
                  </Text>
                </View>
              }
            </View> :
            <View
              style={{
                marginVertical: 10,
              }}
            >
              <View style={styles.separator} />
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingVertical: 5,
                  paddingRight: width / 10,
                }}
              >
                <Text style={[styles.detailText, { color: 'grey' }]}>JOINED WITH</Text>
                <Text style={[styles.detailText, { color: 'grey' }]}>POINTS</Text>
                <Text style={[styles.detailText, { color: 'grey' }]}>RANK</Text>
              </View>
              {
                props.contest.my_teams.map((team, index) => {
                  return (
                    <View
                      key={index}
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        paddingVertical: 5,
                        paddingRight: width / 8,
                      }}
                    >
                      <Text style={styles.detailText}>{team.team_name}</Text>
                      <Text style={styles.detailText}>{team.points}</Text>
                      <Text style={styles.detailText}>#{team.rank}</Text>
                    </View>
                  );
                })}
              <TouchableOpacity
                onPress={() => alert('Goto Contest leaderboard')}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingVertical: 10,
                  paddingHorizontal: 5,
                  marginVertical: 5,
                  backgroundColor: Colors.selectedFilterColor,
                }}
              >
                <Text style={styles.detailText}>View Leaderboard</Text>
                <Image style={{ width: 20, height: 20 }} source={images.next} />
              </TouchableOpacity>
            </View>}
        </View>
      </View>
    </TouchableOpacity>
  );
};

MyContest.propTypes = {
  contest: PropTypes.objectOf(PropTypes.any),
  createTeam: PropTypes.func,
  inviteFriends: PropTypes.func,
  goToMyContestDetails: PropTypes.func,
  goToEditContestName: PropTypes.func,
};

MyContest.defaultProps = {
  contest: {},
  inviteFriends: () => {},
  createTeam: () => {},
  goToMyContestDetails: () => {},
  goToEditContestName: () => {},
};

export default MyContest;
