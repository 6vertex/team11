import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Dimensions,
  ListView,
  Text,
} from 'react-native';
import { images } from '../../../assets/images';
import MyContest from './MyContest';
import { Colors } from '../../../theme/index';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignSelf: 'stretch',
    width,
    backgroundColor: Colors.primarybackgroundColor,
  },
});

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const Container = props => (
  <View style={styles.background}>
    {props.myContests.length === 0 && props.myContestDetailsLoading === false &&
    <Text
      style={{
        alignSelf: 'stretch',
        paddingVertical: 100,
        textAlign: 'center',
        marginHorizontal: 5,
        fontSize: 16,
        backgroundColor: Colors.selectedFilterColor,
        color: 'orange',
      }}
    >Oops! No contest available
    </Text>}
    <ListView
      enableEmptySections
      dataSource={ds.cloneWithRows(props.myContests)}
      renderRow={(rowData, sectionID, rowID) => (
        <MyContest
          key={rowID}
          contest={rowData}
          goToMyContestDetails={props.goToMyContestDetails}
          inviteFriends={props.inviteFriends}
          createTeam={props.createTeam}
          goToEditContestName={props.goToEditContestName}
        />
      )}
    />
  </View>
);

Container.propTypes = {
  myContestDetailsLoading: PropTypes.bool,
  myContests: PropTypes.arrayOf(PropTypes.any),
  goToMyContestDetails: PropTypes.func,
  createTeam: PropTypes.func,
  inviteFriends: PropTypes.func,
  goToEditContestName: PropTypes.func,
};

Container.defaultProps = {
  myContestDetailsLoading: false,
  myContests: [],
  inviteFriends: () => {},
  createTeam: () => {},
  goToMyContestDetails: () => {},
  goToEditContestName: () => {},
};

export default Container;
