import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Alert } from 'react-native';
import { connect } from 'react-redux';
import FBSDK from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import LoginActions from '../../actions';
import LandingPageContainer from './components/LandingPageContainer';
import { StatusBar, Loader } from '../../components';
import { containerStyles } from '../../theme';
import {
  constant,
  isIOS,
  networkConnectivity,
  isResponseValidated,
  isResponseSuccess,
  resetRoute,
} from '../../utils';
import Utils from '../../utils/utils';

const { LoginManager, AccessToken } = FBSDK;

class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.utils = new Utils();
    this.deviceToken = '';
    this.deviceType = '';
    this.state = {};
  }

  componentDidMount() {
    this.utils.getItemWithKey(constant.DEVICE_TOKEN_PN, (value) => { this.deviceToken = value; });
    if (!isIOS) {
      LoginManager.setLoginBehavior('web_only');
      this.deviceType = 'android';
    } else {
      LoginManager.setLoginBehavior('web');
      this.deviceType = 'ios';
    }
    this.resetLoginState();
    this.setupGoogleSignin();
  }

  componentWillReceiveProps(nextProps) {
    // Handle response for social login
    if (isResponseValidated(nextProps.socialLoginResponse)) {
      if (isResponseSuccess(nextProps.socialLoginResponse)) {
        this.saveUserDetailsInStorage(nextProps.socialLoginResponse.response);
      } else {
        Alert.alert('Message', nextProps.socialLoginResponse.response.message);
      }
    }
  }

  async setupGoogleSignin() {
    try {
      await GoogleSignin.hasPlayServices({ autoResolve: true });
      await GoogleSignin.configure({
        androidClientId: constant.androidClientId,
        iosClientId: constant.iosClientId,
        webClientId: constant.webClientId,
        offlineAccess: false,
      });
    } catch (err) {
      console.log('Google signin error', err.code, err.message);
    }
  }

  async saveUserDetailsInStorage(userDetails) {
    await this.utils.setItemWithKeyAndValue(constant.USER_DETAILS, userDetails);
    this.pushNext();
  }

  pushNext() {
    resetRoute(this.props.navigation, constant.screens.DRAWER_STACK);
  }

  resetLoginState() {
    LoginManager.logOut();
  }

  facebookLoginPressed() {
    LoginManager.logInWithPublishPermissions(['publish_actions']).then(
      (result) => {
        if (result.isCancelled) {
          Alert.alert('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(data =>
            this.callSocialLogin('facebook', data.accessToken));
        }
      },
      (error) => {
        console.log(error);
      },
    );
  }

  googleLoginPressed() {
    GoogleSignin.signIn().then((user) => {
      this.callSocialLogin('google_oauth2', user.accessToken);
    }).catch((googleError) => {
      console.log(googleError);
    }).done();
  }

  callSocialLogin(provider, token) {
    networkConnectivity().then(() => {
      this.props.socialLoginRequest(provider, token, this.deviceToken, this.deviceType);
    }).catch((error) => {
      console.log(error);
      Alert.alert(constant.noInternetConnection.header, constant.noInternetConnection.message);
    });
  }

  render() {
    return (
      <View style={containerStyles.defaultContainer}>
        <StatusBar
          transculent
          hidden
        />
        <LandingPageContainer
          goToLogin={() => this.props.navigation.navigate(constant.screens.LOGIN)}
          goToRegister={() => this.props.navigation.navigate(constant.screens.REGISTER)}
          facebookLoginPressed={() => this.facebookLoginPressed()}
          googleLoginPressed={() => this.googleLoginPressed()}
        />
        {this.props.isSocialLoading && <Loader isAnimating={this.props.isSocialLoading} />}
      </View>
    );
  }
}

LandingPage.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any),
  socialLoginRequest: PropTypes.func,
  socialLoginResponse: PropTypes.objectOf(PropTypes.any),
  isSocialLoading: PropTypes.bool,
};

LandingPage.defaultProps = {
  navigation: {},
  socialLoginRequest: () => {},
  isSocialLoading: false,
  socialLoginResponse: {},
};

const mapStateToProps = state => ({
  isSocialLoading: state.user.isLoading,
  socialLoginResponse: state.user.socialLoginResponse,
});

const mapDispatchToProps = () => LoginActions;

const LandingPageScreen = connect(mapStateToProps, mapDispatchToProps)(LandingPage);

export default LandingPageScreen;
