import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import { images } from '../../../assets/images';
import { dimensions } from '../../../theme';

const width = dimensions.getViewportWidth();
const height = dimensions.getViewportHeight();

const styles = StyleSheet.create({
  backgroundImage: {
    width,
    height,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#00000070',
    paddingTop: 108,
  },
  logo: {
    width: width * 0.60,
    height: 70,
    marginBottom: height * 0.082,
    resizeMode: 'contain',
  },
  buttonContainer: {
    width: (width / 3) * 2.43,
    paddingVertical: 9,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
    marginVertical: 12,
  },
  orContainer: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 28,
    fontWeight: '400',
    textAlign: 'center',
  },
  separator: {
    height: 1,
    backgroundColor: '#768F70',
    width: 70,
    marginHorizontal: 15,
  },
  socialLoginBtnContainer: {
    alignSelf: 'center',
    marginVertical: 12,
  },
  socialLoginBtn: {
    width: width * 0.6,
    height: 44,
    resizeMode: 'contain',
  },
});

const LandingPageContainer = props => (
  <Image
    style={styles.backgroundImage}
    source={images.landingBackground}
  >
    <View style={styles.container}>
      <Image
        source={images.crickSixLogo}
        style={styles.logo}
      />
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => props.goToLogin()}
      >
        <View style={styles.buttonContainer}>
          <Text style={styles.buttonText}>Login</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => props.goToRegister()}
      >
        <View style={styles.buttonContainer}>
          <Text style={styles.buttonText}>Register</Text>
        </View>
      </TouchableOpacity>
      <View style={styles.orContainer}>
        <View style={styles.separator} />
        <Text style={styles.buttonText}>OR</Text>
        <View style={styles.separator} />
      </View>
      <TouchableOpacity
        style={styles.socialLoginBtnContainer}
        activeOpacity={0.7}
        onPress={() => props.facebookLoginPressed()}
      >
        <Image style={styles.socialLoginBtn} source={images.facebookLogin} />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.socialLoginBtnContainer}
        activeOpacity={0.7}
        onPress={() => props.googleLoginPressed()}
      >
        <Image style={styles.socialLoginBtn} source={images.googlePlusLogin} />
      </TouchableOpacity>
    </View>
  </Image>
);

LandingPageContainer.propTypes = {
  goToLogin: PropTypes.func,
  goToRegister: PropTypes.func,
  facebookLoginPressed: PropTypes.func,
  googleLoginPressed: PropTypes.func,
};

LandingPageContainer.defaultProps = {
  goToLogin: () => {},
  goToRegister: () => {},
  facebookLoginPressed: () => {},
  googleLoginPressed: () => {},
};

export default LandingPageContainer;
