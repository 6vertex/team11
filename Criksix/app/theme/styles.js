import * as Colors from './colors';

export const containerStyles = {
  defaultContainer: {
    flex: 1,
    backgroundColor: Colors.primarybackgroundColor,
  },
};

export const labelStyles = {
  defaultWhiteLabel: {
    fontSize: 14,
    color: 'white',
    fontWeight: '600',
    padding: 5,
  },
  largeWhiteLabel: {
    fontSize: 18,
    color: 'white',
    fontWeight: '500',
  },
};

export const buttonStyles = {
  // TODO: Button styles
};

export const iconStyles = {
  defaultIcon: {
    width: 20,
    height: 20,
    margin: 5,
  },
};
