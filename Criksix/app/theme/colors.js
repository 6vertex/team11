/* Actual colors */
/* Start */

// thik thak hai
const themeColor = '#e71636'; // nav bar color

// maja ni aaya bhiya
const suggestedThemeColor = 'rgb(219, 69, 74)'; // status bar color

// maja ni aaya bhiya
const backgroundColor = '#0E101F'; // black shade color.

// utta maja ni aaya
const btnBgColor = '#4f5386'; // color for buttons, inputs, drop down
const btnBgColor2 = '#00000070'; // color for buttons, inputs, drop down
const btnBgColor3 = themeColor; // color for buttons, inputs, drop down

// acha lagi riyo hai.
const niceColor = 'rgb(20, 31, 52)';

// ye bhi acha lagi riyo hai
const niceColor2 = 'rgb(50, 91, 120)';

/* Actual colors */
/* Stop */

/* Colors used in whole application */
/* Start */

export const statusBarColor = suggestedThemeColor;
export const navigationBackgroundColor = themeColor;
export const sidebarColor = themeColor;
export const primarybackgroundColor = backgroundColor;
export const selectedFilterColor = niceColor;
export const filterByColor = niceColor2;
export const cardColor = niceColor;
export const buttonColor = btnBgColor;
export const defaultbuttonColor = btnBgColor2;
export const primarybuttonColor = btnBgColor3;
export const greenShadeColor = 'rgb(84, 181, 53)';
export const orangeShadeColor = 'orange';

/* Colors used in whole application */
/* Stop */
