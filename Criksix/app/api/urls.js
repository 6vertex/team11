/* eslint camelcase:0 */

import {
  getBaseUrl,
} from '../config/appConfig';

/* end point urls for user management  */
export const registerUserUrl = `${getBaseUrl()}/api/users/sign_up`;

export const loginUserUrl = `${getBaseUrl()}/api/users/sign_in`;

export const socialLoginUrl = `${getBaseUrl()}/api/users/authenticate`;

export const inviteCodeUrl = `${getBaseUrl()}/api/users/validate_invite`;

export const forgotPasswordUrl = `${getBaseUrl()}/api/users/forgot_password`;

export const getUserProfileUrl = `${getBaseUrl()}/api/users/me`;

export const updateUserProfileUrl = `${getBaseUrl()}/api/users/update_profile`;

export const updatePasswordUrl = `${getBaseUrl()}/api/users/reset_password`;

export const deleteUserUrl = `${getBaseUrl()}/api/users/sign_out`;

/* end point url for side bar menus  */
export const sidebarMenusUrl = `${getBaseUrl()}/api/users/sidebar_items`;

/* end point urls for match management  */

export const getMatchFiltersUrl = `${getBaseUrl()}/api/matches/filters`;

export const getMatchDetailUrl = matchID => `${getBaseUrl()}/api/v1/matches/${matchID}`;

export const getMatchScoreUrl = matchID => `${getBaseUrl()}/api/v1/matches/${matchID}/score`;

export const getFilteredMatchUrl = `${getBaseUrl()}/api/matches/search`;

/* Contest management  */

// List of lobby contest.
export const getContestsUrl = page => `${getBaseUrl()}/api/contests/search?page=${page}`;

// List of my contest
export const getMyContestsUrl = `${getBaseUrl()}/api/contests`; // GET

// Contest detail
export const getContestDetailsUrl = id => `${getBaseUrl()}/api/contests/${id}`;

// Join contest without creating lineup
export const joinContestUrl = `${getBaseUrl()}/api/contests/join`;

// Update Contest Name

export const updateContestNameUrl = contestID => `${getBaseUrl()}/api/contests/${contestID}`;


// Create contest endpoints
// Start
export const createContestOptionsUrl = `${getBaseUrl()}/api/contests/create_options`; // Get

export const getMatchesInDurationUrl = (leadue_id, duration) =>
  `${getBaseUrl()}/api/matches/fixtures?fid=${leadue_id}&duration=${duration}`; // Get

export const getPrizePayoutUrl = `${getBaseUrl()}/api/contests/prize_structure`; // Post

export const createContestUrl = `${getBaseUrl()}/api/contests`; // Post
// Stop

/* Team management  */
// Start
export const getTeamLineUpRulesUrl = `${getBaseUrl()}/api/teams/team_linups_rules`;

export const getContestPlayersLineUpUrl = contestID => `${getBaseUrl()}/api/contests/${contestID}/player_lineup`;

export const getSubmitLineUpUrl = `${getBaseUrl()}/api/teams/submit_team_lineup`;

export const createTeamLineUpUrl = `${getBaseUrl()}/api/teams/create_team_lineup`;
// Stop
/* Team management  */

export const getPlayerDetailsUrl = player_id => `${getBaseUrl()}/api/players/player_detail?fid=${player_id}`;

export const getTeamsListForContestUrl = contestID => `${getBaseUrl()}/api/teams`;

/* Account and Payment managment endpoints */
/* Start */

// TODO: Replace this from actual endpoints in near future
export const getMyAccountUrl = `https://gist.githubusercontent.com/anonymous/0694debbba62fc616993af0182c46d8b/raw/00e8e42c8f22ba7f363472ab954cf6cc148996e0/blob.json`;
/* Stop */
