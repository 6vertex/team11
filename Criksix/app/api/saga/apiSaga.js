import { AsyncStorage } from 'react-native';
import { call, put, takeEvery } from 'redux-saga/effects';
import {
  apiCall,
} from '../apiInterface';
import { API_ACTION } from '../actions/apiActions';
import { SESSION_EXPIRE_ACTION } from '../../actions/authentication';
import constant from '../../utils/constant';

function* manageResponseData(action, response) {
  try {
    const parsedResponse = JSON.parse(response._bodyInit);
    if (response.status && response.status === 401) {
      AsyncStorage.removeItem(constant.USER_DETAILS);
      // Api called successfully and return token invalidate error msg with code <= 400.
      yield put({ type: SESSION_EXPIRE_ACTION });
    } else if (response.error || (response.status && (response.status >= 500))) {
      // Api called and got internal server error with code <= 500.
      const serverErrorResponse = {
        response: {
          message: parsedResponse.message ?
            parsedResponse.message : `${constant.SERVER_ERROR_MESSAGE}`,
        },
        status: response.status,
      };
      yield put({ type: action.types[2], data: serverErrorResponse });
    } else if (response.error || (response.status &&
      (response.status < 200 || response.status >= 300))) {
      // Api call failed due to unknown error.
      const errorResponse = {
        response: {
          message: parsedResponse.message ?
            parsedResponse.message : `${constant.SERVER_ERROR_MESSAGE}`,
        },
        status: response.status,
      };
      yield put({ type: action.types[2], data: errorResponse });
    } else {
      const successResponse = {
        response: parsedResponse,
        status: response.status,
      };
      yield put({ type: action.types[1], data: successResponse, filters: action.filters });
    }
  } catch (error) {
    const unParsedResponse = {
      response: { message: `${constant.SERVER_ERROR_MESSAGE}` },
      status: response.status,
    };
    yield put({ type: action.types[2], data: unParsedResponse });
  }
}

function* apiInterface(action) {
  try {
    yield put({ type: action.types[0] });
    const response = yield call(
      apiCall, action.url, action.method,
      action.body, action.header, action.headers,
    );
    yield call(manageResponseData, action, response);
  } catch (e) {
    yield put({ type: action.types[2], data: e });
  }
}

export default function* watchApi() {
  yield takeEvery(API_ACTION, apiInterface);
}
