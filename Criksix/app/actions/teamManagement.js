import {
  getTeamLineUpRulesUrl,
  getContestPlayersLineUpUrl,
  getSubmitLineUpUrl,
  createTeamLineUpUrl,
  getTeamsListForContestUrl,
} from '../api/urls';
import { getApiAction, postApiAction } from '../api/actions/apiActions';

export const GET_TEAM_LINEUP_RULES_REQUEST = 'GET_TEAM_LINEUP_RULES_REQUEST';
export const GET_TEAM_LINEUP_RULES_SUCCESS = 'GET_TEAM_LINEUP_RULES_SUCCESS';
export const GET_TEAM_LINEUP_RULES_FAILURE = 'GET_TEAM_LINEUP_RULES_FAILURE';

export const GET_CONTEST_PLAYERS_LINEUP_REQUEST = 'GET_CONTEST_PLAYERS_LINEUP_REQUEST';
export const GET_CONTEST_PLAYERS_LINEUP_SUCCESS = 'GET_CONTEST_PLAYERS_LINEUP_SUCCESS';
export const GET_CONTEST_PLAYERS_LINEUP_FAILURE = 'GET_CONTEST_PLAYERS_LINEUP_FAILURE';

export const SUBMIT_LINEUP_REQUEST = 'SUBMIT_LINEUP_REQUEST';
export const SUBMIT_LINEUP_SUCCESS = 'SUBMIT_LINEUP_SUCCESS';
export const SUBMIT_LINEUP_FAILURE = 'SUBMIT_LINEUP_FAILURE';

export const CREATE_LINEUP_REQUEST = 'CREATE_LINEUP_REQUEST';
export const CREATE_LINEUP_SUCCESS = 'CREATE_LINEUP_SUCCESS';
export const CREATE_LINEUP_FAILURE = 'CREATE_LINEUP_FAILURE';

export const RESET_GET_CONTEST_PLAYERS_LINEUP = 'RESET_GET_CONTEST_PLAYERS_LINEUP';

export const GET_TEAMS_LIST_FOR_CONTEST_REQUEST = 'GET_TEAMS_LIST_FOR_CONTEST_REQUEST';
export const GET_TEAMS_LIST_FOR_CONTEST_SUCCESS = 'GET_TEAMS_LIST_FOR_CONTEST_SUCCESS';
export const GET_TEAMS_LIST_FOR_CONTEST_FAILURE = 'GET_TEAMS_LIST_FOR_CONTEST_FAILURE';

export const getTeamLineUpRules = (bearerToken) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_TEAM_LINEUP_RULES_REQUEST, GET_TEAM_LINEUP_RULES_SUCCESS, GET_TEAM_LINEUP_RULES_FAILURE],
    url: getTeamLineUpRulesUrl,
    header,
  });
};

export const getContestPlayersLineUp = (bearerToken, contestID) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_CONTEST_PLAYERS_LINEUP_REQUEST, GET_CONTEST_PLAYERS_LINEUP_SUCCESS, GET_CONTEST_PLAYERS_LINEUP_FAILURE],
    url: getContestPlayersLineUpUrl(contestID),
    header,
  });
};

export const submitTeamLineup = (bearerToken, team_name, lineup_id, players) => {
  const header = `bearer ${bearerToken}`;
  return postApiAction({
    types: [SUBMIT_LINEUP_REQUEST, SUBMIT_LINEUP_SUCCESS, SUBMIT_LINEUP_FAILURE],
    url: getSubmitLineUpUrl,
    body: JSON.stringify({
      team_name,
      id: lineup_id,
      players,
    }),
    header,
  });
};

export const createTeamLineup = (bearerToken, team_name, contestID, players) => {
  const header = `bearer ${bearerToken}`;
  return postApiAction({
    types: [CREATE_LINEUP_REQUEST, CREATE_LINEUP_SUCCESS, CREATE_LINEUP_FAILURE],
    url: createTeamLineUpUrl,
    body: JSON.stringify({
      team_name,
      contest_id: contestID,
      players,
    }),
    header,
  });
};


export const resetContestPlayersLineUp = () => ({ type: RESET_GET_CONTEST_PLAYERS_LINEUP });

export const getTeamsListForContest = (bearerToken, contestID) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_TEAMS_LIST_FOR_CONTEST_REQUEST, GET_TEAMS_LIST_FOR_CONTEST_SUCCESS, GET_TEAMS_LIST_FOR_CONTEST_FAILURE],
    url: getTeamsListForContestUrl(contestID),
    header,
  });
};
