import { bindActionCreators } from 'redux';
import { store } from '../store';
import {
  userRegisterRequest,
  resetRegister,
  userLoginRequest,
  resetUserLogin,
  socialLoginRequest,
  resetSocialLogin,
  inviteCodeValidateRequest,
  forgotPasswordRequest,
  getUserProfile,
  updateUserProfile,
  updatePassword,
  userLogoutRequest,
  resetlogout,
  resetSessionExpired,
} from './authentication';

import {
  getMatchesFilters,
  getFilteredMatches,
  resetMatchManagement,
} from './matchManagement';

import {
  getAllContests,
  getContestDetails,
  resetJoinContestWithTeamLineUp,
  getMyContests,
  getMyContestDetails,
  resetMyContestDetailInfo,
  getCCO,
  getMID,
  getPayoutStructure,
  createContest,
  resetContestDetailInfo,
  joinContest,
  resetCreateContestResponse,
  resetContestLobby,
  joinContestByInviteCode,
  resetJoinContestByInviteCode,
  updateContestName,
  resetUpdateContestName,
} from './contestManagement';

import {
  getTeamLineUpRules,
  getContestPlayersLineUp,
  resetContestPlayersLineUp,
  submitTeamLineup,
  createTeamLineup,
} from './teamManagement';

import {
  getMyAccount,
} from './accountManagement';

import {
  getPlayerDetails,
  resetPlayer,
} from './player';


const actions = {
  /* User management action creators */
  userRegisterRequest,
  resetRegister,
  userLoginRequest,
  userLogoutRequest,
  resetUserLogin,
  socialLoginRequest,
  resetSocialLogin,
  inviteCodeValidateRequest,
  forgotPasswordRequest,
  getUserProfile,
  updateUserProfile,
  updatePassword,
  resetlogout,
  resetSessionExpired,

  /* Match Management action creator */
  getMatchesFilters,
  getFilteredMatches,
  resetMatchManagement,

  /* Contest Management */
  getAllContests,
  getContestDetails,
  resetJoinContestWithTeamLineUp,
  getMyContests,
  getMyContestDetails,
  resetMyContestDetailInfo,
  getCCO,
  getMID,
  getPayoutStructure,
  createContest,
  resetContestDetailInfo,
  joinContest,
  resetCreateContestResponse,
  resetContestLobby,
  joinContestByInviteCode,
  resetJoinContestByInviteCode,
  updateContestName,
  resetUpdateContestName,

  /* Team Management */
  getTeamLineUpRules,
  getContestPlayersLineUp,
  resetContestPlayersLineUp,
  submitTeamLineup,
  createTeamLineup,

  /* Account and Payment Management */
  getMyAccount,

  /* Player Management */
  getPlayerDetails,
  resetPlayer,
};

export default bindActionCreators(actions, store.dispatch);
