import { getPlayerDetailsUrl } from '../api/urls';

import { getApiAction } from '../api/actions/apiActions';

export const GET_PLAYER_DETAILS_REQUEST = 'GET_PLAYER_DETAILS_REQUEST';
export const GET_PLAYER_DETAILS_SUCCESS = 'GET_PLAYER_DETAILS_SUCCESS';
export const GET_PLAYER_DETAILS_FAILURE = 'GET_PLAYER_DETAILS_FAILURE';

export const RESET_PLAYER = 'RESET_PLAYER';


export const getPlayerDetails = (token, pid) => {
  const header = `bearer ${token}`;
  return getApiAction({
    types: [GET_PLAYER_DETAILS_REQUEST, GET_PLAYER_DETAILS_SUCCESS, GET_PLAYER_DETAILS_FAILURE],
    url: getPlayerDetailsUrl(pid),
    header,
  });
};

export const resetPlayer = () => ({
  type: RESET_PLAYER,
});
