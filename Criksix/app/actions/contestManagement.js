import {
  getContestsUrl,
  getContestDetailsUrl,
  joinContestUrl,
  getMyContestsUrl,
  createContestOptionsUrl,
  getMatchesInDurationUrl,
  getPrizePayoutUrl,
  createContestUrl,
  updateContestNameUrl,
} from '../api/urls';
import { getApiAction, postApiAction, putApiAction } from '../api/actions/apiActions';

/* Create Contest action types */
/* Start */
export const GET_CCO_REQUEST = 'GET_CCO_REQUEST'; // CCO = CONTEST_CREATE_OPTIONS
export const GET_CCO_SUCCESS = 'GET_CCO_SUCCESS';
export const GET_CCO_FAILURE = 'GET_CCO_FAILURE';

export const GET_MID_REQUEST = 'GET_MID_REQUEST'; // MID = MATCHES_IN_DURATION
export const GET_MID_SUCCESS = 'GET_MID_SUCCESS';
export const GET_MID_FAILURE = 'GET_MID_FAILURE';

export const GET_PAYOUT_REQUEST = 'GET_PAYOUT_REQUEST';
export const GET_PAYOUT_SUCCESS = 'GET_PAYOUT_SUCCESS';
export const GET_PAYOUT_FAILURE = 'GET_PAYOUT_FAILURE';

export const CREATE_CONTEST_REQUEST = 'CREATE_CONTEST_REQUEST';
export const CREATE_CONTEST_SUCCESS = 'CREATE_CONTEST_SUCCESS';
export const CREATE_CONTEST_FAILURE = 'CREATE_CONTEST_FAILURE';
export const RESET_CREATE_CONTEST = 'RESET_CREATE_CONTEST';
/* Stop */

/* Contest lobby action types */
/* Start */
export const GET_ALL_CONTESTS_REQUEST = 'GET_ALL_CONTESTS_REQUEST';
export const GET_ALL_CONTESTS_SUCCESS = 'GET_ALL_CONTESTS_SUCCESS';
export const GET_ALL_CONTESTS_FAILURE = 'GET_ALL_CONTESTS_FAILURE';
export const RESET_CONTEST_LOBBY = 'RESET_CONTEST_LOBBY';

export const GET_CONTEST_DETAILS_REQUEST = 'GET_CONTEST_DETAILS_REQUEST';
export const GET_CONTEST_DETAILS_SUCCESS = 'GET_CONTEST_DETAILS_SUCCESS';
export const GET_CONTEST_DETAILS_FAILURE = 'GET_CONTEST_DETAILS_FAILURE';

export const JOIN_CONTEST_REQUEST = 'JOIN_CONTEST_REQUEST';
export const JOIN_CONTEST_SUCCESS = 'JOIN_CONTEST_SUCCESS';
export const JOIN_CONTEST_FAILURE = 'JOIN_CONTEST_FAILURE';
export const RESET_JOIN_CONTEST = 'RESET_JOIN_CONTEST';
/* Stop */

export const JOIN_CONTEST_WITH_TEAM_LINEUP_REQUEST = 'JOIN_CONTEST_WITH_TEAM_LINEUP_REQUEST';
export const JOIN_CONTEST_WITH_TEAM_LINEUP_SUCCESS = 'JOIN_CONTEST_WITH_TEAM_LINEUP_SUCCESS';
export const JOIN_CONTEST_WITH_TEAM_LINEUP_FAILURE = 'JOIN_CONTEST_WITH_TEAM_LINEUP_FAILURE';
export const RESET_JOIN_CONTEST_WITH_TEAM_LINEUP = 'RESET_JOIN_CONTEST_WITH_TEAM_LINEUP';

export const GET_MY_CONTESTS_REQUEST = 'GET_MY_CONTESTS_REQUEST';
export const GET_MY_CONTESTS_SUCCESS = 'GET_MY_CONTESTS_SUCCESS';
export const GET_MY_CONTESTS_FAILURE = 'GET_MY_CONTESTS_FAILURE';

export const RESET_CONTEST_DETAIL = 'RESET_CONTEST_DETAIL';

export const GET_MY_CONTESTS_DETAILS_REQUEST = 'GET_MY_CONTESTS_DETAILS_REQUEST';
export const GET_MY_CONTESTS_DETAILS_SUCCESS = 'GET_MY_CONTESTS_DETAILS_SUCCESS';
export const GET_MY_CONTESTS_DETAILS_FAILURE = 'GET_MY_CONTESTS_DETAILS_FAILURE';

export const RESET_MY_CONTEST_DETAILS = 'RESET_MY_CONTEST_DETAILS';

export const JOIN_CONTEST_WITH_INVITE_CODE_REQUEST = 'JOIN_CONTEST_WITH_INVITE_CODE_REQUEST';
export const JOIN_CONTEST_WITH_INVITE_CODE_SUCCESS = 'JOIN_CONTEST_WITH_INVITE_CODE_SUCCESS';
export const JOIN_CONTEST_WITH_INVITE_CODE_FAILURE = 'JOIN_CONTEST_WITH_INVITE_CODE_FAILURE';
export const RESET_JOIN_CONTEST_WITH_INVITE_CODE = 'RESET_JOIN_CONTEST_WITH_INVITE_CODE';


export const UPDATE_CONTEST_NAME_REQUEST = 'UPDATE_CONTEST_NAME_REQUEST';
export const UPDATE_CONTEST_NAME_SUCCESS = 'UPDATE_CONTEST_NAME_SUCCESS';
export const UPDATE_CONTEST_NAME_FAILURE = 'UPDATE_CONTEST_NAME_FAILURE';
export const REST_UPDATE_CONTEST_NAME_REDUCER = 'REST_UPDATE_CONTEST_NAME_REDUCER';


/* Create Contest action creators */
/* Start */
export const getCCO = (bearerToken) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_CCO_REQUEST, GET_CCO_SUCCESS, GET_CCO_FAILURE],
    url: createContestOptionsUrl,
    header,
  });
};

export const getMID = (bearerToken, leagueId, duration) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_MID_REQUEST, GET_MID_SUCCESS, GET_MID_FAILURE],
    url: getMatchesInDurationUrl(leagueId, duration),
    header,
  });
};

export const getPayoutStructure = (bearerToken, body) => {
  const header = `bearer ${bearerToken}`;
  return postApiAction({
    types: [GET_PAYOUT_REQUEST, GET_PAYOUT_SUCCESS, GET_PAYOUT_FAILURE],
    url: getPrizePayoutUrl,
    body: JSON.stringify(body),
    header,
  });
};

export const createContest = (bearerToken, body) => {
  const header = `bearer ${bearerToken}`;
  return postApiAction({
    types: [CREATE_CONTEST_REQUEST, CREATE_CONTEST_SUCCESS, CREATE_CONTEST_FAILURE],
    url: createContestUrl,
    body: JSON.stringify(body),
    header,
  });
};
/* Stop */

/* Contest lobby action creators */
/* Start */
export const getAllContests = (bearerToken, page, filters) => {
  const header = `bearer ${bearerToken}`;
  return postApiAction({
    types: [GET_ALL_CONTESTS_REQUEST, GET_ALL_CONTESTS_SUCCESS, GET_ALL_CONTESTS_FAILURE],
    url: getContestsUrl(page),
    body: JSON.stringify(filters),
    header,
  });
};

export const getContestDetails = (bearerToken, id) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_CONTEST_DETAILS_REQUEST, GET_CONTEST_DETAILS_SUCCESS, GET_CONTEST_DETAILS_FAILURE],
    url: getContestDetailsUrl(id),
    header,
  });
};

export const resetContestDetailInfo = () => ({
  type: RESET_CONTEST_DETAIL,
});

export const getMyContestDetails = (bearerToken, id) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_MY_CONTESTS_DETAILS_REQUEST, GET_MY_CONTESTS_DETAILS_SUCCESS, GET_MY_CONTESTS_DETAILS_FAILURE],
    url: getContestDetailsUrl(id),
    header,
  });
};

export const resetMyContestDetailInfo = () => ({
  type: RESET_MY_CONTEST_DETAILS,
});

export const resetCreateContestResponse = () => ({
  type: RESET_CREATE_CONTEST,
});

export const resetContestLobby = () => ({
  type: RESET_CONTEST_LOBBY,
});

export const joinContest = (bearerToken, inviteCode) => {
  const header = `bearer ${bearerToken}`;
  return postApiAction({
    types: [JOIN_CONTEST_REQUEST, JOIN_CONTEST_SUCCESS, JOIN_CONTEST_FAILURE],
    url: joinContestUrl,
    body: JSON.stringify({
      invite_code: inviteCode,
    }),
    header,
  });
};
/* Stop */

export const resetJoinContestWithTeamLineUp = () => ({ type: RESET_JOIN_CONTEST_WITH_TEAM_LINEUP });

export const getMyContests = (bearerToken) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_MY_CONTESTS_REQUEST, GET_MY_CONTESTS_SUCCESS, GET_MY_CONTESTS_FAILURE],
    url: getMyContestsUrl,
    header,
  });
};

export const joinContestByInviteCode = (bearerToken, inviteCode) => {
  const header = `bearer ${bearerToken}`;
  return postApiAction({
    types: [
      JOIN_CONTEST_WITH_INVITE_CODE_REQUEST,
      JOIN_CONTEST_WITH_INVITE_CODE_SUCCESS,
      JOIN_CONTEST_WITH_INVITE_CODE_FAILURE,
    ],
    url: joinContestUrl,
    body: JSON.stringify({
      invite_code: inviteCode,
    }),
    header,
  });
};

export const resetJoinContestByInviteCode = () => ({ type: RESET_JOIN_CONTEST_WITH_INVITE_CODE });

export const updateContestName = (bearerToken, contestName, contestID) => {
  const header = `bearer ${bearerToken}`;
  return putApiAction({
    types: [
      UPDATE_CONTEST_NAME_REQUEST,
      UPDATE_CONTEST_NAME_SUCCESS,
      UPDATE_CONTEST_NAME_FAILURE,
    ],
    url: updateContestNameUrl(contestID),
    body: JSON.stringify({
      name: contestName,
    }),
    header,
  });
};

export const resetUpdateContestName = () => ({ type: REST_UPDATE_CONTEST_NAME_REDUCER });
