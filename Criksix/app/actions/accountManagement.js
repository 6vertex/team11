import { getMyAccountUrl } from '../api/urls';

import { getApiAction } from '../api/actions/apiActions';

export const GET_ACCOUNT_REQUEST = 'GET_ACCOUNT_REQUEST';
export const GET_ACCOUNT_SUCCESS = 'GET_ACCOUNT_SUCCESS';
export const GET_ACCOUNT_FAILURE = 'GET_ACCOUNT_FAILURE';

export const ADD_CASH_REQUEST = 'ADD_CASH_REQUEST';
export const ADD_CASH_SUCCESS = 'ADD_CASH_SUCCESS';
export const ADD_CASH_FAILURE = 'ADD_CASH_FAILURE';

export const getMyAccount = (bearerToken, username) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_ACCOUNT_REQUEST, GET_ACCOUNT_SUCCESS, GET_ACCOUNT_FAILURE],
    url: getMyAccountUrl,
    header,
  });
};
