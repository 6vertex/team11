import { getFilteredMatchUrl, getMatchFiltersUrl } from '../api/urls';

import { getApiAction, postApiAction } from '../api/actions/apiActions';

export const FILTERED_MATHCES_REQUEST = 'FILTERED_MATHCES_REQUEST';
export const FILTERED_MATHCES_SUCCESS = 'FILTERED_MATHCES_SUCCESS';
export const FILTERED_MATHCES_FAILURE = 'FILTERED_MATHCES_FAILURE';

export const RESET_MATCH_MANAGEMENT = 'RESET_MATCH_MANAGEMENT';

export const GET_MATCH_FILTERS_REQUEST = 'GET_MATCH_FILTERS_REQUEST';
export const GET_MATCH_FILTERS_SUCCESS = 'GET_MATCH_FILTERS_SUCCESS';
export const GET_MATCH_FILTERS_FAILURE = 'GET_MATCH_FILTERS_FAILURE';

export const getMatchesFilters = (bearerToken) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_MATCH_FILTERS_REQUEST, GET_MATCH_FILTERS_SUCCESS, GET_MATCH_FILTERS_FAILURE],
    url: getMatchFiltersUrl,
    header,
  });
};

export const getFilteredMatches = (bearerToken, body, activeFilters = []) => {
  const header = `bearer ${bearerToken}`;
  return postApiAction({
    types: [FILTERED_MATHCES_REQUEST, FILTERED_MATHCES_SUCCESS, FILTERED_MATHCES_FAILURE],
    url: getFilteredMatchUrl,
    body: JSON.stringify(body),
    header,
    filters: activeFilters,
  });
};

export const resetMatchManagement = () => ({ type: RESET_MATCH_MANAGEMENT });
