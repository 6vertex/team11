import { registerUserUrl, loginUserUrl, socialLoginUrl, inviteCodeUrl, forgotPasswordUrl, getUserProfileUrl, updateUserProfileUrl, updatePasswordUrl, deleteUserUrl, getPlayerDetailsUrl, } from '../api/urls';

import { postApiAction, getApiAction, putApiAction, deleteApiAction, } from '../api/actions/apiActions';

export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';

export const RESET_USER_LOGIN = 'RESET_USER_LOGIN';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const USER_LOGOUT_REQUEST = 'USER_LOGOUT_REQUEST';
export const USER_LOGOUT_SUCCESS = 'USER_LOGOUT_SUCCESS';
export const USER_LOGOUT_FAILURE = 'USER_LOGOUT_FAILURE';

export const RESET_LOGOUT = 'RESET_LOGOUT';

export const SESSION_EXPIRE_ACTION = 'SESSION_EXPIRE_ACTION';

export const RESET_SESSION_EXPIRED = 'RESET_SESSION_EXPIRED';

export const RESET_REGISTER = 'RESET_REGISTER';

export const SOCIAL_LOGIN_REQUEST = 'SOCIAL_LOGIN_REQUEST';
export const SOCIAL_LOGIN_SUCCESS = 'SOCIAL_LOGIN_SUCCESS';
export const SOCIAL_LOGIN_FAILURE = 'SOCIAL_LOGIN_FAILURE';

export const RESET_SOCIAL_LOGIN = 'RESET_SOCIAL_LOGIN';

export const INVITE_CODE_REQUEST = 'INVITE_CODE_REQUEST';
export const INVITE_CODE_SUCCESS = 'INVITE_CODE_SUCCESS';
export const INVITE_CODE_FAILURE = 'INVITE_CODE_FAILURE';

export const FORGOT_PASSWORD_REQUEST = 'FORGOT_PASSWORD_REQUEST';
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_FAILURE = 'FORGOT_PASSWORD_FAILURE';

export const GET_USER_PROFILE_REQUEST = 'GET_USER_PROFILE_REQUEST';
export const GET_USER_PROFILE_SUCCESS = 'GET_USER_PROFILE_SUCCESS';
export const GET_USER_PROFILE_FAILURE = 'GET_USER_PROFILE_FAILURE';

export const GET_UPDATE_USER_PROFILE_REQUEST = 'GET_UPDATE_USER_PROFILE_REQUEST';
export const GET_UPDATE_USER_PROFILE_SUCCESS = 'GET_UPDATE_USER_PROFILE_SUCCESS';
export const GET_UPDATE_USER_PROFILE_FAILURE = 'GET_UPDATE_USER_PROFILE_FAILURE';

export const UPDATE_USER_PROFILE_REQUEST = 'UPDATE_USER_PROFILE_REQUEST';
export const UPDATE_USER_PROFILE_SUCCESS = 'UPDATE_USER_PROFILE_SUCCESS';
export const UPDATE_USER_PROFILE_FAILURE = 'UPDATE_USER_PROFILE_FAILURE';

export const UPDATE_PASSWORD_REQUEST = 'UPDATE_PASSWORD_REQUEST';
export const UPDATE_PASSWORD_SUCCESS = 'UPDATE_PASSWORD_SUCCESS';
export const UPDATE_PASSWORD_FAILURE = 'UPDATE_PASSWORD_FAILURE';

export const GET_PLAYER_DETAILS_REQUEST = 'GET_PLAYER_DETAILS_REQUEST';
export const GET_PLAYER_DETAILS_SUCCESS = 'GET_PLAYER_DETAILS_SUCCESS';
export const GET_PLAYER_DETAILS_FAILURE = 'GET_PLAYER_DETAILS_FAILURE';

export const userRegisterRequest = (firstName, lastName, email, password, invitedByCode) => {
  const body = {
    email,
    password,
    first_name: firstName,
    last_name: lastName,
    invited_by: invitedByCode,
  };
  return postApiAction({
    types: [REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAILURE],
    url: registerUserUrl,
    body: JSON.stringify(body),
  });
};

export const resetRegister = () => ({
  type: RESET_REGISTER,
});

export const userLoginRequest = (email, password, device_token, device_type) => {
  const body = {
    email,
    password,
    device_token,
    device_type,
  };
  return postApiAction({
    types: [LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE],
    url: loginUserUrl,
    body: JSON.stringify(body),
  });
};

export const userLogoutRequest = (bearerToken) => {
  const header = `bearer ${bearerToken}`;
  return postApiAction({
    types: [USER_LOGOUT_REQUEST, USER_LOGOUT_SUCCESS, USER_LOGOUT_FAILURE],
    url: deleteUserUrl,
    header,
  });
};

export const resetlogout = () => ({
  type: RESET_LOGOUT,
});

export const resetSessionExpired = () => ({
  type: RESET_SESSION_EXPIRED,
});

export const resetUserLogin = () => ({
  type: RESET_USER_LOGIN,
});

export const socialLoginRequest = (provider, token, device_token, device_type) => {
  const body = {
    provider,
    token,
    device_token,
    device_type,
  };
  return postApiAction({
    types: [SOCIAL_LOGIN_REQUEST, SOCIAL_LOGIN_SUCCESS, SOCIAL_LOGIN_FAILURE],
    url: socialLoginUrl,
    body: JSON.stringify(body),
  });
};

export const resetSocialLogin = () => ({
  type: RESET_SOCIAL_LOGIN,
});

export const inviteCodeValidateRequest = (inviteCode) => {
  const body = {
    invited_by: inviteCode,
  };
  return postApiAction({
    types: [INVITE_CODE_REQUEST, INVITE_CODE_SUCCESS, INVITE_CODE_FAILURE],
    url: inviteCodeUrl,
    body: JSON.stringify(body),
  });
};

export const forgotPasswordRequest = (email) => {
  const body = {
    email,
  };
  return postApiAction({
    types: [FORGOT_PASSWORD_REQUEST, FORGOT_PASSWORD_SUCCESS, FORGOT_PASSWORD_FAILURE],
    url: forgotPasswordUrl,
    body: JSON.stringify(body),
  });
};

export const getUserProfile = (bearerToken) => {
  const header = `bearer ${bearerToken}`;
  return getApiAction({
    types: [GET_USER_PROFILE_REQUEST, GET_USER_PROFILE_SUCCESS, GET_USER_PROFILE_FAILURE],
    url: getUserProfileUrl,
    header,
  });
};

export const updateUserProfile = (bearerToken, firstName, lastName, gender, address, state, country) => {
  const header = `bearer ${bearerToken}`;
  const bodyData = {
    first_name: firstName,
    last_name: lastName,
    gender,
    city: address,
    state,
    country_alpha2: country,
  };
  return postApiAction({
    types: [UPDATE_USER_PROFILE_REQUEST, UPDATE_USER_PROFILE_SUCCESS, UPDATE_USER_PROFILE_FAILURE],
    url: updateUserProfileUrl,
    header,
    body: JSON.stringify(bodyData),
  });
};

export const updatePassword = (bearerToken, oldPassword, newPassword) => {
  const header = `bearer ${bearerToken}`;
  const bodyData = {
    old_password: oldPassword,
    new_password: newPassword,
  };
  return postApiAction({
    types: [UPDATE_PASSWORD_REQUEST, UPDATE_PASSWORD_SUCCESS, UPDATE_PASSWORD_FAILURE],
    url: updatePasswordUrl,
    header,
    body: JSON.stringify(bodyData),
  });
};

// export const getPlayerDetails = () => {
//   return getApiAction({
//     types: [GET_PLAYER_DETAILS_REQUEST, GET_PLAYER_DETAILS_SUCCESS, GET_PLAYER_DETAILS_FAILURE],
//     url: getPlayerDetailsUrl,
//   });
// };
